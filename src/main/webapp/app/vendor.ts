/* after changing this file run 'yarn run webpack:build' */
/* tslint:disable */
import '../content/scss/vendor.scss';
import 'leaflet/dist/leaflet.js';
import 'mapbox/dist/mapbox-sdk.js';
import 'mapbox-gl/dist/mapbox-gl.js';
import '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.js';
// jhipster-needle-add-element-to-vendor - JHipster will add new menu items here
