import { BaseEntity } from '../';

export class KazpostAddress implements BaseEntity {
    constructor(
        public id?: number,
        public postcode?: string,
        public addressRus?: string,
        public addressKaz?: string,
    ) {
    }
}
