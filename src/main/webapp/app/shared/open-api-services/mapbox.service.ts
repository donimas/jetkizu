import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { KazpostAddress } from './kazpost-address.model';
import { ResponseWrapper, createRequestOption } from '../';

@Injectable()
export class MapboxService {

    private resourceUrlPoints = 'http://api.tiles.mapbox.com/v4/directions/mapbox.driving/';
    private resourceUrlAccessToken = '.json?access_token=';

    private directionUrl1 = 'https://api.mapbox.com/directions/v5/mapbox/driving/';
    private directionUrl2 = '.json?geometries=geojson&alternatives=true&steps=true&overview=full&access_token=';

    private optimizationApi = 'https://api.mapbox.com/optimized-trips/v1/mapbox/driving/';
    private optimizationApiAccessToken = '?geometries=geojson&steps=true&access_token=';

    // https://api.mapbox.com/directions/v5/mapbox/driving/13.4265,52.5080;13.4273,52.5069?steps=true&geometries=geojson&access_token=your-access-token

    constructor(private http: Http) { }

    getDirectionData(source: string[], target: string[], accessToken?: string) {
        const urlPart1 = 'https://api.mapbox.com/directions/v5/mapbox/driving/';
        const urlPart2 = '?steps=false&geometries=geojson&access_token=';
        const sourceCoordinate = source[0]+','+source[1];
        const targetCoordinate = target[0]+','+target[1];
        return this.http.get(`${urlPart1}${sourceCoordinate};${targetCoordinate}${urlPart2}${accessToken}`)
            .map((res: Response) => {
                return res.json();
            });
    }

    query(points?: any, accessToken?: string): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrlPoints}${points}${this.resourceUrlAccessToken}${accessToken}`)
            .map((res: Response) => {
                const result = this.convertResponse(res);
                console.log('wrapper:');
                console.log(result);
                return result;
        });
    }

    directionQuery(latlng?: any, accessToken?: string) {
        const url = `${this.directionUrl1}${latlng}${this.directionUrl2}${accessToken}`;
        console.log('url=> '+url);
        return this.http.get(url).map((res:Response) => {
            return res.json();
        });
    }

    optimizationQuery(points: any, accessToken: string, isRoundTrip: boolean) {
        let url = `${this.optimizationApi}${points}?geometries=geojson&steps=true&roundtrip=${isRoundTrip}&access_token=${accessToken}`;
        if(!isRoundTrip) {
            url += '&source=first&destination=last';
        }
        console.log('request to get optimization route');
        console.log(url);
        return this.http.get(url).map((res: Response) => {
            return res.json();
        });
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json().response.GeoObjectCollection.featureMember;
        const result = [];
        console.log(jsonResponse);
        if (jsonResponse) {
            for (let i = 0; i < jsonResponse.length; i++) {
                result.push(this.convertItemFromServer(jsonResponse[i].GeoObject));
            }
            console.log('collected:');
            console.log(result);
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to KazpostAddress.
     */
    private convertItemFromServer(json: any): KazpostAddress {
        const entity: KazpostAddress = Object.assign(new KazpostAddress(), json);
        console.log('kazpost result:');
        console.log(entity);
        return entity;
    }

    /**
     * Convert a KazpostAddress to a JSON which can be sent to the server.
     */
    private convert(kazpostAddress: KazpostAddress): KazpostAddress {
        const copy: KazpostAddress = Object.assign({}, kazpostAddress);
        return copy;
    }
}
