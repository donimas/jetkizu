import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { KazpostAddress } from './kazpost-address.model';
import { ResponseWrapper, createRequestOption } from '../';

@Injectable()
export class YandexService {

    private resourceUrlAddress = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=';
    private resourceUrlResultsParam = '&results=';
    private resourceUrlFromParam = '&skip=';

    constructor(private http: Http) { }

    query(addressQuery?: any, fromParam?: number, resultsParam?: number): Observable<ResponseWrapper> {
        return this.http.get(this.mergeResourceUrl(addressQuery, fromParam, resultsParam))
            .map((res: Response) => {
                const result = this.convertResponse(res);
                console.log('wrapper:');
                console.log(result);
                return result.json;
        });
    }

    private mergeResourceUrl(addressQuery: String, fromParam: number, resultsParam: number) {
        return this.resourceUrlAddress + addressQuery + this.resourceUrlResultsParam + resultsParam + this.resourceUrlFromParam + fromParam;
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json().response.GeoObjectCollection.featureMember;
        const result = [];
        console.log(jsonResponse);
        if (jsonResponse) {
            for (let i = 0; i < jsonResponse.length; i++) {
                result.push(this.convertItemFromServer(jsonResponse[i].GeoObject));
            }
            console.log('collected:');
            console.log(result);
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to KazpostAddress.
     */
    private convertItemFromServer(json: any): KazpostAddress {
        const entity: KazpostAddress = Object.assign(new KazpostAddress(), json);
        console.log('kazpost result:');
        console.log(entity);
        return entity;
    }

    /**
     * Convert a KazpostAddress to a JSON which can be sent to the server.
     */
    private convert(kazpostAddress: KazpostAddress): KazpostAddress {
        const copy: KazpostAddress = Object.assign({}, kazpostAddress);
        return copy;
    }
}
