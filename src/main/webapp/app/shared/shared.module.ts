import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';

import {
    JetkizuSharedLibsModule,
    JetkizuSharedCommonModule,
    CSRFService,
    AuthServerProvider,
    AccountService,
    UserService,
    StateStorageService,
    LoginService,
    LoginModalService,
    JhiLoginModalComponent,
    ImportModalService,
    ImportModalComponent,
    Principal,
    JhiTrackerService,
    HasAnyAuthorityDirective,
    MapService,
    OsmService,
    KazpostService,
    YandexService,
    MapboxService,
    ScriptLoaderService,
} from './';

@NgModule({
    imports: [
        JetkizuSharedLibsModule,
        JetkizuSharedCommonModule
    ],
    declarations: [
        JhiLoginModalComponent,
        ImportModalComponent,
        HasAnyAuthorityDirective
    ],
    providers: [
        LoginService,
        MapService,
        OsmService,
        KazpostService,
        YandexService,
        MapboxService,
        ScriptLoaderService,
        LoginModalService,
        ImportModalService,
        AccountService,
        StateStorageService,
        Principal,
        CSRFService,
        JhiTrackerService,
        AuthServerProvider,
        UserService,
        DatePipe
    ],
    entryComponents: [JhiLoginModalComponent, ImportModalComponent],
    exports: [
        JetkizuSharedCommonModule,
        JhiLoginModalComponent,
        ImportModalComponent,
        HasAnyAuthorityDirective,
        DatePipe
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class JetkizuSharedModule {}
