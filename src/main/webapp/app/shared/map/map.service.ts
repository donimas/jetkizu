import { Injectable } from '@angular/core';
import { SAVED_ACTIVITIES } from './activities-dict';
import {environment} from '../constants/environment';
import {RouteBranch, RouteBranchType} from '../../entities/route-branch/route-branch.model';
import {Location} from "../../entities/location/location.model";
import {MapboxService} from '../open-api-services/mapbox.service';
import {directionsStyle} from './directions-style';
import {JetkizuActivity} from '../../entities/jetkizu-activity/jetkizu-activity.model';
import {
    RouteBranchStatus,
    RouteBranchStatusHistory
} from '../../entities/route-branch-status-history/route-branch-status-history.model';

const apiToken = environment.MAPBOX_API_KEY;
const styles = directionsStyle.style;
declare let omnivore: any;
declare let L: any;

//const defaultCoords = [71.50092, 51.132565];
const defaultCoords = [76.949754, 43.261208];
const defaultZoom = 15;
let defaultIcon: any;

const hexColors = ['#ef566d', '#6a7fe8', '#6ae8d5', '#70e86a', '#fff65e', '#f27941'];

var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
var MapboxDraw = require('@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.js');
var MapboxDirections = require('@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.js');

@Injectable()
export class MapService {

    constructor(
        private mapboxApiService: MapboxService
    ) { }

    getActivity(id: number) {
        return SAVED_ACTIVITIES.slice(0).find((run) => run.id === id);
    }

    showMap() {
        const map = L.map('map').setView(defaultCoords, defaultZoom);

        L.tileLayer('https://api.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="http://mapbox.com">Mapbox</a>',
            id: 'mapbox.streets',
            accessToken: apiToken,
            maxZoom: 18,
        }).addTo(map);

        //defaultIcon = this.createIcon();

        return map;
    }

    createIcon() {
        return L.icon({
            iconUrl: '/content/images/marker-icon-2x.png',
            shadowUrl: '/content/images/marker-shadow.png',

            iconSize:     [38, 95], // size of the icon
            shadowSize:   [50, 64], // size of the shadow
            iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
            shadowAnchor: [4, 62],  // the same for the shadow
            popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
        });
    }

    addMarker(map, latlng, popupText) {
        const marker = L.marker(latlng).addTo(map);
        if(popupText && popupText !== '') {
            marker.bindPopup(popupText).openPopup();
        }
        map.panTo(latlng);
        return marker;
    }

    plotActivity(id: number) {
        const myStyle = {
            'color': '#3949AB',
            'weight': 5,
            'opacity': 0.95
        };

        const map = this.showMap();

        /*const customLayer = L.geoJson(null, {
            style: myStyle
        });

        const activity = SAVED_ACTIVITIES.slice(0).find((run) => run.id === id);
        console.log(activity);
        let gpxLayer = omnivore.gpx(activity.gpxData, null, customLayer)
            .on('ready', function() {
                map.fitBounds(gpxLayer.getBounds());
            }).addTo(map);*/
    }

    showMapboxglMap(containerId: string, zoomSize: number) {
        mapboxgl.accessToken = apiToken;
        var map = new mapboxgl.Map({
            container: containerId,
            style: 'mapbox://styles/mapbox/streets-v9',
            center: defaultCoords,
            zoom: zoomSize
        });

        /*map.addControl(new MapboxDirections({
            accessToken: mapboxgl.accessToken
        }), 'top-right');*/
        return map;
    }

    /**
     * normal
     * hovered
     * start/end
     *
     * @returns {HTMLElementTagNameMap[string]}
     */
    getMarkerElement(styleType: string, id?: any, text?: string) {
        let el = document.createElement('div');
        el.id = id;

        let markerEl = document.createElement('div');
        markerEl.className = 'jet-marker theme-balloon marker-' + styleType;

        let markerB = document.createElement('div');
        markerB.className = 'jet-marker_b jet-marker_b-' + styleType;

        markerEl.appendChild(markerB);

        let markerContent = document.createElement('div');
        markerContent.className = 'markerContent';

        let markerLabel = document.createElement('div');
        markerLabel.className = 'markerLabel';
        if(text) {
            let textnode = document.createTextNode(text);
            markerLabel.appendChild(textnode);
        }
        markerContent.appendChild(markerLabel);
        markerEl.appendChild(markerContent);

        let markerA = document.createElement('div');
        markerA.className = 'jet-marker_a jet-marker_a-' + styleType;

        markerEl.appendChild(markerA);

        el.appendChild(markerEl);

        return el;
    }

    updateMarkerElementStyle(styleType: string, id?: any) {
        let el = document.getElementById(id+'');
        el.getElementsByClassName('jet-marker')[0].className = 'jet-marker theme-balloon marker-' + styleType;
        el.getElementsByClassName('jet-marker_b')[0].className = 'jet-marker_b jet-marker_b-' + styleType;
        el.getElementsByClassName('jet-marker_a')[0].className = 'jet-marker_a jet-marker_a-' + styleType;

        el.removeEventListener('mouseover', (e) => {
            console.log(e);
            this.hoverMarker(el, styleType);
        });
        el.removeEventListener('mouseout', (e) => {
            this.unhoverMarker(el, styleType);
        });
        /*setTimeout(() => {
            this.addMarkerHoverEvent(el, styleType);
        });*/
    }

    hoverMarker(el, styleType) {
        console.log(el);
        console.log(styleType);
        if(!el) {
            return;
        }
        el.style.zIndex = 10000;
        let markerEl = el.getElementsByClassName('marker-'+styleType)[0];
        let markerBEl = el.getElementsByClassName('jet-marker_b-'+styleType)[0];
        let markerAEl = el.getElementsByClassName('jet-marker_a-'+styleType)[0];

        if(markerEl) {
            markerEl.classList.remove('marker-'+styleType);
            markerEl.classList.add('marker-hover');
        }
        if(markerBEl) {
            markerBEl.classList.remove('jet-marker_b-'+styleType);
            markerBEl.classList.add('jet-marker_b-hover');
        }
        if(markerAEl) {
            markerAEl.classList.remove('jet-marker_a-'+styleType);
            markerAEl.classList.add('jet-marker_a-hover');
        }
    }

    unhoverMarker(el, styleType) {
        console.log(el);
        console.log(styleType);
        if(!el) {
            return;
        }
        el.style.zIndex = 0;
        let markerHoverEl = el.getElementsByClassName('marker-hover')[0];
        let markerBHoverEl = el.getElementsByClassName('jet-marker_b-hover')[0];
        let markerAHoverEl = el.getElementsByClassName('jet-marker_a-hover')[0];

        if(markerHoverEl) {
            markerHoverEl.classList.remove('marker-hover');
            markerHoverEl.classList.add('marker-'+styleType);
        }
        if(markerBHoverEl) {
            markerBHoverEl.classList.remove('jet-marker_b-hover');
            markerBHoverEl.classList.add('jet-marker_b-'+styleType);
        }
        if(markerAHoverEl) {
            markerAHoverEl.classList.remove('jet-marker_a-hover');
            markerAHoverEl.classList.add('jet-marker_a-'+styleType);
        }
    }

    addMarkerHoverEvent(marker, styleType) {
        console.log('request to add hover event');
        console.log(marker);
        console.log(styleType);
        marker.addEventListener('mouseover', (e) => {
            this.hoverMarker(marker, styleType);
        });
        marker.addEventListener('mouseout', (e) => {
            this.unhoverMarker(marker, styleType);
        });
    }

    addMapboxglMarker(map, lonlat, styleType: string, id?: any, title?: string, popupJson?: any) {
        const el = this.getMarkerElement(styleType, id, title);

        let marker = new mapboxgl.Marker(el)
            .setLngLat(lonlat)
            .addTo(map);

        this.addMarkerHoverEvent(el, styleType);

        return marker;
    }

    showDrawTool(map) {
        /*let draw = new MapboxDraw({
            displayControlsDefault: false,
            controls: {
                polygon: true,
                trash: true
            }
        });*/
        let draw = new MapboxDraw();
        map.addControl(draw, 'top-left');
        return draw;
    }

    addStartLocationPoint(map, location) {
        const data = {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': location
            }
        };
        if(map.getSource('origin')) {
            map.getSource('origin').setData(data);
        } else {
            map.addSource('origin', {
                'type': 'geojson',
                'data': data
            });
            // Create a circle layer
            map.addLayer({
                id: 'origin-circle',
                type: 'circle',
                source: 'origin',
                paint: {
                    'circle-radius': 20,
                    'circle-color': 'white',
                    'circle-stroke-color': '#3887be',
                    'circle-stroke-width': 3
                }
            });

            // Create a symbol layer on top of circle layer
            map.addLayer({
                id: 'origin-symbol',
                type: 'symbol',
                source: 'origin',
                layout: {
                    'icon-image': 'car-15',
                    'icon-size': 1
                },
                paint: {
                    'text-color': '#3887be'
                }
            });
        }
        this.flyTo(map, location, null);
    }

    addEndLocationPoint(map, location) {
        const data = {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': location
            }
        };
        if(map.getSource('endLocation')) {
            map.getSource('endLocation').setData(data);
        } else {
            map.addSource('endLocation', {
                'type': 'geojson',
                'data': data
            });
            // Create a circle layer
            map.addLayer({
                id: 'endLocation-circle',
                type: 'circle',
                source: 'endLocation',
                paint: {
                    'circle-radius': 20,
                    'circle-color': 'white',
                    'circle-stroke-color': '#f83a22',
                    'circle-stroke-width': 3
                }
            });

            // Create a symbol layer on top of circle layer
            map.addLayer({
                id: 'endLocation-symbol',
                type: 'symbol',
                source: 'endLocation',
                layout: {
                    'icon-image': 'car-15',
                    'icon-size': 1
                },
                paint: {
                    'text-color': '#3887be'
                }
            });
        }
        this.flyTo(map, location, null);
    }

    createBranchActivity(startEndLocations: Location[]): any {
        const startLocation = startEndLocations[0];
        const endLocation = startEndLocations[1];
        const points = this.getLonLatDirectionCoordinatesFormat(startLocation.lon, startLocation.lat, endLocation.lon, endLocation.lat);
        return this.mapboxApiService.directionQuery(points, apiToken).map((res) => {
            const distance = res.routes[0].distance;
            const duration = res.routes[0].duration;
            let activity = new JetkizuActivity();
            activity.planKm = distance;
            activity.planDuration = duration;
            activity.fromLon = startLocation.lon;
            activity.fromLat = startLocation.lat;

            return activity;
        });
    }

    drawDirection(source: string[], target: string[], id: any, map: any, colorPosition: number) {
        const hexColor = hexColors[colorPosition];
        return new Promise((resolve) => {
            this.mapboxApiService.getDirectionData(source, target, apiToken).subscribe((directionData) => {
                console.log(directionData);
                let activity = new JetkizuActivity();
                activity.planDuration = directionData.routes[0].duration;
                activity.planKm = directionData.routes[0].distance;

                const feature = {
                    'type': 'Feature',
                    'geometry': directionData.routes[0].geometry
                };

                map.addSource('directionSource-'+id, {
                    'type': 'geojson',
                    'data': feature
                });

                map.addLayer({
                    'id': 'directionLine-'+id,
                    'type': 'line',
                    'source': 'directionSource-'+id,
                    'layout': {
                        'line-cap': 'round',
                        'line-join': 'round'
                    },
                    'paint': {
                        //'line-width': 4,
                        'line-width': {
                            base: 1,
                            stops: [[12, 3], [22, 12]]
                        },
                        'line-color': hexColor
                    }
                }, 'waterway-label');

                /*map.addLayer({
                    id: 'routeArrows',
                    type: 'symbol',
                    source: 'directionSource-'+id,
                    layout: {
                        'symbol-placement': 'line',
                        'text-field': '▶',
                        'text-size': {
                            base: 1,
                            stops: [[12, 24], [22, 60]]
                        },
                        'symbol-spacing': {
                            base: 1,
                            stops: [[12, 30], [22, 160]]
                        },
                        'text-keep-upright': false
                    },
                    paint: {
                        'text-color': '#3887be',
                        'text-halo-color': 'hsl(55, 11%, 96%)',
                        'text-halo-width': 3
                    }
                }, 'waterway-label');*/

                resolve(activity);
            });
        });
    }

    showDirection(map, routeBranch: RouteBranch) {
        let startLocation: Location;
        const endLocation = routeBranch.endTo;
        startLocation = new Location();
        startLocation.lon = routeBranch.jetkizuActivity.fromLon;
        startLocation.lat = routeBranch.jetkizuActivity.fromLat;
        /*if(routeBranch.startFrom) {
            startLocation = routeBranch.startFrom;
        } else {
        }*/

        const points = this.getLonLatDirectionCoordinatesFormat(startLocation.lon, startLocation.lat, endLocation.lon, endLocation.lat);
        const order = routeBranch.order;
        this.mapboxApiService.directionQuery(points, apiToken).subscribe((res) => {
            console.log(res);
            const directionData = {
                'type': 'Feature',
                'geometry': res.routes[0].geometry
            };
            const pointData = {
                'type': 'Feature',
                'geometry': {
                    'type': "Point",
                    "coordinates": [endLocation.lon, endLocation.lat]
                }
            };

            const directionSourceId = 'direction_' + order;
            const directionRouteLineLayerId = 'directions-route-line_' + order;
            if(map.getSource(directionSourceId)) {
                map.getSource(directionSourceId).setData(directionData);
                map.removeLayer(directionRouteLineLayerId);
            } else {
                map.addSource(directionSourceId, {
                    'type': 'geojson',
                    'data': directionData
                });
            }
            // Add direction specific styles to the map
            map.addLayer({
                'id': directionRouteLineLayerId,
                'type': 'line',
                'source': directionSourceId,
                'layout': {
                    'line-cap': 'round',
                    'line-join': 'round'
                },
                'paint': {
                    //'line-width': 4,
                    'line-width': {
                        base: 1,
                        stops: [[12, 3], [22, 12]]
                    },
                    'line-color': '#3bb2d0'
                }
            }, 'waterway-label');

            const pointSourceId = 'point_' + order;
            const pointCircleLayerId = 'directions-point-'+order;
            const pointSymbolLayerId = 'directions-origin-label-'+order;
            if(map.getSource(pointSourceId)) {
                map.getSource(pointSourceId).setData(pointData);
                map.removeLayer(pointCircleLayerId);
                map.removeLayer(pointSymbolLayerId);
            } else {
                map.addSource(pointSourceId, {
                    'type': 'geojson',
                    'data': pointData
                });
            }
            // Add point specific styles to the map
            map.addLayer({
                'id': pointCircleLayerId,
                'type': 'circle',
                'source': pointSourceId,
                'paint': {
                    'circle-radius': 18,
                    'circle-color': '#3bb2d0'
                }
            });
            map.addLayer(
                {
                    'id': pointSymbolLayerId,
                    'type': 'symbol',
                    'source': pointSourceId,
                    'layout': {
                        'text-field': order+'',
                        'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
                        'text-size': 12
                    },
                    'paint': {
                        'text-color': '#fff'
                    },
                    'filter': ['==', 'title', order]
                }
            );
        });

    }

    orderBranches(startEndLocations: Location[], branches: RouteBranch[], isRoundTrip: boolean): any {
        const copy: RouteBranch[] = Object.assign([], branches);
        let routeLocations = [];
        routeLocations.push(startEndLocations[0]);
        copy.forEach((branch) => {
            routeLocations.push(branch.endTo);
        });
        if(!isRoundTrip && startEndLocations.length > 1) {
            routeLocations.push(startEndLocations[1]);
        }

        let optimizationPoints = routeLocations.map((l) => l.lon + ',' + l.lat).join(';');
        return this.mapboxApiService.optimizationQuery(optimizationPoints, apiToken, isRoundTrip).map((res) => {
            console.log(res);
            const trips = res.trips[0];
            for(let i=1; i<=res.waypoints.length-1; i++) {
                let orderedBranch = copy[i-1];
                const waypoint = res.waypoints[i];
                orderedBranch.order = waypoint.waypoint_index;
                const leg = trips.legs[waypoint.waypoint_index-1];
                let activity = new JetkizuActivity();
                activity.fromLon = leg.steps[0].geometry.coordinates[0][0] + '';
                activity.fromLat = leg.steps[0].geometry.coordinates[0][1] + '';
                activity.planKm = leg.distance;
                activity.planDuration = leg.duration;
                orderedBranch.jetkizuActivity = activity;
            }

            return copy;
        });
    }

    showMultipleDirections(map: any, startEndLocations: Location[], branches: RouteBranch[], isRoundTrip: boolean){
        let routeLocations = [];
        routeLocations.push(startEndLocations[0]);
        let mappedBranches = [];
        branches.forEach((branch) => {
            routeLocations.push(branch.endTo);
        });
        if(!isRoundTrip) {
            routeLocations.push(startEndLocations[1]);
        }

        let arrivalBranch = new RouteBranch();
        arrivalBranch.routeBranchStatusHistory = new RouteBranchStatusHistory();
        arrivalBranch.routeBranchStatusHistory.status = RouteBranchStatus.IN_PROCESS;
        arrivalBranch.type = RouteBranchType.END;
        arrivalBranch.endTo = startEndLocations[1];
        branches.push(arrivalBranch);

        console.log('route locations: ');
        console.log(routeLocations);
        let optimizationPoints = routeLocations.map((l) => l.lon + ',' + l.lat).join(';');
        console.log(optimizationPoints);
        let result = [];
        this.mapboxApiService.optimizationQuery(optimizationPoints, apiToken, isRoundTrip).subscribe((res) => {
            console.log('optimization route: ');
            console.log(res);
            const trips = res.trips[0];
            const distance = trips.distance;
            const duration = trips.duration;
            const coordinates = trips.geometry.coordinates;
            console.log('distance: '+distance);
            console.log('duration: '+duration);
            console.log('coordinates: '+coordinates);

            let routeData = {
                'type': 'Feature',
                'geometry': {
                    'type': 'LineString',
                    'coordinates': res.trips[0].geometry.coordinates
                }
            };
            if(map.getSource('route')) {
                map.getSource('route').setData(routeData);
                map.removeLayer('routeLine');
                map.removeLayer('routearrows');
            } else {
                map.addSource('route', {
                    'type': 'geojson',
                    'data': routeData
                });
            }
            map.addLayer({
                'id': 'routeLine',
                'source': 'route',
                'type': 'line',
                'layout': {
                    'line-cap': 'round',
                    'line-join': 'round'
                },
                'paint': {
                    //'line-width': 4,
                    'line-width': {
                        base: 1,
                        stops: [[12, 3], [22, 12]]
                    },
                    'line-color': '#3bb2d0'
                }
            }, 'waterway-label');

            map.addLayer({
                id: 'routearrows',
                type: 'symbol',
                source: 'route',
                layout: {
                    'symbol-placement': 'line',
                    'text-field': '▶',
                    'text-size': {
                        base: 1,
                        stops: [[12, 24], [22, 60]]
                    },
                    'symbol-spacing': {
                        base: 1,
                        stops: [[12, 30], [22, 160]]
                    },
                    'text-keep-upright': false
                },
                paint: {
                    'text-color': '#3887be',
                    'text-halo-color': 'hsl(55, 11%, 96%)',
                    'text-halo-width': 3
                }
            }, 'waterway-label');

            let pointFeatures = [];
            console.log('gonna generate order of branches:');
            for(let i=1; i<=res.waypoints.length-1; i++) {
                let orderedBranch = branches[i-1];
                if(orderedBranch.type !== RouteBranchType.END) {
                    pointFeatures.push(this.getPointFeature(res.waypoints[i].location, orderedBranch.id, res.waypoints[i].waypoint_index));
                }
                const waypoint = res.waypoints[i];
                console.log(orderedBranch);
                orderedBranch.order = waypoint.waypoint_index;

                const leg = trips.legs[waypoint.waypoint_index-1];
                console.log(waypoint.waypoint_index-1 + ' -> ');
                console.log(leg);
                console.log(orderedBranch);
                let activity = new JetkizuActivity();
                // activity.routeBranch = orderedBranch;
                activity.fromLon = leg.steps[0].geometry.coordinates[0][0] + '';
                activity.fromLat = leg.steps[0].geometry.coordinates[0][1] + '';
                activity.planKm = leg.distance;
                activity.planDuration = leg.duration;
                orderedBranch.jetkizuActivity = activity;
            }

            if(isRoundTrip) {
                let homeDestination = branches[branches.length-1];
                homeDestination.order = res.waypoints.length;
                let activity = new JetkizuActivity();
                // activity.routeBranch = homeDestination;
                const leg = trips.legs[trips.legs.length - 1];
                activity.fromLon = leg.steps[0].geometry.coordinates[0][0] + '';
                activity.fromLat = leg.steps[0].geometry.coordinates[0][1] + '';
                activity.planKm = leg.distance;
                activity.planDuration = leg.duration;
                homeDestination.jetkizuActivity = activity;
            }
            console.log(pointFeatures);

            for(let i=0; i<trips.legs.length; i++) {
                const leg = trips.legs[i];
                console.log(leg.distance + '-> ' + branches);
                console.log(branches);
                console.log(trips.legs);
                let orderedBranch = branches[i];
            }
            let pointData = {
                'type': 'FeatureCollection',
                'features': pointFeatures
            };
            if(map.getSource('point')) {
                map.getSource('point').setData(pointData);
                map.removeLayer('destinationPoint');
                for(let i=0; i<res.waypoints.length; i++) {
                    if(map.getLayer('directions-origin-label-'+i)){
                        map.removeLayer('directions-origin-label-'+i);
                    }
                }
            } else {
                map.addSource('point', {
                    'type': 'geojson',
                    'data': pointData
                });
            }
            map.addLayer({
                'id': 'destinationPoint',
                'source': 'point',
                'type': 'circle',
                'paint': {
                    'circle-radius': 18,
                    'circle-color': '#3bb2d0'
                }
            });
            for(let i=0; i<res.waypoints.length; i++) {
                map.addLayer({
                    'id': 'directions-origin-label-'+i,
                    'type': 'symbol',
                    'source': 'point',
                    'layout': {
                        'text-field': i+'',
                        'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
                        'text-size': 12
                    },
                    'paint': {
                        'text-color': '#fff'
                    },
                    'filter': ['==', 'title', i]
                });
            }

            map.on("mousemove", "destinationPoint", function(e) {
                console.log(e);
                console.log(e.features[0].properties.name);
                //map.setFilter("state-fills-hover", ["==", "name", e.features[0].properties.name]);
            });
        });
    }

    drawDirections(map: any, startEndLocations:any, branches: RouteBranch[]) {
        let isRoundTrip = true;
        let directions = [];
        directions.push(startEndLocations[0]);
        branches.forEach((branch) => {
            if(branch.jetkizu){
                directions.push(branch.jetkizu.location);
            }
        });
        if(startEndLocations[0].id !== startEndLocations[1].id) {
            isRoundTrip = false;
            directions.push(startEndLocations[1]);
        }

        let optimizationPoints = directions.map((l) => l.lon + ',' + l.lat).join(';');
        this.mapboxApiService.optimizationQuery(optimizationPoints, apiToken, isRoundTrip).subscribe((res) => {
            map.addSource('route', {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'geometry': {
                        'type': 'LineString',
                        'coordinates': res.trips[0].geometry.coordinates
                    }
                }
            });
            map.addLayer({
                'id': 'routeLine',
                'source': 'route',
                'type': 'line',
                'layout': {
                    'line-cap': 'round',
                    'line-join': 'round'
                },
                'paint': {
                    //'line-width': 4,
                    'line-width': {
                        base: 1,
                        stops: [[12, 3], [22, 12]]
                    },
                    'line-color': '#3bb2d0'
                }
            }, 'waterway-label');
            map.addLayer({
                id: 'routearrows',
                type: 'symbol',
                source: 'route',
                layout: {
                    'symbol-placement': 'line',
                    'text-field': '▶',
                    'text-size': {
                        base: 1,
                        stops: [[12, 24], [22, 60]]
                    },
                    'symbol-spacing': {
                        base: 1,
                        stops: [[12, 30], [22, 160]]
                    },
                    'text-keep-upright': false
                },
                paint: {
                    'text-color': '#3887be',
                    'text-halo-color': 'hsl(55, 11%, 96%)',
                    'text-halo-width': 3
                }
            }, 'waterway-label');

            let pointFeatures = [];
            for(let i=1; i<=res.waypoints.length-1; i++) {
                pointFeatures.push(this.getPointFeature(res.waypoints[i].location, res.waypoints[i].waypoint_index, res.waypoints[i].waypoint_index+''));
            }
            map.addSource('point', {
                'type': 'geojson',
                'data': {
                    'type': 'FeatureCollection',
                    'features': pointFeatures
                }
            });
            map.addLayer({
                'id': 'destinationPoint',
                'source': 'point',
                'type': 'circle',
                'paint': {
                    'circle-radius': 18,
                    'circle-color': '#3bb2d0'
                }
            });
            for(let i=0; i<res.waypoints.length; i++) {
                map.addLayer({
                    'id': 'directions-origin-label-'+i,
                    'type': 'symbol',
                    'source': 'point',
                    'layout': {
                        'text-field': i+'',
                        'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
                        'text-size': 12
                    },
                    'paint': {
                        'text-color': '#fff'
                    },
                    'filter': ['==', 'title', i]
                });
            }

            map.on("mousemove", "destinationPoint", function(e) {
                console.log(e);
                console.log(e.features[0].properties.name);
                //map.setFilter("state-fills-hover", ["==", "name", e.features[0].properties.name]);
            });

            this.flyTo(map, [startEndLocations[0].lon, startEndLocations[0].lat], 12);
        });
    }

    flyTo(map, lonlat, zoom) {
        let defaultZoom = 14;
        if(zoom) {
            defaultZoom = zoom;
        }
        map.flyTo({
            center: lonlat,
            zoom: defaultZoom
        });
    }

    getDirectionFeature(coordinates) {
        return {
            'type': 'Feature',
            'geometry': {
                'type': 'LineString',
                'coordinates': coordinates
            }
        };
    }

    getPointFeature(lonlat: string[], id: any, title: string) {
        return {
            'type': 'Feature',
            'geometry': {
                'type': 'Point',
                'coordinates': lonlat
            },
            'properties': {
                'id': id,
                'title': title
            }
        };
    }

    getLonLatDirectionCoordinatesFormat(lonFrom: any, latFrom: any, lonTo: any, latTo: any): string {
        return lonFrom + ',' + latFrom + ';' + lonTo + ',' + latTo;
    }

    generateFeature(coordinates, geometryType, name, id) {
        console.log('name: '+name);
        return {
            'type': 'Feature',
            'properties': {
                'name': name,
                'id': id
            },
            'geometry': {
                'type': geometryType,
                'coordinates': coordinates
            }
        };
    }

    generateFeatureCollection(features) {
        return {
            'type': 'FeatureCollection',
            'features': features
        }
    }

    createRoute(map, routeId, type, data) {
        if(map.getSource(routeId)) {
            map.getSource(routeId).setData(data);
            map.removeLayer('area-fills');
            map.removeLayer('area-borders');
            map.removeLayer('area-fills-hover');
        } else {
            map.addSource(routeId, {
                'type': type,
                'data': data
            });
        }
    }

    removeRoute(map, routeId) {
        if(map.getSource(routeId)) {
            map.removeLayer('area-fills');
            map.removeLayer('area-borders');
            map.removeLayer('area-fills-hover');
        }
    }

    setPaintProperty(map, routeId, layerId, type) {
        map.addLayer({
            "id": "area-fills",
            "type": "fill",
            "source": routeId,
            "layout": {},
            "paint": {
                "fill-color": "#627BC1",
                "fill-opacity": 0.5
            }
        });

        map.addLayer({
            "id": "area-borders",
            "type": "line",
            "source": routeId,
            "layout": {},
            "paint": {
                "line-color": "#627BC1",
                "line-width": 2
            }
        });

        map.addLayer({
            "id": "area-fills-hover",
            "type": "fill",
            "source": routeId,
            "layout": {},
            "paint": {
                "fill-color": "#627BC1",
                "fill-opacity": 1
            },
            "filter": ["==", "name", ""]
        });

        map.addLayer({
            "id": "area-fills-selected",
            "type": "fill",
            "source": routeId,
            "layout": {},
            'paint': {
                'fill-color': '#088',
                'fill-opacity': 0.5
            },
            "filter": ["==", "id", ""]
        });

        // When the user moves their mouse over the states-fill layer, we'll update the filter in
        // the state-fills-hover layer to only show the matching state, thus making a hover effect.
        map.on("mousemove", "area-fills", function(e) {
            //map.setFilter("area-fills-hover", ["==", "name", e.features[0].properties.name]);
        });

        // Reset the state-fills-hover layer's filter when the mouse leaves the layer.
        map.on("mouseleave", "area-fills", function() {
            //map.setFilter("area-fills-hover", ["==", "name", ""]);
        });
    }

    addSymbol(map, layerId, routeId, textField, filterKey, filterValue) {
        map.addLayer({
            'id': layerId,
            'type': 'symbol',
            'source': routeId,
            'layout': {
                'text-field': textField,
                'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
                'text-size': 12
            },
            'paint': {
                'text-color': '#fff'
            },
            'filter': ['==', filterKey, filterValue]
        });
    }
}
