import { Injectable } from '@angular/core';
import {Location} from '../../entities/location/location.model';
import {Http, Response} from '@angular/http';
import {MapboxService} from '../open-api-services/mapbox.service';
import {environment} from '../constants/environment';
import {RouteBranch, RouteBranchType} from '../../entities/route-branch/route-branch.model';
import {
    RouteBranchStatus,
    RouteBranchStatusHistory
} from '../../entities/route-branch-status-history/route-branch-status-history.model';
import {MapService} from '../map/map.service';

const apiToken = environment.MAPBOX_API_KEY;
declare let L: any;

const defaultCoords = [71.50092, 51.132565];
const defaultZoom = 15;

var mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
var MapboxDraw = require('@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.js');
var MapboxDirections = require('@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.js');

@Injectable()
export class OsmService {

    constructor(
        private http: Http,
        private mapboxApiService: MapboxService,
        private mapService: MapService
    ) { }

    osmToLocation(event: any, flagGarage: boolean): Location {
        console.log('request to convert osm to Location');
        console.log(event);
        const osm = event.item;

        return this.convertOsmToLocation(osm, flagGarage);
    }

    convertOsmToLocation(osm: any, flagGarage: boolean) {
        let location = new Location();
        location.fullAddress = osm.display_name;
        location.city = osm.address.city;
        location.home = osm.address.house_number;
        location.postindex = osm.address.postcode;
        location.street = osm.address.road;
        location.lat = osm.lat;
        location.lon = osm.lon;
        location.osmId = osm.osm_id;
        location.placeId = osm.place_id;
        location.osmType = osm.osm_type;
        location.flagGarage = flagGarage;
        console.log(location);

        return location;
    }

    searchAddressFromOsm(query) {
        const osmQuery = 'https://nominatim.openstreetmap.org/search?q='+query+'&format=json&polygon=1&addressdetails=1';
        console.log(osmQuery);
        return this.http.get(osmQuery).map((res:Response) => {
            return res.json();
        });
    }

    showMultipleMarkers(map: any, routeBranches: RouteBranch[], prefix: string){
        let geoJson = [];
        routeBranches.forEach(branch => {
            geoJson.push(this.mapService.getPointFeature([branch.endTo.lon, branch.endTo.lat], branch.id, branch.order+''));
        });

        let markersData = {
            'type': 'FeatureCollection',
            'features': geoJson
        };

        markersData.features.forEach((marker) => {
            console.log(marker);
            const elNormal = this.mapService.getMarkerElement('normal', marker.properties.title);
            elNormal.id = marker.properties.id+'-normal';

            new mapboxgl.Marker(elNormal)
                .setLngLat(marker.geometry.coordinates)
                .addTo(map);

            this.mapService.addMarkerHoverEvent(elNormal, 'normal');
        });
    }

    convertDistanceToKm(distance?: number): any {
        if(!distance) {
            return 'N/A';
        }
        const result = this.round(distance / 1000, 1);
        console.log('distance -> '+distance+' -> '+result);
        return result + ' км';
    }

    convertDurationToHour(duration?: number): any {
        if(!duration) {
            return 'N/A';
        }
        let minutes = Math.round(duration / 60);
        let hours = 0;
        if(minutes >= 60) {
            hours = Math.floor(minutes / 60);
        }
        minutes = minutes - hours * 60;
        let h = hours + '';
        if(hours < 10) {
            h = '0' + hours;
        }
        let m = minutes + '';
        if(minutes < 10) {
            m = '0' + minutes;
        }
        console.log('duration result-> '+duration+' -> minutes: '+minutes+', hours: '+hours);

        return `${h}ч:${m}м`;
    }

    round(number, precision) {
        const shift = (number, precision, reverseShift) => {
            if (reverseShift) {
                precision = -precision;
            }
            const numArray = ('' + number).split('e');
            return + (numArray[0] + 'e' + (numArray[1] ? (+numArray[1] + precision) : precision));
        };
        return shift(Math.round(shift(number, precision, false)), precision, true);
    }

}
