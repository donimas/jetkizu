import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'jetkizuPipe', pure: false })
export class JetkizuPipe implements PipeTransform {

    public transform(items: any[], filter: Object): any {
        if (!items) {
          return items;
        }
        // filter items array, items which match and return true will be
        // kept, false will be filtered out
        return items.filter((item) => item.jetkizu != null);
    }
}
