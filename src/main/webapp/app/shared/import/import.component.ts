import {Component, AfterViewInit, Renderer, ElementRef, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import {JhiDataUtils, JhiEventManager} from 'ng-jhipster';
import {AttachmentFile} from '../../entities/attachment-file/attachment-file.model';
import {AttachmentFileService} from '../../entities/attachment-file/attachment-file.service';
import {CsvFieldService} from '../../entities/csv-field/csv-field.service';
import {CsvField} from '../../entities/csv-field/csv-field.model';
import {ResponseWrapper} from '../model/response-wrapper.model';
import {isUndefined} from 'util';
import {Jetkizu} from '../../entities/jetkizu/jetkizu.model';
import {Customer} from '../../entities/customer/customer.model';
import {Location} from '../../entities/location/location.model';
import {RouteBranch, RouteBranchType} from '../../entities/route-branch/route-branch.model';
import {
    RouteBranchStatus,
    RouteBranchStatusHistory
} from '../../entities/route-branch-status-history/route-branch-status-history.model';
import {OsmService} from '../osm/osm.service';
import {MapService} from '../map/map.service';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {JetkizuRouteService} from '../../entities/jetkizu-route/jetkizu-route.service';
import {JetkizuRoute} from "../../entities/jetkizu-route/jetkizu-route.model";
import {Helpers} from '../../helpers';
import {DropzoneConfigInterface} from 'ngx-dropzone-wrapper';


@Component({
    selector: 'import-modal',
    templateUrl: './import.component.html'
})
export class ImportModalComponent implements OnInit, AfterViewInit {

    attachmentFile: AttachmentFile;
    isSaving: boolean;
    csvFileHeader: any;
    lines: any;

    csvFields: CsvField[];
    importStatus: string = 'upload';
    importStatuses = ['upload', 'field', 'address'];

    routeBranches: RouteBranch[];
    routeBranchContainer: any;

    map: any;
    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribedOsm = new Observable(() => () => this.searching = false);

    dropzoneConfig: DropzoneConfigInterface = {
        // Change this to your upload POST address:
        url: 'http://localhost:9000',
        maxFilesize: 1,
        acceptedFiles: 'csv/*'
    };

    constructor(
        private eventManager: JhiEventManager,
        private elementRef: ElementRef,
        private renderer: Renderer,
        private router: Router,
        private dataUtils: JhiDataUtils,
        public activeModal: NgbActiveModal,
        private attachmentFileService: AttachmentFileService,
        private csvFieldService: CsvFieldService,
        private osmService: OsmService,
        private mapService: MapService,
        private jetkizuRouteService: JetkizuRouteService,
    ) {
        this.attachmentFile = new AttachmentFile();
    }

    ngOnInit() {
        this.csvFieldService.query().subscribe(
            (res: ResponseWrapper) => {
                this.csvFields = res.json;
                console.log(this.csvFields);
            }
        );
    }

    ngAfterViewInit() {
        // this.renderer.invokeElementMethod(this.elementRef.nativeElement.querySelector('#username'), 'focus', []);
    }

    importCsv() {
        console.log('gonna import');
        this.attachmentFileService.importCsv(this.attachmentFile)
            .subscribe((res) => console.log(res),
                (error) => console.log(error));
    }

    cancel() {
        this.activeModal.dismiss('cancel');
    }

    back() {
        if(this.importStatus === 'field') {
            this.importStatus = 'upload';
            this.lines = null;
            this.csvFileHeader = null;
            this.attachmentFile = new AttachmentFile();
        } else if (this.importStatus === 'address') {
            this.importStatus = 'field';
            this.routeBranchContainer = null;
        }
    }

    proceed() {
        Helpers.setLoading(true);
        this.importStatus = 'address';
        setTimeout(() => {
            this.map = this.mapService.showMapboxglMap('map', 12);
        });
        this.generateRouteBranchesWithOsmAddresses().then((res) => {
            this.routeBranchContainer = res;
            let row = 0;
            if(this.routeBranchContainer.foundBranches.length) {
                this.routeBranchContainer.foundBranches = this.routeBranchContainer.foundBranches
                    .filter((branch: RouteBranch) => !branch.endTo.flagOsmNotFound);

                for(let i = 0; i < this.routeBranchContainer.foundBranches.length; i++) {
                    row++;
                    this.routeBranchContainer.foundBranches[i].order = row;
                }
            }
            if(this.routeBranchContainer.notFoundBranches.length) {
                for(let i = 0; i < this.routeBranchContainer.notFoundBranches.length; i++) {
                    row++;
                    this.routeBranchContainer.notFoundBranches[i].order = row;
                }
            }
            console.log('Result from promise: ', res);

            this.map.on('load', () => {
                for(let i=0; i < this.routeBranchContainer.foundBranches.length; i++) {
                    const branch = this.routeBranchContainer.foundBranches[i];
                    let marker = this.mapService.addMapboxglMarker(
                        this.map,
                        [branch.endTo.lon, branch.endTo.lat],
                        'normal-5',
                        branch.order,
                        branch.order+'');

                    if(i === this.routeBranchContainer.foundBranches.length-1) {
                        Helpers.setLoading(false);
                    }
                }
            });
        });
    }

    generateRouteBranchesWithOsmAddresses() {
        return new Promise((resolve) => {
            let foundBranches: RouteBranch[] = [];
            let notFoundBranches: RouteBranch[] = [];
            let result = {
                foundBranches: foundBranches,
                notFoundBranches: notFoundBranches
            };
            for(let j = 0; j < this.lines.length; j++) {
                let senderFio = null; // senderFio
                let receiverFio = null; // receiverFio
                let receiverAddress = null; // receiverAddress
                let mobilePhone1 = null; // mobilePhone1
                let mobilePhone2 = null; // mobilePhone2
                let price = null; // price
                let deliveryDate = null; // deliveryDate
                let weight = null; // weight
                let name = null;

                for(let i = 0; i < this.csvFileHeader.length; i++) {
                    const col = this.lines[j][i];
                    const header = this.csvFileHeader[i];

                    switch (header.field.code) {
                        case 'senderFio': senderFio = col; break;
                        case 'receiverFio': receiverFio = col; break;
                        case 'mobilePhone1': mobilePhone1 = col; break;
                        case 'mobilePhone2': mobilePhone2 = col; break;

                        case 'name': name = col; break;
                        case 'receiverAddress': receiverAddress = col; break;
                        case 'price': price = col; break;
                        case 'deliveryDate': deliveryDate = col; break;
                        case 'weight': weight = col; break;
                    }
                }

                let customer = new Customer();
                customer.senderFio = senderFio;
                customer.receiverFio = receiverFio;
                customer.cellPhone = mobilePhone1;
                customer.homePhone = mobilePhone2;

                let jetkizu = new Jetkizu();
                // jetkizu.customer = customer;
                jetkizu.name = name;
                jetkizu.orderPrice = price;
                jetkizu.weight = weight;

                let location: Location = new Location();
                location.fullAddress = receiverAddress;

                let routeBranch = new RouteBranch();
                routeBranch.jetkizu = jetkizu;
                routeBranch.customer = customer;
                routeBranch.endTo = location;
                routeBranch.type = RouteBranchType.DELIVERY;
                let routeBranchStatus = new RouteBranchStatusHistory();
                routeBranchStatus.status = RouteBranchStatus.BACKLOG;
                routeBranch.routeBranchStatusHistory = routeBranchStatus;

                if(routeBranch.endTo.fullAddress && receiverAddress.length > 0) {
                    result.foundBranches.push(routeBranch);
                } else {
                    result.notFoundBranches.push(routeBranch);
                }
            }

            let row = 0;
            for(let i = 0; i < result.foundBranches.length; i++) {
                const branch = result.foundBranches[i];
                if(branch.endTo.flagOsmNotFound) {
                    continue;
                }

                this.osmService.searchAddressFromOsm(branch.endTo.fullAddress).subscribe((res) => {
                    console.log('osm found next location -> ', res);
                    row++;
                    console.log('row number -> ', row);
                    if(res.length && (res[0].address.road && res[0].address.house_number)) {
                        branch.endTo = this.osmService.convertOsmToLocation(res[0], false);
                    } else {
                        branch.endTo.flagOsmNotFound = true;
                        result.notFoundBranches.push(branch);
                    }
                    if(row === result.foundBranches.length) {
                        resolve(result);
                    }
                });
            }

        });
    }

    submit() {
        this.isSaving = true;
        Helpers.setLoading(true);
        this.routeBranchContainer.notFoundBranches
            .filter((branch: RouteBranch) => !branch.endTo.flagOsmNotFound)
            .forEach((branch: RouteBranch) => this.routeBranchContainer.foundBranches.push(branch));
        console.log(this.routeBranchContainer);
        let jetkizuRoute = new JetkizuRoute();
        jetkizuRoute.routeBranches = this.routeBranchContainer.foundBranches;
        this.jetkizuRouteService.importBranches(jetkizuRoute).subscribe((response) => {
            this.isSaving = false;
            Helpers.setLoading(false);

            this.eventManager.broadcast({
                name: 'importBranchListModification',
                content: 'Import route branches'
            });
            this.activeModal.dismiss(true);
        }, (error) => {
            this.isSaving = false;
            Helpers.setLoading(false);
            console.log(error);
        });
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }

    getDefaultFieldByIndex(i: number): CsvField {
        console.log('index -> ', i);
        return this.csvFields[i];
    }

    setFileData(event, entity, field, isImage) {
        console.log(event);
        this.dataUtils.setFileData(event, entity, field, isImage);
        const fileReader = event.target.files[0];
        let reader: FileReader = new FileReader();
        reader.readAsText(fileReader);

        this.importStatus = 'field';

        reader.onload = (e) => {
            let csv: string = reader.result;
            let allTextLines = csv.split(/\r|\n|\r/);
            const headers = allTextLines[0].split(',');
            this.csvFileHeader = [];
            for(let i = 0; i < headers.length; i++) {
                let field = this.csvFields.filter((field) => field.code === headers[i])[0];
                if(!field) {
                    field =  i < this.csvFields.length ? this.csvFields[i] : this.csvFields[0];
                }
                console.log(i+' -> ', field);
                const h = {
                    index: i,
                    field: field,
                    text: headers[i]
                };
                this.csvFileHeader.push(h);
            }

            this.lines = [];

            for (let i = 1; i <= allTextLines.length; i++) {
                // split content based on comma
                const textLine = allTextLines[i];
                if(isUndefined(textLine)) {
                    continue;
                }
                let data = textLine.split(',');
                if (data.length === this.csvFileHeader.length) {
                    let tarr = [];
                    for (let j = 0; j < this.csvFileHeader.length; j++) {
                        tarr.push(data[j]);
                    }

                    // log each row to see output
                    console.log(tarr);
                    this.lines.push(tarr);
                }
            }

            console.log(">>>>>>>>>>>>>>>>>", this.lines);
        }
    }

    searchOsm = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this.osmService.searchAddressFromOsm(term)
                    .do((v) => {
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedOsm);

    formatterOsm = (x: {display_name: string}) => x.display_name;

    public selectedOsmItem(event, branch: RouteBranch) {
        console.log(event);
        const row = branch.order;
        branch.endTo = this.osmService.osmToLocation(event, false);
        branch.endTo.flagOsmNotFound = false;
        this.mapService.addMapboxglMarker(this.map, [branch.endTo.lon, branch.endTo.lat], 'normal-5', row, row+'');
        this.mapService.flyTo(this.map, [branch.endTo.lon, branch.endTo.lat], 12);
    }


    onUploadError(e) {
        console.log(e);
    }
    onUploadSuccess(e) {
        console.log(e);
        // this.dataUtils.setFileData(event, entity, field, isImage);
        const fileReader = e[0];
        let reader: FileReader = new FileReader();
        reader.readAsText(fileReader);

        this.importStatus = 'field';

        reader.onload = (e) => {
            let csv: string = reader.result;
            let allTextLines = csv.split(/\r|\n|\r/);
            const headers = allTextLines[0].split(',');
            this.csvFileHeader = [];
            for(let i = 0; i < headers.length; i++) {
                let field = this.csvFields.filter((field) => field.code === headers[i])[0];
                if(!field) {
                    field =  i < this.csvFields.length ? this.csvFields[i] : this.csvFields[0];
                }
                console.log(i+' -> ', field);
                const h = {
                    index: i,
                    field: field,
                    text: headers[i]
                };
                this.csvFileHeader.push(h);
            }

            this.lines = [];

            for (let i = 1; i <= allTextLines.length; i++) {
                // split content based on comma
                const textLine = allTextLines[i];
                if(isUndefined(textLine)) {
                    continue;
                }
                let data = textLine.split(',');
                if (data.length === this.csvFileHeader.length) {
                    let tarr = [];
                    for (let j = 0; j < this.csvFileHeader.length; j++) {
                        tarr.push(data[j]);
                    }

                    // log each row to see output
                    console.log(tarr);
                    this.lines.push(tarr);
                }
            }

            console.log(">>>>>>>>>>>>>>>>>", this.lines);
        }
    }
}
