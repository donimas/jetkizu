import { Injectable } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

import {ImportModalComponent} from './import.component';


@Injectable()
export class ImportModalService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
    ) {}

    open(): NgbModalRef {
        console.log('opening');
        if (this.isOpen) {
            console.log('opened');
            return;
        }
        this.isOpen = true;
        const modalRef = this.modalService.open(ImportModalComponent, {
            size: 'lg',
            backdrop: 'static',
            container: 'nav',
            windowClass: 'modal-xxl'
        });
        modalRef.result.then((result) => {
            this.isOpen = false;
        }, (reason) => {
            this.isOpen = false;
        });
        return modalRef;
    }
}
