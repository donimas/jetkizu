import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { RouteBranchStatusHistory } from './route-branch-status-history.model';
import { RouteBranchStatusHistoryService } from './route-branch-status-history.service';

@Component({
    selector: 'jhi-route-branch-status-history-detail',
    templateUrl: './route-branch-status-history-detail.component.html'
})
export class RouteBranchStatusHistoryDetailComponent implements OnInit, OnDestroy {

    routeBranchStatusHistory: RouteBranchStatusHistory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private routeBranchStatusHistoryService: RouteBranchStatusHistoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRouteBranchStatusHistories();
    }

    load(id) {
        this.routeBranchStatusHistoryService.find(id).subscribe((routeBranchStatusHistory) => {
            this.routeBranchStatusHistory = routeBranchStatusHistory;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRouteBranchStatusHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'routeBranchStatusHistoryListModification',
            (response) => this.load(this.routeBranchStatusHistory.id)
        );
    }
}
