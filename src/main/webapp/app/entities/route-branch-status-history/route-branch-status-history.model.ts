import { BaseEntity } from './../../shared';

export const enum RouteBranchStatus {
    'BACKLOG', 'NOT_STARTED', 'IN_PROCESS', 'DRIVING', 'REACHED', 'FAILED', 'SKIPPED'
    /*'BACKLOG',
    'NOT_STARTED',
    'IN_PROCESS',
    'PICKING',
    'DELIVERING',
    'DELIVERED',
    'FAILED',
    'SKIPPED',
    'ARRIVAL'*/
}

export class RouteBranchStatusHistory implements BaseEntity {
    constructor(
        public id?: number,
        public createDateTime?: any,
        public flagDeleted?: boolean,
        public status?: RouteBranchStatus,
        public routeBranch?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
