import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RouteBranchStatusHistoryComponent } from './route-branch-status-history.component';
import { RouteBranchStatusHistoryDetailComponent } from './route-branch-status-history-detail.component';
import { RouteBranchStatusHistoryPopupComponent } from './route-branch-status-history-dialog.component';
import { RouteBranchStatusHistoryDeletePopupComponent } from './route-branch-status-history-delete-dialog.component';

export const routeBranchStatusHistoryRoute: Routes = [
    {
        path: 'route-branch-status-history',
        component: RouteBranchStatusHistoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranchStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'route-branch-status-history/:id',
        component: RouteBranchStatusHistoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranchStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const routeBranchStatusHistoryPopupRoute: Routes = [
    {
        path: 'route-branch-status-history-new',
        component: RouteBranchStatusHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranchStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-branch-status-history/:id/edit',
        component: RouteBranchStatusHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranchStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-branch-status-history/:id/delete',
        component: RouteBranchStatusHistoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranchStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
