import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    RouteBranchStatusHistoryService,
    RouteBranchStatusHistoryPopupService,
    RouteBranchStatusHistoryComponent,
    RouteBranchStatusHistoryDetailComponent,
    RouteBranchStatusHistoryDialogComponent,
    RouteBranchStatusHistoryPopupComponent,
    RouteBranchStatusHistoryDeletePopupComponent,
    RouteBranchStatusHistoryDeleteDialogComponent,
    routeBranchStatusHistoryRoute,
    routeBranchStatusHistoryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...routeBranchStatusHistoryRoute,
    ...routeBranchStatusHistoryPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RouteBranchStatusHistoryComponent,
        RouteBranchStatusHistoryDetailComponent,
        RouteBranchStatusHistoryDialogComponent,
        RouteBranchStatusHistoryDeleteDialogComponent,
        RouteBranchStatusHistoryPopupComponent,
        RouteBranchStatusHistoryDeletePopupComponent,
    ],
    entryComponents: [
        RouteBranchStatusHistoryComponent,
        RouteBranchStatusHistoryDialogComponent,
        RouteBranchStatusHistoryPopupComponent,
        RouteBranchStatusHistoryDeleteDialogComponent,
        RouteBranchStatusHistoryDeletePopupComponent,
    ],
    providers: [
        RouteBranchStatusHistoryService,
        RouteBranchStatusHistoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuRouteBranchStatusHistoryModule {}
