import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { RouteBranchStatusHistory } from './route-branch-status-history.model';
import { RouteBranchStatusHistoryService } from './route-branch-status-history.service';

@Injectable()
export class RouteBranchStatusHistoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private routeBranchStatusHistoryService: RouteBranchStatusHistoryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.routeBranchStatusHistoryService.find(id).subscribe((routeBranchStatusHistory) => {
                    routeBranchStatusHistory.createDateTime = this.datePipe
                        .transform(routeBranchStatusHistory.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.routeBranchStatusHistoryModalRef(component, routeBranchStatusHistory);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.routeBranchStatusHistoryModalRef(component, new RouteBranchStatusHistory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    routeBranchStatusHistoryModalRef(component: Component, routeBranchStatusHistory: RouteBranchStatusHistory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.routeBranchStatusHistory = routeBranchStatusHistory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
