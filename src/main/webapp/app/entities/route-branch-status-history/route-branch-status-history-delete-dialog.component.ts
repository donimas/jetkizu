import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RouteBranchStatusHistory } from './route-branch-status-history.model';
import { RouteBranchStatusHistoryPopupService } from './route-branch-status-history-popup.service';
import { RouteBranchStatusHistoryService } from './route-branch-status-history.service';

@Component({
    selector: 'jhi-route-branch-status-history-delete-dialog',
    templateUrl: './route-branch-status-history-delete-dialog.component.html'
})
export class RouteBranchStatusHistoryDeleteDialogComponent {

    routeBranchStatusHistory: RouteBranchStatusHistory;

    constructor(
        private routeBranchStatusHistoryService: RouteBranchStatusHistoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.routeBranchStatusHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'routeBranchStatusHistoryListModification',
                content: 'Deleted an routeBranchStatusHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-route-branch-status-history-delete-popup',
    template: ''
})
export class RouteBranchStatusHistoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private routeBranchStatusHistoryPopupService: RouteBranchStatusHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.routeBranchStatusHistoryPopupService
                .open(RouteBranchStatusHistoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
