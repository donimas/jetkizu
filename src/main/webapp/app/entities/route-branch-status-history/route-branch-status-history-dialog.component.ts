import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RouteBranchStatusHistory } from './route-branch-status-history.model';
import { RouteBranchStatusHistoryPopupService } from './route-branch-status-history-popup.service';
import { RouteBranchStatusHistoryService } from './route-branch-status-history.service';
import { RouteBranch, RouteBranchService } from '../route-branch';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-route-branch-status-history-dialog',
    templateUrl: './route-branch-status-history-dialog.component.html'
})
export class RouteBranchStatusHistoryDialogComponent implements OnInit {

    routeBranchStatusHistory: RouteBranchStatusHistory;
    isSaving: boolean;

    routebranches: RouteBranch[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private routeBranchStatusHistoryService: RouteBranchStatusHistoryService,
        private routeBranchService: RouteBranchService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.routeBranchService.query()
            .subscribe((res: ResponseWrapper) => { this.routebranches = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.routeBranchStatusHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.routeBranchStatusHistoryService.update(this.routeBranchStatusHistory));
        } else {
            this.subscribeToSaveResponse(
                this.routeBranchStatusHistoryService.create(this.routeBranchStatusHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<RouteBranchStatusHistory>) {
        result.subscribe((res: RouteBranchStatusHistory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: RouteBranchStatusHistory) {
        this.eventManager.broadcast({ name: 'routeBranchStatusHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRouteBranchById(index: number, item: RouteBranch) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-route-branch-status-history-popup',
    template: ''
})
export class RouteBranchStatusHistoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private routeBranchStatusHistoryPopupService: RouteBranchStatusHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.routeBranchStatusHistoryPopupService
                    .open(RouteBranchStatusHistoryDialogComponent as Component, params['id']);
            } else {
                this.routeBranchStatusHistoryPopupService
                    .open(RouteBranchStatusHistoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
