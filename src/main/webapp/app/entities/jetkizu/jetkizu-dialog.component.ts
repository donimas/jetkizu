import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Jetkizu } from './jetkizu.model';
import { JetkizuPopupService } from './jetkizu-popup.service';
import { JetkizuService } from './jetkizu.service';
import { JetkizuStatusHistory, JetkizuStatusHistoryService } from '../jetkizu-status-history';
import { Location, LocationService } from '../location';
import { Customer, CustomerService } from '../customer';
import { RouteBranch, RouteBranchService } from '../route-branch';
import { ResponseWrapper } from '../../shared';
import {YandexService} from '../../shared/open-api-services/yandex.service';
import {KazpostService} from '../../shared/open-api-services/kazpost.service';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';
import {MapService} from '../../shared/map/map.service';
import { CELL_PHONE_MASK } from '../../app.constants';
import { HOME_PHONE_MASK } from '../../app.constants';

@Component({
    selector: 'jhi-jetkizu-dialog',
    templateUrl: './jetkizu-dialog.component.html'
})
export class JetkizuDialogComponent implements OnInit, AfterViewInit {

    jetkizu: Jetkizu;
    isSaving: boolean;
    routebranches: RouteBranch[];
    jetkizustatushistories: JetkizuStatusHistory[];

    location: Location;
    customer: Customer;

    modelOsm: any;
    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribedOsm = new Observable(() => () => this.searching = false);
    hideSearchingWhenUnsubscribedKazpost = new Observable(() => () => this.searching = false);
    hideSearchingWhenUnsubscribedYandex = new Observable(() => () => this.searching = false);

    map: any;
    marker: any;
    cellPhoneMask = CELL_PHONE_MASK;
    homePhoneMask = HOME_PHONE_MASK;
    isKazpostNotFound = false;
    addressEmptyError = {
        'message': 'error.address-empty'
    };
    coordinateEmptyError = {
        'message': 'error.coordinate-empty'
    };

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private jetkizuService: JetkizuService,
        private jetkizuStatusHistoryService: JetkizuStatusHistoryService,
        private locationService: LocationService,
        private customerService: CustomerService,
        private routeBranchService: RouteBranchService,
        private eventManager: JhiEventManager,
        private _serviceKazpost: KazpostService,
        private _serviceYandex: YandexService,
        private _serviceMap: MapService,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        if(this.jetkizu.id) {
            this.location = this.jetkizu.location;
            this.customer = this.jetkizu.customer;
        } else {
            this.location = new Location();
            this.customer = new Customer();
        }
        this.jetkizuStatusHistoryService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizustatushistories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        /*this.locationService.query()
            .subscribe((res: ResponseWrapper) => { this.locations = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.customerService.query()
            .subscribe((res: ResponseWrapper) => { this.customers = res.json; }, (res: ResponseWrapper) => this.onError(res.json));*/
        this.routeBranchService.query()
            .subscribe((res: ResponseWrapper) => { this.routebranches = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngAfterViewInit() {
        /*this.map = this._serviceMap.showMapboxglMap('map');
        if(this.location.id) {
            this.marker = this._serviceMap.addMarker(this.map, [this.location.lat, this.location.lon], this.location.fullAddress);
        }
        this.map.on('click', (event) => this.onMapClick(event));*/
    }

    onMapClick(e) {
        console.log(e);
        console.log('clicked map');
        /*const lat = e.latlng.lat;
        const lon = e.latlng.lon;
        console.log(lat +', '+lon);*/
        this.addLocationMarker(e.latlng);
    }

    addLocationMarker(latlng) {
        if(!this.isLocationAddressEntered()) {
            this.onError(this.addressEmptyError);
            return;
        }
        if(this.marker) {
            this.marker.remove();
            //this.map.removeLayer(this.marker); // remove
        }
        let fullAddress = this.toFullAddress(this.location);
        this.location.lat = latlng.lat;
        this.location.lon = latlng.lng;
        //this.marker = this._serviceMap.addMarker(this.map, latlng, fullAddress);
        this.marker = this._serviceMap.addMapboxglMarker(this.map, latlng, 'normal');
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        if(!this.isLocationAddressEntered()) {
            this.onError(this.addressEmptyError);
            return;
        }
        if(!this.isLocationCoordinateEntered()) {
            this.onError(this.coordinateEmptyError);
            return;
        }
        this.location.fullAddress = this.toFullAddress(this.location);
        this.jetkizu.location = this.location;
        this.jetkizu.customer = this.customer;
        console.log(this.jetkizu);
        this.isSaving = true;
        if (this.jetkizu.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jetkizuService.update(this.jetkizu));
        } else {
            this.subscribeToSaveResponse(
                this.jetkizuService.create(this.jetkizu));
        }
    }

    private subscribeToSaveResponse(result: Observable<Jetkizu>) {
        result.subscribe((res: Jetkizu) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Jetkizu) {
        this.eventManager.broadcast({ name: 'jetkizuListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJetkizuStatusHistoryById(index: number, item: JetkizuStatusHistory) {
        return item.id;
    }

    trackLocationById(index: number, item: Location) {
        return item.id;
    }

    trackCustomerById(index: number, item: Customer) {
        return item.id;
    }

    trackRouteBranchById(index: number, item: RouteBranch) {
        return item.id;
    }

    searchKazpost = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceKazpost.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedKazpost);

    formatterKazpost = (x: {addressRus: string}) => x.addressRus;

    selectedKazpostItem(l) {
        console.log(l);
        this.location.fullAddress = l.item.addressRus;
        this.location.home = l.item.fullAddress.number;
        this.location.street = l.item.fullAddress.parts[0].nameRus;
        this.location.postindex = l.item.postcode;
        this.resetLocationCoordinates();
        console.log(this.location);
    }

    kazpostNotFound() {
        this.isKazpostNotFound = !this.isKazpostNotFound;
    }

    resetLocationCoordinates() {
        /*this.location.fullAddress = '';
        this.location.street = '';
        this.location.home = '';
        this.location.flat = '';
        this.location.postindex = '';*/
        this.map.removeLayer(this.marker); // remove
        this.location.lat = '';
        this.location.lon = '';
    }

    searchYandex = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceYandex.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedYandex);

    formatterYandex = (x: {metaDataProperty: {GeocoderMetaData: {text: string}}}) => x.metaDataProperty.GeocoderMetaData.text;

    public selectedYandexItem(event) {
        if(!this.isLocationAddressEntered()) {
            this.onError(this.addressEmptyError);
            return;
        }
        const coordinates = event.item.Point.pos.split(' ');
        this.location.lon = coordinates[0];
        this.location.lat = coordinates[1];
        let pos = {
            'lat': this.location.lat,
            'lng': this.location.lon
        };
        this.addLocationMarker(pos);
    }

    isLocationAddressEntered(): boolean {
        return ((this.location.street && this.location.street !== '') &&
            (this.location.home && this.location.home !== ''));
    }

    isLocationCoordinateEntered(): boolean {
        return ((this.location.lat && this.location.lat !== '') &&
            (this.location.lon && this.location.lon !== ''));
    }

    toFullAddress(location: Location): string {
        let fullAddress = location.fullAddress;
        if(!fullAddress || fullAddress === '') {
            fullAddress = this.location.street + ', ' + this.location.home;
        }
        return fullAddress;
    }
}

@Component({
    selector: 'jhi-jetkizu-popup',
    template: ''
})
export class JetkizuPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuPopupService: JetkizuPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jetkizuPopupService
                    .open(JetkizuDialogComponent as Component, params['id']);
            } else {
                this.jetkizuPopupService
                    .open(JetkizuDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
