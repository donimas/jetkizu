import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Jetkizu } from './jetkizu.model';
import { JetkizuPopupService } from './jetkizu-popup.service';
import { JetkizuService } from './jetkizu.service';

@Component({
    selector: 'jhi-jetkizu-delete-dialog',
    templateUrl: './jetkizu-delete-dialog.component.html'
})
export class JetkizuDeleteDialogComponent {

    jetkizu: Jetkizu;

    constructor(
        private jetkizuService: JetkizuService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jetkizuService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jetkizuListModification',
                content: 'Deleted an jetkizu'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-delete-popup',
    template: ''
})
export class JetkizuDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuPopupService: JetkizuPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuPopupService
                .open(JetkizuDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
