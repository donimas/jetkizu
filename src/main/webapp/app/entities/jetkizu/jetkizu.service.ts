import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Jetkizu } from './jetkizu.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class JetkizuService {

    private resourceUrl = SERVER_API_URL + 'api/jetkizus';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/jetkizus';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(jetkizu: Jetkizu): Observable<Jetkizu> {
        const copy = this.convert(jetkizu);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(jetkizu: Jetkizu): Observable<Jetkizu> {
        const copy = this.convert(jetkizu);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<Jetkizu> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    findAllBacklog(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/backlog`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to Jetkizu.
     */
    private convertItemFromServer(json: any): Jetkizu {
        const entity: Jetkizu = Object.assign(new Jetkizu(), json);
        entity.createDateTime = this.dateUtils
            .convertDateTimeFromServer(json.createDateTime);
        entity.updateDateTime = this.dateUtils
            .convertDateTimeFromServer(json.updateDateTime);
        return entity;
    }

    /**
     * Convert a Jetkizu to a JSON which can be sent to the server.
     */
    private convert(jetkizu: Jetkizu): Jetkizu {
        const copy: Jetkizu = Object.assign({}, jetkizu);

        copy.createDateTime = this.dateUtils.toDate(jetkizu.createDateTime);

        copy.updateDateTime = this.dateUtils.toDate(jetkizu.updateDateTime);
        return copy;
    }
}
