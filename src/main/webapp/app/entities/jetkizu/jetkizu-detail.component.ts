import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { Jetkizu } from './jetkizu.model';
import { JetkizuService } from './jetkizu.service';
import {JetkizuPopupService} from './jetkizu-popup.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'jhi-jetkizu-detail',
    templateUrl: './jetkizu-detail.component.html'
})
export class JetkizuDetailComponent implements OnInit, OnDestroy {

    jetkizu: Jetkizu;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager,
        private jetkizuService: JetkizuService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        /*this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });*/
        this.load(this.jetkizu.id);
        // this.registerChangeInJetkizus();
    }

    load(id) {
        this.jetkizuService.find(id).subscribe((jetkizu) => {
            this.jetkizu = jetkizu;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        /*this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);*/
    }

    registerChangeInJetkizus() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jetkizuListModification',
            (response) => this.load(this.jetkizu.id)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }
}

@Component({
    selector: 'jhi-jetkizu-detail-popup',
    template: ''
})
export class JetkizuDetailPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuPopupService: JetkizuPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            console.log('popup id');
            console.log(params['id']);
            if ( params['id'] ) {
                this.jetkizuPopupService
                    .open(JetkizuDetailComponent as Component, params['id']);
            } else {
                this.jetkizuPopupService
                    .open(JetkizuDetailComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
