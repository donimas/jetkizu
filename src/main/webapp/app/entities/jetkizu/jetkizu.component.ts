import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager, JhiParseLinks, JhiAlertService } from 'ng-jhipster';

import { Jetkizu } from './jetkizu.model';
import { JetkizuService } from './jetkizu.service';
import { ITEMS_PER_PAGE, Principal, ResponseWrapper } from '../../shared';
import {JetkizuRoute} from '../jetkizu-route/jetkizu-route.model';
import {JetkizuRouteService} from '../jetkizu-route/jetkizu-route.service';
import {RouteBranchService} from '../route-branch/route-branch.service';
import {RouteBranch} from '../route-branch/route-branch.model';
import {NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ImportModalService} from '../../shared/import/import-modal.service';
import {OsmService} from '../../shared/osm/osm.service';
import {Helpers} from "../../helpers";

@Component({
    selector: 'jhi-jetkizu',
    templateUrl: './jetkizu.component.html'
})
export class JetkizuComponent implements OnInit, OnDestroy {

    jetkizus: Jetkizu[];
    routeBranches: RouteBranch[];
    currentAccount: any;
    eventSubscriber: Subscription;
    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    currentSearch: string;

    jetkizuRoutes: JetkizuRoute[];
    routeData: any;
    linksRoute: any;
    totalItemsRoute: any;
    queryCountRoute: any;
    itemsPerPageRoute: any;
    pageRoute: any;
    predicateRoute: any;
    previousPageRoute: any;
    reverseRoute: any;

    modalRef: NgbModalRef;

    constructor(
        private jetkizuRouteService: JetkizuRouteService,
        private jetkizuService: JetkizuService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private parseLinks: JhiParseLinks,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,
        private routeBranchService: RouteBranchService,
        private importModalService: ImportModalService,
        private osmService: OsmService,
    ) {
        this.jetkizus = [];
        this.routeBranches = [];
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';

        this.itemsPerPageRoute = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            console.log(data);
            this.pageRoute = data['pagingParams'].page;
            this.previousPageRoute = data['pagingParams'].page;
            this.reverseRoute = data['pagingParams'].ascending;
            this.predicateRoute = data['pagingParams'].predicate;
        });
    }

    loadAll() {
        Helpers.setLoading(true);
        this.jetkizuRouteService.findAllNotStopped(null).subscribe(
            (res: ResponseWrapper) => this.onSuccessRoute(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
        if (this.currentSearch) {
            this.jetkizuService.search({
                query: this.currentSearch,
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.routeBranchService.getAllBacklog({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    reset() {
        console.log('resetting');
        this.page = 0;
        //this.jetkizus = [];
        this.routeBranches = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    clear() {
        this.jetkizus = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.jetkizus = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = '_score';
        this.reverse = false;
        this.currentSearch = query;
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInJetkizus();
        this.registerChangeInJetkizuRoutes();
        this.registerImportedRouteBranches();

    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: RouteBranch) {
        return item.id;
    }
    registerChangeInJetkizus() {
        this.eventSubscriber = this.eventManager.subscribe('jetkizuListModification', (response) => this.reset());
    }
    registerChangeInJetkizuRoutes() {
        this.eventSubscriber = this.eventManager.subscribe('jetkizuRouteListModification', (response) => this.reset());
    }
    registerImportedRouteBranches() {
        this.eventSubscriber = this.eventManager.subscribe('importBranchListModification', (response) => this.reset());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    sortRoute() {
        const result = [this.predicateRoute + ',' + (this.reverseRoute ? 'asc' : 'desc')];
        if (this.predicateRoute !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            //this.jetkizus.push(data[i]);
            this.routeBranches.push(data[i]);
        }
    }
    private onSuccessRoute(data, headers) {
        this.linksRoute = this.parseLinks.parse(headers.get('link'));
        this.totalItemsRoute = headers.get('X-Total-Count');
        this.queryCountRoute = this.totalItemsRoute;
        // this.pageRoute = pagingParams.page;
        this.jetkizuRoutes = data;
        for(let i=0; i<this.jetkizuRoutes.length; i++) {
            const r = this.jetkizuRoutes[i];
            r.durationText = this.convertDuration(r.totalPlanDuration);
            r.distanceText = this.convertDistance(r.totalPlanDistance);
            this.routeBranchService.getAllByRoute(r.id, null).subscribe(
                (res: ResponseWrapper) => {
                    r.routeBranches = res.json;
                    r.routeBranches.forEach((branch) => {
                        if(branch.jetkizuActivity) {
                            branch.textPlanDistance = this.convertDistance(branch.jetkizuActivity.planKm);
                            branch.textPlanDuration = this.convertDuration(branch.jetkizuActivity.planDuration);
                        } else {
                            branch.textPlanDistance = this.convertDistance(0);
                            branch.textPlanDuration = this.convertDuration(0);
                        }
                    });

                    if(i === this.jetkizuRoutes.length-1) {
                        Helpers.setLoading(false);
                    }
                },
                (error: ResponseWrapper) => {
                    console.log(error.json);
                    Helpers.setLoading(false);
                }
            );
        }
        /*this.jetkizuRoutes.forEach((r: JetkizuRoute) => {

        });*/
        console.log(this.jetkizuRoutes);
    }

    private onError(error) {
        Helpers.setLoading(false);
        this.jhiAlertService.error(error.message, null, null);
    }

    trackIdRoute(index: number, item: JetkizuRoute) {
        return item.id;
    }

    addToRoute(routeBranch: RouteBranch, id: number) {
        let route = new JetkizuRoute();
        route.id = id;
        routeBranch.jetkizuRoute = route;
        this.routeBranchService.addToRoute(routeBranch).subscribe(
            (res) => {
                const i = this.routeBranches.indexOf(routeBranch);
                console.log('index of jet: '+i);
                this.routeBranches.splice(i, 1);
                this.jetkizuRoutes.forEach((route) => {
                    if(route.id === id) {
                        if(!route.routeBranches) {
                            route.routeBranches = [];
                        }
                        if(route.routeBranches.indexOf(res) === -1) {
                            route.routeBranches.push(res);
                        }
                    }
                });
            },
            (error) => console.log(error)
        );
    }

    showImportModal() {
        console.log('import modal');
        this.modalRef = this.importModalService.open();
    }

    getAddressSuffix(length: number) {
        return this.jetkizuRouteService.getAddressSuffix(length);
    }

    convertDistance(distance: number): string {
        return this.osmService.convertDistanceToKm(distance);
    }

    convertDuration(duration: number): string {
        return this.osmService.convertDurationToHour(duration);
    }
 }
