import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { Jetkizu } from './jetkizu.model';
import { JetkizuService } from './jetkizu.service';

@Injectable()
export class JetkizuPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private jetkizuService: JetkizuService,
    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, routeId?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.jetkizuService.find(id).subscribe((jetkizu) => {
                    jetkizu.createDateTime = this.datePipe
                        .transform(jetkizu.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    jetkizu.updateDateTime = this.datePipe
                        .transform(jetkizu.updateDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.jetkizuModalRef(component, jetkizu);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.jetkizuModalRef(component, new Jetkizu());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    jetkizuModalRef(component: Component, jetkizu: Jetkizu): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', windowClass: 'modal-xxl', backdrop: 'static'});
        modalRef.componentInstance.jetkizu = jetkizu;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
