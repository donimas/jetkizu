import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JetkizuComponent } from './jetkizu.component';
import {JetkizuDetailComponent, JetkizuDetailPopupComponent} from './jetkizu-detail.component';
import {JetkizuDialogComponent, JetkizuPopupComponent} from './jetkizu-dialog.component';
import { JetkizuDeletePopupComponent } from './jetkizu-delete-dialog.component';

@Injectable()
export class JetkizuResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const jetkizuRoute: Routes = [
    {
        path: 'jetkizu',
        component: JetkizuComponent,
        resolve: {
            'pagingParams': JetkizuResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService]
    }/*, {
        path: 'jetkizu-new',
        component: JetkizuDialogComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService]
    },*/
];

export const jetkizuPopupRoute: Routes = [
    {
        path: 'jetkizu-new',
        component: JetkizuPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }, {
        path: 'jetkizu/:id/detail',
        component: JetkizuDetailPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu/:id/edit',
        component: JetkizuPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu/:id/delete',
        component: JetkizuDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
