import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    JetkizuService,
    JetkizuPopupService,
    JetkizuComponent,
    JetkizuDetailComponent,
    JetkizuDialogComponent,
    JetkizuPopupComponent,
    JetkizuDetailPopupComponent,
    JetkizuDeletePopupComponent,
    JetkizuDeleteDialogComponent,
    jetkizuRoute,
    jetkizuPopupRoute,
    JetkizuResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...jetkizuRoute,
    ...jetkizuPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JetkizuComponent,
        JetkizuDetailComponent,
        JetkizuDialogComponent,
        JetkizuDeleteDialogComponent,
        JetkizuPopupComponent,
        JetkizuDetailPopupComponent,
        JetkizuDeletePopupComponent,
    ],
    entryComponents: [
        JetkizuComponent,
        JetkizuDetailComponent,
        JetkizuDialogComponent,
        JetkizuPopupComponent,
        JetkizuDetailPopupComponent,
        JetkizuDeleteDialogComponent,
        JetkizuDeletePopupComponent,
    ],
    providers: [
        JetkizuService,
        JetkizuPopupService,
        JetkizuResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuJetkizuModule {}
