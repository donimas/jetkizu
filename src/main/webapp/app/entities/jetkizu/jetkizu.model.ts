import { BaseEntity } from './../../shared';
import {JetkizuStatusHistory} from "../jetkizu-status-history/jetkizu-status-history.model";
import {Customer} from "../customer/customer.model";
import {Location} from "../location/location.model";
import {RouteBranch} from "../route-branch/route-branch.model";

export class Jetkizu implements BaseEntity {
    constructor(
        public id?: number,
        public weight?: number,
        public createDateTime?: any,
        public updateDateTime?: any,
        public flagDeleted?: boolean,
        public name?: string,
        public description?: string,
        public jetkizuStatusHistory?: JetkizuStatusHistory,
        public location?: Location,
        public customer?: Customer,
        public routeBranch?: RouteBranch,
        public orderPrice?: number,
        public fromTime?: any,
        public tillTime?: any,
        public serviceDuration?: any,
    ) {
        this.flagDeleted = false;
    }
}
