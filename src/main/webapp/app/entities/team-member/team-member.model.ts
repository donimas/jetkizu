import { BaseEntity } from './../../shared';

export class TeamMember implements BaseEntity {
    constructor(
        public id?: number,
        public fio?: string,
        public cellPhone?: string,
        public identifier?: string,
        public team?: BaseEntity,
    ) {
    }
}
