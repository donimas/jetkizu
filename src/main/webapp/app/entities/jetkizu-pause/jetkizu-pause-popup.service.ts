import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { JetkizuPause } from './jetkizu-pause.model';
import { JetkizuPauseService } from './jetkizu-pause.service';

@Injectable()
export class JetkizuPausePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private jetkizuPauseService: JetkizuPauseService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.jetkizuPauseService.find(id).subscribe((jetkizuPause) => {
                    jetkizuPause.createDateTime = this.datePipe
                        .transform(jetkizuPause.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.jetkizuPauseModalRef(component, jetkizuPause);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.jetkizuPauseModalRef(component, new JetkizuPause());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    jetkizuPauseModalRef(component: Component, jetkizuPause: JetkizuPause): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.jetkizuPause = jetkizuPause;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
