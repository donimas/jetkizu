import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuPause } from './jetkizu-pause.model';
import { JetkizuPauseService } from './jetkizu-pause.service';

@Component({
    selector: 'jhi-jetkizu-pause-detail',
    templateUrl: './jetkizu-pause-detail.component.html'
})
export class JetkizuPauseDetailComponent implements OnInit, OnDestroy {

    jetkizuPause: JetkizuPause;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jetkizuPauseService: JetkizuPauseService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJetkizuPauses();
    }

    load(id) {
        this.jetkizuPauseService.find(id).subscribe((jetkizuPause) => {
            this.jetkizuPause = jetkizuPause;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJetkizuPauses() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jetkizuPauseListModification',
            (response) => this.load(this.jetkizuPause.id)
        );
    }
}
