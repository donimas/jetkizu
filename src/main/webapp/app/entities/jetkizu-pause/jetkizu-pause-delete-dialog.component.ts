import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuPause } from './jetkizu-pause.model';
import { JetkizuPausePopupService } from './jetkizu-pause-popup.service';
import { JetkizuPauseService } from './jetkizu-pause.service';

@Component({
    selector: 'jhi-jetkizu-pause-delete-dialog',
    templateUrl: './jetkizu-pause-delete-dialog.component.html'
})
export class JetkizuPauseDeleteDialogComponent {

    jetkizuPause: JetkizuPause;

    constructor(
        private jetkizuPauseService: JetkizuPauseService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jetkizuPauseService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jetkizuPauseListModification',
                content: 'Deleted an jetkizuPause'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-pause-delete-popup',
    template: ''
})
export class JetkizuPauseDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuPausePopupService: JetkizuPausePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuPausePopupService
                .open(JetkizuPauseDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
