import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { JetkizuPause } from './jetkizu-pause.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class JetkizuPauseService {

    private resourceUrl = SERVER_API_URL + 'api/jetkizu-pauses';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/jetkizu-pauses';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(jetkizuPause: JetkizuPause): Observable<JetkizuPause> {
        const copy = this.convert(jetkizuPause);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(jetkizuPause: JetkizuPause): Observable<JetkizuPause> {
        const copy = this.convert(jetkizuPause);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<JetkizuPause> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to JetkizuPause.
     */
    private convertItemFromServer(json: any): JetkizuPause {
        const entity: JetkizuPause = Object.assign(new JetkizuPause(), json);
        entity.createDateTime = this.dateUtils
            .convertDateTimeFromServer(json.createDateTime);
        return entity;
    }

    /**
     * Convert a JetkizuPause to a JSON which can be sent to the server.
     */
    private convert(jetkizuPause: JetkizuPause): JetkizuPause {
        const copy: JetkizuPause = Object.assign({}, jetkizuPause);

        copy.createDateTime = this.dateUtils.toDate(jetkizuPause.createDateTime);
        return copy;
    }
}
