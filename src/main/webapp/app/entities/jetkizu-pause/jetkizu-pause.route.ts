import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JetkizuPauseComponent } from './jetkizu-pause.component';
import { JetkizuPauseDetailComponent } from './jetkizu-pause-detail.component';
import { JetkizuPausePopupComponent } from './jetkizu-pause-dialog.component';
import { JetkizuPauseDeletePopupComponent } from './jetkizu-pause-delete-dialog.component';

@Injectable()
export class JetkizuPauseResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const jetkizuPauseRoute: Routes = [
    {
        path: 'jetkizu-pause',
        component: JetkizuPauseComponent,
        resolve: {
            'pagingParams': JetkizuPauseResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuPause.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'jetkizu-pause/:id',
        component: JetkizuPauseDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuPause.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jetkizuPausePopupRoute: Routes = [
    {
        path: 'jetkizu-pause-new',
        component: JetkizuPausePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuPause.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-pause/:id/edit',
        component: JetkizuPausePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuPause.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-pause/:id/delete',
        component: JetkizuPauseDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuPause.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
