import { BaseEntity } from './../../shared';

export const enum JetkizuPauseType {
    'DELIVER',
    'TECHNICAL_PROBLEM',
    'REST',
    'OTHER'
}

export class JetkizuPause implements BaseEntity {
    constructor(
        public id?: number,
        public duration?: number,
        public type?: JetkizuPauseType,
        public description?: string,
        public createDateTime?: any,
        public flagDeleted?: boolean,
        public jetkizu?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
