import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JetkizuPause } from './jetkizu-pause.model';
import { JetkizuPausePopupService } from './jetkizu-pause-popup.service';
import { JetkizuPauseService } from './jetkizu-pause.service';
import { Jetkizu, JetkizuService } from '../jetkizu';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-jetkizu-pause-dialog',
    templateUrl: './jetkizu-pause-dialog.component.html'
})
export class JetkizuPauseDialogComponent implements OnInit {

    jetkizuPause: JetkizuPause;
    isSaving: boolean;

    jetkizus: Jetkizu[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private jetkizuPauseService: JetkizuPauseService,
        private jetkizuService: JetkizuService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jetkizuService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizus = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.jetkizuPause.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jetkizuPauseService.update(this.jetkizuPause));
        } else {
            this.subscribeToSaveResponse(
                this.jetkizuPauseService.create(this.jetkizuPause));
        }
    }

    private subscribeToSaveResponse(result: Observable<JetkizuPause>) {
        result.subscribe((res: JetkizuPause) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: JetkizuPause) {
        this.eventManager.broadcast({ name: 'jetkizuPauseListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJetkizuById(index: number, item: Jetkizu) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-jetkizu-pause-popup',
    template: ''
})
export class JetkizuPausePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuPausePopupService: JetkizuPausePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jetkizuPausePopupService
                    .open(JetkizuPauseDialogComponent as Component, params['id']);
            } else {
                this.jetkizuPausePopupService
                    .open(JetkizuPauseDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
