import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    JetkizuPauseService,
    JetkizuPausePopupService,
    JetkizuPauseComponent,
    JetkizuPauseDetailComponent,
    JetkizuPauseDialogComponent,
    JetkizuPausePopupComponent,
    JetkizuPauseDeletePopupComponent,
    JetkizuPauseDeleteDialogComponent,
    jetkizuPauseRoute,
    jetkizuPausePopupRoute,
    JetkizuPauseResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...jetkizuPauseRoute,
    ...jetkizuPausePopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JetkizuPauseComponent,
        JetkizuPauseDetailComponent,
        JetkizuPauseDialogComponent,
        JetkizuPauseDeleteDialogComponent,
        JetkizuPausePopupComponent,
        JetkizuPauseDeletePopupComponent,
    ],
    entryComponents: [
        JetkizuPauseComponent,
        JetkizuPauseDialogComponent,
        JetkizuPausePopupComponent,
        JetkizuPauseDeleteDialogComponent,
        JetkizuPauseDeletePopupComponent,
    ],
    providers: [
        JetkizuPauseService,
        JetkizuPausePopupService,
        JetkizuPauseResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuJetkizuPauseModule {}
