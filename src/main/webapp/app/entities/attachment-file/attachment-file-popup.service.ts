import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { AttachmentFile } from './attachment-file.model';
import { AttachmentFileService } from './attachment-file.service';

@Injectable()
export class AttachmentFilePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private attachmentFileService: AttachmentFileService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.attachmentFileService.find(id).subscribe((attachmentFile) => {
                    attachmentFile.uploadDateTime = this.datePipe
                        .transform(attachmentFile.uploadDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.attachmentFileModalRef(component, attachmentFile);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.attachmentFileModalRef(component, new AttachmentFile());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    attachmentFileModalRef(component: Component, attachmentFile: AttachmentFile): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.attachmentFile = attachmentFile;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
