import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AttachmentFile } from './attachment-file.model';
import { AttachmentFilePopupService } from './attachment-file-popup.service';
import { AttachmentFileService } from './attachment-file.service';

@Component({
    selector: 'jhi-attachment-file-delete-dialog',
    templateUrl: './attachment-file-delete-dialog.component.html'
})
export class AttachmentFileDeleteDialogComponent {

    attachmentFile: AttachmentFile;

    constructor(
        private attachmentFileService: AttachmentFileService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.attachmentFileService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'attachmentFileListModification',
                content: 'Deleted an attachmentFile'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-attachment-file-delete-popup',
    template: ''
})
export class AttachmentFileDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private attachmentFilePopupService: AttachmentFilePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.attachmentFilePopupService
                .open(AttachmentFileDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
