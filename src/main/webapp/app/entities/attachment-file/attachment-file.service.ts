import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { AttachmentFile } from './attachment-file.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AttachmentFileService {

    private resourceUrl = SERVER_API_URL + 'api/attachment-files';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/attachment-files';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(attachmentFile: AttachmentFile): Observable<AttachmentFile> {
        const copy = this.convert(attachmentFile);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(attachmentFile: AttachmentFile): Observable<AttachmentFile> {
        const copy = this.convert(attachmentFile);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AttachmentFile> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    importCsv(attachmentFile: AttachmentFile): Observable<AttachmentFile> {
        const copy = this.convert(attachmentFile);
        return this.http.post(`${this.resourceUrl}/import-csv`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AttachmentFile.
     */
    private convertItemFromServer(json: any): AttachmentFile {
        const entity: AttachmentFile = Object.assign(new AttachmentFile(), json);
        entity.uploadDateTime = this.dateUtils
            .convertDateTimeFromServer(json.uploadDateTime);
        return entity;
    }

    /**
     * Convert a AttachmentFile to a JSON which can be sent to the server.
     */
    private convert(attachmentFile: AttachmentFile): AttachmentFile {
        const copy: AttachmentFile = Object.assign({}, attachmentFile);

        copy.uploadDateTime = this.dateUtils.toDate(attachmentFile.uploadDateTime);
        return copy;
    }
}
