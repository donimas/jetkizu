import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AttachmentFileComponent } from './attachment-file.component';
import { AttachmentFileDetailComponent } from './attachment-file-detail.component';
import { AttachmentFilePopupComponent } from './attachment-file-dialog.component';
import { AttachmentFileDeletePopupComponent } from './attachment-file-delete-dialog.component';

@Injectable()
export class AttachmentFileResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const attachmentFileRoute: Routes = [
    {
        path: 'attachment-file',
        component: AttachmentFileComponent,
        resolve: {
            'pagingParams': AttachmentFileResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'attachment-file/:id',
        component: AttachmentFileDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const attachmentFilePopupRoute: Routes = [
    {
        path: 'attachment-file-new',
        component: AttachmentFilePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'attachment-file/:id/edit',
        component: AttachmentFilePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'attachment-file/:id/delete',
        component: AttachmentFileDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.attachmentFile.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
