export * from './attachment-file.model';
export * from './attachment-file-popup.service';
export * from './attachment-file.service';
export * from './attachment-file-dialog.component';
export * from './attachment-file-delete-dialog.component';
export * from './attachment-file-detail.component';
export * from './attachment-file.component';
export * from './attachment-file.route';
