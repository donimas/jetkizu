import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    AttachmentFileService,
    AttachmentFilePopupService,
    AttachmentFileComponent,
    AttachmentFileDetailComponent,
    AttachmentFileDialogComponent,
    AttachmentFilePopupComponent,
    AttachmentFileDeletePopupComponent,
    AttachmentFileDeleteDialogComponent,
    attachmentFileRoute,
    attachmentFilePopupRoute,
    AttachmentFileResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...attachmentFileRoute,
    ...attachmentFilePopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AttachmentFileComponent,
        AttachmentFileDetailComponent,
        AttachmentFileDialogComponent,
        AttachmentFileDeleteDialogComponent,
        AttachmentFilePopupComponent,
        AttachmentFileDeletePopupComponent,
    ],
    entryComponents: [
        AttachmentFileComponent,
        AttachmentFileDialogComponent,
        AttachmentFilePopupComponent,
        AttachmentFileDeleteDialogComponent,
        AttachmentFileDeletePopupComponent,
    ],
    providers: [
        AttachmentFileService,
        AttachmentFilePopupService,
        AttachmentFileResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuAttachmentFileModule {}
