import { BaseEntity } from './../../shared';

export class AttachmentFile implements BaseEntity {
    constructor(
        public id?: number,
        public uid?: string,
        public fileName?: string,
        public contentType?: string,
        public fileSize?: number,
        public fileDesc?: string,
        public fileHash?: string,
        public containerClass?: string,
        public containerId?: string,
        public documentCategory?: string,
        public uploadDateTime?: any,
        public fileContentType?: string,
        public file?: any,
        public imageFileContentType?: string,
        public imageFile?: any,
    ) {
    }
}
