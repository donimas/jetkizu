import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JetkizuJetkizuModule } from './jetkizu/jetkizu.module';
import { JetkizuJetkizuStatusHistoryModule } from './jetkizu-status-history/jetkizu-status-history.module';
import { JetkizuJetkizuPauseModule } from './jetkizu-pause/jetkizu-pause.module';
import { JetkizuTeamModule } from './team/team.module';
import { JetkizuTeamMemberModule } from './team-member/team-member.module';
import { JetkizuLocationModule } from './location/location.module';
import { JetkizuCustomerModule } from './customer/customer.module';
import { JetkizuAreaModule } from './area/area.module';
import { JetkizuAreaNodeModule } from './area-node/area-node.module';
import { JetkizuJetkizuRouteModule } from './jetkizu-route/jetkizu-route.module';
import { JetkizuRouteBranchModule } from './route-branch/route-branch.module';
import { JetkizuRouteBranchStatusHistoryModule } from './route-branch-status-history/route-branch-status-history.module';
import { OrderWrapperModule } from './order-wrapper/order-wrapper.module';
import { JetkizuJetkizuActivityModule } from './jetkizu-activity/jetkizu-activity.module';
import { JetkizuReportModule } from './report/report.module';
import { JetkizuVehicleModule } from './vehicle/vehicle.module';
import { JetkizuAttachmentFileModule } from './attachment-file/attachment-file.module';
import { JetkizuCsvFieldModule } from './csv-field/csv-field.module';
import { JetkizuRouteOptimizationModule } from './route-optimization/route-optimization.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        JetkizuJetkizuModule,
        JetkizuJetkizuStatusHistoryModule,
        JetkizuJetkizuPauseModule,
        JetkizuTeamModule,
        JetkizuTeamMemberModule,
        JetkizuLocationModule,
        JetkizuCustomerModule,
        JetkizuAreaModule,
        JetkizuAreaNodeModule,
        JetkizuJetkizuRouteModule,
        JetkizuRouteBranchModule,
        JetkizuRouteBranchStatusHistoryModule,
        OrderWrapperModule,
        JetkizuJetkizuActivityModule,
        JetkizuReportModule,
        JetkizuVehicleModule,
        JetkizuAttachmentFileModule,
        JetkizuCsvFieldModule,
        JetkizuRouteOptimizationModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuEntityModule {}
