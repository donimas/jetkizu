import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Location } from './location.model';
import { LocationPopupService } from './location-popup.service';
import { LocationService } from './location.service';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';
import {KazpostService} from '../../shared/open-api-services/kazpost.service';
import {YandexService} from '../../shared/open-api-services/yandex.service';
import {environment} from '../../shared/index';

const apiToken = environment.MAPBOX_API_KEY;
const mapboxgl = require('mapbox-gl');
const MapboxDirections = require('@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions.js');

@Component({
    selector: 'jhi-location-dialog',
    templateUrl: './location-dialog.component.html'
})
export class LocationDialogComponent implements OnInit, AfterViewInit {

    location: Location;
    isSaving: boolean;
    modelKazpost: any;
    modelYandex: any;
    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribedKazpost = new Observable(() => () => this.searching = false);
    hideSearchingWhenUnsubscribedYandex = new Observable(() => () => this.searching = false);

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private locationService: LocationService,
        private eventManager: JhiEventManager,
        private _serviceKazpost: KazpostService,
        private _serviceYandex: YandexService,
    ) {
    }

    initDirectionsMap() {
        mapboxgl.accessToken = apiToken;
        const map = new mapboxgl.Map({
            container: 'map', // container id
            style: 'mapbox://styles/mapbox/streets-v9', // stylesheet location
            center: [-74.50, 40], // starting position [lng, lat]
            zoom: 9 // starting zoom
        });
        const directions = new MapboxDirections({
            accessToken: apiToken,
            unit: 'metric',
            profile: 'mapbox/cycling'
        });

        map.addControl(directions, 'top-left');
    }

    ngOnInit() {
        this.isSaving = false;
    }

    ngAfterViewInit() {
        this.initDirectionsMap();
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.location.id !== undefined) {
            this.subscribeToSaveResponse(
                this.locationService.update(this.location));
        } else {
            this.subscribeToSaveResponse(
                this.locationService.create(this.location));
        }
    }

    private subscribeToSaveResponse(result: Observable<Location>) {
        result.subscribe((res: Location) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Location) {
        this.eventManager.broadcast({ name: 'locationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    searchKazpost = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceKazpost.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedKazpost);

    formatterKazpost = (x: {addressRus: string}) => x.addressRus;

    searchYandex = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceYandex.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedYandex);

    formatterYandex = (x: {metaDataProperty: {GeocoderMetaData: {text: string}}}) => x.metaDataProperty.GeocoderMetaData.text;

    public selectedYandexItem(event) {
        console.log('coordinate');
        console.log(event.item.Point.pos);
    }
}

@Component({
    selector: 'jhi-location-popup',
    template: ''
})
export class LocationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private locationPopupService: LocationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.locationPopupService
                    .open(LocationDialogComponent as Component, params['id']);
            } else {
                this.locationPopupService
                    .open(LocationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
