import { BaseEntity } from './../../shared';

export class Location implements BaseEntity {
    constructor(
        public id?: number,
        public street?: string,
        public home?: string,
        public flat?: string,
        public postindex?: string,
        public lat?: string,
        public lon?: string,
        public fullAddress?: string,
        public flagGarage?: boolean,
        public placeId?: number,
        public osmId?: number,
        public osmType?: string,
        public city?: string,
        public flagOsmNotFound?: boolean
    ) {
        flagOsmNotFound = false;
    }
}
