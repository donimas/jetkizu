import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuActivity } from './jetkizu-activity.model';
import { JetkizuActivityService } from './jetkizu-activity.service';

@Component({
    selector: 'jhi-jetkizu-activity-detail',
    templateUrl: './jetkizu-activity-detail.component.html'
})
export class JetkizuActivityDetailComponent implements OnInit, OnDestroy {

    jetkizuActivity: JetkizuActivity;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jetkizuActivityService: JetkizuActivityService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJetkizuActivities();
    }

    load(id) {
        this.jetkizuActivityService.find(id).subscribe((jetkizuActivity) => {
            this.jetkizuActivity = jetkizuActivity;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJetkizuActivities() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jetkizuActivityListModification',
            (response) => this.load(this.jetkizuActivity.id)
        );
    }
}
