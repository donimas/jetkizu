import { BaseEntity } from './../../shared';
import {RouteBranch} from '../route-branch/route-branch.model';

export class JetkizuActivity implements BaseEntity {
    constructor(
        public id?: number,
        public planKm?: number,
        public factKm?: number,
        public planDuration?: number,
        public factDuration?: number,
        public planDistanceToNext?: number,
        public planDurationToNext?: number,
        public factDistanceToNext?: number,
        public factDurationToNext?: number,
        public createDateTime?: any,
        public fromLat?: string,
        public fromLon?: string,
        public routeBranch?: RouteBranch,
        public jetkizuPause?: BaseEntity,
    ) {
    }
}
