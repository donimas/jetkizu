import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { JetkizuActivity } from './jetkizu-activity.model';
import { JetkizuActivityService } from './jetkizu-activity.service';

@Injectable()
export class JetkizuActivityPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private jetkizuActivityService: JetkizuActivityService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.jetkizuActivityService.find(id).subscribe((jetkizuActivity) => {
                    jetkizuActivity.createDateTime = this.datePipe
                        .transform(jetkizuActivity.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.jetkizuActivityModalRef(component, jetkizuActivity);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.jetkizuActivityModalRef(component, new JetkizuActivity());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    jetkizuActivityModalRef(component: Component, jetkizuActivity: JetkizuActivity): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.jetkizuActivity = jetkizuActivity;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
