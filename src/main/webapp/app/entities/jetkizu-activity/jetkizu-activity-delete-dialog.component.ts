import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuActivity } from './jetkizu-activity.model';
import { JetkizuActivityPopupService } from './jetkizu-activity-popup.service';
import { JetkizuActivityService } from './jetkizu-activity.service';

@Component({
    selector: 'jhi-jetkizu-activity-delete-dialog',
    templateUrl: './jetkizu-activity-delete-dialog.component.html'
})
export class JetkizuActivityDeleteDialogComponent {

    jetkizuActivity: JetkizuActivity;

    constructor(
        private jetkizuActivityService: JetkizuActivityService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jetkizuActivityService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jetkizuActivityListModification',
                content: 'Deleted an jetkizuActivity'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-activity-delete-popup',
    template: ''
})
export class JetkizuActivityDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuActivityPopupService: JetkizuActivityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuActivityPopupService
                .open(JetkizuActivityDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
