import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JetkizuActivityComponent } from './jetkizu-activity.component';
import { JetkizuActivityDetailComponent } from './jetkizu-activity-detail.component';
import { JetkizuActivityPopupComponent } from './jetkizu-activity-dialog.component';
import { JetkizuActivityDeletePopupComponent } from './jetkizu-activity-delete-dialog.component';

export const jetkizuActivityRoute: Routes = [
    {
        path: 'jetkizu-activity',
        component: JetkizuActivityComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuActivity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'jetkizu-activity/:id',
        component: JetkizuActivityDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuActivity.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jetkizuActivityPopupRoute: Routes = [
    {
        path: 'jetkizu-activity-new',
        component: JetkizuActivityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuActivity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-activity/:id/edit',
        component: JetkizuActivityPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuActivity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-activity/:id/delete',
        component: JetkizuActivityDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuActivity.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
