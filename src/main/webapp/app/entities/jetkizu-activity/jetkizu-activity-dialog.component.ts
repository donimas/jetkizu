import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JetkizuActivity } from './jetkizu-activity.model';
import { JetkizuActivityPopupService } from './jetkizu-activity-popup.service';
import { JetkizuActivityService } from './jetkizu-activity.service';
import { RouteBranch, RouteBranchService } from '../route-branch';
import { JetkizuPause, JetkizuPauseService } from '../jetkizu-pause';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-jetkizu-activity-dialog',
    templateUrl: './jetkizu-activity-dialog.component.html'
})
export class JetkizuActivityDialogComponent implements OnInit {

    jetkizuActivity: JetkizuActivity;
    isSaving: boolean;

    routebranches: RouteBranch[];

    jetkizupauses: JetkizuPause[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private jetkizuActivityService: JetkizuActivityService,
        private routeBranchService: RouteBranchService,
        private jetkizuPauseService: JetkizuPauseService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.routeBranchService.query()
            .subscribe((res: ResponseWrapper) => { this.routebranches = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.jetkizuPauseService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizupauses = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.jetkizuActivity.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jetkizuActivityService.update(this.jetkizuActivity));
        } else {
            this.subscribeToSaveResponse(
                this.jetkizuActivityService.create(this.jetkizuActivity));
        }
    }

    private subscribeToSaveResponse(result: Observable<JetkizuActivity>) {
        result.subscribe((res: JetkizuActivity) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: JetkizuActivity) {
        this.eventManager.broadcast({ name: 'jetkizuActivityListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackRouteBranchById(index: number, item: RouteBranch) {
        return item.id;
    }

    trackJetkizuPauseById(index: number, item: JetkizuPause) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-jetkizu-activity-popup',
    template: ''
})
export class JetkizuActivityPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuActivityPopupService: JetkizuActivityPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jetkizuActivityPopupService
                    .open(JetkizuActivityDialogComponent as Component, params['id']);
            } else {
                this.jetkizuActivityPopupService
                    .open(JetkizuActivityDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
