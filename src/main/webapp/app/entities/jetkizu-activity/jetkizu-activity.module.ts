import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    JetkizuActivityService,
    JetkizuActivityPopupService,
    JetkizuActivityComponent,
    JetkizuActivityDetailComponent,
    JetkizuActivityDialogComponent,
    JetkizuActivityPopupComponent,
    JetkizuActivityDeletePopupComponent,
    JetkizuActivityDeleteDialogComponent,
    jetkizuActivityRoute,
    jetkizuActivityPopupRoute,
} from './';

const ENTITY_STATES = [
    ...jetkizuActivityRoute,
    ...jetkizuActivityPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JetkizuActivityComponent,
        JetkizuActivityDetailComponent,
        JetkizuActivityDialogComponent,
        JetkizuActivityDeleteDialogComponent,
        JetkizuActivityPopupComponent,
        JetkizuActivityDeletePopupComponent,
    ],
    entryComponents: [
        JetkizuActivityComponent,
        JetkizuActivityDialogComponent,
        JetkizuActivityPopupComponent,
        JetkizuActivityDeleteDialogComponent,
        JetkizuActivityDeletePopupComponent,
    ],
    providers: [
        JetkizuActivityService,
        JetkizuActivityPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuJetkizuActivityModule {}
