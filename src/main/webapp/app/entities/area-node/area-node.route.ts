import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AreaNodeComponent } from './area-node.component';
import { AreaNodeDetailComponent } from './area-node-detail.component';
import { AreaNodePopupComponent } from './area-node-dialog.component';
import { AreaNodeDeletePopupComponent } from './area-node-delete-dialog.component';

export const areaNodeRoute: Routes = [
    {
        path: 'area-node',
        component: AreaNodeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.areaNode.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'area-node/:id',
        component: AreaNodeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.areaNode.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const areaNodePopupRoute: Routes = [
    {
        path: 'area-node-new/:areaId',
        component: AreaNodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.areaNode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'area-node/:id/:areaId/edit',
        component: AreaNodePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.areaNode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'area-node/:id/delete',
        component: AreaNodeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.areaNode.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
