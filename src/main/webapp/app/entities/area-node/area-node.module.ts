import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    AreaNodeService,
    AreaNodePopupService,
    AreaNodeComponent,
    AreaNodeDetailComponent,
    AreaNodeDialogComponent,
    AreaNodePopupComponent,
    AreaNodeDeletePopupComponent,
    AreaNodeDeleteDialogComponent,
    areaNodeRoute,
    areaNodePopupRoute,
} from './';

const ENTITY_STATES = [
    ...areaNodeRoute,
    ...areaNodePopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        AreaNodeComponent,
        AreaNodeDetailComponent,
        AreaNodeDialogComponent,
        AreaNodeDeleteDialogComponent,
        AreaNodePopupComponent,
        AreaNodeDeletePopupComponent,
    ],
    entryComponents: [
        AreaNodeComponent,
        AreaNodeDialogComponent,
        AreaNodePopupComponent,
        AreaNodeDeleteDialogComponent,
        AreaNodeDeletePopupComponent,
    ],
    providers: [
        AreaNodeService,
        AreaNodePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuAreaNodeModule {}
