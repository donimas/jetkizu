import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AreaNode } from './area-node.model';
import { AreaNodeService } from './area-node.service';

@Component({
    selector: 'jhi-area-node-detail',
    templateUrl: './area-node-detail.component.html'
})
export class AreaNodeDetailComponent implements OnInit, OnDestroy {

    areaNode: AreaNode;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private areaNodeService: AreaNodeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAreaNodes();
    }

    load(id) {
        this.areaNodeService.find(id).subscribe((areaNode) => {
            this.areaNode = areaNode;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAreaNodes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'areaNodeListModification',
            (response) => this.load(this.areaNode.id)
        );
    }
}
