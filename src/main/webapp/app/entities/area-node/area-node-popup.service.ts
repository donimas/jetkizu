import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AreaNode } from './area-node.model';
import { AreaNodeService } from './area-node.service';
import {AreaService} from '../area/area.service';
import {Area} from '../area/area.model';

@Injectable()
export class AreaNodePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private areaNodeService: AreaNodeService,
        private areaService: AreaService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, areaId?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.areaNodeService.find(id).subscribe((areaNode) => {
                    this.ngbModalRef = this.areaNodeModalRef(component, areaNode, areaId);
                    resolve(this.ngbModalRef);
                });
            } else if (areaId) {
                setTimeout(() => {
                    this.ngbModalRef = this.areaNodeModalRef(component, new AreaNode(), areaId);
                    resolve(this.ngbModalRef);
                }, 0);
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.areaNodeModalRef(component, new AreaNode(), null);
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    areaNodeModalRef(component: Component, areaNode: AreaNode, areaId: number): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.areaNode = areaNode;
        modalRef.componentInstance.areaId = areaId;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
