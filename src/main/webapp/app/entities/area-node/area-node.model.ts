import { BaseEntity } from './../../shared';
import {Team} from '../team/team.model';
import {Area} from '../area/area.model';

export class AreaNode implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public polygon?: string[][],
        public area?: Area,
        public team?: Team,
        public selected?: boolean,
    ) {
    }
}
