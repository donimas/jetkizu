import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AreaNode } from './area-node.model';
import { AreaNodePopupService } from './area-node-popup.service';
import { AreaNodeService } from './area-node.service';

@Component({
    selector: 'jhi-area-node-delete-dialog',
    templateUrl: './area-node-delete-dialog.component.html'
})
export class AreaNodeDeleteDialogComponent {

    areaNode: AreaNode;

    constructor(
        private areaNodeService: AreaNodeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.areaNodeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'areaNodeListModification',
                content: 'Deleted an areaNode'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-area-node-delete-popup',
    template: ''
})
export class AreaNodeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private areaNodePopupService: AreaNodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.areaNodePopupService
                .open(AreaNodeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
