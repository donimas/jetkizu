import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Http, Response} from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AreaNode } from './area-node.model';
import { AreaNodePopupService } from './area-node-popup.service';
import { AreaNodeService } from './area-node.service';
import { Area, AreaService } from '../area';
import { ResponseWrapper } from '../../shared';
import {MapService} from '../../shared/map/map.service';
import {TeamService} from '../team/team.service';
import {Team} from '../team/team.model';

@Component({
    selector: 'jhi-area-node-dialog',
    templateUrl: './area-node-dialog.component.html'
})
export class AreaNodeDialogComponent implements OnInit, AfterViewInit {

    areaNode: AreaNode;
    areaId: number;
    area: Area;
    teams: Team[];
    isSaving: boolean;
    map: any;
    draw: any;
    areaNodes: AreaNode[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private areaNodeService: AreaNodeService,
        private areaService: AreaService,
        private eventManager: JhiEventManager,
        private mapService: MapService,
        private teamService: TeamService,
        private http: Http
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.teamService.query(null)
            .subscribe((res: ResponseWrapper) => { this.teams = res.json; },
                (res: ResponseWrapper) => this.onError(res.json));

        this.areaService.find(this.areaId).subscribe((area) => {
            this.area = area;
        }, (res: ResponseWrapper) => this.onError(res.json));
    }

    ngAfterViewInit() {
        this.map = this.mapService.showMapboxglMap('map', 12);
        this.draw = this.mapService.showDrawTool(this.map);

        this.map.on('click', (event) => this.onMapClick(event));

        this.map.on('draw.create', () => this.updateArea());
        this.map.on('draw.delete', () => this.updateArea());
        this.map.on('draw.update', () => this.updateArea());

        this.map.on('load', () => {
            this.areaNodeService.findByAreaId(this.areaId, null)
                .subscribe(
                    (res: ResponseWrapper) => {
                        this.areaNodes = res.json;

                        let featureCollection = [];
                        this.areaNodes
                            .filter(areaNode => areaNode.polygon != null && areaNode.polygon.length > 1)
                            .forEach(areaNode => {
                                featureCollection.push(
                                    this.mapService.generateFeature([areaNode.polygon], 'Polygon', areaNode.name, areaNode.id)
                                );
                            });
                        const data = this.mapService.generateFeatureCollection(featureCollection);
                        this.mapService.createRoute(this.map, 'areaNode', 'geojson', data);
                        this.mapService.setPaintProperty(this.map, 'areaNode', 'areaNodeFill', 'fill');

                        if(this.areaNode && this.areaNode.id) {
                            console.log('selecting polygon');
                            console.log(this.areaNode.id);
                            this.map.setFilter("area-fills-selected", ["==", "id", this.areaNode.id]);
                        }
                    }, (res: ResponseWrapper) => this.onError(res.json));
        });
    }

    onMapClick(e) {
        console.log(e);
        console.log('branch map clicked');
        const lat = e.lngLat.lat;
        const lon = e.lngLat.lng;
        console.log(lat + ', '+lon);
        const headers = new Headers();
        headers.append('Access-Control-Allow-Headers', 'Content-Type');
        headers.append('Access-Control-Allow-Methods', 'GET');
        headers.append('Access-Control-Allow-Origin', '*');
        const query = `https://nominatim.openstreetmap.org/search?q=Астана Бокейхана 11&format=xml&polygon=1&addressdetails=1`;

        // http://overpass-api.de/api/interpreter?data=(node["building"="yes"](-6.185440796831979,106.82374835014343,-6.178966266481431,106.83127999305725);way["building"="yes"](-6.185440796831979,106.82374835014343,-6.178966266481431,106.83127999305725);relation["building"="yes"](-6.185440796831979,106.82374835014343,-6.178966266481431,106.83127999305725););(._;>;);out body;
        this.requestToMapbox(query);
    }

    requestToMapbox(query) {
        this.http.get(query).map((res:Response) => {
            return res;
        }).subscribe((res) => console.log(res), (error) => console.log(error));
    }

    updateArea() {
        const data = this.draw.getAll();
        console.log(data);
        /*POLYGON((71.49557700389127 51.13138117995763, 71.51269549809791 51.13623169784492, 71.51438744229185 51.13381691142635,
        71.49723577271493 51.12898695972183, 71.49557700389127 51.13138117995763))*/
        const polygon = data.features[0].geometry.coordinates[0];
        this.areaNode.polygon = polygon;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if(this.area) {
            this.areaNode.area = this.area;
        }
        console.log(this.areaNode);
        if (this.areaNode.id !== undefined) {
            this.subscribeToSaveResponse(
                this.areaNodeService.update(this.areaNode));
        } else {
            this.subscribeToSaveResponse(
                this.areaNodeService.create(this.areaNode));
        }
    }

    private subscribeToSaveResponse(result: Observable<AreaNode>) {
        result.subscribe((res: AreaNode) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AreaNode) {
        this.eventManager.broadcast({ name: 'areaNodeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackAreaById(index: number, item: Area) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-area-node-popup',
    template: ''
})
export class AreaNodePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private areaNodePopupService: AreaNodePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.areaNodePopupService
                    .open(AreaNodeDialogComponent as Component, params['id'], params['areaId']);
            } else if ( params['areaId'] ) {
                this.areaNodePopupService
                    .open(AreaNodeDialogComponent as Component, null, params['areaId']);
            } else {
                this.areaNodePopupService
                    .open(AreaNodeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
