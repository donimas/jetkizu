import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { AreaNode } from './area-node.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import {MapService} from '../../shared/map/map.service';

@Injectable()
export class AreaNodeService {

    private resourceUrl = SERVER_API_URL + 'api/area-nodes';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/area-nodes';

    constructor(private http: Http,
                private mapService: MapService) { }

    create(areaNode: AreaNode): Observable<AreaNode> {
        const copy = this.convert(areaNode);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(areaNode: AreaNode): Observable<AreaNode> {
        const copy = this.convert(areaNode);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AreaNode> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    drawAreaPolygons(areaNodes: AreaNode[], map: any) {
        let featureCollection = [];
        areaNodes
            .filter(areaNode => areaNode.polygon != null && areaNode.polygon.length > 1)
            .forEach(areaNode => {
                featureCollection.push(
                    this.mapService.generateFeature([areaNode.polygon], 'Polygon', areaNode.name, areaNode.id)
                );
            });
        const data = this.mapService.generateFeatureCollection(featureCollection);
        this.mapService.createRoute(map, 'area', 'geojson', data);
        this.mapService.setPaintProperty(map, 'area', 'areaFill', 'fill');
        areaNodes
            .forEach(areaNode => {
                this.mapService.addSymbol(map, areaNode.id+'', 'area', areaNode.name, 'id', areaNode.id);
            });
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AreaNode.
     */
    private convertItemFromServer(json: any): AreaNode {
        const entity: AreaNode = Object.assign(new AreaNode(), json);
        return entity;
    }

    /**
     * Convert a AreaNode to a JSON which can be sent to the server.
     */
    private convert(areaNode: AreaNode): AreaNode {
        const copy: AreaNode = Object.assign({}, areaNode);
        return copy;
    }

    findByAreaId(id: number, req?: any): Observable<ResponseWrapper> {
        return this.http.get(`${this.resourceUrl}/by-area/${id}`, req)
            .map((res: Response) => this.convertResponse(res));
    }
}
