import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';
import {OrderWrapperComponent} from './order-wrapper.component';
import {JetkizuComponent} from '../jetkizu/jetkizu.component';
import {TeamComponent} from '../team/team.component';
import {JetkizuRouteComponent} from '../jetkizu-route/jetkizu-route.component';
import {JetkizuRouteResolvePagingParams} from '../jetkizu-route/jetkizu-route.route';
import {VehicleComponent} from '../vehicle/vehicle.component';

@Injectable()
export class OrderResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

@Injectable()
export class JetkizuResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
}

export const orderWrapperRoute: Routes = [
    {
        path: 'order-wrapper',
        component: OrderWrapperComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizu.home.title'
        },
        canActivate: [UserRouteAccessService],
        children: [
            {
                path: '',
                redirectTo: 'jetkizu-old',
                pathMatch: 'full'
            },
            {
                path: 'jetkizu-old',
                component: JetkizuComponent,
                resolve: {
                    'pagingParams': JetkizuResolvePagingParams
                },
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jetkizuApp.jetkizu.home.title'
                }
            },
            {
                path: 'jetkizu-route-old',
                component: JetkizuRouteComponent,
                resolve: {
                    'pagingParams': OrderResolvePagingParams
                },
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
                }
            },
            {
                path: 'team-old',
                component: TeamComponent,
                resolve: {
                    'pagingParams': OrderResolvePagingParams
                },
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jetkizuApp.team.home.title'
                }
            },
            {
                path: 'vehicle-old',
                component: VehicleComponent,
                resolve: {
                    'pagingParams': OrderResolvePagingParams
                },
                data: {
                    authorities: ['ROLE_USER'],
                    pageTitle: 'jetkizuApp.vehicle.home.title'
                }
            }
        ]
    }
];
