import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import {
    //OrderWrapperComponent,
    //orderWrapperRoute,
    //OrderResolvePagingParams,
    //JetkizuResolvePagingParams
} from './';

const ENTITY_STATES = [
    //...orderWrapperRoute
];

@NgModule({
    imports: [
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        //OrderWrapperComponent
    ],
    entryComponents: [
    ],
    providers: [
        //OrderResolvePagingParams,
        //JetkizuResolvePagingParams
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class OrderWrapperModule {}
