import { BaseEntity } from './../../shared';

export const enum VehicleType {
    'FURA',
    ' GAZEL',
    ' LARGUS',
    ' SEDAN'
}

export class Vehicle implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public govNumber?: string,
        public techPassportNumber?: string,
        public model?: string,
        public buildYear?: number,
        public type?: VehicleType,
        public height?: number,
        public length?: number,
        public width?: number,
        public totalWeight?: number,
        public deliveryWeight?: number,
        public note?: string,
    ) {
    }
}
