import { BaseEntity } from './../../shared';

export const enum ReportType {
    'FULL',
    ' PART',
    ' EMPTY'
}

export class Report implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public type?: ReportType,
    ) {
    }
}
