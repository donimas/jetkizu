import { BaseEntity, User } from './../../shared';
import {RouteBranch} from '../route-branch/route-branch.model';
import {Team} from '../team/team.model';
import {AreaNode} from '../area-node/area-node.model';
import {Location} from '../location/location.model';
import {RouteOptimization} from '../route-optimization/route-optimization.model';

export const enum JetkizuRouteStatus {
    'NOT_STARTED',
    'STARTED',
    'STOPPED'
}

export const enum TripType {
    'ROUND_TRIP', 'ONE_TRIP', 'AT_ANY_STOP', 'LAST_STOP'
}

export class JetkizuRoute implements BaseEntity {
    constructor(
        public id?: number,
        public jetkizuDate?: any,
        public jetkizuStartTime?: any,
        public startTimeHM?: any,
        public startedDateTime?: any,
        public stoppedDateTime?: any,
        public createDateTime?: any,
        public status?: JetkizuRouteStatus,
        public team?: Team,
        public startedBy?: User,
        public stoppedBy?: User,
        public routeBranches?: RouteBranch[],
        public name?: string,
        public totalPlanDistance?: number,
        public totalPlanDuration?: number,
        public totalFactDuration?: number,
        public totalFactDistance?: number,
        public serviceDuration?: number,
        public serviceDurationHM?: string,
        public plannedTotalDistance?: any,
        public areaNode?: AreaNode,
        public areaNodes?: AreaNode[],
        public tripType?: TripType,
        public start?: Location,
        public end?: Location,
        public routeOptimization?: RouteOptimization,
        public flagWithoutPolygons?: boolean,
        public addressesAmount?: number,
        public distanceText?: any,
        public durationText?: any,
    ) {
        flagWithoutPolygons = false;
    }
}
