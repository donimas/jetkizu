import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { JetkizuRoute } from './jetkizu-route.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import {JetkizuRouteContainerDto} from "./jetkizu-route-container.model";

@Injectable()
export class JetkizuRouteService {

    private resourceUrl = SERVER_API_URL + 'api/jetkizu-routes';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/jetkizu-routes';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(jetkizuRoute: JetkizuRoute): Observable<JetkizuRoute> {
        const copy = this.convert(jetkizuRoute);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    startOptimizedRoute(jetkizuRouteContainerDto: JetkizuRouteContainerDto): Observable<number[]> {
        const copy = this.convert(jetkizuRouteContainerDto.jetkizuRoute);
        jetkizuRouteContainerDto.jetkizuRoute = copy;
        console.log(jetkizuRouteContainerDto);
        return this.http.post(`${this.resourceUrl}/start-optimized`, jetkizuRouteContainerDto).map((res: Response) => {
            const jsonResponse = res.json();
            console.log('result from server');
            console.log(jsonResponse);
            //return this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(jetkizuRoute: JetkizuRoute): Observable<JetkizuRoute> {
        const copy = this.convert(jetkizuRoute);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    updateWithNewElements(jetkizuRoute: JetkizuRoute): Observable<JetkizuRoute> {
        const copy = this.convert(jetkizuRoute);
        return this.http.put(`${this.resourceUrl}/with-new-elements`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<JetkizuRoute> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    findAllNotStopped(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/not-stopped`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    findAllStarted(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/all-started`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getOptimizedAddresses(jetkizuRoute: JetkizuRoute): Observable<ResponseWrapper> {
        const copy = this.convert(jetkizuRoute);
        return this.http.post(`${this.resourceUrl}/addresses`, copy)
            .map((res: Response) => this.convertResponse(res));
    }

    getOptimizedRoutes(jetkizuRouteContainerDto: JetkizuRouteContainerDto): Observable<ResponseWrapper> {
        return this.http.post(`${this.resourceUrl}/optimized-routes`, jetkizuRouteContainerDto)
            .map((res: Response) => this.convertContainer(res));
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    importBranches(jetkizuRoute: JetkizuRoute): Observable<Response> {
        return this.http.put(`${this.resourceUrl}/import-branches`, jetkizuRoute);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to JetkizuRoute.
     */
    private convertItemFromServer(json: any): JetkizuRoute {
        const entity: JetkizuRoute = Object.assign(new JetkizuRoute(), json);
        entity.jetkizuDate = this.dateUtils
            .convertLocalDateFromServer(json.jetkizuDate);
        entity.startedDateTime = this.dateUtils
            .convertDateTimeFromServer(json.startedDateTime);
        entity.stoppedDateTime = this.dateUtils
            .convertDateTimeFromServer(json.stoppedDateTime);
        entity.createDateTime = this.dateUtils
            .convertDateTimeFromServer(json.createDateTime);
        return entity;
    }

    private convertContainer(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        result.push(jsonResponse);
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a JetkizuRoute to a JSON which can be sent to the server.
     */
    private convert(jetkizuRoute: JetkizuRoute): JetkizuRoute {
        const copy: JetkizuRoute = Object.assign({}, jetkizuRoute);
        copy.jetkizuDate = this.dateUtils
            .convertLocalDateToServer(jetkizuRoute.jetkizuDate);

        copy.startedDateTime = this.dateUtils.toDate(jetkizuRoute.startedDateTime);

        copy.stoppedDateTime = this.dateUtils.toDate(jetkizuRoute.stoppedDateTime);

        copy.createDateTime = this.dateUtils.toDate(jetkizuRoute.createDateTime);
        return copy;
    }

    getAddressSuffix(length: number) {
        let result = '';
        switch (length) {
            case 0: result = ''; break;
            case 1: result = 'адрес'; break;
            case 2:
            case 3:
            case 4: result = 'адреса'; break;
            default: result = 'адресов';
        }

        return result;
    }
}
