import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import { JetkizuAdminModule } from '../../admin/admin.module';
import {
    JetkizuRouteService,
    JetkizuRoutePopupService,
    JetkizuRouteAddItemPopupService,
    JetkizuRoutePopupDetailService,
    JetkizuRouteComponent,
    JetkizuRouteDetailComponent,
    JetkizuRouteDialogComponent,
    JetkizuRouteStartDialogComponent,
    JetkizuRoutePopupComponent,
    JetkizuRouteAddJetkizuPopupComponent,
    JetkizuRouteAddJetkizuDialogComponent,
    JetkizuRouteStartPopupComponent,
    JetkizuRouteDetailPopupComponent,
    JetkizuRouteDeletePopupComponent,
    JetkizuRouteDeleteDialogComponent,
    jetkizuRouteRoute,
    jetkizuRoutePopupRoute,
    JetkizuRouteResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...jetkizuRouteRoute,
    ...jetkizuRoutePopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        JetkizuAdminModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JetkizuRouteComponent,
        JetkizuRouteDetailComponent,
        JetkizuRouteDialogComponent,
        JetkizuRouteStartDialogComponent,
        JetkizuRouteDeleteDialogComponent,
        JetkizuRoutePopupComponent,
        JetkizuRouteAddJetkizuDialogComponent,
        JetkizuRouteAddJetkizuPopupComponent,
        JetkizuRouteStartPopupComponent,
        JetkizuRouteDetailPopupComponent,
        JetkizuRouteDeletePopupComponent,
    ],
    entryComponents: [
        JetkizuRouteComponent,
        JetkizuRouteDialogComponent,
        JetkizuRouteStartDialogComponent,
        JetkizuRoutePopupComponent,
        JetkizuRouteAddJetkizuDialogComponent,
        JetkizuRouteAddJetkizuPopupComponent,
        JetkizuRouteStartPopupComponent,
        JetkizuRouteDetailPopupComponent,
        JetkizuRouteDeleteDialogComponent,
        JetkizuRouteDeletePopupComponent,
    ],
    providers: [
        JetkizuRouteService,
        JetkizuRoutePopupService,
        JetkizuRouteAddItemPopupService,
        JetkizuRoutePopupDetailService,
        JetkizuRouteResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuJetkizuRouteModule {}
