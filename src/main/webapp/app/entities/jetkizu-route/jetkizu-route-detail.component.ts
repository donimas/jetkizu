import {Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import { JetkizuRoute } from './jetkizu-route.model';
import { JetkizuRouteService } from './jetkizu-route.service';
import {JetkizuRoutePopupDetailService} from './jetkizu-route-popup-detail.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {RouteBranchService} from '../route-branch/route-branch.service';
import {MapService} from '../../shared/map/map.service';
import {RouteBranch, RouteBranchType} from '../route-branch/route-branch.model';
import {
    RouteBranchStatus,
    RouteBranchStatusHistory
} from '../route-branch-status-history/route-branch-status-history.model';
import {DatePipe} from '@angular/common';
import {OsmService} from '../../shared/osm/osm.service';
import {Helpers} from '../../helpers';

@Component({
    selector: 'jhi-jetkizu-route-detail',
    templateUrl: './jetkizu-route-detail.component.html'
})
export class JetkizuRouteDetailComponent implements OnInit, OnDestroy {

    jetkizuRoute: JetkizuRoute;
    routeBranches: RouteBranch[];
    originalRouteBranches: RouteBranch[];
    pickingBranch: RouteBranch;
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    map: any;
    backlogBranches: RouteBranch[] = [];
    isSaving: boolean;
    orderStep: number = 0.1;

    constructor(
        private datePipe: DatePipe,
        private eventManager: JhiEventManager,
        private jetkizuRouteService: JetkizuRouteService,
        private routeBranchService: RouteBranchService,
        private mapService: MapService,
        private route: ActivatedRoute,
        private jhiAlertService: JhiAlertService,
        private osmService: OsmService,
    ) {
    }

    save() {
        this.isSaving = true;
        for(let i=1; i<this.routeBranches.length; i++) {
            let branch = this.routeBranches[i];
            branch.startFrom = this.routeBranches[i-1].endTo;
            /*if(!branch.startFrom) {
            }*/
            if(branch.jetkizu && !branch.type) {
                branch.type = RouteBranchType.DELIVERY;
            }
        }
        this.jetkizuRoute.routeBranches = this.routeBranches;
        console.log(this.jetkizuRoute);
        this.jetkizuRouteService.updateWithNewElements(this.jetkizuRoute).subscribe((res) => {
            this.isSaving = false;
            this.jetkizuRoute = res;
            console.log(this.jetkizuRoute);
            this.load(this.jetkizuRoute.id);
        }, (error) => {
            this.onSaveError(error);
        });
    }

    private onSaveError(error: any) {
        this.isSaving = false;
        console.log(error);
        this.jhiAlertService.error(error.message, null, null);
    }

    ngOnInit() {
        this.isSaving = false;
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJetkizuRoutes();
        this.registerChangeInRouteBranches();
    }

    load(id) {
        this.routeBranches = [];
        this.originalRouteBranches = [];
        this.jetkizuRoute = new JetkizuRoute();
        this.jetkizuRouteService.find(id).subscribe((jetkizuRoute) => {
            if (jetkizuRoute.jetkizuDate) {
                jetkizuRoute.jetkizuDate = {
                    year: jetkizuRoute.jetkizuDate.getFullYear(),
                    month: jetkizuRoute.jetkizuDate.getMonth() + 1,
                    day: jetkizuRoute.jetkizuDate.getDate()
                };
            }
            jetkizuRoute.startedDateTime = this.datePipe
                .transform(jetkizuRoute.startedDateTime, 'yyyy-MM-ddTHH:mm:ss');
            jetkizuRoute.stoppedDateTime = this.datePipe
                .transform(jetkizuRoute.stoppedDateTime, 'yyyy-MM-ddTHH:mm:ss');
            jetkizuRoute.createDateTime = this.datePipe
                .transform(jetkizuRoute.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
            this.jetkizuRoute = jetkizuRoute;
            this.jetkizuRoute.distanceText = this.osmService.convertDistanceToKm(this.jetkizuRoute.totalPlanDistance);
            this.jetkizuRoute.durationText = this.osmService.convertDurationToHour(this.jetkizuRoute.totalPlanDuration);
        });
        this.loadBranches(id);
    }

    loadBranches(id) {
        Helpers.setLoading(true);
        this.routeBranchService.getAllByRoute(id, null).subscribe(
            (res: ResponseWrapper) => {
                this.routeBranches = res.json;
                this.originalRouteBranches = Object.assign([], this.routeBranches);
                this.mapViewInit(this.routeBranches);

                if(this.routeBranches.length) {
                    this.routeBranches
                        .filter((b) => b.jetkizuActivity)
                        .forEach((b) => {
                        b.textPlanDuration = this.osmService.convertDurationToHour(b.jetkizuActivity.planDuration);
                        b.textPlanDistance = this.osmService.convertDistanceToKm(b.jetkizuActivity.planKm);
                    });
                }

                Helpers.setLoading(false);
            },
            (error: ResponseWrapper) => {
                console.log(error.json);
                Helpers.setLoading(false);
            }
        );
    }

    mapViewInit(branches) {
        this.map = this.mapService.showMapboxglMap('map', 11);
        this.map.on('load', () => {
            this.mapService.addMapboxglMarker(
                this.map,
                [this.jetkizuRoute.start.lon, this.jetkizuRoute.start.lat],
                'se',
                'se',
                'S/E');
            this.routeBranchService.drawBranchesDirection(branches, this.map, 0);
            /*branches.forEach((branch) => {
                this.mapService.showDirection(this.map, branch);
            });*/
        });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJetkizuRoutes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jetkizuRouteListModification',
            (response) => this.load(this.jetkizuRoute.id)
        );
    }

    registerChangeInRouteBranches() {
        this.eventSubscriber = this.eventManager.subscribe(
            'addRouteBranchesInRouteListModification',
            (response) => {

                const selectedBranches = response.selectedBranches;
                selectedBranches.forEach((selectedBranch) => {
                    if(!this.backlogBranches.filter((bb) => selectedBranch.id === bb.id).length){
                        this.backlogBranches.push(selectedBranch);
                    }
                });
                this.routeBranchService.getAllByRoute(this.jetkizuRoute.id, null).subscribe(
                    (res: ResponseWrapper) => {
                        this.routeBranches = res.json;
                        this.originalRouteBranches = Object.assign([], this.routeBranches);
                        this.addBacklogOrders();
                    },
                    (error: ResponseWrapper) => console.log(error.json)
                );
            }
        );
    }

    addBacklogOrders() {
        this.addPickingBranch().subscribe((pickingBranch) => {
            this.pickingBranch = pickingBranch;
            const orderFrom = this.pickingBranch.order;
            this.routeBranches = [];
            let arrivalDirection = null;
            for(let i=0; i<this.originalRouteBranches.length; i++) {
                const originalBranch = Object.assign({}, this.originalRouteBranches[i]);
                this.routeBranches.push(originalBranch);
                if(originalBranch.type && RouteBranchType[originalBranch.type] == RouteBranchType.END.toString()) {
                    arrivalDirection = originalBranch;
                }
            }
            this.routeBranches.push(this.pickingBranch);
            this.mapService.orderBranches([this.pickingBranch.endTo], this.backlogBranches, false).subscribe((orderedBranches) => {
                console.log('ordered branches ->');
                console.log(orderedBranches);
                let min = 0;
                let lastDeliveryDirection = null;
                for(let i=0; i<orderedBranches.length; i++) {
                    let branch = orderedBranches[i];
                    branch.order = orderFrom + branch.order / 10;
                    this.routeBranches.push(branch);
                    if(min < branch.order) {
                        min = branch.order;
                        lastDeliveryDirection = branch;
                    }
                }
                setTimeout(() => {
                    //const lastDeliveryDirection = this.originalRouteBranches[this.originalRouteBranches.length-2];
                    console.log('last delivery direction');
                    console.log(lastDeliveryDirection);
                    arrivalDirection.startFrom = lastDeliveryDirection.endTo;
                    this.mapService.createBranchActivity([arrivalDirection.startFrom, arrivalDirection.endTo]).subscribe((res) => {
                        arrivalDirection.jetkizuActivity = res;
                    });
                    console.log('arrival direction');
                    console.log(arrivalDirection);
                    this.routeBranches.forEach((branch) => {
                        this.mapService.showDirection(this.map, branch);
                    });
                }, 0);
            });
        });
    }

    addPickingBranch(): any {
        console.log(this.originalRouteBranches);
        let firstDirection = null;
        let arrivalDirection = null;
        let max = 100;
        let isPickup = true;
        let orderFrom = null;
        let pickingBranch: RouteBranch = null;
        for(let i=0; i<this.originalRouteBranches.length; i++) {
            const branch = this.originalRouteBranches[i];
            if(branch.order < max) {
                max = branch.order;
                firstDirection = branch;
            }
            if(branch.type && RouteBranchType[branch.type] == RouteBranchType.END.toString()) {
                arrivalDirection = branch;
            }
            if(branch.type && RouteBranchType[branch.type] == RouteBranchType.PICKUP.toString() && branch.routeBranchStatusHistory) {
                if(branch.routeBranchStatusHistory.status == RouteBranchStatus.REACHED) {
                    console.log('reached');
                    let pickupCount = 0;
                    let pickupInProcessCount = 0;
                    for(let j=0; j<this.originalRouteBranches.length; j++) {
                        const pickupDelivery = this.originalRouteBranches[j];
                        if((pickupDelivery.order > branch.order) && (pickupDelivery.order < branch.order + 1)) {
                            pickupCount++;
                            if(pickupDelivery.routeBranchStatusHistory.status == RouteBranchStatus.IN_PROCESS) {
                                pickupInProcessCount++;
                            }
                        }
                    }
                    if(pickupCount === pickupInProcessCount) {
                        console.log('pickup reached by still on process');
                        isPickup = false;
                        orderFrom = branch.order;
                        pickingBranch = branch;
                    }
                } else {
                    console.log('pickup in process');
                    isPickup = false;
                    orderFrom = branch.order;
                    pickingBranch = branch;
                }
            }
        }

        if(!pickingBranch) {
            console.log('picking branch:');
            console.log(firstDirection);
            console.log(arrivalDirection);
            pickingBranch.order = arrivalDirection.order;
            arrivalDirection.order = arrivalDirection.order+1;
            pickingBranch.type = RouteBranchType.PICKUP;
            pickingBranch.startFrom = arrivalDirection.startFrom;
            pickingBranch.endTo = firstDirection.startFrom;
            let routeBranchStatus = new RouteBranchStatusHistory();
            routeBranchStatus.status = RouteBranchStatus.IN_PROCESS;
            pickingBranch.routeBranchStatusHistory = routeBranchStatus;
        } else {
            for(let i=this.originalRouteBranches.length - 1; i>=0; i--) {
                const originalBranch = this.originalRouteBranches[i];
                console.log('after pickup delivery:');
                if((originalBranch.order >= pickingBranch.order) && (originalBranch.order < pickingBranch.order + 1)) {
                    console.log(originalBranch);
                    const copyBranch: RouteBranch = Object.assign({}, originalBranch);
                    this.originalRouteBranches.splice(i, 1);
                    if(!this.backlogBranches.filter((bb) => copyBranch.id === bb.id).length){
                        if(copyBranch.id !== pickingBranch.id) {
                            this.backlogBranches.push(copyBranch);
                        }
                    }
                }
            }
            console.log('removed from original');
            console.log(this.originalRouteBranches);
        }

        return this.mapService.createBranchActivity([pickingBranch.startFrom, pickingBranch.endTo]).map((res) => {
            if(!pickingBranch.jetkizuActivity) {
                pickingBranch.jetkizuActivity = res;
            }
            return pickingBranch;
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-route-popup-detail',
    template: ''
})
export class JetkizuRouteDetailPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuRoutePopupDetailService: JetkizuRoutePopupDetailService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuRoutePopupDetailService
                .open(JetkizuRouteDetailComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
