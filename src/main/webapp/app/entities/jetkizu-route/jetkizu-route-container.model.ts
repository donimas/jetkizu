import { BaseEntity, User } from './../../shared';
import {Location} from '../location/location.model';
import {JetkizuRoute, TripType} from './jetkizu-route.model';

export class JetkizuRouteContainerDto implements BaseEntity {
    constructor(
        public id?: any,
        public jetkizuRoutes?: JetkizuRoute[],
        public jetkizuRoute?: JetkizuRoute,
        public tripType?: TripType,
        public start?: Location,
        public end?: Location,
    ) {
    }
}
