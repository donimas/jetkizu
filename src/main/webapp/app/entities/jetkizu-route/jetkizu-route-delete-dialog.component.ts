import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuRoute } from './jetkizu-route.model';
import { JetkizuRoutePopupService } from './jetkizu-route-popup.service';
import { JetkizuRouteService } from './jetkizu-route.service';

@Component({
    selector: 'jhi-jetkizu-route-delete-dialog',
    templateUrl: './jetkizu-route-delete-dialog.component.html'
})
export class JetkizuRouteDeleteDialogComponent {

    jetkizuRoute: JetkizuRoute;

    constructor(
        private jetkizuRouteService: JetkizuRouteService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jetkizuRouteService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jetkizuRouteListModification',
                content: 'Deleted an jetkizuRoute'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-route-delete-popup',
    template: ''
})
export class JetkizuRouteDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuRoutePopupService: JetkizuRoutePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuRoutePopupService
                .open(JetkizuRouteDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
