import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JetkizuRoute } from './jetkizu-route.model';
import { JetkizuRoutePopupService } from './jetkizu-route-popup.service';
import { JetkizuRouteService } from './jetkizu-route.service';
import { Team, TeamService } from '../team';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';
import {RouteBranch, RouteBranchType} from '../route-branch/route-branch.model';
import {RouteBranchService} from '../route-branch/route-branch.service';
import {MapService} from '../../shared/map/map.service';
import {Location} from '../location/location.model';
import {YandexService} from '../../shared/open-api-services/yandex.service';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';
import {
    RouteBranchStatus,
    RouteBranchStatusHistory
} from '../route-branch-status-history/route-branch-status-history.model';
import {LocationService} from '../location/location.service';

@Component({
    selector: 'jhi-jetkizu-route-start-dialog',
    templateUrl: './jetkizu-route-start-dialog.component.html'
})
export class JetkizuRouteStartDialogComponent implements OnInit, AfterViewInit {

    jetkizuRoute: JetkizuRoute;
    originalRouteBranches: RouteBranch[];
    isSaving: boolean;

    teams: Team[];

    users: User[];
    jetkizuDateDp: any;
    serviceDuration: any;
    map: any;
    startLocation: Location;
    endLocation: Location;
    roundtrip = true;
    garages: Location[];

    modelYandex: any;
    endYandexLocation: any;
    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribedYandex = new Observable(() => () => this.searching = false);
    startLocationEmpty = {
        'message': 'error.startLocation-empty'
    };
    endLocationEmpty = {
        'message': 'error.endLocation-empty'
    };
    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private jetkizuRouteService: JetkizuRouteService,
        private teamService: TeamService,
        private userService: UserService,
        private eventManager: JhiEventManager,
        private routeBranchService: RouteBranchService,
        private mapService: MapService,
        private _serviceYandex: YandexService,
        private locationService: LocationService
    ) {
        this.startLocation = new Location();
        this.endLocation = new Location();
        this.originalRouteBranches = [];
    }

    ngOnInit() {
        this.isSaving = false;
        this.routeBranchService.getAllByRoute(this.jetkizuRoute.id, null).subscribe(
            (res: ResponseWrapper) => {
                this.jetkizuRoute.routeBranches = res.json;
                this.originalRouteBranches = Object.assign([], this.jetkizuRoute.routeBranches);
            },
            (error: ResponseWrapper) => console.log(error.json)
        );
        this.teamService.query()
            .subscribe((res: ResponseWrapper) => { this.teams = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.locationService.findAllGarage({
            page: 0,
            size: 3,
            sort: ['id']
        }).subscribe(
            (res: ResponseWrapper) => {
                this.garages = res.json;
                console.log('garages: ');
                console.log(this.garages);
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngAfterViewInit() {
        this.map = this.mapService.showMapboxglMap('map', 11);
        let startPoint = new Location();
        startPoint.lat = '51.11617641057029';
        startPoint.lon = '71.41387939453126';
        let endPoint = startPoint;

        /*this.routeBranchService.getAllByRoute(this.jetkizuRoute.id, null).subscribe(
            (res: ResponseWrapper) => {
                this.jetkizuRoute.routeBranches = res.json;
                this.mapService.showMultipleDirections(this.map, startPoint, this.jetkizuRoute.routeBranches);
            },
            (error: ResponseWrapper) => console.log(error.json)
        );*/
    }

    generateRoute() {
        console.log(this.startLocation);
        console.log(this.endLocation);
        if(!this.startLocation || (!this.startLocation.lon && !this.startLocation.lat)) {
            this.onError(this.startLocationEmpty);
            return;
        }
        if(this.roundtrip) {
            console.log('initing end');
            this.endLocation = this.startLocation;
        } else if(!this.endLocation || (!this.endLocation.lon && !this.endLocation.lat)) {
            this.onError(this.endLocationEmpty);
            return;
        }

        console.log(this.roundtrip);
        this.jetkizuRoute.routeBranches = Object.assign([], this.originalRouteBranches);
        let startEndLocations = [this.startLocation, this.endLocation];
        console.log(startEndLocations);
        console.log(this.jetkizuRoute.routeBranches);
        this.mapService.showMultipleDirections(this.map, startEndLocations, this.jetkizuRoute.routeBranches, this.roundtrip);
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        for(let i=0; i<this.jetkizuRoute.routeBranches.length; i++) {
            let branch = this.jetkizuRoute.routeBranches[i];
            if(branch.order !== 1) {
                branch.startFrom = this.jetkizuRoute.routeBranches[i-1].endTo;
            } else {
                branch.startFrom = this.startLocation;
            }
        }
        console.log(this.jetkizuRoute);
        this.jetkizuRoute.routeBranches
            .forEach((branch) => {
                if(branch.order === 1) {
                    branch.startFrom = this.startLocation;
                }
            });
        /*this.subscribeToSaveResponse(
            this.jetkizuRouteService.startOptimizedRoute(this.jetkizuRoute));*/
    }

    private subscribeToSaveResponse(result: Observable<JetkizuRoute>) {
        result.subscribe((res: JetkizuRoute) =>
            this.onSaveSuccess(res), (error: Response) => this.onError(error));
    }

    private onSaveSuccess(result: JetkizuRoute) {
        console.log('onSaveSuccess ->');
        console.log(result);
        this.eventManager.broadcast({ name: 'jetkizuRouteListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(res) {
        this.isSaving = false;
        console.log(res);
    }

    private onError(error: any) {
        this.isSaving = false;
        console.log(error);
    }

    trackBranchByOrder(index: number, item: RouteBranch) {
        return item.order;
    }

    trackTeamById(index: number, item: Team) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    clickRoundTrip(flag) {
        this.roundtrip = flag;
        console.log(this.startLocation);
        console.log(this.endLocation);
    }

    searchYandex = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceYandex.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedYandex);

    formatterYandex = (x: {metaDataProperty: {GeocoderMetaData: {text: string}}}) => x.metaDataProperty.GeocoderMetaData.text;

    public selectedYandexItem(event) {
        console.log(event);
        this.initLocation(event, this.startLocation);
        this.selectStartGarage(this.startLocation);
    }
    public selectedYandexItemEnd(event) {
        console.log(event);
        this.initLocation(event, this.endLocation);
        this.selectEndGarage(this.endLocation);
    }
    private initLocation(event, location: Location) {
        console.log(event);
        const coordinates = event.item.Point.pos.split(' ');
        event.item.metaDataProperty.GeocoderMetaData.Address.Components
            .forEach((component) => {
                console.log('v: '+component);
                if(component.kind === 'street') {
                    location.street = component.name;
                } else if(component.kind === 'house') {
                    location.home = component.name;
                }
            });

        location.lon = coordinates[0];
        location.lat = coordinates[1];
        location.fullAddress = event.item.name;
        console.log(location);
    }

    private selectStartGarage(garage: Location) {
        this.startLocation = garage;
        this.mapService.addStartLocationPoint(this.map, [garage.lon, garage.lat]);
        console.log(this.startLocation);
        console.log(this.endLocation);
    }

    private selectEndGarage(garage: Location) {
        this.endLocation = garage;

        console.log(this.startLocation);
        console.log(this.endLocation);
        this.mapService.addEndLocationPoint(this.map, [garage.lon, garage.lat]);
    }
}

@Component({
    selector: 'jhi-jetkizu-route-popup',
    template: ''
})
export class JetkizuRouteStartPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuRoutePopupService: JetkizuRoutePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jetkizuRoutePopupService
                    .open(JetkizuRouteStartDialogComponent as Component, params['id']);
            } else {
                this.jetkizuRoutePopupService
                    .open(JetkizuRouteStartDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
