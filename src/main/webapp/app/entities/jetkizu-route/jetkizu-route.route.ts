import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JetkizuRouteComponent } from './jetkizu-route.component';
import {JetkizuRouteDetailComponent, JetkizuRouteDetailPopupComponent} from './jetkizu-route-detail.component';
import { JetkizuRoutePopupComponent } from './jetkizu-route-dialog.component';
import { JetkizuRouteDeletePopupComponent } from './jetkizu-route-delete-dialog.component';
import {JetkizuRouteStartPopupComponent} from './jetkizu-route-start-dialog.component';
import {JetkizuRouteAddJetkizuPopupComponent} from "./jetkizu-route-add-jetkizu-dialog.component";

@Injectable()
export class JetkizuRouteResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const jetkizuRouteRoute: Routes = [
    {
        path: 'jetkizu-route',
        component: JetkizuRouteComponent,
        resolve: {
            'pagingParams': JetkizuRouteResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'jetkizu-route/:id',
        component: JetkizuRouteDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jetkizuRoutePopupRoute: Routes = [
    {
        path: 'jetkizu-route-new',
        component: JetkizuRoutePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-route/:id/start',
        component: JetkizuRouteStartPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.startTitle'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-route/:id/detail',
        component: JetkizuRouteDetailPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.startTitle'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-route/:id/add-jetkizu',
        component: JetkizuRouteAddJetkizuPopupComponent,
        resolve: {
            'pagingParams': JetkizuRouteResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-route/:id/edit',
        component: JetkizuRoutePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-route/:id/delete',
        component: JetkizuRouteDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuRoute.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
