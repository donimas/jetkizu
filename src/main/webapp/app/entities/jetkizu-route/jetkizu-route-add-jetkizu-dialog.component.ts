import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {JhiAlertService, JhiEventManager, JhiParseLinks} from 'ng-jhipster';

import { ITEMS_PER_PAGE } from '../../shared';
import { JetkizuRoute } from './jetkizu-route.model';
import { JetkizuRouteService } from './jetkizu-route.service';
import {JetkizuRouteAddItemPopupService} from './jetkizu-route-add-item-popup.service';
import {RouteBranch} from '../route-branch/route-branch.model';
import {RouteBranchService} from '../route-branch/route-branch.service';
import {Observable} from 'rxjs/Observable';
import {JetkizuService} from '../jetkizu/jetkizu.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';

@Component({
    selector: 'jhi-jetkizu-route-add-jetkizu-dialog',
    templateUrl: './jetkizu-route-add-jetkizu-dialog.component.html'
})
export class JetkizuRouteAddJetkizuDialogComponent implements OnInit {

    jetkizuRoute: JetkizuRoute;
    isSaving: boolean;
    routeBranches: RouteBranch[] = [];

    itemsPerPage: number;
    links: any;
    page: any;
    predicate: any;
    queryCount: any;
    reverse: any;
    totalItems: number;
    currentSearch: string;
    allJetkizu = false;

    constructor(
        private jetkizuRouteService: JetkizuRouteService,
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private routeBranchService: RouteBranchService,
        private jetkizuService: JetkizuService,
        private parseLinks: JhiParseLinks,
        private activatedRoute: ActivatedRoute,
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 0;
        this.links = {
            last: 0
        };
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = activatedRoute.snapshot.params['search'] ? activatedRoute.snapshot.params['search'] : '';
    }

    ngOnInit() {
        this.isSaving = false;
        this.loadAll();
    }

    loadAll() {
        if (this.currentSearch) {
            this.jetkizuService.search({
                query: this.currentSearch,
                page: this.page,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
                (res: ResponseWrapper) => this.onError(res.json)
            );
            return;
        }
        this.routeBranchService.getAllBacklog({
            page: this.page,
            size: this.itemsPerPage,
            sort: this.sort()
        }).subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json, res.headers),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        const selectedBranches = this.routeBranches.filter((branch) => branch.selected);
        console.log(selectedBranches);
        if(selectedBranches.length) {
            this.onSaveSuccess(selectedBranches);
        } else {
            const errorMsg = {
                'message': 'error.branch-not-selected'
            };
            this.onError(errorMsg);
        }
    }

    private onSaveSuccess(result: RouteBranch[]) {
        this.eventManager.broadcast({ name: 'addRouteBranchesInRouteListModification', content: 'OK',
        'selectedBranches': result});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    reset() {
        this.page = 0;
        //this.jetkizus = [];
        this.routeBranches = [];
        this.loadAll();
    }

    loadPage(page) {
        this.page = page;
        this.loadAll();
    }

    clearPage() {
        this.routeBranches = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = 'id';
        this.reverse = true;
        this.currentSearch = '';
        this.loadAll();
    }

    search(query) {
        if (!query) {
            return this.clearPage();
        }
        this.routeBranches = [];
        this.links = {
            last: 0
        };
        this.page = 0;
        this.predicate = '_score';
        this.reverse = false;
        this.currentSearch = query;
        this.loadAll();
    }

    trackId(index: number, item: RouteBranch) {
        return item.id;
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        for (let i = 0; i < data.length; i++) {
            this.routeBranches.push(data[i]);
        }
    }

    selectAll(flag) {
        console.log(flag);

        this.routeBranches.forEach((branch) => {
            branch.selected = flag;
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-route-add-jetkizu-popup',
    template: ''
})
export class JetkizuRouteAddJetkizuPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuRoutePopupService: JetkizuRouteAddItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuRoutePopupService
                .open(JetkizuRouteAddJetkizuDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
