import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { JetkizuRoute } from './jetkizu-route.model';
import { JetkizuRouteService } from './jetkizu-route.service';

@Injectable()
export class JetkizuRoutePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private jetkizuRouteService: JetkizuRouteService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.jetkizuRouteService.find(id).subscribe((jetkizuRoute) => {
                    if (jetkizuRoute.jetkizuDate) {
                        jetkizuRoute.jetkizuDate = {
                            year: jetkizuRoute.jetkizuDate.getFullYear(),
                            month: jetkizuRoute.jetkizuDate.getMonth() + 1,
                            day: jetkizuRoute.jetkizuDate.getDate()
                        };
                    }
                    jetkizuRoute.startedDateTime = this.datePipe
                        .transform(jetkizuRoute.startedDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    jetkizuRoute.stoppedDateTime = this.datePipe
                        .transform(jetkizuRoute.stoppedDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    jetkizuRoute.createDateTime = this.datePipe
                        .transform(jetkizuRoute.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.jetkizuRouteModalRef(component, jetkizuRoute);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.jetkizuRouteModalRef(component, new JetkizuRoute());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    jetkizuRouteModalRef(component: Component, jetkizuRoute: JetkizuRoute): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', windowClass: 'modal-xxl', backdrop: 'static'});
        modalRef.componentInstance.jetkizuRoute = jetkizuRoute;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
