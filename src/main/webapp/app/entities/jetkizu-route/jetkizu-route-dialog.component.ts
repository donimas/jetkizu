import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Http, Response} from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import {JetkizuRoute, TripType} from './jetkizu-route.model';
import { JetkizuRoutePopupService } from './jetkizu-route-popup.service';
import { JetkizuRouteService } from './jetkizu-route.service';
import { Team, TeamService } from '../team';
import { User, UserService } from '../../shared';
import { ResponseWrapper } from '../../shared';
import { JhiDateUtils } from 'ng-jhipster';
import {AreaService} from '../area/area.service';
import {Area} from '../area/area.model';
import {AreaNodeService} from '../area-node/area-node.service';
import {AreaNode} from '../area-node/area-node.model';
import {of} from 'rxjs/observable/of';
import {OsmService} from '../../shared/osm/osm.service';
import {RouteBranch} from '../route-branch/route-branch.model';
import {MapService} from '../../shared/map/map.service';
import {ScriptLoaderService} from '../../shared/loader/script-loader.service';
import {JetkizuRouteContainerDto} from './jetkizu-route-container.model';
import {DatePipe} from '@angular/common';
import {JetkizuActivity} from "../jetkizu-activity/jetkizu-activity.model";
import {TIME_MASK} from '../../app.constants';
import {RouteOptimization} from '../route-optimization/route-optimization.model';
import {Helpers} from "../../helpers";

@Component({
    selector: 'jhi-jetkizu-route-dialog',
    templateUrl: './jetkizu-route-dialog.component.html'
})
export class JetkizuRouteDialogComponent implements OnInit, AfterViewInit {

    jetkizuRoute: JetkizuRoute;
    isSaving: boolean;
    jetkizuRouteContainerDto: JetkizuRouteContainerDto;

    teams: Team[];
    areas: Area[];
    selectedArea: Area;
    areaNodes: AreaNode[];

    users: User[];
    jetkizuDateDp: any;
    areNodesAllSelected: boolean = false;
    deliveryFrom: string;

    modelOsm: any;
    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribedOsm = new Observable(() => () => this.searching = false);

    optimizedAddresses: JetkizuRoute[] = [];

    map: any;
    innerHeight: any;

    timeMask = TIME_MASK;
    serviceDuration: any;
    jetkizuStartTime: any;
    optimization: RouteOptimization;

    isAddressTabLoading: boolean = false;
    isTeamTabLoading: boolean = false;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private jetkizuRouteService: JetkizuRouteService,
        private teamService: TeamService,
        private userService: UserService,
        private eventManager: JhiEventManager,
        private dateUtils: JhiDateUtils,
        private areaService: AreaService,
        private areaNodeService: AreaNodeService,
        private http: Http,
        private osmService: OsmService,
        private mapService: MapService,
        private _script: ScriptLoaderService,
        private datePipe: DatePipe
    ) {
    }

    ngOnInit() {
        // this.innerHeight = window.screen.height - 400;
        const date = new Date();
        this.jetkizuRoute.name = this.datePipe.transform(date,"dd-MM-yyyy");
        this.innerHeight = 600;
        this.isSaving = false;
        this.jetkizuRoute.tripType = TripType.ROUND_TRIP;
        this.optimization = new RouteOptimization();
        this.teamService.query()
            .subscribe((res: ResponseWrapper) => { this.teams = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.userService.query()
            .subscribe((res: ResponseWrapper) => { this.users = res.json; }, (res: ResponseWrapper) => this.onError(res.json));

        this.areaService.query(null).subscribe(
            (res: ResponseWrapper) => this.areas = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    ngAfterViewInit() {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.jetkizuRoute.routeOptimization = this.optimization;
        this.jetkizuRouteContainerDto.jetkizuRoute = this.jetkizuRoute;
        console.log('gonna save container');
        console.log(this.jetkizuRouteContainerDto);

        this.isSaving = true;
        this.jetkizuRouteService.startOptimizedRoute(this.jetkizuRouteContainerDto).subscribe(
            (res: number[]) => {
                console.log(res);
                this.eventManager.broadcast({ name: 'jetkizuRouteListModification', content: 'OK'});
                this.isSaving = false;
                this.activeModal.dismiss(res);
            }, (res: Response) => this.onSaveError()
        );
        /*if (this.jetkizuRoute.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jetkizuRouteService.update(this.jetkizuRoute));
        } else {
            this.subscribeToSaveResponse(
                this.jetkizuRouteService.create(this.jetkizuRoute));
        }*/
    }

    optimize() {
        this.isSaving = true;
        this.isAddressTabLoading = true;
        this.jetkizuRouteContainerDto = new JetkizuRouteContainerDto();
        this.jetkizuRouteContainerDto.tripType = this.jetkizuRoute.tripType;
        this.jetkizuRouteContainerDto.start = this.jetkizuRoute.start;
        this.jetkizuRouteContainerDto.end = this.jetkizuRoute.end;
        this.jetkizuRouteContainerDto.jetkizuRoutes = this.optimizedAddresses;
        console.log(this.jetkizuRouteContainerDto);
        this.jetkizuRouteService.getOptimizedRoutes(this.jetkizuRouteContainerDto).subscribe(
            (res) => {
                this.isSaving = false;
                console.log(res.json);
                this.jetkizuRouteContainerDto = res.json[0];
                this.optimizedAddresses = this.jetkizuRouteContainerDto.jetkizuRoutes;
                console.log(this.optimizedAddresses);

                this.mapService.removeRoute(this.map, 'area');

                this.mapService.addMapboxglMarker(
                    this.map,
                    [this.jetkizuRouteContainerDto.start.lon, this.jetkizuRouteContainerDto.start.lat],
                    'se',
                    'se',
                    'S/E');

                setTimeout(() => {
                    this.refreshMarkerText(this.optimizedAddresses);
                });

                for(let i=0; i<this.optimizedAddresses.length; i++) {
                    if(this.optimizedAddresses[i].flagWithoutPolygons) {
                        continue;
                    }
                    this.optimizedAddresses[i].routeBranches.forEach((branch) => {
                        this.mapService.drawDirection(
                            [branch.startFrom.lon, branch.startFrom.lat],
                            [branch.endTo.lon, branch.endTo.lat],
                            branch.id,
                            this.map, i)
                            .then((activity: JetkizuActivity) => {
                                console.log('activity =>');
                                console.log(activity);
                                branch.jetkizuActivity = activity;
                            });

                        if(branch.nextStop && branch.nextStop.flagGarage) {
                            this.mapService.drawDirection(
                                [branch.endTo.lon, branch.endTo.lat],
                                [branch.nextStop.lon, branch.nextStop.lat],
                                'garage-'+branch.id,
                                this.map, i)
                                .then((activity: JetkizuActivity) => {
                                    branch.jetkizuActivity.planDistanceToNext = activity.planKm;
                                    branch.jetkizuActivity.planDurationToNext = activity.planDuration;
                                });
                        }
                    });
                }


                this.isAddressTabLoading = false;
            }, (error: Response) => {
                this.isSaving = false;
                this.isAddressTabLoading = false;
                console.log(error);
                this.onSaveError();
            }
        );
    }

    private subscribeToSaveResponse(result: Observable<JetkizuRoute>) {
        result.subscribe((res: JetkizuRoute) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: JetkizuRoute) {
        this.eventManager.broadcast({ name: 'jetkizuRouteListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackTeamById(index: number, item: Team) {
        return item.id;
    }

    trackUserById(index: number, item: User) {
        return item.id;
    }

    trackAreaById(index: number, item: Area) {
        return item.id;
    }
    trackAreaNodeId(index: number, item: AreaNode) {
        return item.id;
    }

    getAreaNodes(e) {
        console.log('getting area nodes ->');
        console.log(this.selectedArea);
        if(!this.selectedArea) {
            return;
        }
        this.isTeamTabLoading = true;
        this.areaNodeService.findByAreaId(this.selectedArea.id, null)
            .subscribe((res: ResponseWrapper) => {
                this.areaNodes = res.json;
                console.log(this.areaNodes);

                this.isTeamTabLoading = false;
            },
            (error: ResponseWrapper) => {
                this.isTeamTabLoading = false;
                this.onError(error.json)
            });
    }

    selectAllNodes() {
        if(!this.areaNodes) {
            return;
        }
        this.areaNodes.forEach((areaNode) => areaNode.selected = this.areNodesAllSelected);
    }

    searchOsm = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this.osmService.searchAddressFromOsm(term)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedOsm);

    formatterOsm = (x: {display_name: string}) => x.display_name;

    public selectedOsmItem(event) {
        console.log(event);
        this.jetkizuRoute.start = this.osmService.osmToLocation(event, true);
        console.log(this.jetkizuRoute);
    }

    public tabChange(event) {
        console.log(event);
        if(event.nextId !== 'ngb-tab-5') {
            return;
        }
        if(!this.jetkizuRoute.start) {
            alert('Empty start address!');
            return;
        }
        if(!this.areaNodes || !this.areaNodes.length) {
            alert('Empty nodes!');
            return;
        }
        this.isAddressTabLoading = true;

        this.jetkizuRoute.areaNodes = this.areaNodes.filter((areaNode) => areaNode.selected);
        console.log(this.jetkizuRoute);
        this.jetkizuRouteService.getOptimizedAddresses(this.jetkizuRoute)
            .subscribe((res) => {
                this.optimizedAddresses = res.json;
                console.log('Optimized addresses');
                console.log(this.optimizedAddresses);
                if(!this.map) {
                    setTimeout(() => {
                        this.map = this.mapService.showMapboxglMap('optimizedRouteMap', 11);
                        this.map.on('load', () => {

                            // area polygons
                            this.areaNodeService.drawAreaPolygons(this.areaNodes, this.map);

                            let row = 0;
                            this.optimizedAddresses.forEach(o => {
                                let i = 0;
                                o.routeBranches.forEach((branch) => {
                                    if(!o.flagWithoutPolygons) {
                                        i = branch.order - 1;
                                    } else {
                                        i++;
                                    }
                                    this.mapService.addMapboxglMarker(
                                        this.map,
                                        [branch.endTo.lon, branch.endTo.lat],
                                        'normal-'+row,
                                        branch.id,
                                        i + '');
                                    }
                                );
                                row++;
                            });
                        });
                    });
                }

                this.isAddressTabLoading = false;
            }, (error) => {
                console.log(error);
                this.isAddressTabLoading = false;
            });
    }

    selectAddress(branch: RouteBranch) {
        console.log(branch);
        branch.selected = !branch.selected;
    }

    branchMouseEnter(branchId: number, routeIndex: number) {
        console.log(routeIndex);
        const el = document.getElementById(branchId+'');
        this.mapService.hoverMarker(el, 'normal-'+routeIndex);
    }
    branchMouseLeave(branchId: number, routeIndex: number) {
        const el = document.getElementById(branchId+'');
        this.mapService.unhoverMarker(el, 'normal-'+routeIndex);
    }

    getAddressSuffix(length: number) {
        let result = '';
        switch (length) {
            case 0: result = ''; break;
            case 1: result = 'адрес'; break;
            case 2:
            case 3:
            case 4: result = 'адреса'; break;
            default: result = 'адресов';
        }

        return result;
    }

    moveToRoute(sourceAreaNode: AreaNode, targetAreaNode: AreaNode) {
        let sourceRoute, targetRoute: JetkizuRoute;
        if(sourceAreaNode) {
            sourceRoute = this.optimizedAddresses
                .filter((route) => route.areaNode && route.areaNode.id === sourceAreaNode.id)[0];
        } else {
            sourceRoute = this.optimizedAddresses
                .filter((route) => !route.areaNode)[0];
        }
        console.log(sourceRoute);
        let newStyleType = 'normal-';
        if(targetAreaNode) {
            for(let i=0; i<this.optimizedAddresses.length; i++) {
                const route = this.optimizedAddresses[i];
                if(route.areaNode && route.areaNode.id === targetAreaNode.id) {
                    targetRoute = route;
                    newStyleType = newStyleType + i;
                }
            }
        } else {
            for(let i=0; i<this.optimizedAddresses.length; i++) {
                const route = this.optimizedAddresses[i];
                if(!route.areaNode) {
                    targetRoute = route;
                    newStyleType = newStyleType + i;
                }
            }
        }
        console.log(targetRoute);

        let count = 0;
        sourceRoute.routeBranches
            .filter((branch) => branch.selected)
            .forEach((selectedBranch) => {
                count++;
                console.log(selectedBranch);
                const copy: RouteBranch = Object.assign({}, selectedBranch);
                copy.selected = false;
                targetRoute.routeBranches.push(copy);

                this.mapService.updateMarkerElementStyle(newStyleType, copy.id);
            });
        sourceRoute.routeBranches = sourceRoute.routeBranches.filter((branch) => !branch.selected);

        if(count == 0) {
            alert('Выберите адрес для перемещения');
            return;
        }

        setTimeout(() => {
            this.refreshMarkerText(this.optimizedAddresses);
        });

    }

    refreshMarkerText(jetkizuRoutes: JetkizuRoute[]) {
        jetkizuRoutes
            .filter((route) => route.routeBranches.length)
            .forEach((route) => {
                this.changeMarkerElementTextNode(route.routeBranches);
            });
    }

    changeMarkerElementTextNode(branches: RouteBranch[]) {
        let i = 0;
        branches
            .forEach((branch) => {
                i++;
                let marker = document.getElementById(branch.id+'');
                let markerLabel = marker.getElementsByClassName('markerLabel')[0];
                markerLabel.childNodes[0].nodeValue = i + '';
            });
    }
}

@Component({
    selector: 'jhi-jetkizu-route-popup',
    template: ''
})
export class JetkizuRoutePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuRoutePopupService: JetkizuRoutePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jetkizuRoutePopupService
                    .open(JetkizuRouteDialogComponent as Component, params['id']);
            } else {
                this.jetkizuRoutePopupService
                    .open(JetkizuRouteDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
