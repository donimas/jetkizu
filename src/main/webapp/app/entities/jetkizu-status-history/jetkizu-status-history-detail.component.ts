import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuStatusHistory } from './jetkizu-status-history.model';
import { JetkizuStatusHistoryService } from './jetkizu-status-history.service';

@Component({
    selector: 'jhi-jetkizu-status-history-detail',
    templateUrl: './jetkizu-status-history-detail.component.html'
})
export class JetkizuStatusHistoryDetailComponent implements OnInit, OnDestroy {

    jetkizuStatusHistory: JetkizuStatusHistory;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private jetkizuStatusHistoryService: JetkizuStatusHistoryService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInJetkizuStatusHistories();
    }

    load(id) {
        this.jetkizuStatusHistoryService.find(id).subscribe((jetkizuStatusHistory) => {
            this.jetkizuStatusHistory = jetkizuStatusHistory;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInJetkizuStatusHistories() {
        this.eventSubscriber = this.eventManager.subscribe(
            'jetkizuStatusHistoryListModification',
            (response) => this.load(this.jetkizuStatusHistory.id)
        );
    }
}
