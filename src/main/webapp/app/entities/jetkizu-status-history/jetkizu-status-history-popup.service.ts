import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';
import { JetkizuStatusHistory } from './jetkizu-status-history.model';
import { JetkizuStatusHistoryService } from './jetkizu-status-history.service';

@Injectable()
export class JetkizuStatusHistoryPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private jetkizuStatusHistoryService: JetkizuStatusHistoryService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.jetkizuStatusHistoryService.find(id).subscribe((jetkizuStatusHistory) => {
                    jetkizuStatusHistory.createDateTime = this.datePipe
                        .transform(jetkizuStatusHistory.createDateTime, 'yyyy-MM-ddTHH:mm:ss');
                    this.ngbModalRef = this.jetkizuStatusHistoryModalRef(component, jetkizuStatusHistory);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.jetkizuStatusHistoryModalRef(component, new JetkizuStatusHistory());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    jetkizuStatusHistoryModalRef(component: Component, jetkizuStatusHistory: JetkizuStatusHistory): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.jetkizuStatusHistory = jetkizuStatusHistory;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
