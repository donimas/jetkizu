import { BaseEntity } from './../../shared';

export const enum JetkizuStatus {
    'BACKLOG',
    'DRAFT',
    'IN_PROCESS',
    'DELIVERED',
    'FAILED'
}

export const enum FailType {
    'ADDRESS_NOT_FOUND',
    'PHONE_NOT_ALLOWED',
    'NOT_OCCUPIED'
}

export class JetkizuStatusHistory implements BaseEntity {
    constructor(
        public id?: number,
        public createDateTime?: any,
        public flagDeleted?: boolean,
        public status?: JetkizuStatus,
        public failType?: FailType,
        public jetkizu?: BaseEntity,
    ) {
        this.flagDeleted = false;
    }
}
