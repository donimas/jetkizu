import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { JetkizuStatusHistoryComponent } from './jetkizu-status-history.component';
import { JetkizuStatusHistoryDetailComponent } from './jetkizu-status-history-detail.component';
import { JetkizuStatusHistoryPopupComponent } from './jetkizu-status-history-dialog.component';
import { JetkizuStatusHistoryDeletePopupComponent } from './jetkizu-status-history-delete-dialog.component';

export const jetkizuStatusHistoryRoute: Routes = [
    {
        path: 'jetkizu-status-history',
        component: JetkizuStatusHistoryComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'jetkizu-status-history/:id',
        component: JetkizuStatusHistoryDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const jetkizuStatusHistoryPopupRoute: Routes = [
    {
        path: 'jetkizu-status-history-new',
        component: JetkizuStatusHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-status-history/:id/edit',
        component: JetkizuStatusHistoryPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'jetkizu-status-history/:id/delete',
        component: JetkizuStatusHistoryDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.jetkizuStatusHistory.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
