import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { JetkizuStatusHistory } from './jetkizu-status-history.model';
import { JetkizuStatusHistoryPopupService } from './jetkizu-status-history-popup.service';
import { JetkizuStatusHistoryService } from './jetkizu-status-history.service';

@Component({
    selector: 'jhi-jetkizu-status-history-delete-dialog',
    templateUrl: './jetkizu-status-history-delete-dialog.component.html'
})
export class JetkizuStatusHistoryDeleteDialogComponent {

    jetkizuStatusHistory: JetkizuStatusHistory;

    constructor(
        private jetkizuStatusHistoryService: JetkizuStatusHistoryService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.jetkizuStatusHistoryService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'jetkizuStatusHistoryListModification',
                content: 'Deleted an jetkizuStatusHistory'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-jetkizu-status-history-delete-popup',
    template: ''
})
export class JetkizuStatusHistoryDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuStatusHistoryPopupService: JetkizuStatusHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.jetkizuStatusHistoryPopupService
                .open(JetkizuStatusHistoryDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
