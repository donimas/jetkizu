import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { JetkizuStatusHistory } from './jetkizu-status-history.model';
import { JetkizuStatusHistoryPopupService } from './jetkizu-status-history-popup.service';
import { JetkizuStatusHistoryService } from './jetkizu-status-history.service';
import { Jetkizu, JetkizuService } from '../jetkizu';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-jetkizu-status-history-dialog',
    templateUrl: './jetkizu-status-history-dialog.component.html'
})
export class JetkizuStatusHistoryDialogComponent implements OnInit {

    jetkizuStatusHistory: JetkizuStatusHistory;
    isSaving: boolean;

    jetkizus: Jetkizu[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private jetkizuStatusHistoryService: JetkizuStatusHistoryService,
        private jetkizuService: JetkizuService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jetkizuService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizus = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.jetkizuStatusHistory.id !== undefined) {
            this.subscribeToSaveResponse(
                this.jetkizuStatusHistoryService.update(this.jetkizuStatusHistory));
        } else {
            this.subscribeToSaveResponse(
                this.jetkizuStatusHistoryService.create(this.jetkizuStatusHistory));
        }
    }

    private subscribeToSaveResponse(result: Observable<JetkizuStatusHistory>) {
        result.subscribe((res: JetkizuStatusHistory) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: JetkizuStatusHistory) {
        this.eventManager.broadcast({ name: 'jetkizuStatusHistoryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJetkizuById(index: number, item: Jetkizu) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-jetkizu-status-history-popup',
    template: ''
})
export class JetkizuStatusHistoryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private jetkizuStatusHistoryPopupService: JetkizuStatusHistoryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.jetkizuStatusHistoryPopupService
                    .open(JetkizuStatusHistoryDialogComponent as Component, params['id']);
            } else {
                this.jetkizuStatusHistoryPopupService
                    .open(JetkizuStatusHistoryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
