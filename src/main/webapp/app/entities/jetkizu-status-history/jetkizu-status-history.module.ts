import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    JetkizuStatusHistoryService,
    JetkizuStatusHistoryPopupService,
    JetkizuStatusHistoryComponent,
    JetkizuStatusHistoryDetailComponent,
    JetkizuStatusHistoryDialogComponent,
    JetkizuStatusHistoryPopupComponent,
    JetkizuStatusHistoryDeletePopupComponent,
    JetkizuStatusHistoryDeleteDialogComponent,
    jetkizuStatusHistoryRoute,
    jetkizuStatusHistoryPopupRoute,
} from './';

const ENTITY_STATES = [
    ...jetkizuStatusHistoryRoute,
    ...jetkizuStatusHistoryPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        JetkizuStatusHistoryComponent,
        JetkizuStatusHistoryDetailComponent,
        JetkizuStatusHistoryDialogComponent,
        JetkizuStatusHistoryDeleteDialogComponent,
        JetkizuStatusHistoryPopupComponent,
        JetkizuStatusHistoryDeletePopupComponent,
    ],
    entryComponents: [
        JetkizuStatusHistoryComponent,
        JetkizuStatusHistoryDialogComponent,
        JetkizuStatusHistoryPopupComponent,
        JetkizuStatusHistoryDeleteDialogComponent,
        JetkizuStatusHistoryDeletePopupComponent,
    ],
    providers: [
        JetkizuStatusHistoryService,
        JetkizuStatusHistoryPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuJetkizuStatusHistoryModule {}
