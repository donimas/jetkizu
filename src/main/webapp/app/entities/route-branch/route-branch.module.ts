import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    RouteBranchService,
    RouteBranchPopupService,
    RouteBranchComponent,
    RouteBranchDetailComponent,
    RouteBranchDialogComponent,
    RouteBranchPopupComponent,
    RouteBranchDeletePopupComponent,
    RouteBranchDeleteDialogComponent,
    routeBranchRoute,
    routeBranchPopupRoute,
} from './';

const ENTITY_STATES = [
    ...routeBranchRoute,
    ...routeBranchPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RouteBranchComponent,
        RouteBranchDetailComponent,
        RouteBranchDialogComponent,
        RouteBranchDeleteDialogComponent,
        RouteBranchPopupComponent,
        RouteBranchDeletePopupComponent,
    ],
    entryComponents: [
        RouteBranchComponent,
        RouteBranchDialogComponent,
        RouteBranchPopupComponent,
        RouteBranchDeleteDialogComponent,
        RouteBranchDeletePopupComponent,
    ],
    providers: [
        RouteBranchService,
        RouteBranchPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuRouteBranchModule {}
