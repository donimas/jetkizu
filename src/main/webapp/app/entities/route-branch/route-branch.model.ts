import { BaseEntity } from './../../shared';
import {JetkizuRoute} from '../jetkizu-route/jetkizu-route.model';
import {Jetkizu} from '../jetkizu/jetkizu.model';
import {Location} from '../location/location.model';
import {JetkizuActivity} from '../jetkizu-activity/jetkizu-activity.model';
import {RouteBranchStatusHistory} from '../route-branch-status-history/route-branch-status-history.model';
import {Customer} from '../customer/customer.model';

export enum RouteBranchType {
    'START' = 0,
    'PICKUP' = 1,
    'DELIVERY' = 2,
    'END' = 3
}

export class RouteBranch implements BaseEntity {
    constructor(
        public id?: number,
        public order?: number,
        public jetkizu?: Jetkizu,
        public jetkizuRoute?: JetkizuRoute,
        public routeBranchStatusHistory?: RouteBranchStatusHistory,
        public jetkizuActivity?: JetkizuActivity,
        public startFrom?: Location,
        public endTo?: Location,
        public nextStop?: Location,
        public customer?: Customer,
        public type?: RouteBranchType,
        public selected?: boolean,
        public textPlanDistance?: any,
        public textPlanDuration?: any,
    ) {
        selected = false;
    }
}
