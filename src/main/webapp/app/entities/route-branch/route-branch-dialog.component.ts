import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Http, Response} from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RouteBranch } from './route-branch.model';
import { RouteBranchPopupService } from './route-branch-popup.service';
import { RouteBranchService } from './route-branch.service';
import { Jetkizu, JetkizuService } from '../jetkizu';
import { JetkizuRoute, JetkizuRouteService } from '../jetkizu-route';
import { RouteBranchStatusHistory, RouteBranchStatusHistoryService } from '../route-branch-status-history';
import { JetkizuActivity, JetkizuActivityService } from '../jetkizu-activity';
import { ResponseWrapper } from '../../shared';
import { Location } from '../location';
import { Customer } from '../customer';
import {CELL_PHONE_MASK, HOME_PHONE_MASK} from '../../app.constants';
import {KazpostService} from '../../shared/open-api-services/kazpost.service';
import {YandexService} from '../../shared/open-api-services/yandex.service';
import {MapService} from '../../shared/map/map.service';
import {of} from 'rxjs/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/merge';
import {OsmService} from '../../shared/osm/osm.service';

@Component({
    selector: 'jhi-route-branch-dialog',
    templateUrl: '../jetkizu/jetkizu-dialog.component.html'
})
export class RouteBranchDialogComponent implements OnInit, AfterViewInit {

    routeBranch: RouteBranch;
    isSaving: boolean;

    jetkizus: Jetkizu[];

    jetkizuroutes: JetkizuRoute[];

    routebranchstatushistories: RouteBranchStatusHistory[];

    jetkizuactivities: JetkizuActivity[];

    jetkizu: Jetkizu;
    location: Location;
    customer: Customer;

    modelOsm: any;
    searching = false;
    searchFailed = false;
    hideSearchingWhenUnsubscribedOsm = new Observable(() => () => this.searching = false);
    hideSearchingWhenUnsubscribedKazpost = new Observable(() => () => this.searching = false);
    hideSearchingWhenUnsubscribedYandex = new Observable(() => () => this.searching = false);

    map: any;
    marker: any;
    cellPhoneMask = CELL_PHONE_MASK;
    homePhoneMask = HOME_PHONE_MASK;
    isKazpostNotFound = false;
    addressEmptyError = {
        'message': 'error.address-empty'
    };
    coordinateEmptyError = {
        'message': 'error.coordinate-empty'
    };

    businessDetails: any = {};
    jetkizuSchedule: any = {};

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private routeBranchService: RouteBranchService,
        private jetkizuService: JetkizuService,
        private jetkizuRouteService: JetkizuRouteService,
        private routeBranchStatusHistoryService: RouteBranchStatusHistoryService,
        private jetkizuActivityService: JetkizuActivityService,
        private eventManager: JhiEventManager,
        private _serviceKazpost: KazpostService,
        private _serviceYandex: YandexService,
        private _serviceMap: MapService,
        private http: Http,
        private osmService: OsmService,
    ) {
        this.jetkizu = new Jetkizu();
        this.location = new Location();
        this.customer = new Customer();
    }

    ngOnInit() {
        console.log('branch:');
        console.log(this.routeBranch);
        this.isSaving = false;
        if(this.routeBranch.id){
            this.jetkizu = this.routeBranch.jetkizu;
            this.customer = this.routeBranch.customer;
            this.location = this.routeBranch.endTo;
        }
        this.jetkizuService
            .query({filter: 'routebranch-is-null'})
            .subscribe((res: ResponseWrapper) => {
                if (!this.routeBranch.jetkizu || !this.routeBranch.jetkizu.id) {
                    this.jetkizus = res.json;
                } else {
                    this.jetkizuService
                        .find(this.routeBranch.jetkizu.id)
                        .subscribe((subRes: Jetkizu) => {
                            this.jetkizus = [subRes].concat(res.json);
                        }, (subRes: ResponseWrapper) => this.onError(subRes.json));
                }
            }, (res: ResponseWrapper) => this.onError(res.json));
        /*this.jetkizuRouteService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizuroutes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.routeBranchStatusHistoryService.query()
            .subscribe((res: ResponseWrapper) => { this.routebranchstatushistories = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.jetkizuActivityService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizuactivities = res.json; }, (res: ResponseWrapper) => this.onError(res.json));*/
    }

    ngAfterViewInit() {
        /*this.map = this._serviceMap.showMapboxglMap('map');
        if(this.location.id) {
            this.marker = this._serviceMap.addMapboxglMarker(this.map, [this.location.lon, this.location.lat], this.location.fullAddress);
        }
        this.map.on('click', (event) => this.onMapClick(event));*/
    }

    onMapClick(e) {
        console.log(e);
        console.log('branch map clicked');
        const lat = e.lngLat.lat;
        const lon = e.lngLat.lng;
        console.log(lat + ', '+lon);
        //const query = `https://master.apis.dev.openstreetmap.org/query?lat=${lat}&lon=${lon}`;
        const query = `https://master.apis.dev.openstreetmap.org/api/capabilities`;
        const query2 = `https://master.apis.dev.openstreetmap.org/api/0.6/map?bbox=${lon},${lat},${lon},${lat}`;
        /*var xhr = new XMLHttpRequest();
        console.log(xhr);
        this.createCORSRequest('GET', query);*/
        this.requestToMapbox(query);
        this.requestToMapbox(query2);
        this.addLocationMarker(e.latlng);
    }

    requestToMapbox(query) {
        this.http.get(query).map((res:Response) => {
            return res;
        }).subscribe((res) => console.log(res), (error) => console.log(error));
    }

    createCORSRequest(method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
            console.log('withCredentials');
            // Check if the XMLHttpRequest object has a "withCredentials" property.
            // "withCredentials" only exists on XMLHTTPRequest2 objects.
            xhr.open(method, url, true);

        }/* else if (typeof XDomainRequest != "undefined") {
            console.log('undefined');

            // Otherwise, check if XDomainRequest.
            // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
            xhr = new XDomainRequest();
            xhr.open(method, url);

        }*/ else {
            console.log('null');

            // Otherwise, CORS is not supported by the browser.
            xhr = null;

        }
        return xhr;
    }

    addLocationMarker(latlng) {
        if(!this.isLocationAddressEntered()) {
            this.onError(this.addressEmptyError);
            return;
        }
        if(this.marker) {
            this.map.removeLayer(this.marker); // remove
        }
        let fullAddress = this.toFullAddress(this.location);
        this.location.lat = latlng.lat;
        this.location.lon = latlng.lng;
        this.marker = this._serviceMap.addMapboxglMarker(this.map, latlng, 'normal');
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        /*if(!this.isLocationAddressEntered()) {
            this.onError(this.addressEmptyError);
            return;
        }
        if(!this.isLocationCoordinateEntered()) {
            this.onError(this.coordinateEmptyError);
            return;
        }*/
        //this.location.fullAddress = this.toFullAddress(this.location);
        /*this.jetkizu.location = this.location;
        this.jetkizu.customer = this.customer;*/
        this.routeBranch.endTo = this.location;
        this.routeBranch.customer = this.customer;
        this.routeBranch.jetkizu = this.jetkizu;
        this.isSaving = true;
        console.log(this.routeBranch);
        if (this.routeBranch.id !== undefined) {
            this.subscribeToSaveResponse(
                this.routeBranchService.update(this.routeBranch));
        } else if (!this.routeBranch.jetkizuRoute) {
            this.subscribeToSaveResponse(
                this.routeBranchService.create(this.routeBranch));
        } else {
            this.subscribeToSaveResponse(
                this.routeBranchService.addNewToRoute(this.routeBranch));
        }
    }

    private subscribeToSaveResponse(result: Observable<RouteBranch>) {
        result.subscribe((res: RouteBranch) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: RouteBranch) {
        //this.eventManager.broadcast({ name: 'routeBranchListModification', content: 'OK'});
        this.eventManager.broadcast({ name: 'jetkizuListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJetkizuById(index: number, item: Jetkizu) {
        return item.id;
    }

    trackJetkizuRouteById(index: number, item: JetkizuRoute) {
        return item.id;
    }

    trackRouteBranchStatusHistoryById(index: number, item: RouteBranchStatusHistory) {
        return item.id;
    }

    trackJetkizuActivityById(index: number, item: JetkizuActivity) {
        return item.id;
    }

    searchKazpost = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceKazpost.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedKazpost);

    formatterKazpost = (x: {addressRus: string}) => x.addressRus;

    selectedKazpostItem(l) {
        console.log(l);
        this.location.fullAddress = l.item.addressRus;
        this.location.home = l.item.fullAddress.number;
        this.location.street = l.item.fullAddress.parts[0].nameRus;
        this.location.postindex = l.item.postcode;
        this.resetLocationCoordinates();
        console.log(this.location);
    }

    kazpostNotFound() {
        this.isKazpostNotFound = !this.isKazpostNotFound;
    }

    resetLocationCoordinates() {
        /*this.location.fullAddress = '';
        this.location.street = '';
        this.location.home = '';
        this.location.flat = '';
        this.location.postindex = '';*/
        this.map.removeLayer(this.marker); // remove
        this.location.lat = '';
        this.location.lon = '';
    }

    searchYandex = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this._serviceYandex.query(term, 0)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedYandex);

    formatterYandex = (x: {metaDataProperty: {GeocoderMetaData: {text: string}}}) => x.metaDataProperty.GeocoderMetaData.text;

    public selectedYandexItem(event) {
        if(!this.isLocationAddressEntered()) {
            this.onError(this.addressEmptyError);
            return;
        }
        const coordinates = event.item.Point.pos.split(' ');
        this.location.lon = coordinates[0];
        this.location.lat = coordinates[1];
        let pos = {
            'lat': this.location.lat,
            'lng': this.location.lon
        };
        this.addLocationMarker(pos);
    }

    isLocationAddressEntered(): boolean {
        return ((this.location.street && this.location.street !== '') &&
            (this.location.home && this.location.home !== ''));
    }

    isLocationCoordinateEntered(): boolean {
        return ((this.location.lat && this.location.lat !== '') &&
            (this.location.lon && this.location.lon !== ''));
    }

    toFullAddress(location: Location): string {
        let fullAddress = location.fullAddress;
        if(!fullAddress || fullAddress === '') {
            fullAddress = this.location.street + ', ' + this.location.home;
        }
        return fullAddress;
    }

    searchOsm = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do((v) => {
                console.log('do1:');
                console.log(v);
                this.searching = true;
            })
            .switchMap((term) => {
                return this.osmService.searchAddressFromOsm(term)
                    .do((v) => {
                        console.log('do2:');
                        console.log(v);
                        this.searchFailed = false;
                    })
                    .catch((v) => {
                        console.log('catch1:');
                        console.log(v);
                        this.searchFailed = true;
                        return of([]);
                    })
            })
            .do((v) => {
                console.log('do3:');
                console.log(v);
                this.searching = false;
            })
            .merge(this.hideSearchingWhenUnsubscribedOsm);

    formatterOsm = (x: {display_name: string}) => x.display_name;

    public selectedOsmItem(event) {
        console.log(event);
        this.location = this.osmService.osmToLocation(event, false);
    }
}

@Component({
    selector: 'jhi-route-branch-popup',
    template: ''
})
export class RouteBranchPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private routeBranchPopupService: RouteBranchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.routeBranchPopupService
                    .open(RouteBranchDialogComponent as Component, params['id']);
            } else if(params['routeId']) {
                this.routeBranchPopupService
                    .open(RouteBranchDialogComponent as Component, null, params['routeId']);
            } else {
                this.routeBranchPopupService
                    .open(RouteBranchDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
