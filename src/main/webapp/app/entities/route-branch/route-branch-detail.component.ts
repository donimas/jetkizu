import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { RouteBranch } from './route-branch.model';
import { RouteBranchService } from './route-branch.service';

@Component({
    selector: 'jhi-route-branch-detail',
    templateUrl: './route-branch-detail.component.html'
})
export class RouteBranchDetailComponent implements OnInit, OnDestroy {

    routeBranch: RouteBranch;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private routeBranchService: RouteBranchService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRouteBranches();
    }

    load(id) {
        this.routeBranchService.find(id).subscribe((routeBranch) => {
            this.routeBranch = routeBranch;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRouteBranches() {
        this.eventSubscriber = this.eventManager.subscribe(
            'routeBranchListModification',
            (response) => this.load(this.routeBranch.id)
        );
    }
}
