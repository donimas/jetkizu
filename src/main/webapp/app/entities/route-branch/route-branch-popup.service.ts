import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { RouteBranch } from './route-branch.model';
import { RouteBranchService } from './route-branch.service';
import {JetkizuRouteService} from '../jetkizu-route/jetkizu-route.service';
import {JetkizuRoute} from '../jetkizu-route/jetkizu-route.model';

@Injectable()
export class RouteBranchPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private routeBranchService: RouteBranchService,
        private jetkizuRouteService: JetkizuRouteService,

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any, routeId?: number): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.routeBranchService.find(id).subscribe((routeBranch) => {
                    this.ngbModalRef = this.routeBranchModalRef(component, routeBranch);
                    resolve(this.ngbModalRef);
                });
            } else if (routeId) {
                this.jetkizuRouteService.find(routeId).subscribe((jetkizuRoute) => {
                    let routeBranch = new RouteBranch();
                    routeBranch.jetkizuRoute = jetkizuRoute;
                    this.ngbModalRef = this.routeBranchModalRef(component, routeBranch);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.routeBranchModalRef(component, new RouteBranch());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    routeBranchModalRef(component: Component, routeBranch: RouteBranch): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.routeBranch = routeBranch;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
