import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { RouteBranch } from './route-branch.model';
import { ResponseWrapper, createRequestOption } from '../../shared';
import {MapService} from '../../shared/map/map.service';

@Injectable()
export class RouteBranchService {

    private resourceUrl = SERVER_API_URL + 'api/route-branches';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/route-branches';

    constructor(private http: Http,
                private mapService: MapService
                ) { }

    create(routeBranch: RouteBranch): Observable<RouteBranch> {
        const copy = this.convert(routeBranch);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    addNewToRoute(routeBranch: RouteBranch): Observable<RouteBranch> {
        const copy = this.convert(routeBranch);
        return this.http.post(`${this.resourceUrl}/add-new-to-route`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    addToRoute(routeBranch: RouteBranch): Observable<RouteBranch> {
        const copy = this.convert(routeBranch);
        return this.http.post(`${this.resourceUrl}/add-to-route`, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(routeBranch: RouteBranch): Observable<RouteBranch> {
        const copy = this.convert(routeBranch);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<RouteBranch> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    getAllByRoute(id:number, req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/route/${id}`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    getAllBacklog(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(`${this.resourceUrl}/backlog`, options)
            .map((res: Response) => this.convertResponse(res));
    }

    drawBranchesDirection(branches: RouteBranch[], map: any, colorPosition: number) {
        branches.forEach((branch) => {

            // 1 - blue - in process
            // 3 - green - driving
            // 0 - red - skipped

            this.mapService.addMapboxglMarker(
                map,
                [branch.endTo.lon, branch.endTo.lat],
                'normal-5',
                branch.id,
                branch.order - 1 +''
                );

            this.mapService.drawDirection(
                [branch.startFrom.lon, branch.startFrom.lat],
                [branch.endTo.lon, branch.endTo.lat],
                branch.id,
                map, 1)
                .then((activity: any) => {
                    console.log('activity =>');
                    console.log(activity);
                });

            if(branch.nextStop && branch.nextStop.flagGarage) {
                this.mapService.drawDirection(
                    [branch.endTo.lon, branch.endTo.lat],
                    [branch.nextStop.lon, branch.nextStop.lat],
                    'garage-'+branch.id,
                    map, 1)
                    .then((activity: any) => {
                        console.log(activity);
                    });
            }
        });
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RouteBranch.
     */
    private convertItemFromServer(json: any): RouteBranch {
        const entity: RouteBranch = Object.assign(new RouteBranch(), json);
        return entity;
    }

    /**
     * Convert a RouteBranch to a JSON which can be sent to the server.
     */
    private convert(routeBranch: RouteBranch): RouteBranch {
        const copy: RouteBranch = Object.assign({}, routeBranch);
        return copy;
    }
}
