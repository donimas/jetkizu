import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RouteBranch } from './route-branch.model';
import { RouteBranchPopupService } from './route-branch-popup.service';
import { RouteBranchService } from './route-branch.service';

@Component({
    selector: 'jhi-route-branch-delete-dialog',
    templateUrl: './route-branch-delete-dialog.component.html'
})
export class RouteBranchDeleteDialogComponent {

    routeBranch: RouteBranch;

    constructor(
        private routeBranchService: RouteBranchService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.routeBranchService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                // name: 'routeBranchListModification',
                name: 'jetkizuListModification',
                content: 'Deleted an routeBranch'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-route-branch-delete-popup',
    template: ''
})
export class RouteBranchDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private routeBranchPopupService: RouteBranchPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.routeBranchPopupService
                .open(RouteBranchDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
