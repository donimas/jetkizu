import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RouteBranchComponent } from './route-branch.component';
import { RouteBranchDetailComponent } from './route-branch-detail.component';
import { RouteBranchPopupComponent } from './route-branch-dialog.component';
import { RouteBranchDeletePopupComponent } from './route-branch-delete-dialog.component';

export const routeBranchRoute: Routes = [
    {
        path: 'route-branch',
        component: RouteBranchComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'route-branch/:id',
        component: RouteBranchDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranch.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const routeBranchPopupRoute: Routes = [
    {
        path: 'route-branch-new',
        component: RouteBranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-branch/:id/edit',
        component: RouteBranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-branch/:routeId/add',
        component: RouteBranchPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-branch/:id/delete',
        component: RouteBranchDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeBranch.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
