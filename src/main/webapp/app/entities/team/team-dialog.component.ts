import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Team } from './team.model';
import { TeamPopupService } from './team-popup.service';
import { TeamService } from './team.service';
import { Area, AreaService } from '../area';
import { ResponseWrapper } from '../../shared';
import {User} from '../../shared/user/user.model';
import {UserService} from '../../shared/user/user.service';
import {TeamMember} from '../team-member/team-member.model';
import { CELL_PHONE_MASK } from '../../app.constants';
import {TeamMemberService} from '../team-member/team-member.service';
import {VehicleService} from '../vehicle/vehicle.service';
import {Vehicle} from '../vehicle/vehicle.model';

@Component({
    selector: 'jhi-team-dialog',
    templateUrl: './team-dialog.component.html'
})
export class TeamDialogComponent implements OnInit {

    team: Team;
    isSaving: boolean;
    leads: User[];
    areas: Area[];
    teamMembers: TeamMember[];
    teamMember: TeamMember;
    cellPhoneMask = CELL_PHONE_MASK;
    vehicles: Vehicle[];
    memberValidationError = {
        'message': 'error.member-not-valid'
    };

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private teamService: TeamService,
        private areaService: AreaService,
        private eventManager: JhiEventManager,
        private userService: UserService,
        private teamMemberService: TeamMemberService,
        private vehicleService: VehicleService,
    ) {
        this.teamMember = new TeamMember();
        this.teamMembers = [];
    }

    ngOnInit() {
        this.isSaving = false;
        this.loadLeads();
        this.loadTeamMembers(this.team.id);
        this.loadVehicles();
        this.areaService.query()
            .subscribe((res: ResponseWrapper) => { this.areas = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    loadLeads() {
        this.userService.leads().subscribe(
            (res: ResponseWrapper) => this.onSuccess(res.json),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    loadTeamMembers(id) {
        if(!id) {
            return;
        }
        this.teamMemberService.findByTeamId(id).subscribe(
            (res: ResponseWrapper) => this.teamMembers = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        )
    }

    loadVehicles() {
        this.vehicleService.query(null).subscribe(
            (res: ResponseWrapper) => this.vehicles = res.json,
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.team.teamMembers = this.teamMembers;
        console.log(this.team);
        this.isSaving = true;
        if (this.team.id !== undefined) {
            this.subscribeToSaveResponse(
                this.teamService.update(this.team));
        } else {
            this.subscribeToSaveResponse(
                this.teamService.create(this.team));
        }
    }

    private subscribeToSaveResponse(result: Observable<Team>) {
        result.subscribe((res: Team) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: Team) {
        this.eventManager.broadcast({ name: 'teamListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onSuccess(data) {
        console.log('leads:');
        console.log(data);
        this.leads = data;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackLeadById(index: number, item: User) {
        return item.id;
    }
    trackAreaById(index: number, item: Area) {
        return item.id;
    }
    trackVehicleById(index: number, item: Vehicle) {
        return item.id;
    }

    addTeamMember(member) {
        if(!this.isMemberValid(member)) {
            this.onError(this.memberValidationError);
            return;
        }
        const i = this.teamMembers.indexOf(member);
        if(i === -1) {
            this.teamMembers.push(member);
        }
        this.teamMember = new TeamMember();
    }

    isMemberValid(member) {
        let flag = true;
        if(!member.fio || member.fio === '') {
            flag = false;
        }
        if(member.cellPhone.indexOf('_') !== -1) {
            flag = false;
        }
        if(!member.identifier || member.identifier === '') {
            flag = false;
        }
        return flag;
    }

    removeTeamMember(member) {
        console.log(member);
        if(member.id) {
            member.isGonnaDelete = true;
            // let timer = Observable.timer(1,1);
            // timer.subscribe((t) => this.resetRemoveButton(t));
            setTimeout(function() {
                member.isGonnaDelete = false;
            }, 3000);
            return;
        }
        const i = this.teamMembers.indexOf(member);
        this.teamMembers.splice(i, 1);
    }

    resetRemoveButton(e) {
        console.log(e);
    }

    setTeamMemberFlagDeleted(member) {
        console.log(member);
        if(!member.id) {
            return;
        }
        this.teamMemberService.delete(member.id).subscribe(
            (res: ResponseWrapper) => this.loadTeamMembers(this.team.id),
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    editTeamMember(member) {
        console.log(member);
        this.teamMember = member;
    }
}

@Component({
    selector: 'jhi-team-popup',
    template: ''
})
export class TeamPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private teamPopupService: TeamPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.teamPopupService
                    .open(TeamDialogComponent as Component, params['id']);
            } else {
                this.teamPopupService
                    .open(TeamDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
