import { BaseEntity } from './../../shared';
import {Area} from '../area/area.model';
import {User} from '../../shared/user/user.model';
import {TeamMember} from '../team-member/team-member.model';
import {Vehicle} from '../vehicle/vehicle.model';

export class Team implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public lead?: User,
        public teamMembers?: TeamMember[],
        public vehicle?: Vehicle,
    ) {
    }
}
