import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { CsvFieldComponent } from './csv-field.component';
import { CsvFieldDetailComponent } from './csv-field-detail.component';
import { CsvFieldPopupComponent } from './csv-field-dialog.component';
import { CsvFieldDeletePopupComponent } from './csv-field-delete-dialog.component';

@Injectable()
export class CsvFieldResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const csvFieldRoute: Routes = [
    {
        path: 'csv-field',
        component: CsvFieldComponent,
        resolve: {
            'pagingParams': CsvFieldResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.csvField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'csv-field/:id',
        component: CsvFieldDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.csvField.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const csvFieldPopupRoute: Routes = [
    {
        path: 'csv-field-new',
        component: CsvFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.csvField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csv-field/:id/edit',
        component: CsvFieldPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.csvField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'csv-field/:id/delete',
        component: CsvFieldDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.csvField.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
