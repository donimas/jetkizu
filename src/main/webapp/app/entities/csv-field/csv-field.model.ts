import { BaseEntity } from './../../shared';

export const enum CsvFieldType {
    'ORDER'
}

export class CsvField implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public code?: string,
        public description?: string,
        public type?: CsvFieldType,
    ) {
    }
}
