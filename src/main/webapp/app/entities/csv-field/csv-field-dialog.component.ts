import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CsvField } from './csv-field.model';
import { CsvFieldPopupService } from './csv-field-popup.service';
import { CsvFieldService } from './csv-field.service';

@Component({
    selector: 'jhi-csv-field-dialog',
    templateUrl: './csv-field-dialog.component.html'
})
export class CsvFieldDialogComponent implements OnInit {

    csvField: CsvField;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private csvFieldService: CsvFieldService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.csvField.id !== undefined) {
            this.subscribeToSaveResponse(
                this.csvFieldService.update(this.csvField));
        } else {
            this.subscribeToSaveResponse(
                this.csvFieldService.create(this.csvField));
        }
    }

    private subscribeToSaveResponse(result: Observable<CsvField>) {
        result.subscribe((res: CsvField) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: CsvField) {
        this.eventManager.broadcast({ name: 'csvFieldListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-csv-field-popup',
    template: ''
})
export class CsvFieldPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private csvFieldPopupService: CsvFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.csvFieldPopupService
                    .open(CsvFieldDialogComponent as Component, params['id']);
            } else {
                this.csvFieldPopupService
                    .open(CsvFieldDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
