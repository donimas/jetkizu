import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    CsvFieldService,
    CsvFieldPopupService,
    CsvFieldComponent,
    CsvFieldDetailComponent,
    CsvFieldDialogComponent,
    CsvFieldPopupComponent,
    CsvFieldDeletePopupComponent,
    CsvFieldDeleteDialogComponent,
    csvFieldRoute,
    csvFieldPopupRoute,
    CsvFieldResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...csvFieldRoute,
    ...csvFieldPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CsvFieldComponent,
        CsvFieldDetailComponent,
        CsvFieldDialogComponent,
        CsvFieldDeleteDialogComponent,
        CsvFieldPopupComponent,
        CsvFieldDeletePopupComponent,
    ],
    entryComponents: [
        CsvFieldComponent,
        CsvFieldDialogComponent,
        CsvFieldPopupComponent,
        CsvFieldDeleteDialogComponent,
        CsvFieldDeletePopupComponent,
    ],
    providers: [
        CsvFieldService,
        CsvFieldPopupService,
        CsvFieldResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuCsvFieldModule {}
