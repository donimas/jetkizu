import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CsvField } from './csv-field.model';
import { CsvFieldPopupService } from './csv-field-popup.service';
import { CsvFieldService } from './csv-field.service';

@Component({
    selector: 'jhi-csv-field-delete-dialog',
    templateUrl: './csv-field-delete-dialog.component.html'
})
export class CsvFieldDeleteDialogComponent {

    csvField: CsvField;

    constructor(
        private csvFieldService: CsvFieldService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.csvFieldService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'csvFieldListModification',
                content: 'Deleted an csvField'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-csv-field-delete-popup',
    template: ''
})
export class CsvFieldDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private csvFieldPopupService: CsvFieldPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.csvFieldPopupService
                .open(CsvFieldDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
