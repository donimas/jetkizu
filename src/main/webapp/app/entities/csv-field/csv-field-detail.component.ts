import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { CsvField } from './csv-field.model';
import { CsvFieldService } from './csv-field.service';

@Component({
    selector: 'jhi-csv-field-detail',
    templateUrl: './csv-field-detail.component.html'
})
export class CsvFieldDetailComponent implements OnInit, OnDestroy {

    csvField: CsvField;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private csvFieldService: CsvFieldService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCsvFields();
    }

    load(id) {
        this.csvFieldService.find(id).subscribe((csvField) => {
            this.csvField = csvField;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCsvFields() {
        this.eventSubscriber = this.eventManager.subscribe(
            'csvFieldListModification',
            (response) => this.load(this.csvField.id)
        );
    }
}
