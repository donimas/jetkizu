import { BaseEntity } from './../../shared';

export class Area implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public description?: string,
        public areaNodes?: BaseEntity[],
    ) {
    }
}
