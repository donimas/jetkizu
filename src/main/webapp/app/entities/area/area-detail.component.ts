import {Component, OnInit, OnDestroy, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';

import { Area } from './area.model';
import { AreaService } from './area.service';
import {AreaNodeService} from '../area-node/area-node.service';
import {ResponseWrapper} from '../../shared/model/response-wrapper.model';
import {AreaNode} from '../area-node/area-node.model';
import {MapService} from '../../shared/map/map.service';

@Component({
    selector: 'jhi-area-detail',
    templateUrl: './area-detail.component.html'
})
export class AreaDetailComponent implements OnInit, AfterViewInit, OnDestroy {

    area: Area;
    areaNodes: AreaNode[];
    private subscription: Subscription;
    private eventSubscriber: Subscription;
    map: any;
    areaId: number;

    constructor(
        private areaNodeService: AreaNodeService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private areaService: AreaService,
        private route: ActivatedRoute,
        private mapService: MapService
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.areaId = params['id'];
            this.load(this.areaId);
        });
        this.registerChangeInAreas();
        this.registerChangeInAreaNodes();
    }

    load(id) {
        this.areaService.find(id).subscribe((area) => {
            this.area = area;
        });
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.map = this.mapService.showMapboxglMap('areaMap', 10);
            this.map.on('load', () => this.loadAll(this.areaId));

        }, 1000);
    }

    loadAll(id) {
        this.areaNodeService.findByAreaId(id, null).subscribe(
            (res: ResponseWrapper) => {
                this.areaNodes = res.json;
                this.areaNodeService.drawAreaPolygons(this.areaNodes, this.map);
                this.drawAreaPolygons(this.areaNodes);
            },
            (res: ResponseWrapper) => this.onError(res.json)
        );
    }

    drawAreaPolygons(areaNodes) {
        let featureCollection = [];
        areaNodes
            .filter(areaNode => areaNode.polygon != null && areaNode.polygon.length > 1)
            .forEach(areaNode => {
                featureCollection.push(
                    this.mapService.generateFeature([areaNode.polygon], 'Polygon', areaNode.name, areaNode.id)
                );
            });
        const data = this.mapService.generateFeatureCollection(featureCollection);
        this.mapService.createRoute(this.map, 'area', 'geojson', data);
        this.mapService.setPaintProperty(this.map, 'area', 'areaFill', 'fill');
        areaNodes
            .forEach(areaNode => {
                this.mapService.addSymbol(this.map, areaNode.id+'', 'area', areaNode.name, 'id', areaNode.id);
            });
    }

    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAreas() {
        this.eventSubscriber = this.eventManager.subscribe(
            'areaListModification',
            (response) => this.load(this.area.id)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    registerChangeInAreaNodes() {
        this.eventSubscriber = this.eventManager.subscribe('areaNodeListModification', (response) => this.loadAll(this.area.id));
    }

    mouseEnter(areaNodeId, latlng) {
        this.map.setFilter("area-fills-selected", ['==', 'id', areaNodeId]);
        this.mapService.flyTo(this.map, latlng, 12);
    }
    mouseLeave(areaNodeId) {
        this.map.setFilter("area-fills-selected", ['==', 'id', '']);
    }
}
