import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JetkizuSharedModule } from '../../shared';
import {
    RouteOptimizationService,
    RouteOptimizationPopupService,
    RouteOptimizationComponent,
    RouteOptimizationDetailComponent,
    RouteOptimizationDialogComponent,
    RouteOptimizationPopupComponent,
    RouteOptimizationDeletePopupComponent,
    RouteOptimizationDeleteDialogComponent,
    routeOptimizationRoute,
    routeOptimizationPopupRoute,
} from './';

const ENTITY_STATES = [
    ...routeOptimizationRoute,
    ...routeOptimizationPopupRoute,
];

@NgModule({
    imports: [
        JetkizuSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        RouteOptimizationComponent,
        RouteOptimizationDetailComponent,
        RouteOptimizationDialogComponent,
        RouteOptimizationDeleteDialogComponent,
        RouteOptimizationPopupComponent,
        RouteOptimizationDeletePopupComponent,
    ],
    entryComponents: [
        RouteOptimizationComponent,
        RouteOptimizationDialogComponent,
        RouteOptimizationPopupComponent,
        RouteOptimizationDeleteDialogComponent,
        RouteOptimizationDeletePopupComponent,
    ],
    providers: [
        RouteOptimizationService,
        RouteOptimizationPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JetkizuRouteOptimizationModule {}
