import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { RouteOptimization } from './route-optimization.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class RouteOptimizationService {

    private resourceUrl = SERVER_API_URL + 'api/route-optimizations';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/route-optimizations';

    constructor(private http: Http) { }

    create(routeOptimization: RouteOptimization): Observable<RouteOptimization> {
        const copy = this.convert(routeOptimization);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(routeOptimization: RouteOptimization): Observable<RouteOptimization> {
        const copy = this.convert(routeOptimization);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<RouteOptimization> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to RouteOptimization.
     */
    private convertItemFromServer(json: any): RouteOptimization {
        const entity: RouteOptimization = Object.assign(new RouteOptimization(), json);
        return entity;
    }

    /**
     * Convert a RouteOptimization to a JSON which can be sent to the server.
     */
    private convert(routeOptimization: RouteOptimization): RouteOptimization {
        const copy: RouteOptimization = Object.assign({}, routeOptimization);
        return copy;
    }
}
