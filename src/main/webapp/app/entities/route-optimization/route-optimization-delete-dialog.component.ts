import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RouteOptimization } from './route-optimization.model';
import { RouteOptimizationPopupService } from './route-optimization-popup.service';
import { RouteOptimizationService } from './route-optimization.service';

@Component({
    selector: 'jhi-route-optimization-delete-dialog',
    templateUrl: './route-optimization-delete-dialog.component.html'
})
export class RouteOptimizationDeleteDialogComponent {

    routeOptimization: RouteOptimization;

    constructor(
        private routeOptimizationService: RouteOptimizationService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.routeOptimizationService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'routeOptimizationListModification',
                content: 'Deleted an routeOptimization'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-route-optimization-delete-popup',
    template: ''
})
export class RouteOptimizationDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private routeOptimizationPopupService: RouteOptimizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.routeOptimizationPopupService
                .open(RouteOptimizationDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
