import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RouteOptimization } from './route-optimization.model';
import { RouteOptimizationPopupService } from './route-optimization-popup.service';
import { RouteOptimizationService } from './route-optimization.service';
import { JetkizuRoute, JetkizuRouteService } from '../jetkizu-route';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-route-optimization-dialog',
    templateUrl: './route-optimization-dialog.component.html'
})
export class RouteOptimizationDialogComponent implements OnInit {

    routeOptimization: RouteOptimization;
    isSaving: boolean;

    jetkizuroutes: JetkizuRoute[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private routeOptimizationService: RouteOptimizationService,
        private jetkizuRouteService: JetkizuRouteService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.jetkizuRouteService.query()
            .subscribe((res: ResponseWrapper) => { this.jetkizuroutes = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.routeOptimization.id !== undefined) {
            this.subscribeToSaveResponse(
                this.routeOptimizationService.update(this.routeOptimization));
        } else {
            this.subscribeToSaveResponse(
                this.routeOptimizationService.create(this.routeOptimization));
        }
    }

    private subscribeToSaveResponse(result: Observable<RouteOptimization>) {
        result.subscribe((res: RouteOptimization) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: RouteOptimization) {
        this.eventManager.broadcast({ name: 'routeOptimizationListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackJetkizuRouteById(index: number, item: JetkizuRoute) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-route-optimization-popup',
    template: ''
})
export class RouteOptimizationPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private routeOptimizationPopupService: RouteOptimizationPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.routeOptimizationPopupService
                    .open(RouteOptimizationDialogComponent as Component, params['id']);
            } else {
                this.routeOptimizationPopupService
                    .open(RouteOptimizationDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
