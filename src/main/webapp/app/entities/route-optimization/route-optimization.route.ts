import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { RouteOptimizationComponent } from './route-optimization.component';
import { RouteOptimizationDetailComponent } from './route-optimization-detail.component';
import { RouteOptimizationPopupComponent } from './route-optimization-dialog.component';
import { RouteOptimizationDeletePopupComponent } from './route-optimization-delete-dialog.component';

export const routeOptimizationRoute: Routes = [
    {
        path: 'route-optimization',
        component: RouteOptimizationComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeOptimization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'route-optimization/:id',
        component: RouteOptimizationDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeOptimization.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const routeOptimizationPopupRoute: Routes = [
    {
        path: 'route-optimization-new',
        component: RouteOptimizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeOptimization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-optimization/:id/edit',
        component: RouteOptimizationPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeOptimization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'route-optimization/:id/delete',
        component: RouteOptimizationDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jetkizuApp.routeOptimization.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
