import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { RouteOptimization } from './route-optimization.model';
import { RouteOptimizationService } from './route-optimization.service';

@Component({
    selector: 'jhi-route-optimization-detail',
    templateUrl: './route-optimization-detail.component.html'
})
export class RouteOptimizationDetailComponent implements OnInit, OnDestroy {

    routeOptimization: RouteOptimization;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private routeOptimizationService: RouteOptimizationService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRouteOptimizations();
    }

    load(id) {
        this.routeOptimizationService.find(id).subscribe((routeOptimization) => {
            this.routeOptimization = routeOptimization;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRouteOptimizations() {
        this.eventSubscriber = this.eventManager.subscribe(
            'routeOptimizationListModification',
            (response) => this.load(this.routeOptimization.id)
        );
    }
}
