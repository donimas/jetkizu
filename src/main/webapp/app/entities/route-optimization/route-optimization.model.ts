import { BaseEntity } from './../../shared';

export class RouteOptimization implements BaseEntity {
    constructor(
        public id?: number,
        public maxVehicleDistance?: number,
        public maxRouteDuration?: number,
        public maxWeight?: number,
        public maxRouteRevenue?: number,
        public maxRouteStops?: number,
        public jetkizuRoute?: BaseEntity,
    ) {
    }
}
