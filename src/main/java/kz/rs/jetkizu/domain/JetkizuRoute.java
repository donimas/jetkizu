package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.rs.jetkizu.domain.enumeration.TripType;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import kz.rs.jetkizu.domain.enumeration.JetkizuRouteStatus;

/**
 * A JetkizuRoute.
 */
@Entity
@Table(name = "jetkizu_route")
@Document(indexName = "jetkizuroute")
public class JetkizuRoute extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jetkizu_date")
    private LocalDate jetkizuDate;

    @Column(name = "started_date_time")
    private ZonedDateTime startedDateTime;

    @Column(name = "stopped_date_time")
    private ZonedDateTime stoppedDateTime;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "name")
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private JetkizuRouteStatus status;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @Column(name = "flag_stopped")
    private Boolean flagStopped;

    @Column(name = "total_plan_distance")
    private BigDecimal totalPlanDistance;

    @Column(name = "total_plan_duration")
    private BigDecimal totalPlanDuration;

    @Column(name = "total_fact_distance")
    private BigDecimal totalFactDistance;

    @Column(name = "total_fact_duration")
    private BigDecimal totalFactDuration;

    @Column(name = "service_duration")
    private Long serviceDuration;
    @Column(name = "service_duration_h_m")
    private String serviceDurationHM;

    @Column(name = "jetkizu_start_time")
    private ZonedDateTime jetkizuStartTime;
    @Column(name = "start_time_h_m")
    private String startTimeHM;

    private BigDecimal plannedTotalDistance;

    @Enumerated(EnumType.STRING)
    private TripType tripType;

    @ManyToOne
    private Team team;

    @ManyToOne
    private User startedBy;

    @ManyToOne
    private User stoppedBy;

    @ManyToOne
    private Location start;

    @ManyToOne
    private Location end;

    @OneToMany(mappedBy = "jetkizuRoute")
    @JsonIgnore
    private Set<RouteBranch> routeBranches = new HashSet<>();

    @ManyToOne
    private AreaNode areaNode;

    @ManyToOne
    private RouteOptimization routeOptimization;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getJetkizuDate() {
        return jetkizuDate;
    }

    public JetkizuRoute jetkizuDate(LocalDate jetkizuDate) {
        this.jetkizuDate = jetkizuDate;
        return this;
    }

    public void setJetkizuDate(LocalDate jetkizuDate) {
        this.jetkizuDate = jetkizuDate;
    }

    public ZonedDateTime getStartedDateTime() {
        return startedDateTime;
    }

    public JetkizuRoute startedDateTime(ZonedDateTime startedDateTime) {
        this.startedDateTime = startedDateTime;
        return this;
    }

    public void setStartedDateTime(ZonedDateTime startedDateTime) {
        this.startedDateTime = startedDateTime;
    }

    public ZonedDateTime getStoppedDateTime() {
        return stoppedDateTime;
    }

    public JetkizuRoute stoppedDateTime(ZonedDateTime stoppedDateTime) {
        this.stoppedDateTime = stoppedDateTime;
        return this;
    }

    public void setStoppedDateTime(ZonedDateTime stoppedDateTime) {
        this.stoppedDateTime = stoppedDateTime;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public JetkizuRoute createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public JetkizuRouteStatus getStatus() {
        return status;
    }

    public JetkizuRoute status(JetkizuRouteStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(JetkizuRouteStatus status) {
        this.status = status;
    }

    public Team getTeam() {
        return team;
    }

    public JetkizuRoute team(Team team) {
        this.team = team;
        return this;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public User getStartedBy() {
        return startedBy;
    }

    public JetkizuRoute startedBy(User user) {
        this.startedBy = user;
        return this;
    }

    public void setStartedBy(User user) {
        this.startedBy = user;
    }

    public User getStoppedBy() {
        return stoppedBy;
    }

    public JetkizuRoute stoppedBy(User user) {
        this.stoppedBy = user;
        return this;
    }

    public void setStoppedBy(User user) {
        this.stoppedBy = user;
    }

    public Set<RouteBranch> getRouteBranches() {
        return routeBranches;
    }

    public JetkizuRoute routeBranches(Set<RouteBranch> routeBranches) {
        this.routeBranches = routeBranches;
        return this;
    }

    public JetkizuRoute addRouteBranch(RouteBranch routeBranch) {
        this.routeBranches.add(routeBranch);
        routeBranch.setJetkizuRoute(this);
        return this;
    }

    public JetkizuRoute removeRouteBranch(RouteBranch routeBranch) {
        this.routeBranches.remove(routeBranch);
        routeBranch.setJetkizuRoute(null);
        return this;
    }

    public void setRouteBranches(Set<RouteBranch> routeBranches) {
        this.routeBranches = routeBranches;
    }

    public Boolean getFlagDeleted() {
        return flagDeleted;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public Boolean getFlagStopped() {
        return flagStopped;
    }

    public void setFlagStopped(Boolean flagStopped) {
        this.flagStopped = flagStopped;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getTotalPlanDistance() {
        return totalPlanDistance;
    }

    public void setTotalPlanDistance(BigDecimal totalPlanDistance) {
        this.totalPlanDistance = totalPlanDistance;
    }

    public BigDecimal getTotalPlanDuration() {
        return totalPlanDuration;
    }

    public void setTotalPlanDuration(BigDecimal totalPlanDuration) {
        this.totalPlanDuration = totalPlanDuration;
    }

    public BigDecimal getTotalFactDistance() {
        return totalFactDistance;
    }

    public void setTotalFactDistance(BigDecimal totalFactDistance) {
        this.totalFactDistance = totalFactDistance;
    }

    public BigDecimal getTotalFactDuration() {
        return totalFactDuration;
    }

    public void setTotalFactDuration(BigDecimal totalFactDuration) {
        this.totalFactDuration = totalFactDuration;
    }

    public Long getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(Long serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    public BigDecimal getPlannedTotalDistance() {
        return plannedTotalDistance;
    }

    public void setPlannedTotalDistance(BigDecimal plannedTotalDistance) {
        this.plannedTotalDistance = plannedTotalDistance;
    }

    public AreaNode getAreaNode() {
        return areaNode;
    }

    public void setAreaNode(AreaNode areaNode) {
        this.areaNode = areaNode;
    }

    public ZonedDateTime getJetkizuStartTime() {
        return jetkizuStartTime;
    }

    public void setJetkizuStartTime(ZonedDateTime jetkizuStartTime) {
        this.jetkizuStartTime = jetkizuStartTime;
    }

    public RouteOptimization getRouteOptimization() {
        return routeOptimization;
    }

    public void setRouteOptimization(RouteOptimization routeOptimization) {
        this.routeOptimization = routeOptimization;
    }

    public TripType getTripType() {
        return tripType;
    }

    public void setTripType(TripType tripType) {
        this.tripType = tripType;
    }

    public Location getStart() {
        return start;
    }

    public void setStart(Location start) {
        this.start = start;
    }

    public Location getEnd() {
        return end;
    }

    public void setEnd(Location end) {
        this.end = end;
    }

    public String getServiceDurationHM() {
        return serviceDurationHM;
    }

    public void setServiceDurationHM(String serviceDurationHM) {
        this.serviceDurationHM = serviceDurationHM;
    }

    public String getStartTimeHM() {
        return startTimeHM;
    }

    public void setStartTimeHM(String startTimeHM) {
        this.startTimeHM = startTimeHM;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JetkizuRoute jetkizuRoute = (JetkizuRoute) o;
        if (jetkizuRoute.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jetkizuRoute.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JetkizuRoute{" +
            "id=" + getId() +
            ", jetkizuDate='" + getJetkizuDate() + "'" +
            ", startedDateTime='" + getStartedDateTime() + "'" +
            ", stoppedDateTime='" + getStoppedDateTime() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", flagDeleted='" + getFlagDeleted() + "'" +
            ", flagStopped='" + getFlagStopped() + "'" +
            ", name='" + getName() + "'" +
            ", totalPlanDuration='" + getTotalPlanDuration() + "'" +
            ", totalPlanDistance='" + getTotalPlanDistance() + "'" +
            ", totalFactDistance='" + getTotalFactDistance() + "'" +
            ", totalFactDuration='" + getTotalFactDuration() + "'" +
            ", serviceDuration='" + getServiceDuration() + "'" +
            ", serviceDurationHM='" + getServiceDurationHM() + "'" +
            ", startTimeHM='" + getStartTimeHM() + "'" +
            ", plannedTotalDistance='" + getPlannedTotalDistance() + "'" +
            "}";
    }
}
