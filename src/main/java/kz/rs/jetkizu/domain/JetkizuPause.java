package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

import kz.rs.jetkizu.domain.enumeration.JetkizuPauseType;

/**
 * A JetkizuPause.
 */
@Entity
@Table(name = "jetkizu_pause")
@Document(indexName = "jetkizupause")
public class JetkizuPause extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "duration", precision=10, scale=2)
    private BigDecimal duration;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private JetkizuPauseType type;

    @Column(name = "description")
    private String description;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @JsonIgnore
    @ManyToOne(optional = false)
    private Jetkizu jetkizu;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getDuration() {
        return duration;
    }

    public JetkizuPause duration(BigDecimal duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(BigDecimal duration) {
        this.duration = duration;
    }

    public JetkizuPauseType getType() {
        return type;
    }

    public JetkizuPause type(JetkizuPauseType type) {
        this.type = type;
        return this;
    }

    public void setType(JetkizuPauseType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public JetkizuPause description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public JetkizuPause createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public JetkizuPause flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public Jetkizu getJetkizu() {
        return jetkizu;
    }

    public JetkizuPause jetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
        return this;
    }

    public void setJetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JetkizuPause jetkizuPause = (JetkizuPause) o;
        if (jetkizuPause.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jetkizuPause.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JetkizuPause{" +
            "id=" + getId() +
            ", duration='" + getDuration() + "'" +
            ", type='" + getType() + "'" +
            ", description='" + getDescription() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}
