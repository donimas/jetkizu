package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import kz.rs.jetkizu.domain.enumeration.RouteBranchType;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A RouteBranch.
 */
@Entity
@Table(name = "route_branch")
@Document(indexName = "routebranch")
public class RouteBranch extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_order")
    private Float order;

    @ManyToOne
    private Jetkizu jetkizu;

    @ManyToOne
    private JetkizuRoute jetkizuRoute;

    @ManyToOne
    private RouteBranchStatusHistory routeBranchStatusHistory;

    @ManyToOne
    private JetkizuActivity jetkizuActivity;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @Enumerated(EnumType.STRING)
    @Column(name = "route_branch_type")
    private RouteBranchType type;

    @ManyToOne
    private Customer customer;

    @ManyToOne
    private Location startFrom;

    @ManyToOne
    private Location endTo;

    @ManyToOne(cascade = CascadeType.ALL)
    private Location nextStop;

    @Column(name = "osm_id")
    private Long osmId;

    @Column(name = "place_id")
    private Long placeId;

    @Column(name = "osm_type")
    private String osmType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getOrder() {
        return order;
    }

    public RouteBranch order(Float order) {
        this.order = order;
        return this;
    }

    public void setOrder(Float order) {
        this.order = order;
    }

    public Jetkizu getJetkizu() {
        return jetkizu;
    }

    public RouteBranch jetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
        return this;
    }

    public void setJetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
    }

    public JetkizuRoute getJetkizuRoute() {
        return jetkizuRoute;
    }

    public RouteBranch jetkizuRoute(JetkizuRoute jetkizuRoute) {
        this.jetkizuRoute = jetkizuRoute;
        return this;
    }

    public void setJetkizuRoute(JetkizuRoute jetkizuRoute) {
        this.jetkizuRoute = jetkizuRoute;
    }

    public RouteBranchStatusHistory getRouteBranchStatusHistory() {
        return routeBranchStatusHistory;
    }

    public RouteBranch routeBranchStatusHistory(RouteBranchStatusHistory routeBranchStatusHistory) {
        this.routeBranchStatusHistory = routeBranchStatusHistory;
        return this;
    }

    public void setRouteBranchStatusHistory(RouteBranchStatusHistory routeBranchStatusHistory) {
        this.routeBranchStatusHistory = routeBranchStatusHistory;
    }

    public JetkizuActivity getJetkizuActivity() {
        return jetkizuActivity;
    }

    public RouteBranch jetkizuActivity(JetkizuActivity jetkizuActivity) {
        this.jetkizuActivity = jetkizuActivity;
        return this;
    }

    public void setJetkizuActivity(JetkizuActivity jetkizuActivity) {
        this.jetkizuActivity = jetkizuActivity;
    }

    public Boolean getFlagDeleted() {
        return flagDeleted;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public Location getStartFrom() {
        return startFrom;
    }

    public void setStartFrom(Location startFrom) {
        this.startFrom = startFrom;
    }

    public Location getEndTo() {
        return endTo;
    }

    public void setEndTo(Location endTo) {
        this.endTo = endTo;
    }

    public RouteBranchType getType() {
        return type;
    }

    public void setType(RouteBranchType type) {
        this.type = type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Long getOsmId() {
        return osmId;
    }

    public void setOsmId(Long osmId) {
        this.osmId = osmId;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public String getOsmType() {
        return osmType;
    }

    public void setOsmType(String osmType) {
        this.osmType = osmType;
    }

    public Location getNextStop() {
        return nextStop;
    }

    public void setNextStop(Location nextStop) {
        this.nextStop = nextStop;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RouteBranch routeBranch = (RouteBranch) o;
        if (routeBranch.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), routeBranch.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RouteBranch{" +
            "id=" + getId() +
            ", order='" + getOrder() + "'" +
            ", flagDeleted='" + getFlagDeleted() + "'" +
            ", startFrom='" + getStartFrom() + "'" +
            ", endTo='" + getEndTo() + "'" +
            ", next='" + getNextStop() + "'" +
            ", jetkizu='" + getJetkizu() + "'" +
            ", jetkizuActivity='" + getJetkizuActivity() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
