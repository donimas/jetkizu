package kz.rs.jetkizu.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A AttachmentFile.
 */
@Entity
@Table(name = "attachment_file")
@Document(indexName = "attachmentfile")
public class AttachmentFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_uid")
    private String uid;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "content_type")
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "file_desc")
    private String fileDesc;

    @Column(name = "file_hash")
    private String fileHash;

    @Column(name = "container_class")
    private String containerClass;

    @Column(name = "container_id")
    private String containerId;

    @Column(name = "document_category")
    private String documentCategory;

    @Column(name = "upload_date_time")
    private ZonedDateTime uploadDateTime;

    @Lob
    @Column(name = "jhi_file")
    private byte[] file;

    @Column(name = "jhi_file_content_type")
    private String fileContentType;

    @Lob
    @Column(name = "image_file")
    private byte[] imageFile;

    @Column(name = "image_file_content_type")
    private String imageFileContentType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public AttachmentFile uid(String uid) {
        this.uid = uid;
        return this;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFileName() {
        return fileName;
    }

    public AttachmentFile fileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public AttachmentFile contentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public AttachmentFile fileSize(Long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public AttachmentFile fileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
        return this;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public String getFileHash() {
        return fileHash;
    }

    public AttachmentFile fileHash(String fileHash) {
        this.fileHash = fileHash;
        return this;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public String getContainerClass() {
        return containerClass;
    }

    public AttachmentFile containerClass(String containerClass) {
        this.containerClass = containerClass;
        return this;
    }

    public void setContainerClass(String containerClass) {
        this.containerClass = containerClass;
    }

    public String getContainerId() {
        return containerId;
    }

    public AttachmentFile containerId(String containerId) {
        this.containerId = containerId;
        return this;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getDocumentCategory() {
        return documentCategory;
    }

    public AttachmentFile documentCategory(String documentCategory) {
        this.documentCategory = documentCategory;
        return this;
    }

    public void setDocumentCategory(String documentCategory) {
        this.documentCategory = documentCategory;
    }

    public ZonedDateTime getUploadDateTime() {
        return uploadDateTime;
    }

    public AttachmentFile uploadDateTime(ZonedDateTime uploadDateTime) {
        this.uploadDateTime = uploadDateTime;
        return this;
    }

    public void setUploadDateTime(ZonedDateTime uploadDateTime) {
        this.uploadDateTime = uploadDateTime;
    }

    public byte[] getFile() {
        return file;
    }

    public AttachmentFile file(byte[] file) {
        this.file = file;
        return this;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public AttachmentFile fileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
        return this;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public byte[] getImageFile() {
        return imageFile;
    }

    public AttachmentFile imageFile(byte[] imageFile) {
        this.imageFile = imageFile;
        return this;
    }

    public void setImageFile(byte[] imageFile) {
        this.imageFile = imageFile;
    }

    public String getImageFileContentType() {
        return imageFileContentType;
    }

    public AttachmentFile imageFileContentType(String imageFileContentType) {
        this.imageFileContentType = imageFileContentType;
        return this;
    }

    public void setImageFileContentType(String imageFileContentType) {
        this.imageFileContentType = imageFileContentType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AttachmentFile attachmentFile = (AttachmentFile) o;
        if (attachmentFile.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), attachmentFile.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AttachmentFile{" +
            "id=" + getId() +
            ", uid='" + getUid() + "'" +
            ", fileName='" + getFileName() + "'" +
            ", contentType='" + getContentType() + "'" +
            ", fileSize='" + getFileSize() + "'" +
            ", fileDesc='" + getFileDesc() + "'" +
            ", fileHash='" + getFileHash() + "'" +
            ", containerClass='" + getContainerClass() + "'" +
            ", containerId='" + getContainerId() + "'" +
            ", documentCategory='" + getDocumentCategory() + "'" +
            ", uploadDateTime='" + getUploadDateTime() + "'" +
            ", file='" + getFile() + "'" +
            ", fileContentType='" + fileContentType + "'" +
            ", imageFile='" + getImageFile() + "'" +
            ", imageFileContentType='" + imageFileContentType + "'" +
            "}";
    }
}
