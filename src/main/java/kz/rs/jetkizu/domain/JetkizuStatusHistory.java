package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;

import kz.rs.jetkizu.domain.enumeration.FailType;

/**
 * A JetkizuStatusHistory.
 */
@Entity
@Table(name = "jetkizu_status_history")
@Document(indexName = "jetkizustatushistory")
public class JetkizuStatusHistory extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private JetkizuStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "fail_type")
    private FailType failType;

    @JsonIgnore
    @ManyToOne(optional = false)
    private Jetkizu jetkizu;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public JetkizuStatusHistory createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public JetkizuStatusHistory flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public JetkizuStatus getStatus() {
        return status;
    }

    public JetkizuStatusHistory status(JetkizuStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(JetkizuStatus status) {
        this.status = status;
    }

    public FailType getFailType() {
        return failType;
    }

    public JetkizuStatusHistory failType(FailType failType) {
        this.failType = failType;
        return this;
    }

    public void setFailType(FailType failType) {
        this.failType = failType;
    }

    public Jetkizu getJetkizu() {
        return jetkizu;
    }

    public JetkizuStatusHistory jetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
        return this;
    }

    public void setJetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JetkizuStatusHistory jetkizuStatusHistory = (JetkizuStatusHistory) o;
        if (jetkizuStatusHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jetkizuStatusHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JetkizuStatusHistory{" +
            "id=" + getId() +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", status='" + getStatus() + "'" +
            ", failType='" + getFailType() + "'" +
            "}";
    }
}
