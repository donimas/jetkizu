package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Area.
 */
@Entity
@Table(name = "area")
@Document(indexName = "area")
public class Area extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "area_name")
    private String name;

    @Column(name = "description", columnDefinition = "text")
    private String description;

    @OneToMany(mappedBy = "area")
    @JsonIgnore
    private Set<AreaNode> areaNodes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<AreaNode> getAreaNodes() {
        return areaNodes;
    }

    public Area areaNodes(Set<AreaNode> areaNodes) {
        this.areaNodes = areaNodes;
        return this;
    }

    public Area addAreaNode(AreaNode areaNode) {
        this.areaNodes.add(areaNode);
        areaNode.setArea(this);
        return this;
    }

    public Area removeAreaNode(AreaNode areaNode) {
        this.areaNodes.remove(areaNode);
        areaNode.setArea(null);
        return this;
    }

    public void setAreaNodes(Set<AreaNode> areaNodes) {
        this.areaNodes = areaNodes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Area area = (Area) o;
        if (area.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), area.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Area{" +
            "id=" + getId() +
            "}";
    }
}
