package kz.rs.jetkizu.domain;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A JetkizuActivity.
 */
@Entity
@Table(name = "jetkizu_activity")
@Document(indexName = "jetkizuactivity")
public class JetkizuActivity extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "plan_km", precision=10, scale=2)
    private BigDecimal planKm;

    @Column(name = "fact_km", precision=10, scale=2)
    private BigDecimal factKm;

    @Column(name = "plan_duration", precision=10, scale=2)
    private BigDecimal planDuration;

    @Column(name = "fact_duration", precision=10, scale=2)
    private BigDecimal factDuration;

    @Column(name = "plan_distance_to_next")
    private BigDecimal planDistanceToNext;
    @Column(name = "plan_duration_to_next")
    private BigDecimal planDurationToNext;
    @Column(name = "fact_distance_to_next")
    private BigDecimal factDistanceToNext;
    @Column(name = "fact_duration_to_next")
    private BigDecimal factDurationToNext;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "from_lat")
    private String fromLat;

    @Column(name = "from_lon")
    private String fromLon;

    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(using = GeometryDeserializer.class)
    @Column(name = "where_is", columnDefinition = "geometry")
    private Geometry whereIs;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnore
    private RouteBranch routeBranch;

    @ManyToOne
    private JetkizuPause jetkizuPause;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPlanKm() {
        return planKm;
    }

    public JetkizuActivity planKm(BigDecimal planKm) {
        this.planKm = planKm;
        return this;
    }

    public void setPlanKm(BigDecimal planKm) {
        this.planKm = planKm;
    }

    public BigDecimal getFactKm() {
        return factKm;
    }

    public JetkizuActivity factKm(BigDecimal factKm) {
        this.factKm = factKm;
        return this;
    }

    public void setFactKm(BigDecimal factKm) {
        this.factKm = factKm;
    }

    public BigDecimal getPlanDuration() {
        return planDuration;
    }

    public JetkizuActivity planDuration(BigDecimal planDuration) {
        this.planDuration = planDuration;
        return this;
    }

    public void setPlanDuration(BigDecimal planDuration) {
        this.planDuration = planDuration;
    }

    public BigDecimal getFactDuration() {
        return factDuration;
    }

    public JetkizuActivity factDuration(BigDecimal factDuration) {
        this.factDuration = factDuration;
        return this;
    }

    public void setFactDuration(BigDecimal factDuration) {
        this.factDuration = factDuration;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public JetkizuActivity createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getFromLat() {
        return fromLat;
    }

    public JetkizuActivity fromLat(String fromLat) {
        this.fromLat = fromLat;
        return this;
    }

    public void setFromLat(String fromLat) {
        this.fromLat = fromLat;
    }

    public String getFromLon() {
        return fromLon;
    }

    public JetkizuActivity fromLon(String fromLon) {
        this.fromLon = fromLon;
        return this;
    }

    public void setFromLon(String fromLon) {
        this.fromLon = fromLon;
    }

    public RouteBranch getRouteBranch() {
        return routeBranch;
    }

    public JetkizuActivity routeBranch(RouteBranch routeBranch) {
        this.routeBranch = routeBranch;
        return this;
    }

    public void setRouteBranch(RouteBranch routeBranch) {
        this.routeBranch = routeBranch;
    }

    public JetkizuPause getJetkizuPause() {
        return jetkizuPause;
    }

    public JetkizuActivity jetkizuPause(JetkizuPause jetkizuPause) {
        this.jetkizuPause = jetkizuPause;
        return this;
    }

    public void setJetkizuPause(JetkizuPause jetkizuPause) {
        this.jetkizuPause = jetkizuPause;
    }

    public Geometry getWhereIs() {
        return whereIs;
    }

    public void setWhereIs(Geometry whereIs) {
        this.whereIs = whereIs;
    }

    public BigDecimal getPlanDistanceToNext() {
        return planDistanceToNext;
    }

    public void setPlanDistanceToNext(BigDecimal planDistanceToNext) {
        this.planDistanceToNext = planDistanceToNext;
    }

    public BigDecimal getPlanDurationToNext() {
        return planDurationToNext;
    }

    public void setPlanDurationToNext(BigDecimal planDurationToNext) {
        this.planDurationToNext = planDurationToNext;
    }

    public BigDecimal getFactDistanceToNext() {
        return factDistanceToNext;
    }

    public void setFactDistanceToNext(BigDecimal factDistanceToNext) {
        this.factDistanceToNext = factDistanceToNext;
    }

    public BigDecimal getFactDurationToNext() {
        return factDurationToNext;
    }

    public void setFactDurationToNext(BigDecimal factDurationToNext) {
        this.factDurationToNext = factDurationToNext;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JetkizuActivity jetkizuActivity = (JetkizuActivity) o;
        if (jetkizuActivity.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jetkizuActivity.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JetkizuActivity{" +
            "id=" + getId() +
            ", planKm='" + getPlanKm() + "'" +
            ", factKm='" + getFactKm() + "'" +
            ", planDuration='" + getPlanDuration() + "'" +
            ", factDuration='" + getFactDuration() + "'" +
            ", planDurationToNext='" + getPlanDurationToNext() + "'" +
            ", planDistanceToNext='" + getPlanDistanceToNext() + "'" +
            ", factDurationToNext='" + getFactDurationToNext() + "'" +
            ", factDistanceToNext='" + getFactDistanceToNext() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", fromLat='" + getFromLat() + "'" +
            ", fromLon='" + getFromLon() + "'" +
            "}";
    }
}
