package kz.rs.jetkizu.domain;

import com.bedatadriven.jackson.datatype.jts.serialization.GeometryDeserializer;
import com.bedatadriven.jackson.datatype.jts.serialization.GeometrySerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.vividsolutions.jts.geom.Geometry;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Location.
 */
@Entity
@Table(name = "location")
@Document(indexName = "location")
public class Location extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "street", nullable = false)
    private String street;

    @NotNull
    @Column(name = "home", nullable = false)
    private String home;

    @Column(name = "flat")
    private String flat;

    @Column(name = "postindex")
    private String postindex;

    @Column(name = "lat")
    private String lat;

    @Column(name = "lon")
    private String lon;

    @Column(name = "full_address")
    private String fullAddress;

    @Column(name = "flag_garage")
    private Boolean flagGarage;

    @JsonSerialize(using = GeometrySerializer.class)
    @JsonDeserialize(using = GeometryDeserializer.class)
    @Column(name = "where_is", columnDefinition = "geometry")
    private Geometry whereIs;

    @Column(name = "osm_place_id")
    private Long placeId;

    @Column(name = "osm_type")
    private String osmType;

    @Column(name = "osm_id")
    private Long osmId;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public Location street(String street) {
        this.street = street;
        return this;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public Location home(String home) {
        this.home = home;
        return this;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getFlat() {
        return flat;
    }

    public Location flat(String flat) {
        this.flat = flat;
        return this;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getPostindex() {
        return postindex;
    }

    public Location postindex(String postindex) {
        this.postindex = postindex;
        return this;
    }

    public void setPostindex(String postindex) {
        this.postindex = postindex;
    }

    public String getLat() {
        return lat;
    }

    public Location lat(String lat) {
        this.lat = lat;
        return this;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public Location lon(String lon) {
        this.lon = lon;
        return this;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Boolean getFlagGarage() {
        return flagGarage;
    }

    public void setFlagGarage(Boolean flagGarage) {
        this.flagGarage = flagGarage;
    }

    public Geometry getWhereIs() {
        return whereIs;
    }

    public void setWhereIs(Geometry whereIs) {
        this.whereIs = whereIs;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public String getOsmType() {
        return osmType;
    }

    public void setOsmType(String osmType) {
        this.osmType = osmType;
    }

    public Long getOsmId() {
        return osmId;
    }

    public void setOsmId(Long osmId) {
        this.osmId = osmId;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        if (location.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), location.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Location{" +
            "id=" + getId() +
            ", street='" + getStreet() + "'" +
            ", home='" + getHome() + "'" +
            ", flat='" + getFlat() + "'" +
            ", postindex='" + getPostindex() + "'" +
            ", lat='" + getLat() + "'" +
            ", lon='" + getLon() + "'" +
            ", fullAddresss='" + getFullAddress() + "'" +
            ", flagGarage='" + getFlagGarage() + "'" +
            ", osmType='" + getOsmType() + "'" +
            ", placeId='" + getPlaceId() + "'" +
            ", osmId='" + getOsmId() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}
