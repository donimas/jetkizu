package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;

/**
 * A RouteBranchStatusHistory.
 */
@Entity
@Table(name = "route_branch_status_history")
@Document(indexName = "routebranchstatushistory")
public class RouteBranchStatusHistory extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private RouteBranchStatus status;

    @JsonIgnore
    @ManyToOne(optional = false)
    private RouteBranch routeBranch;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public RouteBranchStatusHistory createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public RouteBranchStatusHistory flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public RouteBranchStatus getStatus() {
        return status;
    }

    public RouteBranchStatusHistory status(RouteBranchStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(RouteBranchStatus status) {
        this.status = status;
    }

    public RouteBranch getRouteBranch() {
        return routeBranch;
    }

    public RouteBranchStatusHistory routeBranch(RouteBranch routeBranch) {
        this.routeBranch = routeBranch;
        return this;
    }

    public void setRouteBranch(RouteBranch routeBranch) {
        this.routeBranch = routeBranch;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RouteBranchStatusHistory routeBranchStatusHistory = (RouteBranchStatusHistory) o;
        if (routeBranchStatusHistory.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), routeBranchStatusHistory.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RouteBranchStatusHistory{" +
            "id=" + getId() +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
