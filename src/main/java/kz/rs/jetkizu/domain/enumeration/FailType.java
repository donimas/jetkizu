package kz.rs.jetkizu.domain.enumeration;

/**
 * The FailType enumeration.
 */
public enum FailType {
    ADDRESS_NOT_FOUND, PHONE_NOT_ALLOWED, NOT_OCCUPIED
}
