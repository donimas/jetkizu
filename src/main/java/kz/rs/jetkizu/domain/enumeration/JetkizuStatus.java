package kz.rs.jetkizu.domain.enumeration;

/**
 * The JetkizuStatus enumeration.
 */
public enum JetkizuStatus {
    BACKLOG, NOT_STARTED, IN_PROCESS, DELIVERED, FAILED
}
