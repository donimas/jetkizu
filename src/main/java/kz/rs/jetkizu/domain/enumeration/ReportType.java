package kz.rs.jetkizu.domain.enumeration;

/**
 * The ReportType enumeration.
 */
public enum ReportType {
    FULL,  PART,  EMPTY
}
