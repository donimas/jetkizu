package kz.rs.jetkizu.domain.enumeration;

/**
 * The VehicleType enumeration.
 */
public enum VehicleType {
    FURA,  GAZEL,  LARGUS,  SEDAN
}
