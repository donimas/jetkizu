package kz.rs.jetkizu.domain.enumeration;

public enum TripType {

    ROUND_TRIP, ONE_TRIP, AT_ANY_STOP, LAST_STOP

}
