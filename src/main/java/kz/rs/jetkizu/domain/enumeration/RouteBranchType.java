package kz.rs.jetkizu.domain.enumeration;

public enum RouteBranchType {
    START, PICKUP, DELIVERY, END
}
