package kz.rs.jetkizu.domain.enumeration;

/**
 * The JetkizuRouteStatus enumeration.
 */
public enum JetkizuRouteStatus {
    NOT_STARTED, STARTED, STOPPED
}
