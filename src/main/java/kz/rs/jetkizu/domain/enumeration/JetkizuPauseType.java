package kz.rs.jetkizu.domain.enumeration;

/**
 * The JetkizuPauseType enumeration.
 */
public enum JetkizuPauseType {
    DELIVER, TECHNICAL_PROBLEM, REST, OTHER
}
