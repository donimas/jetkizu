package kz.rs.jetkizu.domain.enumeration;

/**
 * The RouteBranchStatus enumeration.
 */
public enum RouteBranchStatus {
    BACKLOG, NOT_STARTED, IN_PROCESS, DRIVING, REACHED, FAILED, SKIPPED
}
