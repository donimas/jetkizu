package kz.rs.jetkizu.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import kz.rs.jetkizu.domain.enumeration.VehicleType;

/**
 * A Vehicle.
 */
@Entity
@Table(name = "vehicle")
@Document(indexName = "vehicle")
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "gov_number", nullable = false)
    private String govNumber;

    @Column(name = "tech_passport_number")
    private String techPassportNumber;

    @Column(name = "model")
    private String model;

    @Column(name = "build_year")
    private Integer buildYear;

    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type")
    private VehicleType type;

    @Column(name = "height")
    private Float height;

    @Column(name = "length")
    private Float length;

    @Column(name = "width")
    private Float width;

    @Column(name = "total_weight")
    private Float totalWeight;

    @Column(name = "delivery_weight", precision=10, scale=2)
    private BigDecimal deliveryWeight;

    @Column(name = "note")
    private String note;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Vehicle name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGovNumber() {
        return govNumber;
    }

    public Vehicle govNumber(String govNumber) {
        this.govNumber = govNumber;
        return this;
    }

    public void setGovNumber(String govNumber) {
        this.govNumber = govNumber;
    }

    public String getTechPassportNumber() {
        return techPassportNumber;
    }

    public Vehicle techPassportNumber(String techPassportNumber) {
        this.techPassportNumber = techPassportNumber;
        return this;
    }

    public void setTechPassportNumber(String techPassportNumber) {
        this.techPassportNumber = techPassportNumber;
    }

    public String getModel() {
        return model;
    }

    public Vehicle model(String model) {
        this.model = model;
        return this;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getBuildYear() {
        return buildYear;
    }

    public Vehicle buildYear(Integer buildYear) {
        this.buildYear = buildYear;
        return this;
    }

    public void setBuildYear(Integer buildYear) {
        this.buildYear = buildYear;
    }

    public VehicleType getType() {
        return type;
    }

    public Vehicle type(VehicleType type) {
        this.type = type;
        return this;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    public Float getHeight() {
        return height;
    }

    public Vehicle height(Float height) {
        this.height = height;
        return this;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getLength() {
        return length;
    }

    public Vehicle length(Float length) {
        this.length = length;
        return this;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getWidth() {
        return width;
    }

    public Vehicle width(Float width) {
        this.width = width;
        return this;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public Vehicle totalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
        return this;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }

    public BigDecimal getDeliveryWeight() {
        return deliveryWeight;
    }

    public Vehicle deliveryWeight(BigDecimal deliveryWeight) {
        this.deliveryWeight = deliveryWeight;
        return this;
    }

    public void setDeliveryWeight(BigDecimal deliveryWeight) {
        this.deliveryWeight = deliveryWeight;
    }

    public String getNote() {
        return note;
    }

    public Vehicle note(String note) {
        this.note = note;
        return this;
    }

    public void setNote(String note) {
        this.note = note;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Vehicle vehicle = (Vehicle) o;
        if (vehicle.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), vehicle.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Vehicle{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", govNumber='" + getGovNumber() + "'" +
            ", techPassportNumber='" + getTechPassportNumber() + "'" +
            ", model='" + getModel() + "'" +
            ", buildYear='" + getBuildYear() + "'" +
            ", type='" + getType() + "'" +
            ", height='" + getHeight() + "'" +
            ", length='" + getLength() + "'" +
            ", width='" + getWidth() + "'" +
            ", totalWeight='" + getTotalWeight() + "'" +
            ", deliveryWeight='" + getDeliveryWeight() + "'" +
            ", note='" + getNote() + "'" +
            "}";
    }
}
