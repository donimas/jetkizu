package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A RouteOptimization.
 */
@Entity
@Table(name = "route_optimization")
@Document(indexName = "routeoptimization")
public class RouteOptimization implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "max_vehicle_distance", precision=10, scale=2)
    private BigDecimal maxVehicleDistance;

    @Column(name = "max_route_duration", precision=10, scale=2)
    private BigDecimal maxRouteDuration;

    @Column(name = "max_weight", precision=10, scale=2)
    private BigDecimal maxWeight;

    @Column(name = "max_route_revenue", precision=10, scale=2)
    private BigDecimal maxRouteRevenue;

    @Column(name = "max_route_stops")
    private Integer maxRouteStops;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getMaxVehicleDistance() {
        return maxVehicleDistance;
    }

    public RouteOptimization maxVehicleDistance(BigDecimal maxVehicleDistance) {
        this.maxVehicleDistance = maxVehicleDistance;
        return this;
    }

    public void setMaxVehicleDistance(BigDecimal maxVehicleDistance) {
        this.maxVehicleDistance = maxVehicleDistance;
    }

    public BigDecimal getMaxRouteDuration() {
        return maxRouteDuration;
    }

    public RouteOptimization maxRouteDuration(BigDecimal maxRouteDuration) {
        this.maxRouteDuration = maxRouteDuration;
        return this;
    }

    public void setMaxRouteDuration(BigDecimal maxRouteDuration) {
        this.maxRouteDuration = maxRouteDuration;
    }

    public BigDecimal getMaxWeight() {
        return maxWeight;
    }

    public RouteOptimization maxWeight(BigDecimal maxWeight) {
        this.maxWeight = maxWeight;
        return this;
    }

    public void setMaxWeight(BigDecimal maxWeight) {
        this.maxWeight = maxWeight;
    }

    public BigDecimal getMaxRouteRevenue() {
        return maxRouteRevenue;
    }

    public RouteOptimization maxRouteRevenue(BigDecimal maxRouteRevenue) {
        this.maxRouteRevenue = maxRouteRevenue;
        return this;
    }

    public void setMaxRouteRevenue(BigDecimal maxRouteRevenue) {
        this.maxRouteRevenue = maxRouteRevenue;
    }

    public Integer getMaxRouteStops() {
        return maxRouteStops;
    }

    public RouteOptimization maxRouteStops(Integer maxRouteStops) {
        this.maxRouteStops = maxRouteStops;
        return this;
    }

    public void setMaxRouteStops(Integer maxRouteStops) {
        this.maxRouteStops = maxRouteStops;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RouteOptimization routeOptimization = (RouteOptimization) o;
        if (routeOptimization.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), routeOptimization.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RouteOptimization{" +
            "id=" + getId() +
            ", maxVehicleDistance='" + getMaxVehicleDistance() + "'" +
            ", maxRouteDuration='" + getMaxRouteDuration() + "'" +
            ", maxWeight='" + getMaxWeight() + "'" +
            ", maxRouteRevenue='" + getMaxRouteRevenue() + "'" +
            ", maxRouteStops='" + getMaxRouteStops() + "'" +
            "}";
    }
}
