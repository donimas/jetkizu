package kz.rs.jetkizu.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Jetkizu.
 */
@Entity
@Table(name = "jetkizu")
@Document(indexName = "jetkizu")
public class Jetkizu extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "weight", precision=10, scale=2)
    private BigDecimal weight;

    @Column(name = "create_date_time")
    private ZonedDateTime createDateTime;

    @Column(name = "update_date_time")
    private ZonedDateTime updateDateTime;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "code")
    private String code;

    @Column(name = "delivery_date")
    private ZonedDateTime deliveryDate;

    @Column(name = "from_time")
    private ZonedDateTime fromTime;

    @Column(name = "till_time")
    private ZonedDateTime tillTime;

    @Column(name = "service_duration")
    private Long serviceDuration;

    @Column(name = "order_price")
    private BigDecimal orderPrice;

    @ManyToOne
    private JetkizuStatusHistory jetkizuStatusHistory;

    @ManyToOne
    @JsonIgnore
    private RouteBranch routeBranch;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public Jetkizu weight(BigDecimal weight) {
        this.weight = weight;
        return this;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public Jetkizu createDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public ZonedDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public Jetkizu updateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
        return this;
    }

    public void setUpdateDateTime(ZonedDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Jetkizu flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public String getName() {
        return name;
    }

    public Jetkizu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Jetkizu description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

    public JetkizuStatusHistory getJetkizuStatusHistory() {
        return jetkizuStatusHistory;
    }

    public Jetkizu jetkizuStatusHistory(JetkizuStatusHistory jetkizuStatusHistory) {
        this.jetkizuStatusHistory = jetkizuStatusHistory;
        return this;
    }

    public void setJetkizuStatusHistory(JetkizuStatusHistory jetkizuStatusHistory) {
        this.jetkizuStatusHistory = jetkizuStatusHistory;
    }

    public ZonedDateTime getFromTime() {
        return fromTime;
    }

    public void setFromTime(ZonedDateTime fromTime) {
        this.fromTime = fromTime;
    }

    public ZonedDateTime getTillTime() {
        return tillTime;
    }

    public void setTillTime(ZonedDateTime tillTime) {
        this.tillTime = tillTime;
    }

    public RouteBranch getRouteBranch() {
        return routeBranch;
    }

    public Jetkizu routeBranch(RouteBranch routeBranch) {
        this.routeBranch = routeBranch;
        return this;
    }

    public void setRouteBranch(RouteBranch routeBranch) {
        this.routeBranch = routeBranch;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ZonedDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(ZonedDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getServiceDuration() {
        return serviceDuration;
    }

    public void setServiceDuration(Long serviceDuration) {
        this.serviceDuration = serviceDuration;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Jetkizu jetkizu = (Jetkizu) o;
        if (jetkizu.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jetkizu.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Jetkizu{" +
            "id=" + getId() +
            ", weight='" + getWeight() + "'" +
            ", createDateTime='" + getCreateDateTime() + "'" +
            ", updateDateTime='" + getUpdateDateTime() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", code='" + getCode() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", fromTime='" + getFromTime() + "'" +
            ", tillTime='" + getTillTime() + "'" +
            ", serviceDuration='" + getServiceDuration() + "'" +
            "}";
    }
}
