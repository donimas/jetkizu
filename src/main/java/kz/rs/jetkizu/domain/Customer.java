package kz.rs.jetkizu.domain;

import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Customer.
 */
@Entity
@Table(name = "customer")
@Document(indexName = "customer")
public class Customer extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "sender_fio")
    private String senderFio;

    @NotNull
    @Column(name = "receiver_fio", nullable = false)
    private String receiverFio;

    @NotNull
    @Column(name = "cell_phone", nullable = false)
    private String cellPhone;

    @Column(name = "home_phone")
    private String homePhone;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderFio() {
        return senderFio;
    }

    public Customer senderFio(String senderFio) {
        this.senderFio = senderFio;
        return this;
    }

    public void setSenderFio(String senderFio) {
        this.senderFio = senderFio;
    }

    public String getReceiverFio() {
        return receiverFio;
    }

    public Customer receiverFio(String receiverFio) {
        this.receiverFio = receiverFio;
        return this;
    }

    public void setReceiverFio(String receiverFio) {
        this.receiverFio = receiverFio;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public Customer cellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
        return this;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public Customer homePhone(String homePhone) {
        this.homePhone = homePhone;
        return this;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Customer customer = (Customer) o;
        if (customer.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Customer{" +
            "id=" + getId() +
            ", senderFio='" + getSenderFio() + "'" +
            ", receiverFio='" + getReceiverFio() + "'" +
            ", cellPhone='" + getCellPhone() + "'" +
            ", homePhone='" + getHomePhone() + "'" +
            "}";
    }
}
