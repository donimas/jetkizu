package kz.rs.jetkizu.service.util;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GisUtils {

    private final static Logger log = LoggerFactory.getLogger(GisUtils.class);

    private GisUtils() {}

    public static Geometry toPoint(String lon, String lat) {
        log.debug("Util request to generate GIS POINT Geometry: {}, {}", lon, lat);
        WKTReader wktReader = new WKTReader();
        Geometry result = null;
        try {
            result = wktReader.read(String.format("POINT(%s %s)", lon, lat));
        } catch (ParseException e) {
            log.debug(e.getMessage(), e);
            e.printStackTrace();
        }

        return result;
    }

    public static Geometry toPolygon(String[][] data) {
        log.debug("Util request to generate polygon geometry: {}", data);
        WKTReader wktReader = new WKTReader();
        Geometry result = null;
        try {
            String polygonPoints = Arrays.stream(data)
                .filter(d -> !StringUtils.isEmpty(d[0]) && !StringUtils.isEmpty(d[1]))
                .map(d -> d[0] + " " + d[1])
                .collect(Collectors.joining(","));
            String geo = data.length > 1 ? "POLYGON(("+polygonPoints+"))" : "POINT("+polygonPoints+")";
            log.debug(geo);
            result = wktReader.read(geo);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String[][] toPolygonHumanReadableCoordinates(Geometry geometry) {
        log.debug("Util request from geometry to human readable coordinates");
        log.debug("Geo type: {}", geometry.getGeometryType());
        log.debug("Geo length: {}", geometry.getLength());
        log.debug("Geo area: {}", geometry.getArea());
        log.debug("Geo SRID: {}", geometry.getSRID());
        log.debug("Geo num points: {}", geometry.getNumPoints());
        Coordinate coordinate = geometry.getCoordinate();
        if(coordinate != null) {
            log.debug("Coordinate x:{}, y: {}", coordinate.x, coordinate.y);
        } else {
            log.debug("Coordinate is null");
        }
        Coordinate[] coordinates = geometry.getCoordinates();
        if(coordinates != null && coordinates.length > 0) {
            String[][] polygon = new String[coordinates.length][2];
            AtomicInteger i = new AtomicInteger();
            Arrays.stream(coordinates).forEach(c -> {
                polygon[i.get()][0] = c.x + "";
                polygon[i.get()][1] = c.y + "";
                i.incrementAndGet();
            });
            String polCoordinate = Arrays.stream(coordinates).map(c -> c.x + " " + c.y).collect(Collectors.joining(","));
            log.debug("Geo coordinates: {}", polCoordinate);

            return polygon;
        } else {
            log.debug("Coordinates null");
        }

        return null;
    }

}
