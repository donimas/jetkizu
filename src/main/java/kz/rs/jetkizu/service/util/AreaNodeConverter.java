package kz.rs.jetkizu.service.util;

import kz.rs.jetkizu.domain.AreaNode;
import kz.rs.jetkizu.service.dto.AreaNodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AreaNodeConverter {

    private final static Logger log = LoggerFactory.getLogger(AreaNodeConverter.class);

    private AreaNodeConverter() {}

    public static AreaNode convert(AreaNodeDto areaNodeDto) {
        log.debug("Util request to convert Area Node: {}", areaNodeDto);
        if(areaNodeDto == null) {
            return null;
        }
        AreaNode areaNode = new AreaNode();
        areaNode.setId(areaNodeDto.getId());
        areaNode.setName(areaNodeDto.getName());
        areaNode.setArea(areaNodeDto.getArea());
        if(areaNodeDto.getTeam() != null) {
            areaNode.setTeam(TeamConverter.convert(areaNodeDto.getTeam()));
        }
        if(areaNodeDto.getPolygon() != null) {
            areaNode.setPolygon(GisUtils.toPolygon(areaNodeDto.getPolygon()));
        }

        return areaNode;
    }

}
