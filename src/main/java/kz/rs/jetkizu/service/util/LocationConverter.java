package kz.rs.jetkizu.service.util;

import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.service.dto.LocationDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocationConverter {

    private final Logger log = LoggerFactory.getLogger(LocationConverter.class);

    private LocationConverter(){
    }

    public static Location convert(LocationDto locationDto) {
        if(locationDto == null) {
            return null;
        }
        Location result = new Location();
        result.setId(locationDto.getId());
        result.setStreet(locationDto.getStreet());
        result.setHome(locationDto.getHome());
        result.setFlat(locationDto.getFlat());
        result.setPostindex(locationDto.getPostindex());
        result.setLat(locationDto.getLat());
        result.setLon(locationDto.getLon());
        result.setFullAddress(locationDto.getFullAddress());
        result.setFlagGarage(locationDto.getFlagGarage());
        result.setPlaceId(locationDto.getPlaceId());
        result.setOsmType(locationDto.getOsmType());
        result.setOsmId(locationDto.getOsmId());
        result.setCreateDate(locationDto.getCreateDate());

        return result;
    }

}
