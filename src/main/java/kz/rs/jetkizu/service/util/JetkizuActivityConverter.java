package kz.rs.jetkizu.service.util;

import kz.rs.jetkizu.domain.JetkizuActivity;
import kz.rs.jetkizu.service.dto.JetkizuActivityDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JetkizuActivityConverter {

    private final Logger log = LoggerFactory.getLogger(JetkizuActivityConverter.class);

    private JetkizuActivityConverter() {}

    public static JetkizuActivity convert(JetkizuActivityDto jetkizuActivityDto) {
        if(jetkizuActivityDto == null) {
            return null;
        }
        JetkizuActivity result = new JetkizuActivity();
        result.setId(jetkizuActivityDto.getId());
        result.setPlanKm(jetkizuActivityDto.getPlanKm());
        result.setFactKm(jetkizuActivityDto.getFactKm());
        result.setPlanDuration(jetkizuActivityDto.getPlanDuration());
        result.setFactDuration(jetkizuActivityDto.getFactDuration());

        result.setPlanDurationToNext(jetkizuActivityDto.getPlanDurationToNext());
        result.setPlanDistanceToNext(jetkizuActivityDto.getPlanDistanceToNext());
        result.setFactDurationToNext(jetkizuActivityDto.getFactDurationToNext());
        result.setFactDistanceToNext(jetkizuActivityDto.getFactDistanceToNext());

        result.setCreateDateTime(jetkizuActivityDto.getCreateDateTime());
        result.setFromLat(jetkizuActivityDto.getFromLat());
        result.setFromLon(jetkizuActivityDto.getFromLon());
        result.setJetkizuPause(jetkizuActivityDto.getJetkizuPause());

        return result;
    }

}
