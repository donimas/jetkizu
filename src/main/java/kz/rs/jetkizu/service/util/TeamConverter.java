package kz.rs.jetkizu.service.util;

import kz.rs.jetkizu.domain.Team;
import kz.rs.jetkizu.service.dto.TeamDTO;

public class TeamConverter {

    public static Team convert(TeamDTO teamDTO) {
        Team team = new Team();
        team.setId(teamDTO.id);
        team.setName(teamDTO.name);
        team.setLead(teamDTO.lead);
        team.setVehicle(teamDTO.vehicle);

        return team;
    }

}
