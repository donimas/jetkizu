package kz.rs.jetkizu.service.util;

import kz.rs.jetkizu.domain.*;
import kz.rs.jetkizu.domain.enumeration.JetkizuRouteStatus;
import kz.rs.jetkizu.service.dto.JetkizuRouteDTO;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class JetkizuRouteConverter {

    public static JetkizuRoute convert(JetkizuRouteDTO jetkizuRouteDTO) {
        if(jetkizuRouteDTO == null) {
            return null;
        }
        JetkizuRoute jetkizuRoute = new JetkizuRoute();
        jetkizuRoute.setId(jetkizuRouteDTO.id);
        jetkizuRoute.setJetkizuDate(jetkizuRouteDTO.jetkizuDate);
        jetkizuRoute.setStartedDateTime(jetkizuRouteDTO.startedDateTime);
        jetkizuRoute.setStoppedDateTime(jetkizuRouteDTO.stoppedDateTime);
        jetkizuRoute.setCreateDateTime(jetkizuRouteDTO.createDateTime);
        jetkizuRoute.setName(jetkizuRouteDTO.name);
        jetkizuRoute.setStatus(jetkizuRouteDTO.status);
        jetkizuRoute.setTeam(jetkizuRouteDTO.team);
        jetkizuRoute.setStartedBy(jetkizuRouteDTO.startedBy);
        jetkizuRoute.setStoppedBy(jetkizuRouteDTO.stoppedBy);
        jetkizuRoute.setFlagDeleted(jetkizuRouteDTO.flagDeleted);
        jetkizuRoute.setFlagStopped(jetkizuRouteDTO.flagStopped);
        jetkizuRoute.setRouteBranches(jetkizuRouteDTO.routeBranches.stream().map(RouteBranchConverter::convert).collect(Collectors.toSet()));
        jetkizuRoute.setTotalPlanDistance(jetkizuRouteDTO.totalPlanDistance);
        jetkizuRoute.setTotalPlanDuration(jetkizuRouteDTO.totalPlanDuration);
        jetkizuRoute.setTotalFactDistance(jetkizuRouteDTO.totalFactDistance);
        jetkizuRoute.setTotalFactDuration(jetkizuRouteDTO.totalFactDuration);
        jetkizuRoute.setServiceDuration(jetkizuRouteDTO.serviceDuration);
        jetkizuRoute.setRouteOptimization(jetkizuRouteDTO.routeOptimization);
        jetkizuRoute.setStart(LocationConverter.convert(jetkizuRouteDTO.start));
        jetkizuRoute.setEnd(LocationConverter.convert(jetkizuRouteDTO.end));

        return jetkizuRoute;
    }

}
