package kz.rs.jetkizu.service.util;

import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.service.dto.LocationDto;
import kz.rs.jetkizu.service.dto.RouteBranchDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RouteBranchConverter {

    private final Logger log = LoggerFactory.getLogger(RouteBranchConverter.class);

    private RouteBranchConverter(){}

    public static RouteBranch convert(RouteBranchDto routeBranchDto){
        if(routeBranchDto == null) {
            return null;
        }
        RouteBranch result = new RouteBranch();
        result.setId(routeBranchDto.getId());
        result.setOrder(routeBranchDto.getOrder());
        result.setJetkizu(routeBranchDto.getJetkizu());
        //result.setJetkizuRoute(JetkizuRouteConverter.convert(routeBranchDto.getJetkizuRoute()));
        result.setRouteBranchStatusHistory(routeBranchDto.getRouteBranchStatusHistory());
        result.setJetkizuActivity(JetkizuActivityConverter.convert(routeBranchDto.getJetkizuActivity()));
        result.setFlagDeleted(routeBranchDto.getFlagDeleted());
        result.setType(routeBranchDto.getType());
        result.setCustomer(routeBranchDto.getCustomer());
        result.setStartFrom(LocationConverter.convert(routeBranchDto.getStartFrom()));
        result.setEndTo(LocationConverter.convert(routeBranchDto.getEndTo()));
        result.setNextStop(LocationConverter.convert(routeBranchDto.getNextStop()));
        result.setOsmId(routeBranchDto.getOsmId());
        result.setPlaceId(routeBranchDto.getPlaceId());
        result.setOsmType(routeBranchDto.getOsmType());

        return result;
    }

    public static RouteBranch convertForSequencing(RouteBranchDto routeBranchDto) {
        RouteBranch result = new RouteBranch();
        result.setId(routeBranchDto.getId());
        result.setEndTo(convertLocationOfOsm(routeBranchDto.getEndTo()));

        return result;
    }

    public static Location convertLocationOfOsm(LocationDto locationDto) {
        Location result = new Location();
        result.setId(locationDto.getId());
        result.setOsmId(locationDto.getOsmId());

        return result;
    }

}
