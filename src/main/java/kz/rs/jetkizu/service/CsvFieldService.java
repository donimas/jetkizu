package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.CsvField;
import kz.rs.jetkizu.repository.CsvFieldRepository;
import kz.rs.jetkizu.repository.search.CsvFieldSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing CsvField.
 */
@Service
@Transactional
public class CsvFieldService {

    private final Logger log = LoggerFactory.getLogger(CsvFieldService.class);

    private final CsvFieldRepository csvFieldRepository;

    private final CsvFieldSearchRepository csvFieldSearchRepository;

    public CsvFieldService(CsvFieldRepository csvFieldRepository, CsvFieldSearchRepository csvFieldSearchRepository) {
        this.csvFieldRepository = csvFieldRepository;
        this.csvFieldSearchRepository = csvFieldSearchRepository;
    }

    /**
     * Save a csvField.
     *
     * @param csvField the entity to save
     * @return the persisted entity
     */
    public CsvField save(CsvField csvField) {
        log.debug("Request to save CsvField : {}", csvField);
        CsvField result = csvFieldRepository.save(csvField);
        csvFieldSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the csvFields.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CsvField> findAll(Pageable pageable) {
        log.debug("Request to get all CsvFields");
        return csvFieldRepository.findAll(pageable);
    }

    /**
     *  Get one csvField by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CsvField findOne(Long id) {
        log.debug("Request to get CsvField : {}", id);
        return csvFieldRepository.findOne(id);
    }

    /**
     *  Delete the  csvField by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CsvField : {}", id);
        csvFieldRepository.delete(id);
        csvFieldSearchRepository.delete(id);
    }

    /**
     * Search for the csvField corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<CsvField> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of CsvFields for query {}", query);
        Page<CsvField> result = csvFieldSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
