package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.Team;
import kz.rs.jetkizu.domain.TeamMember;
import java.util.List;
import java.util.Set;

/**
 * Service Interface for managing TeamMember.
 */
public interface TeamMemberService {

    /**
     * Save a teamMember.
     *
     * @param teamMember the entity to save
     * @return the persisted entity
     */
    TeamMember save(TeamMember teamMember);

    /**
     *  Get all the teamMembers.
     *
     *  @return the list of entities
     */
    List<TeamMember> findAll();

    /**
     *  Get the "id" teamMember.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    TeamMember findOne(Long id);

    /**
     *  Delete the "id" teamMember.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the teamMember corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @return the list of entities
     */
    List<TeamMember> search(String query);

    List<TeamMember> findAllByTeamId(Long id);

    void saveAllTeamMembers(Team team, Set<TeamMember> teamMembers);
}
