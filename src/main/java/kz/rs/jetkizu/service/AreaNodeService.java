package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.AreaNode;
import kz.rs.jetkizu.service.dto.AreaNodeDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing AreaNode.
 */
public interface AreaNodeService {

    /**
     * Save a areaNode.
     *
     * @param areaNode the entity to save
     * @return the persisted entity
     */
    AreaNode save(AreaNode areaNode);

    /**
     *  Get all the areaNodes.
     *
     *  @return the list of entities
     */
    List<AreaNodeDto> findAll();

    /**
     *  Get the "id" areaNode.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    AreaNodeDto findOne(Long id);

    /**
     *  Delete the "id" areaNode.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the areaNode corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @return the list of entities
     */
    List<AreaNodeDto> search(String query);

    List<AreaNodeDto> findAllByAreaId(Long areaId, Pageable pageable);
}
