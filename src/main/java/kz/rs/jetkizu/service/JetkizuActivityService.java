package kz.rs.jetkizu.service;

import com.vividsolutions.jts.geom.Geometry;
import kz.rs.jetkizu.domain.JetkizuActivity;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.repository.JetkizuActivityRepository;
import kz.rs.jetkizu.repository.search.JetkizuActivitySearchRepository;
import kz.rs.jetkizu.service.util.GisUtils;
import kz.rs.jetkizu.service.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing JetkizuActivity.
 */
@Service
@Transactional
public class JetkizuActivityService {

    private final Logger log = LoggerFactory.getLogger(JetkizuActivityService.class);

    private final JetkizuActivityRepository jetkizuActivityRepository;

    private final JetkizuActivitySearchRepository jetkizuActivitySearchRepository;

    public JetkizuActivityService(JetkizuActivityRepository jetkizuActivityRepository, JetkizuActivitySearchRepository jetkizuActivitySearchRepository) {
        this.jetkizuActivityRepository = jetkizuActivityRepository;
        this.jetkizuActivitySearchRepository = jetkizuActivitySearchRepository;
    }

    /**
     * Save a jetkizuActivity.
     *
     * @param jetkizuActivity the entity to save
     * @return the persisted entity
     */
    public JetkizuActivity save(JetkizuActivity jetkizuActivity) {
        log.debug("Request to save JetkizuActivity : {}", jetkizuActivity);
        if(!StringUtils.isEmpty(jetkizuActivity.getFromLat()) && !StringUtils.isEmpty(jetkizuActivity.getFromLon())) {
            Geometry whereIs = GisUtils.toPoint(jetkizuActivity.getFromLon(), jetkizuActivity.getFromLat());
            jetkizuActivity.setWhereIs(whereIs);
        }
        JetkizuActivity result = jetkizuActivityRepository.save(jetkizuActivity);
        jetkizuActivitySearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the jetkizuActivities.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<JetkizuActivity> findAll() {
        log.debug("Request to get all JetkizuActivities");
        return jetkizuActivityRepository.findAll();
    }

    /**
     *  Get one jetkizuActivity by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public JetkizuActivity findOne(Long id) {
        log.debug("Request to get JetkizuActivity : {}", id);
        return jetkizuActivityRepository.findOne(id);
    }

    /**
     *  Delete the  jetkizuActivity by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete JetkizuActivity : {}", id);
        jetkizuActivityRepository.delete(id);
        jetkizuActivitySearchRepository.delete(id);
    }

    /**
     * Search for the jetkizuActivity corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<JetkizuActivity> search(String query) {
        log.debug("Request to search JetkizuActivities for query {}", query);
        return StreamSupport
            .stream(jetkizuActivitySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    public JetkizuActivity create(JetkizuActivity jetkizuActivity) {
        log.debug("Request to create jetkizu activity: {}", jetkizuActivity);
        if(jetkizuActivity.getCreateDateTime() == null) {
            jetkizuActivity.setCreateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        }
        return save(jetkizuActivity);
    }
}
