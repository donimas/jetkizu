package kz.rs.jetkizu.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import kz.rs.jetkizu.domain.Report;
import kz.rs.jetkizu.domain.*; // for static metamodels
import kz.rs.jetkizu.repository.ReportRepository;
import kz.rs.jetkizu.repository.search.ReportSearchRepository;
import kz.rs.jetkizu.service.dto.ReportCriteria;

import kz.rs.jetkizu.domain.enumeration.ReportType;

/**
 * Service for executing complex queries for Report entities in the database.
 * The main input is a {@link ReportCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Report} or a {@link Page} of {%link Report} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class ReportQueryService extends QueryService<Report> {

    private final Logger log = LoggerFactory.getLogger(ReportQueryService.class);


    private final ReportRepository reportRepository;

    private final ReportSearchRepository reportSearchRepository;

    public ReportQueryService(ReportRepository reportRepository, ReportSearchRepository reportSearchRepository) {
        this.reportRepository = reportRepository;
        this.reportSearchRepository = reportSearchRepository;
    }

    /**
     * Return a {@link List} of {%link Report} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Report> findByCriteria(ReportCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Report> specification = createSpecification(criteria);
        return reportRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Report} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Report> findByCriteria(ReportCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Report> specification = createSpecification(criteria);
        return reportRepository.findAll(specification, page);
    }

    /**
     * Function to convert ReportCriteria to a {@link Specifications}
     */
    private Specifications<Report> createSpecification(ReportCriteria criteria) {
        Specifications<Report> specification = Specifications.where(null);
        /*if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Report_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), Report_.code));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), Report_.type));
            }
        }*/
        return specification;
    }

}
