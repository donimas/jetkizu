package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.Vehicle;
import kz.rs.jetkizu.repository.VehicleRepository;
import kz.rs.jetkizu.repository.search.VehicleSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Vehicle.
 */
@Service
@Transactional
public class VehicleService {

    private final Logger log = LoggerFactory.getLogger(VehicleService.class);

    private final VehicleRepository vehicleRepository;

    private final VehicleSearchRepository vehicleSearchRepository;

    public VehicleService(VehicleRepository vehicleRepository, VehicleSearchRepository vehicleSearchRepository) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleSearchRepository = vehicleSearchRepository;
    }

    /**
     * Save a vehicle.
     *
     * @param vehicle the entity to save
     * @return the persisted entity
     */
    public Vehicle save(Vehicle vehicle) {
        log.debug("Request to save Vehicle : {}", vehicle);
        Vehicle result = vehicleRepository.save(vehicle);
        vehicleSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the vehicles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Vehicle> findAll(Pageable pageable) {
        log.debug("Request to get all Vehicles");
        return vehicleRepository.findAll(pageable);
    }

    /**
     *  Get one vehicle by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public Vehicle findOne(Long id) {
        log.debug("Request to get Vehicle : {}", id);
        return vehicleRepository.findOne(id);
    }

    /**
     *  Delete the  vehicle by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Vehicle : {}", id);
        vehicleRepository.delete(id);
        vehicleSearchRepository.delete(id);
    }

    /**
     * Search for the vehicle corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Vehicle> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Vehicles for query {}", query);
        Page<Vehicle> result = vehicleSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
