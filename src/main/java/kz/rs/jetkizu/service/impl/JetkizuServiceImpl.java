package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.domain.Customer;
import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;
import kz.rs.jetkizu.service.CustomerService;
import kz.rs.jetkizu.service.JetkizuService;
import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.repository.JetkizuRepository;
import kz.rs.jetkizu.repository.search.JetkizuSearchRepository;
import kz.rs.jetkizu.service.JetkizuStatusHistoryService;
import kz.rs.jetkizu.service.LocationService;
import kz.rs.jetkizu.web.rest.constants.Errors;
import kz.rs.jetkizu.web.rest.errors.FLCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Jetkizu.
 */
@Service
@Transactional
public class JetkizuServiceImpl implements JetkizuService{

    private final Logger log = LoggerFactory.getLogger(JetkizuServiceImpl.class);

    private final JetkizuRepository jetkizuRepository;

    private final JetkizuSearchRepository jetkizuSearchRepository;

    private final LocationService locationService;
    private final CustomerService customerService;
    private final JetkizuStatusHistoryService jetkizuStatusHistoryService;

    public JetkizuServiceImpl(JetkizuRepository jetkizuRepository, JetkizuSearchRepository jetkizuSearchRepository,
                              LocationService locationService, CustomerService customerService, JetkizuStatusHistoryService jetkizuStatusHistoryService) {
        this.jetkizuRepository = jetkizuRepository;
        this.jetkizuSearchRepository = jetkizuSearchRepository;
        this.locationService = locationService;
        this.customerService = customerService;
        this.jetkizuStatusHistoryService = jetkizuStatusHistoryService;
    }

    /**
     * Save a jetkizu.
     *
     * @param jetkizu the entity to save
     * @return the persisted entity
     */
    @Override
    public Jetkizu save(Jetkizu jetkizu) {
        log.debug("Request to save Jetkizu : {}", jetkizu);
        jetkizu.setUpdateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        Jetkizu result = jetkizuRepository.save(jetkizu);
        jetkizuSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the jetkizus.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Jetkizu> findAll(Pageable pageable) {
        log.debug("Request to get all Jetkizus");
        return jetkizuRepository.findAll(pageable);
    }


    /**
     *  get all the jetkizus where RouteBranch is null.
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Jetkizu> findAllWhereRouteBranchIsNull() {
        log.debug("Request to get all jetkizus where RouteBranch is null");
        return StreamSupport
            .stream(jetkizuRepository.findAll().spliterator(), false)
            .filter(jetkizu -> jetkizu.getRouteBranch() == null)
            .collect(Collectors.toList());
    }

    /**
     *  Get one jetkizu by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Jetkizu findOne(Long id) {
        log.debug("Request to get Jetkizu : {}", id);
        return jetkizuRepository.findOne(id);
    }

    /**
     *  Delete the  jetkizu by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Jetkizu : {}", id);
        /*jetkizuRepository.delete(id);
        jetkizuSearchRepository.delete(id);*/
        Jetkizu jetkizu = findOne(id);
        if(!JetkizuStatus.BACKLOG.equals(jetkizu.getJetkizuStatusHistory().getStatus())) {
            throw new FLCException(Errors.ACTIVE_JETKIZU_DELETE_ERROR);
        }
        jetkizu.setFlagDeleted(true);
        save(jetkizu);
    }

    /**
     * Search for the jetkizu corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Jetkizu> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Jetkizus for query {}", query);
        Page<Jetkizu> result = jetkizuSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    @Override
    public Jetkizu create(Jetkizu jetkizu) {
        log.debug("Request to create jetkizu: {}", jetkizu);
        jetkizu.setCreateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        jetkizu.setFlagDeleted(false);

        /*Location savedLocation = locationService.create(jetkizu.getLocation());
        Customer savedCustomer = customerService.create(jetkizu.getCustomer());
        jetkizu.setLocation(savedLocation);
        jetkizu.setCustomer(savedCustomer);*/

        Jetkizu savedJetkizu = save(jetkizu);
        return setStatus(savedJetkizu, JetkizuStatus.BACKLOG);
    }

    @Override
    public Jetkizu update(Jetkizu jetkizu) {
        log.debug("Request to update Jetkizu: {}", jetkizu);
        /*locationService.save(jetkizu.getLocation());
        customerService.save(jetkizu.getCustomer());*/
        return save(jetkizu);
    }

    @Override
    public Jetkizu setStatus(Jetkizu jetkizu, JetkizuStatus status) {
        JetkizuStatusHistory savedStatus = jetkizuStatusHistoryService.create(jetkizu, status);
        jetkizu.setJetkizuStatusHistory(savedStatus);

        return save(jetkizu);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Jetkizu> findAllBacklog(Pageable pageable) {
        log.debug("Request to get all backlog Jetkizus");
        return jetkizuRepository.findAllByFlagDeletedFalseAndJetkizuStatusHistory_Status(JetkizuStatus.BACKLOG, pageable);
    }
}
