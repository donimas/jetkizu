package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;
import kz.rs.jetkizu.service.RouteBranchStatusHistoryService;
import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import kz.rs.jetkizu.repository.RouteBranchStatusHistoryRepository;
import kz.rs.jetkizu.repository.search.RouteBranchStatusHistorySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RouteBranchStatusHistory.
 */
@Service
@Transactional
public class RouteBranchStatusHistoryServiceImpl implements RouteBranchStatusHistoryService{

    private final Logger log = LoggerFactory.getLogger(RouteBranchStatusHistoryServiceImpl.class);

    private final RouteBranchStatusHistoryRepository routeBranchStatusHistoryRepository;

    private final RouteBranchStatusHistorySearchRepository routeBranchStatusHistorySearchRepository;

    public RouteBranchStatusHistoryServiceImpl(RouteBranchStatusHistoryRepository routeBranchStatusHistoryRepository, RouteBranchStatusHistorySearchRepository routeBranchStatusHistorySearchRepository) {
        this.routeBranchStatusHistoryRepository = routeBranchStatusHistoryRepository;
        this.routeBranchStatusHistorySearchRepository = routeBranchStatusHistorySearchRepository;
    }

    /**
     * Save a routeBranchStatusHistory.
     *
     * @param routeBranchStatusHistory the entity to save
     * @return the persisted entity
     */
    @Override
    public RouteBranchStatusHistory save(RouteBranchStatusHistory routeBranchStatusHistory) {
        log.debug("Request to save RouteBranchStatusHistory : {}", routeBranchStatusHistory);
        RouteBranchStatusHistory result = routeBranchStatusHistoryRepository.save(routeBranchStatusHistory);
        routeBranchStatusHistorySearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the routeBranchStatusHistories.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RouteBranchStatusHistory> findAll() {
        log.debug("Request to get all RouteBranchStatusHistories");
        return routeBranchStatusHistoryRepository.findAll();
    }

    /**
     *  Get one routeBranchStatusHistory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RouteBranchStatusHistory findOne(Long id) {
        log.debug("Request to get RouteBranchStatusHistory : {}", id);
        return routeBranchStatusHistoryRepository.findOne(id);
    }

    /**
     *  Delete the  routeBranchStatusHistory by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete RouteBranchStatusHistory : {}", id);
        routeBranchStatusHistoryRepository.delete(id);
        routeBranchStatusHistorySearchRepository.delete(id);
    }

    /**
     * Search for the routeBranchStatusHistory corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RouteBranchStatusHistory> search(String query) {
        log.debug("Request to search RouteBranchStatusHistories for query {}", query);
        return StreamSupport
            .stream(routeBranchStatusHistorySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Override
    public RouteBranchStatusHistory create(RouteBranch routeBranch, RouteBranchStatus status) {
        log.debug("Request to create route branch status history: {} -> {}", routeBranch, status);
        RouteBranchStatusHistory statusHistory = new RouteBranchStatusHistory();
        statusHistory.setCreateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        statusHistory.setStatus(status);
        statusHistory.setFlagDeleted(false);
        statusHistory.setRouteBranch(routeBranch);

        return save(statusHistory);
    }
}
