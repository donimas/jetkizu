package kz.rs.jetkizu.service.impl;

import com.vividsolutions.jts.geom.Geometry;
import kz.rs.jetkizu.domain.*;
import kz.rs.jetkizu.domain.enumeration.*;
import kz.rs.jetkizu.service.*;
import kz.rs.jetkizu.repository.JetkizuRouteRepository;
import kz.rs.jetkizu.repository.search.JetkizuRouteSearchRepository;
import kz.rs.jetkizu.service.dto.JetkizuRouteContainerDto;
import kz.rs.jetkizu.service.dto.JetkizuRouteDTO;
import kz.rs.jetkizu.service.dto.RouteBranchDto;
import kz.rs.jetkizu.service.util.*;
import kz.rs.jetkizu.web.rest.constants.Errors;
import kz.rs.jetkizu.web.rest.errors.FLCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing JetkizuRoute.
 */
@Service
@Transactional
public class JetkizuRouteServiceImpl implements JetkizuRouteService{

    private final Logger log = LoggerFactory.getLogger(JetkizuRouteServiceImpl.class);

    private final JetkizuRouteRepository jetkizuRouteRepository;

    private final JetkizuRouteSearchRepository jetkizuRouteSearchRepository;
    private final RouteBranchService routeBranchService;
    private final JetkizuService jetkizuService;
    private final LocationService locationService;
    private final UserService userService;
    private final JetkizuActivityService jetkizuActivityService;
    private final RouteBranchStatusHistoryService routeBranchStatusHistoryService;
    private final OsmService osmService;
    private final RouteOptimizationService routeOptimizationService;

    public JetkizuRouteServiceImpl(JetkizuRouteRepository jetkizuRouteRepository, JetkizuRouteSearchRepository jetkizuRouteSearchRepository,
                                   RouteBranchService routeBranchService, JetkizuService jetkizuService, LocationService locationService, UserService userService,
                                   JetkizuActivityService jetkizuActivityService, RouteBranchStatusHistoryService routeBranchStatusHistoryService, OsmService osmService, RouteOptimizationService routeOptimizationService) {
        this.jetkizuRouteRepository = jetkizuRouteRepository;
        this.jetkizuRouteSearchRepository = jetkizuRouteSearchRepository;
        this.routeBranchService = routeBranchService;
        this.jetkizuService = jetkizuService;
        this.locationService = locationService;
        this.userService = userService;
        this.jetkizuActivityService = jetkizuActivityService;
        this.routeBranchStatusHistoryService = routeBranchStatusHistoryService;
        this.osmService = osmService;
        this.routeOptimizationService = routeOptimizationService;
    }

    /**
     * Save a jetkizuRoute.
     *
     * @param jetkizuRoute the entity to save
     * @return the persisted entity
     */
    @Override
    public JetkizuRoute save(JetkizuRoute jetkizuRoute) {
        log.debug("Request to save JetkizuRoute : {}", jetkizuRoute);
        JetkizuRoute result = jetkizuRouteRepository.save(jetkizuRoute);
        jetkizuRouteSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the jetkizuRoutes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JetkizuRoute> findAll(Pageable pageable) {
        log.debug("Request to get all JetkizuRoutes");
        return jetkizuRouteRepository.findAllByFlagDeletedFalse(pageable);
    }

    /**
     *  Get one jetkizuRoute by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public JetkizuRoute findOne(Long id) {
        log.debug("Request to get JetkizuRoute : {}", id);
        return jetkizuRouteRepository.findOne(id);
    }

    /**
     *  Delete the  jetkizuRoute by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JetkizuRoute : {}", id);
        JetkizuRoute jetkizuRoute = findOne(id);
        if(!JetkizuRouteStatus.NOT_STARTED.equals(jetkizuRoute.getStatus())) {
            throw new FLCException(Errors.ACTIVE_ROUTE_DELETE_ERROR);
        }
        jetkizuRoute.getRouteBranches().forEach(branch -> {
            routeBranchService.delete(branch.getId());
        });
        jetkizuRouteRepository.delete(id);
        jetkizuRouteSearchRepository.delete(id);
    }

    /**
     * Search for the jetkizuRoute corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JetkizuRoute> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of JetkizuRoutes for query {}", query);
        Page<JetkizuRoute> result = jetkizuRouteSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    @Override
    public JetkizuRoute create(JetkizuRoute jetkizuRoute) {
        log.debug("Request to create jetkizuRoute: {}", jetkizuRoute);
        jetkizuRoute.setCreateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        jetkizuRoute.setFlagDeleted(false);
        jetkizuRoute.setFlagStopped(false);
        jetkizuRoute.setStatus(JetkizuRouteStatus.NOT_STARTED);
        return save(jetkizuRoute);
    }

    @Override
    public Page<JetkizuRoute> findByFlagStoppedFalse(Pageable pageable) {
        log.debug("Request to get all not stopped JetkizuRoutes");
        return jetkizuRouteRepository.findAllByFlagStoppedFalseAndFlagDeletedFalse(pageable);
    }

    @Override
    public Page<JetkizuRoute> findStarted(Pageable pageable) {
        log.debug("Request to get all started JetkizuRoutes");
        return jetkizuRouteRepository.findAllByStatus(JetkizuRouteStatus.STARTED, pageable);
    }

    @Override
    @Transactional
    public Set<Long> startOptimizedRoute(JetkizuRouteContainerDto jetkizuRouteContainerDto) {
        log.debug("Request to start optimized route: {}", jetkizuRouteContainerDto);

        Set<Long> routeIds = jetkizuRouteContainerDto.jetkizuRoutes.stream()
            .filter(route ->
                (route.flagWithoutPolygons == null || !route.flagWithoutPolygons) ||
                (route.routeBranches != null && route.routeBranches.size() > 0)
            )
            .map(optimizedRoute -> {
                JetkizuRoute jetkizuRoute = JetkizuRouteConverter.convert(jetkizuRouteContainerDto.jetkizuRoute);
                Location start = locationService.save(LocationConverter.convert(jetkizuRouteContainerDto.start));
                Location end = locationService.save(LocationConverter.convert(jetkizuRouteContainerDto.end));
                TripType tripType = jetkizuRouteContainerDto.tripType;
                AreaNode areaNode = AreaNodeConverter.convert(optimizedRoute.areaNode);
                if(areaNode == null) {
                    return null;
                }
                Team team = areaNode.getTeam();
                RouteOptimization routeOptimization = routeOptimizationService.save(jetkizuRoute.getRouteOptimization());

                String name = jetkizuRoute.getName();
                if(team != null && !StringUtils.isEmpty(team.getName())) {
                    name = jetkizuRoute.getName() + " - " + team.getName();
                }
                jetkizuRoute.setName(name);
                jetkizuRoute.setStart(start);
                jetkizuRoute.setEnd(end);
                jetkizuRoute.setTripType(tripType);
                jetkizuRoute.setAreaNode(areaNode);
                jetkizuRoute.setTeam(team);
                jetkizuRoute.setRouteOptimization(routeOptimization);
                jetkizuRoute.setStartedBy(userService.getUserWithAuthorities());
                jetkizuRoute.setStartedDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
                jetkizuRoute.setCreateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
                jetkizuRoute.setFlagDeleted(false);
                jetkizuRoute.setFlagStopped(false);
                jetkizuRoute.setStatus(JetkizuRouteStatus.STARTED);
                JetkizuRoute result = save(jetkizuRoute);

                BigDecimal totalDuration = BigDecimal.ZERO;
                BigDecimal totalDistance = BigDecimal.ZERO;

                for(RouteBranchDto routeBranchDto: optimizedRoute.routeBranches) {
                    RouteBranch branch = RouteBranchConverter.convert(routeBranchDto);
                    if(branch.getStartFrom() != null && branch.getStartFrom().getFlagGarage()) {
                        branch.setStartFrom(start);
                    }
                    if(branch.getNextStop() != null && branch.getNextStop().getFlagGarage()) {
                        if(TripType.ROUND_TRIP.equals(tripType)) {
                            branch.setNextStop(start);
                        } else {
                            branch.setNextStop(end);
                        }
                    }
                    Location previous = branch.getStartFrom();
                    Location current = branch.getEndTo();
                    Location next = branch.getNextStop();

                    branch.setStartFrom(previous);
                    branch.setEndTo(current);
                    branch.setNextStop(next);

                    JetkizuActivity jetkizuActivity = branch.getJetkizuActivity();
                    jetkizuActivity.setRouteBranch(branch);
                    if(jetkizuActivity.getPlanKm() != null) {
                        totalDistance = totalDistance.add(jetkizuActivity.getPlanKm());
                    }
                    if(jetkizuActivity.getPlanDuration() != null) {
                        totalDuration = totalDuration.add(jetkizuActivity.getPlanDuration());
                    }
                    if(jetkizuActivity.getPlanDurationToNext() != null) {
                        totalDuration = totalDuration.add(jetkizuActivity.getPlanDurationToNext());
                    }
                    if(jetkizuActivity.getPlanDistanceToNext() != null) {
                        totalDistance = totalDistance.add(jetkizuActivity.getPlanDistanceToNext());
                    }
                    branch.setJetkizuActivity(jetkizuActivityService.create(jetkizuActivity));

                    branch.setJetkizuRoute(result);
                    branch.setRouteBranchStatusHistory(routeBranchStatusHistoryService.create(branch, RouteBranchStatus.IN_PROCESS));

                    routeBranchService.save(branch);
                }

                result.setTotalPlanDuration(totalDuration);
                result.setTotalPlanDistance(totalDistance);
                save(result);

                return result.getId();
            }).filter(Objects::nonNull).collect(Collectors.toSet());

        /*JetkizuRouteDTO jetkizuRouteDTO = jetkizuRouteContainerDto.jetkizuRoute;
        JetkizuRoute jetkizuRoute = JetkizuRouteConverter.convert(jetkizuRouteDTO);
        Set<RouteBranch> routeBranches = new HashSet<>(jetkizuRouteDTO.routeBranches.stream().map(RouteBranchConverter::convert).collect(Collectors.toSet()));

        jetkizuRoute.setStartedBy(userService.getUserWithAuthorities());
        jetkizuRoute.setStartedDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        jetkizuRoute.setStatus(JetkizuRouteStatus.STARTED);
        JetkizuRoute result = save(jetkizuRoute);

        result.setRouteBranches(routeBranches);
        updateRouteBranches(result, RouteBranchStatus.NOT_STARTED, RouteBranchStatus.IN_PROCESS);*/

        return routeIds;
    }

    @Override
    @Transactional
    public JetkizuRoute updateWithNewElements(JetkizuRouteDTO jetkizuRouteDTO) {
        log.debug("Request to update optimized route: {}", jetkizuRouteDTO);
        JetkizuRoute jetkizuRoute = JetkizuRouteConverter.convert(jetkizuRouteDTO);
        Set<RouteBranch> optimizedBranches = updateRouteBranches(jetkizuRoute, RouteBranchStatus.BACKLOG, RouteBranchStatus.IN_PROCESS);
        BigDecimal totalPlanDistance = BigDecimal.ZERO;
        BigDecimal totalPlanDuration = BigDecimal.ZERO;
        for(RouteBranch branch: optimizedBranches) {
            totalPlanDistance = totalPlanDistance.add(branch.getJetkizuActivity().getPlanKm());
            totalPlanDuration = totalPlanDuration.add(branch.getJetkizuActivity().getPlanDuration());
        }
        jetkizuRoute.setTotalPlanDuration(totalPlanDuration);
        jetkizuRoute.setTotalPlanDistance(totalPlanDistance);
        return save(jetkizuRoute);
    }

    private Set<RouteBranch> updateRouteBranches(JetkizuRoute jetkizuRoute, RouteBranchStatus fromStatus, RouteBranchStatus toStatus) {
        log.debug("Request to update route branches: {}", jetkizuRoute);
        if(jetkizuRoute == null || jetkizuRoute.getId() == null) {
            throw new FLCException(Errors.JETKIZU_ROUTE_EMPTY);
        }
        log.debug("Route branches: {}", jetkizuRoute.getRouteBranches());
        log.debug("Recompiled ---------------------");
        return jetkizuRoute.getRouteBranches()
            .stream()
            .map(branch -> {
                branch.setJetkizuRoute(jetkizuRoute);
                // Save route garages
                locationService.createGarage(branch);

                if(branch.getId() == null) {
                    branch.setFlagDeleted(false);
                    branch = routeBranchService.save(branch);
                }

                RouteBranchStatusHistory statusHistory = branch.getRouteBranchStatusHistory();
                if(statusHistory.getId() == null ||
                    (statusHistory.getStatus() != null && statusHistory.getStatus().equals(fromStatus))) {
                    branch.setRouteBranchStatusHistory(routeBranchStatusHistoryService.create(branch, toStatus));
                }
                // branch.setJetkizuActivity(jetkizuActivityService.create(branch));
                return routeBranchService.save(branch);
            }).collect(Collectors.toSet());
    }

    @Override
    public List<JetkizuRouteDTO> getRouteAddresses(JetkizuRouteDTO jetkizuRoute) {
        log.debug("Request to get route addresses: {}", jetkizuRoute);
        List<Long> areaNodeIds = new ArrayList<Long>();
        List<JetkizuRouteDTO> optimizedRoutes = jetkizuRoute.areaNodes.stream()
            .map(an -> {
                areaNodeIds.add(an.getId());
                Location start = LocationConverter.convert(jetkizuRoute.start);
                Location end = LocationConverter.convert(jetkizuRoute.end);
                TripType tripType = jetkizuRoute.tripType;
                Geometry areaNodePolygon = GisUtils.toPolygon(an.getPolygon());
                List<RouteBranch> backlogRouteBranches = routeBranchService.findAllBacklogWithinPolygon(areaNodePolygon);
                log.debug("Searched backlog route branches within polygon and size: {}", backlogRouteBranches.size());

                JetkizuRouteDTO collectedRoute = new JetkizuRouteDTO();
                collectedRoute.areaNode = an;
                if(backlogRouteBranches.size() == 0) {
                    return collectedRoute;
                }

                Set<RouteBranch> sequencedRouteBranches = toSequencedRouteBranches(backlogRouteBranches, tripType, start, end);

                collectedRoute.routeBranches = sequencedRouteBranches.stream().map(RouteBranchDto::new).collect(Collectors.toSet());
                log.debug("Route collected: {}", collectedRoute);

                return collectedRoute;
            })
            .collect(Collectors.toList());

        List<RouteBranch> backlogs = routeBranchService.findAllBacklogNotWithoutPolygons(areaNodeIds);
        if(backlogs.size() > 0) {
            log.debug("Found {} without polygons orders", backlogs.size());
            JetkizuRouteDTO withoutPolygonsRoute = new JetkizuRouteDTO();
            withoutPolygonsRoute.flagWithoutPolygons = true;
            withoutPolygonsRoute.routeBranches = backlogs.stream().map(RouteBranchDto::new).collect(Collectors.toSet());
            optimizedRoutes.add(withoutPolygonsRoute);
        }

        return optimizedRoutes;
    }

    private Set<RouteBranch> toSequencedRouteBranches(List<RouteBranch> routeBranches, TripType tripType, Location start, Location end) {
        List<Long> osmNodeIds = collectOsmNodeIds(routeBranches);
        Map<Long, RouteBranch> routeBranchNodes = routeBranchesToNodeMap(routeBranches);
        Set<RouteBranch> result = new HashSet<>();

        List<Object[]> optimalRoute = osmService.getSequencedRoute(tripType, osmNodeIds, start, end);
        optimalRoute
            .stream()
            .filter(or -> routeBranchNodes.containsKey(((BigInteger) or[1]).longValue()))
            .forEach(or -> {
                Integer seq = (Integer) or[0];
                Long nodeId = ((BigInteger) or[1]).longValue();
                log.debug("seq: {}, nodeId: {}", seq, nodeId);

                RouteBranch sequencedBranch = routeBranchNodes.get(nodeId);
                sequencedBranch.setOrder(((Integer)seq).floatValue());
                log.debug("current branch: {}", sequencedBranch.getEndTo().getFullAddress());

                Long previousNodeId = ((BigInteger) optimalRoute.get(seq - 2)[1]).longValue();
                Long nextNodeId = ((BigInteger) optimalRoute.get(seq)[1]).longValue();
                log.debug("current: {}, previous: {}, next: {}", nodeId, previousNodeId, nextNodeId);

                // previous address location
                Location previousAddress = null;
                if(seq == 2) {
                    previousAddress = start;
                } else {
                    previousAddress = routeBranchNodes.get(previousNodeId).getEndTo();
                }
                sequencedBranch.setStartFrom(previousAddress);

                // next address location
                Location nextAddress = null;
                if(routeBranchNodes.containsKey(nextNodeId)) {
                    nextAddress = routeBranchNodes.get(nextNodeId).getEndTo();
                } else {
                    if(end != null) {
                        nextAddress = end;
                    } else {
                        nextAddress = start;
                    }
                }
                sequencedBranch.setNextStop(nextAddress);

                log.debug("Current address: {}, previous address: {}, next address: {}",
                    sequencedBranch.getEndTo().getFullAddress(),
                    sequencedBranch.getStartFrom(),
                    sequencedBranch.getNextStop());

                result.add(sequencedBranch);
            });

        return result;
    }

    private List<Long> collectOsmNodeIds(List<RouteBranch> backlogRouteBranches) {
        List<Long> result = new ArrayList<>();

        for(RouteBranch r: backlogRouteBranches) {
            result.add(r.getEndTo().getOsmId());
            log.debug("Added backlog osm id: {}", r.getEndTo().getOsmId());
        }

        return result;
    }

    private Map<Long, RouteBranch> routeBranchesToNodeMap(List<RouteBranch> routeBranches) {
        Map<Long, RouteBranch> result = new HashMap<Long, RouteBranch>();
        routeBranches.forEach(b -> result.put(b.getEndTo().getOsmId(), b));

        return result;
    }

    @Override
    public JetkizuRouteContainerDto optimizeRoutes(JetkizuRouteContainerDto jetkizuRouteContainerDto) {
        log.debug("Request to optimize routes: {}", jetkizuRouteContainerDto);

        Location start = LocationConverter.convert(jetkizuRouteContainerDto.start);
        Location end = LocationConverter.convert(jetkizuRouteContainerDto.end);
        jetkizuRouteContainerDto.jetkizuRoutes.stream()
            .filter(route -> (route.flagWithoutPolygons == null || !route.flagWithoutPolygons) && route.routeBranches.size() > 0)
            .forEach(route -> {
                List<RouteBranch> routeBranches = route.routeBranches
                    .stream()
                    .map(RouteBranchConverter::convert)
                    .collect(Collectors.toList());
                Set<RouteBranch> sequencedRouteBranches =
                    toSequencedRouteBranches(
                        routeBranches,
                        jetkizuRouteContainerDto.tripType,
                        start,
                        end
                    );

                route.routeBranches = sequencedRouteBranches.stream().map(RouteBranchDto::new).collect(Collectors.toSet());

                log.debug("finished to optimize route: {}", route);
            });

        return jetkizuRouteContainerDto;
    }

    @Override
    public Set<RouteBranch> importBranches(JetkizuRouteDTO jetkizuRouteDTO) {
        return jetkizuRouteDTO.routeBranches.stream()
            .map(branchDto -> {
                RouteBranch branch = RouteBranchConverter.convert(branchDto);
                return this.routeBranchService.create(branch);
            }).collect(Collectors.toSet());
    }

    @Override
    @Transactional
    public Integer getBranchesAmount(Long id) {
        log.debug("Request to get branches amount: {}", id);
        return ((Long) this.routeBranchService.countByJetkizuRouteId(id)).intValue();
    }
}
