package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.domain.Team;
import kz.rs.jetkizu.service.TeamMemberService;
import kz.rs.jetkizu.domain.TeamMember;
import kz.rs.jetkizu.repository.TeamMemberRepository;
import kz.rs.jetkizu.repository.search.TeamMemberSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing TeamMember.
 */
@Service
@Transactional
public class TeamMemberServiceImpl implements TeamMemberService{

    private final Logger log = LoggerFactory.getLogger(TeamMemberServiceImpl.class);

    private final TeamMemberRepository teamMemberRepository;

    private final TeamMemberSearchRepository teamMemberSearchRepository;

    public TeamMemberServiceImpl(TeamMemberRepository teamMemberRepository, TeamMemberSearchRepository teamMemberSearchRepository) {
        this.teamMemberRepository = teamMemberRepository;
        this.teamMemberSearchRepository = teamMemberSearchRepository;
    }

    /**
     * Save a teamMember.
     *
     * @param teamMember the entity to save
     * @return the persisted entity
     */
    @Override
    public TeamMember save(TeamMember teamMember) {
        log.debug("Request to save TeamMember : {}", teamMember);
        TeamMember result = teamMemberRepository.save(teamMember);
        teamMemberSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the teamMembers.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TeamMember> findAll() {
        log.debug("Request to get all TeamMembers");
        return teamMemberRepository.findAll();
    }

    /**
     *  Get one teamMember by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public TeamMember findOne(Long id) {
        log.debug("Request to get TeamMember : {}", id);
        return teamMemberRepository.findOne(id);
    }

    /**
     *  Delete the  teamMember by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete TeamMember : {}", id);
        TeamMember teamMember = teamMemberRepository.findOne(id);
        teamMember.setFlagDeleted(true);
        save(teamMember);
        // teamMemberRepository.delete(id);
        // teamMemberSearchRepository.delete(id);
    }

    /**
     * Search for the teamMember corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<TeamMember> search(String query) {
        log.debug("Request to search TeamMembers for query {}", query);
        return StreamSupport
            .stream(teamMemberSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Override
    public List<TeamMember> findAllByTeamId(Long id) {
        log.debug("Request to find all members by team id: {}", id);
        return teamMemberRepository.findAllByTeamIdAndFlagDeletedFalse(id);
    }

    @Override
    public void saveAllTeamMembers(Team team, Set<TeamMember> teamMembers) {
        log.debug("Request to save team members: {}: {}", team, teamMembers);
        teamMembers
            .forEach(teamMember -> {
                teamMember.setTeam(team);
                save(teamMember);
            });
    }
}
