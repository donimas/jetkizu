package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.service.JetkizuPauseService;
import kz.rs.jetkizu.domain.JetkizuPause;
import kz.rs.jetkizu.repository.JetkizuPauseRepository;
import kz.rs.jetkizu.repository.search.JetkizuPauseSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing JetkizuPause.
 */
@Service
@Transactional
public class JetkizuPauseServiceImpl implements JetkizuPauseService{

    private final Logger log = LoggerFactory.getLogger(JetkizuPauseServiceImpl.class);

    private final JetkizuPauseRepository jetkizuPauseRepository;

    private final JetkizuPauseSearchRepository jetkizuPauseSearchRepository;

    public JetkizuPauseServiceImpl(JetkizuPauseRepository jetkizuPauseRepository, JetkizuPauseSearchRepository jetkizuPauseSearchRepository) {
        this.jetkizuPauseRepository = jetkizuPauseRepository;
        this.jetkizuPauseSearchRepository = jetkizuPauseSearchRepository;
    }

    /**
     * Save a jetkizuPause.
     *
     * @param jetkizuPause the entity to save
     * @return the persisted entity
     */
    @Override
    public JetkizuPause save(JetkizuPause jetkizuPause) {
        log.debug("Request to save JetkizuPause : {}", jetkizuPause);
        JetkizuPause result = jetkizuPauseRepository.save(jetkizuPause);
        jetkizuPauseSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the jetkizuPauses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JetkizuPause> findAll(Pageable pageable) {
        log.debug("Request to get all JetkizuPauses");
        return jetkizuPauseRepository.findAll(pageable);
    }

    /**
     *  Get one jetkizuPause by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public JetkizuPause findOne(Long id) {
        log.debug("Request to get JetkizuPause : {}", id);
        return jetkizuPauseRepository.findOne(id);
    }

    /**
     *  Delete the  jetkizuPause by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JetkizuPause : {}", id);
        jetkizuPauseRepository.delete(id);
        jetkizuPauseSearchRepository.delete(id);
    }

    /**
     * Search for the jetkizuPause corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<JetkizuPause> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of JetkizuPauses for query {}", query);
        Page<JetkizuPause> result = jetkizuPauseSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }
}
