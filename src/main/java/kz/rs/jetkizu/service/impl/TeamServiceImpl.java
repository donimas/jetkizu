package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.domain.TeamMember;
import kz.rs.jetkizu.service.TeamMemberService;
import kz.rs.jetkizu.service.TeamService;
import kz.rs.jetkizu.domain.Team;
import kz.rs.jetkizu.repository.TeamRepository;
import kz.rs.jetkizu.repository.search.TeamSearchRepository;
import kz.rs.jetkizu.service.dto.TeamDTO;
import kz.rs.jetkizu.service.util.TeamConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Team.
 */
@Service
@Transactional
public class TeamServiceImpl implements TeamService{

    private final Logger log = LoggerFactory.getLogger(TeamServiceImpl.class);

    private final TeamRepository teamRepository;

    private final TeamSearchRepository teamSearchRepository;

    private final TeamMemberService teamMemberService;

    public TeamServiceImpl(TeamRepository teamRepository, TeamSearchRepository teamSearchRepository, TeamMemberService teamMemberService) {
        this.teamRepository = teamRepository;
        this.teamSearchRepository = teamSearchRepository;
        this.teamMemberService = teamMemberService;
    }

    /**
     * Save a team.
     *
     * @param team the entity to save
     * @return the persisted entity
     */
    @Override
    public Team save(Team team) {
        log.debug("Request to save Team : {}", team);
        Team result = teamRepository.save(team);
        teamSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the teams.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Team> findAll(Pageable pageable) {
        log.debug("Request to get all Teams");
        return teamRepository.findAll(pageable);
    }

    /**
     *  Get one team by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Team findOne(Long id) {
        log.debug("Request to get Team : {}", id);
        return teamRepository.findOne(id);
    }

    /**
     *  Delete the  team by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Team : {}", id);
        teamRepository.delete(id);
        teamSearchRepository.delete(id);
    }

    /**
     * Search for the team corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Team> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Teams for query {}", query);
        Page<Team> result = teamSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }



    @Override
    public Team create(TeamDTO teamDTO) {
        log.debug("Request to create team: {}", teamDTO);
        Team team = TeamConverter.convert(teamDTO);
        String generatedName = generateTeamGroupName();
        team.setName(generatedName);
        Team savedTeam = save(team);
        teamMemberService.saveAllTeamMembers(savedTeam, teamDTO.teamMembers);
        return savedTeam;
    }

    @Override
    public Team update(TeamDTO teamDTO) {
        log.debug("Request to update team: {}", teamDTO);
        Team savedTeam = save(TeamConverter.convert(teamDTO));
        teamMemberService.saveAllTeamMembers(savedTeam, teamDTO.teamMembers);
        return savedTeam;
    }

    public String generateTeamGroupName() {
        // TODO: Need to add prefix to make group name separate by area (or country, city)
        long count = teamRepository.count()+1;
        return "gd-"+count;
    }
}
