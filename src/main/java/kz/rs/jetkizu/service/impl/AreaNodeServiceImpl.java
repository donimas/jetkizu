package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.service.AreaNodeService;
import kz.rs.jetkizu.domain.AreaNode;
import kz.rs.jetkizu.repository.AreaNodeRepository;
import kz.rs.jetkizu.repository.search.AreaNodeSearchRepository;
import kz.rs.jetkizu.service.dto.AreaNodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AreaNode.
 */
@Service
@Transactional
public class AreaNodeServiceImpl implements AreaNodeService{

    private final Logger log = LoggerFactory.getLogger(AreaNodeServiceImpl.class);

    private final AreaNodeRepository areaNodeRepository;

    private final AreaNodeSearchRepository areaNodeSearchRepository;

    public AreaNodeServiceImpl(AreaNodeRepository areaNodeRepository, AreaNodeSearchRepository areaNodeSearchRepository) {
        this.areaNodeRepository = areaNodeRepository;
        this.areaNodeSearchRepository = areaNodeSearchRepository;
    }

    /**
     * Save a areaNode.
     *
     * @param areaNode the entity to save
     * @return the persisted entity
     */
    @Override
    public AreaNode save(AreaNode areaNode) {
        log.debug("Request to save AreaNode : {}", areaNode);
        AreaNode result = areaNodeRepository.save(areaNode);
        areaNodeSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the areaNodes.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AreaNodeDto> findAll() {
        log.debug("Request to get all AreaNodes");
        return areaNodeRepository.findAll().stream().map(AreaNodeDto::new).collect(Collectors.toList());
    }

    /**
     *  Get one areaNode by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public AreaNodeDto findOne(Long id) {
        log.debug("Request to get AreaNode : {}", id);
        return new AreaNodeDto(areaNodeRepository.findOne(id));
    }

    /**
     *  Delete the  areaNode by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AreaNode : {}", id);
        areaNodeRepository.delete(id);
        areaNodeSearchRepository.delete(id);
    }

    /**
     * Search for the areaNode corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<AreaNodeDto> search(String query) {
        log.debug("Request to search AreaNodes for query {}", query);
        return StreamSupport
            .stream(areaNodeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(AreaNodeDto::new)
            .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AreaNodeDto> findAllByAreaId(Long areaId, Pageable pageable) {
        log.debug("Request to get all nodes of area: {}", areaId);
        Page<AreaNode> pages = this.areaNodeRepository.findAllByAreaId(areaId, pageable);
        return pages.getContent()
            .stream()
            .map(AreaNodeDto::new)
            .collect(Collectors.toList());
    }
}
