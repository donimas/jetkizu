package kz.rs.jetkizu.service.impl;

import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;
import kz.rs.jetkizu.service.JetkizuStatusHistoryService;
import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import kz.rs.jetkizu.repository.JetkizuStatusHistoryRepository;
import kz.rs.jetkizu.repository.search.JetkizuStatusHistorySearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing JetkizuStatusHistory.
 */
@Service
@Transactional
public class JetkizuStatusHistoryServiceImpl implements JetkizuStatusHistoryService{

    private final Logger log = LoggerFactory.getLogger(JetkizuStatusHistoryServiceImpl.class);

    private final JetkizuStatusHistoryRepository jetkizuStatusHistoryRepository;

    private final JetkizuStatusHistorySearchRepository jetkizuStatusHistorySearchRepository;

    public JetkizuStatusHistoryServiceImpl(JetkizuStatusHistoryRepository jetkizuStatusHistoryRepository, JetkizuStatusHistorySearchRepository jetkizuStatusHistorySearchRepository) {
        this.jetkizuStatusHistoryRepository = jetkizuStatusHistoryRepository;
        this.jetkizuStatusHistorySearchRepository = jetkizuStatusHistorySearchRepository;
    }

    /**
     * Save a jetkizuStatusHistory.
     *
     * @param jetkizuStatusHistory the entity to save
     * @return the persisted entity
     */
    @Override
    public JetkizuStatusHistory save(JetkizuStatusHistory jetkizuStatusHistory) {
        log.debug("Request to save JetkizuStatusHistory : {}", jetkizuStatusHistory);
        JetkizuStatusHistory result = jetkizuStatusHistoryRepository.save(jetkizuStatusHistory);
        jetkizuStatusHistorySearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the jetkizuStatusHistories.
     *
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<JetkizuStatusHistory> findAll() {
        log.debug("Request to get all JetkizuStatusHistories");
        return jetkizuStatusHistoryRepository.findAll();
    }

    /**
     *  Get one jetkizuStatusHistory by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public JetkizuStatusHistory findOne(Long id) {
        log.debug("Request to get JetkizuStatusHistory : {}", id);
        return jetkizuStatusHistoryRepository.findOne(id);
    }

    /**
     *  Delete the  jetkizuStatusHistory by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete JetkizuStatusHistory : {}", id);
        jetkizuStatusHistoryRepository.delete(id);
        jetkizuStatusHistorySearchRepository.delete(id);
    }

    /**
     * Search for the jetkizuStatusHistory corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<JetkizuStatusHistory> search(String query) {
        log.debug("Request to search JetkizuStatusHistories for query {}", query);
        return StreamSupport
            .stream(jetkizuStatusHistorySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

    @Override
    public JetkizuStatusHistory create(Jetkizu jetkizu, JetkizuStatus status) {
        log.debug("Request to create JetkizuStatusHistory: {}", jetkizu);
        JetkizuStatusHistory statusHistory = new JetkizuStatusHistory();
        statusHistory.setCreateDateTime(ZonedDateTime.now(ZoneId.systemDefault()));
        statusHistory.setFlagDeleted(false);
        statusHistory.setJetkizu(jetkizu);
        statusHistory.setStatus(status);
        return save(statusHistory);
    }
}
