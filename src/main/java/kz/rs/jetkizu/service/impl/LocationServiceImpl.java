package kz.rs.jetkizu.service.impl;

import com.vividsolutions.jts.geom.Geometry;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.domain.enumeration.RouteBranchType;
import kz.rs.jetkizu.service.LocationService;
import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.repository.LocationRepository;
import kz.rs.jetkizu.repository.search.LocationSearchRepository;
import kz.rs.jetkizu.service.OsmService;
import kz.rs.jetkizu.service.util.GisUtils;
import kz.rs.jetkizu.service.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Location.
 */
@Service
@Transactional
public class LocationServiceImpl implements LocationService{

    private final Logger log = LoggerFactory.getLogger(LocationServiceImpl.class);

    private final LocationRepository locationRepository;

    private final LocationSearchRepository locationSearchRepository;
    private final OsmService osmService;

    public LocationServiceImpl(LocationRepository locationRepository, LocationSearchRepository locationSearchRepository, OsmService osmService) {
        this.locationRepository = locationRepository;
        this.locationSearchRepository = locationSearchRepository;
        this.osmService = osmService;
    }

    /**
     * Save a location.
     *
     * @param location the entity to save
     * @return the persisted entity
     */
    @Override
    public Location save(Location location) {
        log.debug("Request to save Location : {}", location);
        if(location == null) {
            return null;
        }
        if(!StringUtils.isEmpty(location.getLat()) && !StringUtils.isEmpty(location.getLon())) {
            Geometry point = GisUtils.toPoint(location.getLon(), location.getLat());
            location.setWhereIs(point);
        }
        location.setOsmId(osmService.convertOsmWayToOsmNodeId(location.getOsmId(), location.getOsmType()));
        location.setOsmType("node");

        Location result = locationRepository.save(location);
        locationSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the locations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Location> findAll(Pageable pageable) {
        log.debug("Request to get all Locations");
        return locationRepository.findAll(pageable);
    }

    /**
     *  Get one location by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Location findOne(Long id) {
        log.debug("Request to get Location : {}", id);
        return locationRepository.findOne(id);
    }

    /**
     *  Delete the  location by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Location : {}", id);
        locationRepository.delete(id);
        locationSearchRepository.delete(id);
    }

    /**
     * Search for the location corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Location> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Locations for query {}", query);
        Page<Location> result = locationSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    @Override
    public Location create(Location location) {
        log.debug("Request to create Location: {}", location);
        if(location.getId() != null) {
            return location;
        }
        return save(location);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Location> findAllGarageLocations(Pageable pageable) {
        log.debug("Request to get all garage Locations");
        return locationRepository.findAllByFlagGarageTrue(pageable);
    }

    @Override
    public Location createGarage(RouteBranch routeBranch) {
        log.debug("Request to create garage: {}", routeBranch);
        Location garage = null;
        if(routeBranch.getOrder().compareTo(1.0F) == 0) {
            garage = routeBranch.getStartFrom();
        } else if(routeBranch.getType() != null && routeBranch.getType().equals(RouteBranchType.END)) {
            garage = routeBranch.getEndTo();
        }
        if(garage == null) {
            return null;
        }
        garage.setFlagGarage(true);
        return save(garage);
    }
}
