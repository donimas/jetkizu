package kz.rs.jetkizu.service.impl;

import com.vividsolutions.jts.geom.Geometry;
import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;
import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;
import kz.rs.jetkizu.service.*;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.repository.RouteBranchRepository;
import kz.rs.jetkizu.repository.search.RouteBranchSearchRepository;
import kz.rs.jetkizu.web.rest.constants.Errors;
import kz.rs.jetkizu.web.rest.errors.FLCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RouteBranch.
 */
@Service
@Transactional
public class RouteBranchServiceImpl implements RouteBranchService{

    private final Logger log = LoggerFactory.getLogger(RouteBranchServiceImpl.class);

    private final RouteBranchRepository routeBranchRepository;

    private final RouteBranchSearchRepository routeBranchSearchRepository;

    private final JetkizuService jetkizuService;
    private final RouteBranchStatusHistoryService routeBranchStatusHistoryService;
    private final CustomerService customerService;
    private final LocationService locationService;

    public RouteBranchServiceImpl(RouteBranchRepository routeBranchRepository, RouteBranchSearchRepository routeBranchSearchRepository,
                                  JetkizuService jetkizuService, RouteBranchStatusHistoryService routeBranchStatusHistoryService,
                                  CustomerService customerService, LocationService locationService) {
        this.routeBranchRepository = routeBranchRepository;
        this.routeBranchSearchRepository = routeBranchSearchRepository;
        this.jetkizuService = jetkizuService;
        this.routeBranchStatusHistoryService = routeBranchStatusHistoryService;
        this.customerService = customerService;
        this.locationService = locationService;
    }

    /**
     * Save a routeBranch.
     *
     * @param routeBranch the entity to save
     * @return the persisted entity
     */
    @Override
    public RouteBranch save(RouteBranch routeBranch) {
        log.debug("Request to save RouteBranch : {}", routeBranch);
        RouteBranch result = routeBranchRepository.save(routeBranch);
        routeBranchSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the routeBranches.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RouteBranch> findAll(Pageable pageable) {
        log.debug("Request to get all RouteBranches");
        return routeBranchRepository.findAll(pageable);
    }

    /**
     *  Get one routeBranch by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public RouteBranch findOne(Long id) {
        log.debug("Request to get RouteBranch : {}", id);
        return routeBranchRepository.findOne(id);
    }

    /**
     *  Delete the  routeBranch by id.
     *
     *  @param id the id of the entity
     */
    @Override
    @Transactional
    public void delete(Long id) {
        log.debug("Request to delete RouteBranch : {}", id);
        RouteBranch routeBranch = findOne(id);
        if(!RouteBranchStatus.NOT_STARTED.equals(routeBranch.getRouteBranchStatusHistory().getStatus())
            && !RouteBranchStatus.BACKLOG.equals(routeBranch.getRouteBranchStatusHistory().getStatus())) {
            throw new FLCException(Errors.ACTIVE_BRANCH_DELETE_ERROR);
        }
        Jetkizu jetkizu = routeBranch.getJetkizu();
        if(RouteBranchStatus.NOT_STARTED.equals(routeBranch.getRouteBranchStatusHistory().getStatus())) {
            jetkizuService.setStatus(jetkizu, JetkizuStatus.BACKLOG);
            routeBranch.setJetkizuRoute(null);
            setStatus(routeBranch, RouteBranchStatus.BACKLOG);
        } else if(RouteBranchStatus.BACKLOG.equals(routeBranch.getRouteBranchStatusHistory().getStatus())) {
            routeBranch.setFlagDeleted(true);
            save(routeBranch);
        }
    }

    /**
     * Search for the routeBranch corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RouteBranch> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of RouteBranches for query {}", query);
        Page<RouteBranch> result = routeBranchSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    @Transactional
    public RouteBranch createNew(RouteBranch routeBranch) {
        log.debug("Request to create new route branch: {}", routeBranch);
        if(routeBranch.getJetkizu() == null) {
            throw new FLCException(Errors.JETKIZU_EMPTY);
        }
        if(routeBranch.getCustomer() == null) {
            throw new FLCException(Errors.CUSTOMER_EMPTY);
        }
        if(routeBranch.getEndTo() == null) {
            throw new FLCException(Errors.LOCATION_EMPTY);
        }
        Jetkizu jetkizu = routeBranch.getJetkizu();
        if(jetkizu.getId() == null) {
            jetkizu = jetkizuService.create(routeBranch.getJetkizu());
        }
        if(routeBranch.getCustomer().getId() == null) {
            routeBranch.setCustomer(customerService.create(routeBranch.getCustomer()));
        }
        if(routeBranch.getEndTo().getId() == null) {
            routeBranch.setEndTo(locationService.create(routeBranch.getEndTo()));
        }
        routeBranch.setJetkizu(jetkizu);
        routeBranch.setFlagDeleted(false);
        RouteBranch result = save(routeBranch);
        jetkizu.setRouteBranch(result);
        Jetkizu savedJetkizu = jetkizuService.save(jetkizu);
        result.setJetkizu(savedJetkizu);

        return result;
    }

    @Override
    @Transactional
    public RouteBranch create(RouteBranch routeBranch) {
        log.debug("Request to create route branch: {}", routeBranch);
        RouteBranch result = createNew(routeBranch);
        jetkizuService.setStatus(result.getJetkizu(), JetkizuStatus.BACKLOG);
        return setStatus(result, RouteBranchStatus.BACKLOG);
    }

    @Override
    @Transactional
    public RouteBranch addNewToRoute(RouteBranch routeBranch) {
        log.debug("Request to add to JetkizuRoute: {}", routeBranch);
        if(routeBranch.getJetkizuRoute() == null) {
            throw new FLCException(Errors.JETKIZU_ROUTE_EMPTY);
        }
        RouteBranch result = createNew(routeBranch);
        jetkizuService.setStatus(result.getJetkizu(), JetkizuStatus.NOT_STARTED);
        return setStatus(result, RouteBranchStatus.NOT_STARTED);
    }

    @Override
    @Transactional
    public RouteBranch addToRoute(RouteBranch routeBranch) {
        log.debug("Request to add to route: {}", routeBranch);
        if(routeBranch.getJetkizuRoute() == null) {
            throw new FLCException(Errors.JETKIZU_ROUTE_EMPTY);
        }
        jetkizuService.setStatus(routeBranch.getJetkizu(), JetkizuStatus.NOT_STARTED);
        return setStatus(routeBranch, RouteBranchStatus.NOT_STARTED);
    }

    @Override
    public RouteBranch update(RouteBranch routeBranch) {
        jetkizuService.update(routeBranch.getJetkizu());
        return routeBranch;
    }

    @Override
    public Page<RouteBranch> findAllByJetkizuRouteId(Long id, Pageable pageable) {
        log.debug("Request to get all branches by route id: {}", id);
        return routeBranchRepository.findAllByJetkizuRouteIdAndFlagDeletedFalse(id, pageable);
    }

    @Override
    public Page<RouteBranch> findAllBacklogBranches(Pageable pageable) {
        log.debug("Request to get all backlog branches");
        return routeBranchRepository.findAllByFlagDeletedFalseAndRouteBranchStatusHistory_Status(RouteBranchStatus.BACKLOG, pageable);
    }

    @Override
    public RouteBranch setStatus(RouteBranch routeBranch, RouteBranchStatus status) {
        RouteBranchStatusHistory createdStatusHistory = routeBranchStatusHistoryService.create(routeBranch, status);
        routeBranch.setRouteBranchStatusHistory(createdStatusHistory);
        return save(routeBranch);
    }

    @Override
    public List<RouteBranch> findAllBacklogWithinPolygon(Geometry polygon) {
        log.debug("Request to find all backlog branches within polygon");
        return routeBranchRepository.findAllWithinPolygon(polygon);
    }

    @Override
    public List<RouteBranch> findAllBacklogNotWithoutPolygons(List<Long> areaNodeIds) {
        log.debug("Request to find all backlog branches without polygons: {}", areaNodeIds);
        return routeBranchRepository.findAllWithoutPolygons(areaNodeIds);
    }

    @Override
    public Long countByJetkizuRouteId(Long id) {
        log.debug("Request to count by route: {}", id);
        return this.routeBranchRepository.countByJetkizuRouteId(id);
    }
}
