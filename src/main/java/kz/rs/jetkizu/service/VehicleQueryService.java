package kz.rs.jetkizu.service;


import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import kz.rs.jetkizu.domain.Vehicle;
import kz.rs.jetkizu.domain.*; // for static metamodels
import kz.rs.jetkizu.repository.VehicleRepository;
import kz.rs.jetkizu.repository.search.VehicleSearchRepository;
import kz.rs.jetkizu.service.dto.VehicleCriteria;

import kz.rs.jetkizu.domain.enumeration.VehicleType;

/**
 * Service for executing complex queries for Vehicle entities in the database.
 * The main input is a {@link VehicleCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link Vehicle} or a {@link Page} of {%link Vehicle} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class VehicleQueryService extends QueryService<Vehicle> {

    private final Logger log = LoggerFactory.getLogger(VehicleQueryService.class);


    private final VehicleRepository vehicleRepository;

    private final VehicleSearchRepository vehicleSearchRepository;

    public VehicleQueryService(VehicleRepository vehicleRepository, VehicleSearchRepository vehicleSearchRepository) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleSearchRepository = vehicleSearchRepository;
    }

    /**
     * Return a {@link List} of {%link Vehicle} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Vehicle> findByCriteria(VehicleCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<Vehicle> specification = createSpecification(criteria);
        return vehicleRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link Vehicle} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Vehicle> findByCriteria(VehicleCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<Vehicle> specification = createSpecification(criteria);
        return vehicleRepository.findAll(specification, page);
    }

    /**
     * Function to convert VehicleCriteria to a {@link Specifications}
     */
    private Specifications<Vehicle> createSpecification(VehicleCriteria criteria) {
        Specifications<Vehicle> specification = Specifications.where(null);
        if (criteria != null) {
            /*if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Vehicle_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Vehicle_.name));
            }
            if (criteria.getGovNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getGovNumber(), Vehicle_.govNumber));
            }
            if (criteria.getTechPassportNumber() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTechPassportNumber(), Vehicle_.techPassportNumber));
            }
            if (criteria.getModel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getModel(), Vehicle_.model));
            }
            if (criteria.getBuildYear() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBuildYear(), Vehicle_.buildYear));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), Vehicle_.type));
            }
            if (criteria.getHeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getHeight(), Vehicle_.height));
            }
            if (criteria.getLength() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLength(), Vehicle_.length));
            }
            if (criteria.getWidth() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWidth(), Vehicle_.width));
            }
            if (criteria.getTotalWeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTotalWeight(), Vehicle_.totalWeight));
            }
            if (criteria.getDeliveryWeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDeliveryWeight(), Vehicle_.deliveryWeight));
            }
            if (criteria.getNote() != null) {
                specification = specification.and(buildStringSpecification(criteria.getNote(), Vehicle_.note));
            }*/
        }
        return specification;
    }

}
