package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.JetkizuRoute;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.service.dto.JetkizuRouteContainerDto;
import kz.rs.jetkizu.service.dto.JetkizuRouteDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

/**
 * Service Interface for managing JetkizuRoute.
 */
public interface JetkizuRouteService {

    /**
     * Save a jetkizuRoute.
     *
     * @param jetkizuRoute the entity to save
     * @return the persisted entity
     */
    JetkizuRoute save(JetkizuRoute jetkizuRoute);

    JetkizuRoute updateWithNewElements(JetkizuRouteDTO jetkizuRouteDTO);

    /**
     *  Get all the jetkizuRoutes.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<JetkizuRoute> findAll(Pageable pageable);

    /**
     *  Get the "id" jetkizuRoute.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    JetkizuRoute findOne(Long id);

    /**
     *  Delete the "id" jetkizuRoute.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the jetkizuRoute corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<JetkizuRoute> search(String query, Pageable pageable);

    JetkizuRoute create(JetkizuRoute jetkizuRoute);

    Page<JetkizuRoute> findByFlagStoppedFalse(Pageable pageable);
    Page<JetkizuRoute> findStarted(Pageable pageable);

    Set<Long> startOptimizedRoute(JetkizuRouteContainerDto jetkizuRouteContainerDto);
    List<JetkizuRouteDTO> getRouteAddresses(JetkizuRouteDTO jetkizuRoute);
    JetkizuRouteContainerDto optimizeRoutes(JetkizuRouteContainerDto jetkizuRouteContainerDto);


    Set<RouteBranch> importBranches(JetkizuRouteDTO jetkizuRouteDTO);
    Integer getBranchesAmount(Long id);
}
