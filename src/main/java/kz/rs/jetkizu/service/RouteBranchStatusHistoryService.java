package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;

import java.util.List;

/**
 * Service Interface for managing RouteBranchStatusHistory.
 */
public interface RouteBranchStatusHistoryService {

    /**
     * Save a routeBranchStatusHistory.
     *
     * @param routeBranchStatusHistory the entity to save
     * @return the persisted entity
     */
    RouteBranchStatusHistory save(RouteBranchStatusHistory routeBranchStatusHistory);

    /**
     *  Get all the routeBranchStatusHistories.
     *
     *  @return the list of entities
     */
    List<RouteBranchStatusHistory> findAll();

    /**
     *  Get the "id" routeBranchStatusHistory.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RouteBranchStatusHistory findOne(Long id);

    /**
     *  Delete the "id" routeBranchStatusHistory.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the routeBranchStatusHistory corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @return the list of entities
     */
    List<RouteBranchStatusHistory> search(String query);

    RouteBranchStatusHistory create(RouteBranch routeBranch, RouteBranchStatus status);
}
