package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.domain.enumeration.TripType;
import kz.rs.jetkizu.repository.OsmRepository;
import kz.rs.jetkizu.service.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class OsmService {

    private final Logger log = LoggerFactory.getLogger(OsmService.class);

    private final OsmRepository osmRepository;

    public OsmService(OsmRepository osmRepository) {
        this.osmRepository = osmRepository;
    }

    public Long convertOsmWayToOsmNodeId(Long osmId, String osmType) {
        log.debug("Request to find osm node id by way id: {}, type: {}", osmId, osmType);
        if(!StringUtils.isEmpty(osmType) && "node".equals(osmType)) {
            return osmId;
        } else if (StringUtils.isEmpty(osmType)){
            log.debug("OSM type is empty");
            return osmId;
        }
        Object[] hstoreValue = osmRepository.findOsmNodeIdByWay(osmId);
        log.debug("hstoreValues length: {}", hstoreValue.length);
        Long result = null;
        if(hstoreValue.length > 0) {
            result = Long.parseLong((String) hstoreValue[0]);
            log.debug("OSM NODE FOUND FROM WAY: {}", result);
        }

        return result;
    }

    public List<Object[]> getSequencedRoute(TripType tripType, List<Long> osmNodeIds, Location start, Location end) {
        log.debug("Request to get sequenced route: {}, {}, {}, {}", tripType, osmNodeIds, start, end);
        List<Object[]> result = null;
        Long startOsmId = convertOsmWayToOsmNodeId(start.getOsmId(), start.getOsmType());
        osmNodeIds.add(startOsmId);
        Long endOsmId = null;
        if(!tripType.equals(TripType.ROUND_TRIP)) {
            endOsmId = convertOsmWayToOsmNodeId(end.getOsmId(), end.getOsmType());
            osmNodeIds.add(endOsmId);
        }
        String osmNodeIdsParam = osmNodeIds
            .stream().map(id -> id+"")
            .collect(Collectors.joining(","));
        if(!tripType.equals(TripType.ROUND_TRIP)) {
            result = osmRepository.findOptimalRoute(osmNodeIdsParam, startOsmId, endOsmId);
        } else {
            result = osmRepository.findOptimalRouteRoundTrip(osmNodeIdsParam, startOsmId);
        }

        if(result.size() > 0) {
            log.debug("Collected optimized route =>");
            result.forEach(o -> {
                log.debug("seq: {}, node: {}, cost: {}, agg_cost: {}", o[0], o[1],o[2],o[3]);
            });
        } else {
            log.debug("Optimal Route is null or empty");
        }

        return result;
    }

}
