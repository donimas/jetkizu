package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

/**
 * Service Interface for managing Jetkizu.
 */
public interface JetkizuService {

    /**
     * Save a jetkizu.
     *
     * @param jetkizu the entity to save
     * @return the persisted entity
     */
    Jetkizu save(Jetkizu jetkizu);

    /**
     *  Get all the jetkizus.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Jetkizu> findAll(Pageable pageable);
    /**
     *  Get all the JetkizuDTO where RouteBranch is null.
     *
     *  @return the list of entities
     */
    List<Jetkizu> findAllWhereRouteBranchIsNull();

    /**
     *  Get the "id" jetkizu.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Jetkizu findOne(Long id);

    /**
     *  Delete the "id" jetkizu.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the jetkizu corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Jetkizu> search(String query, Pageable pageable);

    Jetkizu create(Jetkizu jetkizu);

    Jetkizu update(Jetkizu jetkizu);

    Jetkizu setStatus(Jetkizu jetkizu, JetkizuStatus status);

    Page<Jetkizu> findAllBacklog(Pageable pageable);
}
