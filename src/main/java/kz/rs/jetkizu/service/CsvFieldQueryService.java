package kz.rs.jetkizu.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import kz.rs.jetkizu.domain.CsvField;
import kz.rs.jetkizu.domain.*; // for static metamodels
import kz.rs.jetkizu.repository.CsvFieldRepository;
import kz.rs.jetkizu.repository.search.CsvFieldSearchRepository;
import kz.rs.jetkizu.service.dto.CsvFieldCriteria;

import kz.rs.jetkizu.domain.enumeration.CsvFieldType;

/**
 * Service for executing complex queries for CsvField entities in the database.
 * The main input is a {@link CsvFieldCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link CsvField} or a {@link Page} of {%link CsvField} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class CsvFieldQueryService extends QueryService<CsvField> {

    private final Logger log = LoggerFactory.getLogger(CsvFieldQueryService.class);


    private final CsvFieldRepository csvFieldRepository;

    private final CsvFieldSearchRepository csvFieldSearchRepository;

    public CsvFieldQueryService(CsvFieldRepository csvFieldRepository, CsvFieldSearchRepository csvFieldSearchRepository) {
        this.csvFieldRepository = csvFieldRepository;
        this.csvFieldSearchRepository = csvFieldSearchRepository;
    }

    /**
     * Return a {@link List} of {%link CsvField} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CsvField> findByCriteria(CsvFieldCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<CsvField> specification = createSpecification(criteria);
        return csvFieldRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {%link CsvField} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CsvField> findByCriteria(CsvFieldCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<CsvField> specification = createSpecification(criteria);
        return csvFieldRepository.findAll(specification, page);
    }

    /**
     * Function to convert CsvFieldCriteria to a {@link Specifications}
     */
    private Specifications<CsvField> createSpecification(CsvFieldCriteria criteria) {
        Specifications<CsvField> specification = Specifications.where(null);
        /*if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CsvField_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), CsvField_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), CsvField_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), CsvField_.description));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), CsvField_.type));
            }
        }*/
        return specification;
    }

}
