package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.Location;
import kz.rs.jetkizu.domain.RouteBranch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Location.
 */
public interface LocationService {

    /**
     * Save a location.
     *
     * @param location the entity to save
     * @return the persisted entity
     */
    Location save(Location location);

    /**
     *  Get all the locations.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Location> findAll(Pageable pageable);

    /**
     *  Get the "id" location.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Location findOne(Long id);

    /**
     *  Delete the "id" location.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the location corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Location> search(String query, Pageable pageable);

    Location create(Location location);

    Page<Location> findAllGarageLocations(Pageable pageable);

    Location createGarage(RouteBranch routeBranch);
}
