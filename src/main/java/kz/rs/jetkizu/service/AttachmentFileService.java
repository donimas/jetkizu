package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.AttachmentFile;
import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.repository.AttachmentFileRepository;
import kz.rs.jetkizu.repository.search.AttachmentFileSearchRepository;
import kz.rs.jetkizu.service.dto.OsmObjectDto;
import kz.rs.jetkizu.service.util.StringUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing AttachmentFile.
 */
@Service
@Transactional
public class AttachmentFileService {

    private final Logger log = LoggerFactory.getLogger(AttachmentFileService.class);

    private final AttachmentFileRepository attachmentFileRepository;

    private final AttachmentFileSearchRepository attachmentFileSearchRepository;

    private final OsmService osmService;

    public static final List<String> osmKeys = new ArrayList<>();

    static {
        osmKeys.add("place_id");
        osmKeys.add("osm_type");
        osmKeys.add("osm_id");
        osmKeys.add("lat");
        osmKeys.add("lon");
    }

    public static final String NOMINATIM_QUERY = "https://nominatim.openstreetmap.org/search?q=%s&format=json&polygon=1&addressdetails=1";

    public AttachmentFileService(AttachmentFileRepository attachmentFileRepository, AttachmentFileSearchRepository attachmentFileSearchRepository,
                                 OsmService osmService) {
        this.attachmentFileRepository = attachmentFileRepository;
        this.attachmentFileSearchRepository = attachmentFileSearchRepository;
        this.osmService = osmService;
    }

    /**
     * Save a attachmentFile.
     *
     * @param attachmentFile the entity to save
     * @return the persisted entity
     */
    public AttachmentFile save(AttachmentFile attachmentFile) {
        log.debug("Request to save AttachmentFile : {}", attachmentFile);
        AttachmentFile result = attachmentFileRepository.save(attachmentFile);
        attachmentFileSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the attachmentFiles.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AttachmentFile> findAll(Pageable pageable) {
        log.debug("Request to get all AttachmentFiles");
        return attachmentFileRepository.findAll(pageable);
    }

    /**
     *  Get one attachmentFile by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public AttachmentFile findOne(Long id) {
        log.debug("Request to get AttachmentFile : {}", id);
        return attachmentFileRepository.findOne(id);
    }

    /**
     *  Delete the  attachmentFile by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete AttachmentFile : {}", id);
        attachmentFileRepository.delete(id);
        attachmentFileSearchRepository.delete(id);
    }

    /**
     * Search for the attachmentFile corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<AttachmentFile> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of AttachmentFiles for query {}", query);
        Page<AttachmentFile> result = attachmentFileSearchRepository.search(queryStringQuery(query), pageable);
        return result;
    }

    public AttachmentFile importCsv(AttachmentFile attachmentFile) {
        log.debug("Request to import csv: {}", attachmentFile);
        if(attachmentFile.getFile() == null) {
            return null;
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(attachmentFile.getFile());
        BufferedReader reader = new BufferedReader(new InputStreamReader(bais));
        RestTemplate restTemplate = new RestTemplate();
        String query = null;
        try {
            CSVParser parser = CSVFormat.DEFAULT.withHeader().parse(reader);
            int i = 0;
            for (CSVRecord record: parser) {
                try{
                    i++;
                    String senderFio = getMappedRecord(record, "senderFio");
                    String receiverFio = getMappedRecord(record, "receiverFio");
                    String receiverAddress = getMappedRecord(record, "receiverAddress");
                    String mobilePhone1 = getMappedRecord(record, "mobilePhone1");
                    String mobilePhone2 = getMappedRecord(record, "mobilePhone2");
                    String price = getMappedRecord(record, "price");
                    String deliveryDate = getMappedRecord(record, "deliveryDate");
                    String weight = getMappedRecord(record, "weight");

                    // OSM API
                    if(!StringUtils.isEmpty(receiverAddress)) {
                        query = String.format(NOMINATIM_QUERY, receiverAddress);
                        log.debug("QUERY: {}", query);
                        List resultOsm =
                            (ArrayList) restTemplate.getForObject(query, ArrayList.class);
                        if(!resultOsm.isEmpty()) {
                            log.debug("size: {}, result: {}", resultOsm.size(), resultOsm);
                            OsmObjectDto osmObjectDto = new OsmObjectDto();
                            Map osm = (LinkedHashMap) resultOsm.get(0);
                            osmObjectDto.setPlaceId(Long.parseLong((String) osm.get("place_id")));
                            osmObjectDto.setOsmType((String) osm.get("osm_type"));
                            osmObjectDto.setOsmId(Long.parseLong((String) osm.get("osm_id")));
                            osmObjectDto.setLat((String) osm.get("lat"));
                            osmObjectDto.setLon((String) osm.get("lon"));

                            Map address = (LinkedHashMap) osm.get("address");
                            if(!address.isEmpty()) {
                                osmObjectDto.getAddress().setHouseNumber((String) address.get("house_number"));
                                osmObjectDto.getAddress().setRoad((String) address.get("road"));
                                osmObjectDto.getAddress().setCity((String) address.get("city"));
                                osmObjectDto.getAddress().setPostcode((String) address.get("postcode"));
                            }

                            osmObjectDto.setOsmId(osmService.convertOsmWayToOsmNodeId(osmObjectDto.getOsmId(), osmObjectDto.getOsmType()));
                            osmObjectDto.setOsmType("node");

                            log.debug("Osm object result: {}", osmObjectDto);
                        }
                    }



                    log.debug("{} -> {} -> {} -> {} -> {} -> {} -> {} -> {} -> {}",
                        i, senderFio, receiverFio, receiverAddress, mobilePhone1, mobilePhone2, price, deliveryDate, weight);
                } catch (IllegalArgumentException e) {
                    log.debug(e.getMessage(), e);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*AttachmentFile result = attachmentFileRepository.save(attachmentFile);
        attachmentFileSearchRepository.save(result);*/
        return null;
    }

    private String getMappedRecord(CSVRecord record, String key) {
        if(!record.isMapped(key)) return null;
        String result = record.get(key);
        return result != null ? result.trim() : result;
    }
}
