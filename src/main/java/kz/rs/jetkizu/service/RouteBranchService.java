package kz.rs.jetkizu.service;

import com.vividsolutions.jts.geom.Geometry;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Service Interface for managing RouteBranch.
 */
public interface RouteBranchService {

    /**
     * Save a routeBranch.
     *
     * @param routeBranch the entity to save
     * @return the persisted entity
     */
    RouteBranch save(RouteBranch routeBranch);

    /**
     *  Get all the routeBranches.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RouteBranch> findAll(Pageable pageable);

    /**
     *  Get the "id" routeBranch.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    RouteBranch findOne(Long id);

    /**
     *  Delete the "id" routeBranch.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the routeBranch corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<RouteBranch> search(String query, Pageable pageable);

    RouteBranch create(RouteBranch routeBranch);

    RouteBranch addNewToRoute(RouteBranch routeBranch);
    RouteBranch addToRoute(RouteBranch routeBranch);

    RouteBranch setStatus(RouteBranch routeBranch, RouteBranchStatus status);

    RouteBranch update(RouteBranch routeBranch);

    Page<RouteBranch> findAllByJetkizuRouteId(Long id, Pageable pageable);
    Page<RouteBranch> findAllBacklogBranches(Pageable pageable);
    List<RouteBranch> findAllBacklogWithinPolygon(Geometry polygon);
    List<RouteBranch> findAllBacklogNotWithoutPolygons(List<Long> areaNodeIds);
    Long countByJetkizuRouteId(Long id);
}
