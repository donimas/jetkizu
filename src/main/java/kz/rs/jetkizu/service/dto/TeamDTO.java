package kz.rs.jetkizu.service.dto;

import kz.rs.jetkizu.domain.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TeamDTO implements Serializable {

    public Long id;
    public String name;
    public User lead;
    public Set<TeamMember> teamMembers = new HashSet<>();
    public Vehicle vehicle;

    public TeamDTO() {}

    public TeamDTO(Team team) {
        this.id = team.getId();
        this.name = team.getName();
        this.lead = team.getLead();
        this.vehicle = team.getVehicle();
    }

    @Override
    public String toString() {
        return "TeamDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", lead=" + lead +
            ", teamMembers=" + teamMembers +
            ", vehicle=" + vehicle +
            '}';
    }
}
