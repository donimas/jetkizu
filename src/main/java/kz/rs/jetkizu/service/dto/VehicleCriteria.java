package kz.rs.jetkizu.service.dto;

import java.io.Serializable;
import kz.rs.jetkizu.domain.enumeration.VehicleType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;





/**
 * Criteria class for the Vehicle entity. This class is used in VehicleResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /vehicles?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class VehicleCriteria implements Serializable {
    /**
     * Class for filtering VehicleType
     */
    public static class VehicleTypeFilter extends Filter<VehicleType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter govNumber;

    private StringFilter techPassportNumber;

    private StringFilter model;

    private IntegerFilter buildYear;

    private VehicleTypeFilter type;

    private FloatFilter height;

    private FloatFilter length;

    private FloatFilter width;

    private FloatFilter totalWeight;

    private BigDecimalFilter deliveryWeight;

    private StringFilter note;

    public VehicleCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getGovNumber() {
        return govNumber;
    }

    public void setGovNumber(StringFilter govNumber) {
        this.govNumber = govNumber;
    }

    public StringFilter getTechPassportNumber() {
        return techPassportNumber;
    }

    public void setTechPassportNumber(StringFilter techPassportNumber) {
        this.techPassportNumber = techPassportNumber;
    }

    public StringFilter getModel() {
        return model;
    }

    public void setModel(StringFilter model) {
        this.model = model;
    }

    public IntegerFilter getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(IntegerFilter buildYear) {
        this.buildYear = buildYear;
    }

    public VehicleTypeFilter getType() {
        return type;
    }

    public void setType(VehicleTypeFilter type) {
        this.type = type;
    }

    public FloatFilter getHeight() {
        return height;
    }

    public void setHeight(FloatFilter height) {
        this.height = height;
    }

    public FloatFilter getLength() {
        return length;
    }

    public void setLength(FloatFilter length) {
        this.length = length;
    }

    public FloatFilter getWidth() {
        return width;
    }

    public void setWidth(FloatFilter width) {
        this.width = width;
    }

    public FloatFilter getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(FloatFilter totalWeight) {
        this.totalWeight = totalWeight;
    }

    public BigDecimalFilter getDeliveryWeight() {
        return deliveryWeight;
    }

    public void setDeliveryWeight(BigDecimalFilter deliveryWeight) {
        this.deliveryWeight = deliveryWeight;
    }

    public StringFilter getNote() {
        return note;
    }

    public void setNote(StringFilter note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "VehicleCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (govNumber != null ? "govNumber=" + govNumber + ", " : "") +
                (techPassportNumber != null ? "techPassportNumber=" + techPassportNumber + ", " : "") +
                (model != null ? "model=" + model + ", " : "") +
                (buildYear != null ? "buildYear=" + buildYear + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (height != null ? "height=" + height + ", " : "") +
                (length != null ? "length=" + length + ", " : "") +
                (width != null ? "width=" + width + ", " : "") +
                (totalWeight != null ? "totalWeight=" + totalWeight + ", " : "") +
                (deliveryWeight != null ? "deliveryWeight=" + deliveryWeight + ", " : "") +
                (note != null ? "note=" + note + ", " : "") +
            "}";
    }

}
