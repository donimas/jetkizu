package kz.rs.jetkizu.service.dto;

import kz.rs.jetkizu.domain.Location;

import java.io.Serializable;
import java.time.ZonedDateTime;

public class LocationDto implements Serializable {

    private Long id;
    private String street;
    private String home;
    private String flat;
    private String postindex;
    private String lat;
    private String lon;
    private String fullAddress;
    private Boolean flagGarage;
    private Long placeId;
    private String osmType;
    private Long osmId;
    private ZonedDateTime createDate;

    public LocationDto() {}

    public LocationDto(Location location) {
        if(location != null) {
            this.id = location.getId();
            this.street = location.getStreet();
            this.home = location.getHome();
            this.flat = location.getFlat();
            this.postindex = location.getPostindex();
            this.lat = location.getLat();
            this.lon = location.getLon();
            this.fullAddress = location.getFullAddress();
            this.flagGarage = location.getFlagGarage();
            this.placeId = location.getPlaceId();
            this.osmType = location.getOsmType();
            this.osmId = location.getOsmId();
            this.createDate = location.getCreateDate();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public String getPostindex() {
        return postindex;
    }

    public void setPostindex(String postindex) {
        this.postindex = postindex;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public Boolean getFlagGarage() {
        return flagGarage;
    }

    public void setFlagGarage(Boolean flagGarage) {
        this.flagGarage = flagGarage;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public String getOsmType() {
        return osmType;
    }

    public void setOsmType(String osmType) {
        this.osmType = osmType;
    }

    public Long getOsmId() {
        return osmId;
    }

    public void setOsmId(Long osmId) {
        this.osmId = osmId;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "LocationDto{" +
            "id=" + id +
            ", street='" + street + '\'' +
            ", home='" + home + '\'' +
            ", flat='" + flat + '\'' +
            ", postindex='" + postindex + '\'' +
            ", lat='" + lat + '\'' +
            ", lon='" + lon + '\'' +
            ", fullAddress='" + fullAddress + '\'' +
            ", flagGarage=" + flagGarage +
            ", placeId=" + placeId +
            ", osmType='" + osmType + '\'' +
            ", osmId=" + osmId +
            ", createDate=" + createDate +
            '}';
    }
}
