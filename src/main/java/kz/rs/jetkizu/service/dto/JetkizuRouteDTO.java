package kz.rs.jetkizu.service.dto;

import kz.rs.jetkizu.domain.*;
import kz.rs.jetkizu.domain.enumeration.JetkizuRouteStatus;
import kz.rs.jetkizu.domain.enumeration.TripType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class JetkizuRouteDTO implements Serializable {

    public Long id;
    public LocalDate jetkizuDate;
    public ZonedDateTime jetkizuStartTime;
    public ZonedDateTime startedDateTime;
    public ZonedDateTime stoppedDateTime;
    public ZonedDateTime createDateTime;
    public String name;
    public JetkizuRouteStatus status;
    public Team team;
    public User startedBy;
    public User stoppedBy;
    public Boolean flagDeleted;
    public Boolean flagStopped;
    public BigDecimal totalPlanDistance;
    public BigDecimal totalPlanDuration;
    public BigDecimal totalFactDuration;
    public BigDecimal totalFactDistance;
    public Long serviceDuration;
    public Set<RouteBranchDto> routeBranches = new HashSet<>();
    public AreaNodeDto areaNode;
    public RouteOptimization routeOptimization;
    // To request of addresses
    public Set<AreaNodeDto> areaNodes = new HashSet<>();
    public TripType tripType;
    public LocationDto start;
    public LocationDto end;
    public Boolean flagWithoutPolygons;
    public Integer addressesAmount;

    public JetkizuRouteDTO() {
    }

    public JetkizuRouteDTO(Long id, LocalDate jetkizuDate, ZonedDateTime startedDateTime,
                           String name, JetkizuRouteStatus status, Team team,
                           Boolean flagDeleted, Boolean flagStopped, BigDecimal totalPlanDistance,
                           BigDecimal totalPlanDuration, BigDecimal totalFactDuration,
                           BigDecimal totalFactDistance, Integer addressesAmount) {
        this.id = id;
        this.jetkizuDate = jetkizuDate;
        this.startedDateTime = startedDateTime;
        this.name = name;
        this.status = status;
        this.team = team;
        this.flagDeleted = flagDeleted;
        this.flagStopped = flagStopped;
        this.totalPlanDistance = totalPlanDistance;
        this.totalPlanDuration = totalPlanDuration;
        this.totalFactDuration = totalFactDuration;
        this.totalFactDistance = totalFactDistance;
        this.addressesAmount = addressesAmount;
    }

    public JetkizuRouteDTO(JetkizuRoute jetkizuRoute) {
        if(jetkizuRoute != null) {
            this.id = jetkizuRoute.getId();
            this.jetkizuDate = jetkizuRoute.getJetkizuDate();
            this.jetkizuStartTime = jetkizuRoute.getJetkizuStartTime();
            this.startedDateTime = jetkizuRoute.getStartedDateTime();
            this.stoppedDateTime = jetkizuRoute.getStoppedDateTime();
            this.createDateTime = jetkizuRoute.getCreateDateTime();
            this.name = jetkizuRoute.getName();
            this.status = jetkizuRoute.getStatus();
            this.team = jetkizuRoute.getTeam();
            this.startedBy = jetkizuRoute.getStartedBy();
            this.stoppedBy = jetkizuRoute.getStoppedBy();
            this.flagDeleted = jetkizuRoute.getFlagDeleted();
            this.flagStopped = jetkizuRoute.getFlagStopped();
            this.totalPlanDistance = jetkizuRoute.getTotalPlanDistance();
            this.totalPlanDuration = jetkizuRoute.getTotalPlanDuration();
            this.totalFactDuration = jetkizuRoute.getTotalFactDuration();
            this.totalFactDistance = jetkizuRoute.getTotalFactDistance();
            this.serviceDuration = jetkizuRoute.getServiceDuration();
            this.routeBranches = jetkizuRoute.getRouteBranches().stream().map(RouteBranchDto::new).collect(Collectors.toSet());
            this.areaNode = new AreaNodeDto(jetkizuRoute.getAreaNode());
            this.routeOptimization = jetkizuRoute.getRouteOptimization();
            this.tripType = jetkizuRoute.getTripType();
            this.start = new LocationDto(jetkizuRoute.getStart());
            this.end = new LocationDto(jetkizuRoute.getEnd());
        }
    }

    @Override
    public String toString() {
        return "JetkizuRouteDTO{" +
            "id=" + id +
            ", jetkizuDate=" + jetkizuDate +
            ", jetkizuStartTime=" + jetkizuStartTime +
            ", startedDateTime=" + startedDateTime +
            ", stoppedDateTime=" + stoppedDateTime +
            ", createDateTime=" + createDateTime +
            ", name='" + name + '\'' +
            ", status=" + status +
            ", team=" + team +
            ", startedBy=" + startedBy +
            ", stoppedBy=" + stoppedBy +
            ", flagDeleted=" + flagDeleted +
            ", flagStopped=" + flagStopped +
            ", routeBranches=" + routeBranches +
            ", totalPlanDistance=" + totalPlanDistance +
            ", totalPlanDuration =" + totalPlanDuration +
            ", totalFactDistance=" + totalFactDistance+
            ", totalFactDuration=" + totalFactDuration+
            ", serviceDuration=" + serviceDuration+
            ", areaNodeDto=" + areaNode+
            ", routeOptimization=" + routeOptimization+
            ", areaNodes=" + areaNodes+
            ", tripType=" + tripType+
            ", start=" + start+
            ", end=" + end+
            ", flagWithoutPolygons=" + flagWithoutPolygons+
            ", addressesAmount=" + addressesAmount+
            '}';
    }
}
