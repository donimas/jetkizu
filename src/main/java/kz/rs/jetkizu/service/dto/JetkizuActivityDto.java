package kz.rs.jetkizu.service.dto;

import kz.rs.jetkizu.domain.JetkizuActivity;
import kz.rs.jetkizu.domain.JetkizuPause;

import javax.persistence.Column;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class JetkizuActivityDto implements Serializable {

    private Long id;
    private BigDecimal planKm;
    private BigDecimal factKm;
    private BigDecimal planDuration;
    private BigDecimal factDuration;

    private BigDecimal planDistanceToNext;
    private BigDecimal planDurationToNext;
    private BigDecimal factDistanceToNext;
    private BigDecimal factDurationToNext;

    private ZonedDateTime createDateTime;
    private String fromLat;
    private String fromLon;
    private JetkizuPause jetkizuPause;

    public JetkizuActivityDto() {}

    public JetkizuActivityDto(JetkizuActivity jetkizuActivity) {
        if(jetkizuActivity != null) {
            setId(jetkizuActivity.getId());
            setPlanKm(jetkizuActivity.getPlanKm());
            setFactKm(jetkizuActivity.getFactKm());
            setPlanDuration(jetkizuActivity.getPlanDuration());
            setFactDuration(jetkizuActivity.getFactDuration());
            setCreateDateTime(jetkizuActivity.getCreateDateTime());
            setFromLat(jetkizuActivity.getFromLat());
            setFromLon(jetkizuActivity.getFromLon());
            setJetkizuPause(jetkizuActivity.getJetkizuPause());

            setPlanDurationToNext(jetkizuActivity.getPlanDurationToNext());
            setPlanDistanceToNext(jetkizuActivity.getPlanDistanceToNext());
            setFactDurationToNext(jetkizuActivity.getFactDurationToNext());
            setFactDistanceToNext(jetkizuActivity.getFactDistanceToNext());
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPlanKm() {
        return planKm;
    }

    public void setPlanKm(BigDecimal planKm) {
        this.planKm = planKm;
    }

    public BigDecimal getFactKm() {
        return factKm;
    }

    public void setFactKm(BigDecimal factKm) {
        this.factKm = factKm;
    }

    public BigDecimal getPlanDuration() {
        return planDuration;
    }

    public void setPlanDuration(BigDecimal planDuration) {
        this.planDuration = planDuration;
    }

    public BigDecimal getFactDuration() {
        return factDuration;
    }

    public void setFactDuration(BigDecimal factDuration) {
        this.factDuration = factDuration;
    }

    public ZonedDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(ZonedDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public String getFromLat() {
        return fromLat;
    }

    public void setFromLat(String fromLat) {
        this.fromLat = fromLat;
    }

    public String getFromLon() {
        return fromLon;
    }

    public void setFromLon(String fromLon) {
        this.fromLon = fromLon;
    }

    public JetkizuPause getJetkizuPause() {
        return jetkizuPause;
    }

    public void setJetkizuPause(JetkizuPause jetkizuPause) {
        this.jetkizuPause = jetkizuPause;
    }

    public BigDecimal getPlanDistanceToNext() {
        return planDistanceToNext;
    }

    public void setPlanDistanceToNext(BigDecimal planDistanceToNext) {
        this.planDistanceToNext = planDistanceToNext;
    }

    public BigDecimal getPlanDurationToNext() {
        return planDurationToNext;
    }

    public void setPlanDurationToNext(BigDecimal planDurationToNext) {
        this.planDurationToNext = planDurationToNext;
    }

    public BigDecimal getFactDistanceToNext() {
        return factDistanceToNext;
    }

    public void setFactDistanceToNext(BigDecimal factDistanceToNext) {
        this.factDistanceToNext = factDistanceToNext;
    }

    public BigDecimal getFactDurationToNext() {
        return factDurationToNext;
    }

    public void setFactDurationToNext(BigDecimal factDurationToNext) {
        this.factDurationToNext = factDurationToNext;
    }

    @Override
    public String toString() {
        return "JetkizuActivityDto{" +
            "id=" + id +
            ", planKm=" + planKm +
            ", factKm=" + factKm +
            ", planDuration=" + planDuration +
            ", factDuration=" + factDuration +
            ", planDurationToNext='" + getPlanDurationToNext() + "'" +
            ", planDistanceToNext='" + getPlanDistanceToNext() + "'" +
            ", factDurationToNext='" + getFactDurationToNext() + "'" +
            ", factDistanceToNext='" + getFactDistanceToNext() + "'" +
            ", createDateTime=" + createDateTime +
            ", fromLat='" + fromLat + '\'' +
            ", fromLon='" + fromLon + '\'' +
            ", jetkizuPause=" + jetkizuPause +
            '}';
    }
}
