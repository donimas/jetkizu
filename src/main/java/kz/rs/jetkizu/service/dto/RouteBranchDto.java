package kz.rs.jetkizu.service.dto;

import kz.rs.jetkizu.domain.Customer;
import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import kz.rs.jetkizu.domain.enumeration.RouteBranchType;
import kz.rs.jetkizu.service.util.JetkizuRouteConverter;

import java.io.Serializable;

public class RouteBranchDto implements Serializable {

    private Long id;
    private Float order;
    private Jetkizu jetkizu;
    private JetkizuRouteDTO jetkizuRoute;
    private RouteBranchStatusHistory routeBranchStatusHistory;
    private JetkizuActivityDto jetkizuActivity;
    private Boolean flagDeleted;
    private RouteBranchType type;
    private Customer customer;
    private LocationDto startFrom;
    private LocationDto endTo;
    private LocationDto nextStop;
    private Long osmId;
    private Long placeId;
    private String osmType;

    public RouteBranchDto() {
    }

    public RouteBranchDto(RouteBranch routeBranch) {
        if(routeBranch != null) {
            this.id = routeBranch.getId();
            this.order = routeBranch.getOrder();
            this.jetkizu = routeBranch.getJetkizu();
            this.jetkizuRoute = new JetkizuRouteDTO(routeBranch.getJetkizuRoute());
            this.routeBranchStatusHistory = routeBranch.getRouteBranchStatusHistory();
            this.jetkizuActivity = new JetkizuActivityDto(routeBranch.getJetkizuActivity());
            this.flagDeleted = routeBranch.getFlagDeleted();
            this.type = routeBranch.getType();
            this.customer = routeBranch.getCustomer();
            this.startFrom = new LocationDto(routeBranch.getStartFrom());
            this.endTo = new LocationDto(routeBranch.getEndTo());
            this.nextStop = new LocationDto(routeBranch.getNextStop());
            this.osmId = routeBranch.getOsmId();
            this.placeId = routeBranch.getPlaceId();
            this.osmType = routeBranch.getOsmType();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getOrder() {
        return order;
    }

    public void setOrder(Float order) {
        this.order = order;
    }

    public Jetkizu getJetkizu() {
        return jetkizu;
    }

    public void setJetkizu(Jetkizu jetkizu) {
        this.jetkizu = jetkizu;
    }

    public JetkizuRouteDTO getJetkizuRoute() {
        return jetkizuRoute;
    }

    public void setJetkizuRoute(JetkizuRouteDTO jetkizuRoute) {
        this.jetkizuRoute = jetkizuRoute;
    }

    public RouteBranchStatusHistory getRouteBranchStatusHistory() {
        return routeBranchStatusHistory;
    }

    public void setRouteBranchStatusHistory(RouteBranchStatusHistory routeBranchStatusHistory) {
        this.routeBranchStatusHistory = routeBranchStatusHistory;
    }

    public JetkizuActivityDto getJetkizuActivity() {
        return jetkizuActivity;
    }

    public void setJetkizuActivity(JetkizuActivityDto jetkizuActivity) {
        this.jetkizuActivity = jetkizuActivity;
    }

    public Boolean getFlagDeleted() {
        return flagDeleted;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public RouteBranchType getType() {
        return type;
    }

    public void setType(RouteBranchType type) {
        this.type = type;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocationDto getStartFrom() {
        return startFrom;
    }

    public void setStartFrom(LocationDto startFrom) {
        this.startFrom = startFrom;
    }

    public LocationDto getEndTo() {
        return endTo;
    }

    public void setEndTo(LocationDto endTo) {
        this.endTo = endTo;
    }

    public Long getOsmId() {
        return osmId;
    }

    public void setOsmId(Long osmId) {
        this.osmId = osmId;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public String getOsmType() {
        return osmType;
    }

    public void setOsmType(String osmType) {
        this.osmType = osmType;
    }

    public LocationDto getNextStop() {
        return nextStop;
    }

    public void setNextStop(LocationDto nextStop) {
        this.nextStop = nextStop;
    }

    @Override
    public String toString() {
        return "RouteBranchDto{" +
            "id=" + id +
            ", order=" + order +
           ", jetkizu=" + jetkizu +
             ", jetkizuRoute=" + jetkizuRoute +
            ", routeBranchStatusHistory=" + routeBranchStatusHistory +
            ", jetkizuActivity=" + jetkizuActivity +
            ", flagDeleted=" + flagDeleted +
            ", type=" + type +
            ", customer=" + customer +
            ", startFrom=" + startFrom +
            ", endTo=" + endTo +
            ", nextStop=" + nextStop +
            ", osmId=" + osmId +
            ", placeId=" + placeId +
            ", osmType='" + osmType + '\'' +
            '}';
    }
}
