package kz.rs.jetkizu.service.dto;

import kz.rs.jetkizu.domain.JetkizuRoute;
import kz.rs.jetkizu.domain.enumeration.TripType;

import java.io.Serializable;
import java.util.List;

public class JetkizuRouteContainerDto implements Serializable {

    public Long id;
    public List<JetkizuRouteDTO> jetkizuRoutes;
    public JetkizuRouteDTO jetkizuRoute;
    public LocationDto start;
    public LocationDto end;
    public TripType tripType;

    @Override
    public String toString() {
        return "JetkizuRouteContainerDto{" +
            "jetkizuRoutes=" + jetkizuRoutes +
            ", jetkizuRoute=" + jetkizuRoute +
            ", id=" + id +
            ", start=" + start +
            ", end=" + end +
            ", tripType=" + tripType +
            '}';
    }
}
