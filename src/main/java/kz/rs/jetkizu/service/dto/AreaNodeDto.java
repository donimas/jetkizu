package kz.rs.jetkizu.service.dto;

import com.vividsolutions.jts.io.WKTWriter;
import kz.rs.jetkizu.domain.Area;
import kz.rs.jetkizu.domain.AreaNode;
import kz.rs.jetkizu.service.util.GisUtils;

import java.io.Serializable;
import java.util.Arrays;

public class AreaNodeDto implements Serializable {

    private Long id;
    private String[][] polygon;
    private String name;
    private TeamDTO team;
    private Area area;

    public AreaNodeDto() {}

    public AreaNodeDto(AreaNode areaNode) {
        if(areaNode != null) {
            this.id = areaNode.getId();
            this.name = areaNode.getName();
            this.area = areaNode.getArea();
            if (areaNode.getPolygon() != null) {
                this.polygon = GisUtils.toPolygonHumanReadableCoordinates(areaNode.getPolygon());
            }
            if (areaNode.getTeam() != null) {
                this.team = new TeamDTO(areaNode.getTeam());
            }
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String[][] getPolygon() {
        return polygon;
    }

    public void setPolygon(String[][] polygon) {
        this.polygon = polygon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(TeamDTO team) {
        this.team = team;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "AreaNodeDto{" +
            "id=" + id +
            ", polygon=" + Arrays.toString(polygon) +
            ", name='" + name + '\'' +
            ", teamDTO=" + team +
            '}';
    }
}
