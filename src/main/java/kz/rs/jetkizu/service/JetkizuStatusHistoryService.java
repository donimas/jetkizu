package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;

import java.util.List;

/**
 * Service Interface for managing JetkizuStatusHistory.
 */
public interface JetkizuStatusHistoryService {

    /**
     * Save a jetkizuStatusHistory.
     *
     * @param jetkizuStatusHistory the entity to save
     * @return the persisted entity
     */
    JetkizuStatusHistory save(JetkizuStatusHistory jetkizuStatusHistory);

    /**
     *  Get all the jetkizuStatusHistories.
     *
     *  @return the list of entities
     */
    List<JetkizuStatusHistory> findAll();

    /**
     *  Get the "id" jetkizuStatusHistory.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    JetkizuStatusHistory findOne(Long id);

    /**
     *  Delete the "id" jetkizuStatusHistory.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the jetkizuStatusHistory corresponding to the query.
     *
     *  @param query the query of the search
     *
     *  @return the list of entities
     */
    List<JetkizuStatusHistory> search(String query);

    JetkizuStatusHistory create(Jetkizu jetkizu, JetkizuStatus status);
}
