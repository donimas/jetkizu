package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.RouteOptimization;
import kz.rs.jetkizu.repository.RouteOptimizationRepository;
import kz.rs.jetkizu.repository.search.RouteOptimizationSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing RouteOptimization.
 */
@Service
@Transactional
public class RouteOptimizationService {

    private final Logger log = LoggerFactory.getLogger(RouteOptimizationService.class);

    private final RouteOptimizationRepository routeOptimizationRepository;

    private final RouteOptimizationSearchRepository routeOptimizationSearchRepository;

    public RouteOptimizationService(RouteOptimizationRepository routeOptimizationRepository, RouteOptimizationSearchRepository routeOptimizationSearchRepository) {
        this.routeOptimizationRepository = routeOptimizationRepository;
        this.routeOptimizationSearchRepository = routeOptimizationSearchRepository;
    }

    /**
     * Save a routeOptimization.
     *
     * @param routeOptimization the entity to save
     * @return the persisted entity
     */
    public RouteOptimization save(RouteOptimization routeOptimization) {
        log.debug("Request to save RouteOptimization : {}", routeOptimization);
        if(routeOptimization == null) {
            return null;
        }
        RouteOptimization result = routeOptimizationRepository.save(routeOptimization);
        routeOptimizationSearchRepository.save(result);
        return result;
    }

    /**
     *  Get all the routeOptimizations.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<RouteOptimization> findAll() {
        log.debug("Request to get all RouteOptimizations");
        return routeOptimizationRepository.findAll();
    }

    /**
     *  Get one routeOptimization by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public RouteOptimization findOne(Long id) {
        log.debug("Request to get RouteOptimization : {}", id);
        return routeOptimizationRepository.findOne(id);
    }

    /**
     *  Delete the  routeOptimization by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete RouteOptimization : {}", id);
        routeOptimizationRepository.delete(id);
        routeOptimizationSearchRepository.delete(id);
    }

    /**
     * Search for the routeOptimization corresponding to the query.
     *
     *  @param query the query of the search
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<RouteOptimization> search(String query) {
        log.debug("Request to search RouteOptimizations for query {}", query);
        return StreamSupport
            .stream(routeOptimizationSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
