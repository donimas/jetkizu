package kz.rs.jetkizu.service;

import kz.rs.jetkizu.domain.JetkizuPause;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing JetkizuPause.
 */
public interface JetkizuPauseService {

    /**
     * Save a jetkizuPause.
     *
     * @param jetkizuPause the entity to save
     * @return the persisted entity
     */
    JetkizuPause save(JetkizuPause jetkizuPause);

    /**
     *  Get all the jetkizuPauses.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<JetkizuPause> findAll(Pageable pageable);

    /**
     *  Get the "id" jetkizuPause.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    JetkizuPause findOne(Long id);

    /**
     *  Delete the "id" jetkizuPause.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the jetkizuPause corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<JetkizuPause> search(String query, Pageable pageable);
}
