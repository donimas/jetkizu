package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.RouteOptimization;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RouteOptimization entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RouteOptimizationRepository extends JpaRepository<RouteOptimization, Long> {

}
