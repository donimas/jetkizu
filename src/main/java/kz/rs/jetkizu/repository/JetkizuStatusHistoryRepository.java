package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JetkizuStatusHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JetkizuStatusHistoryRepository extends JpaRepository<JetkizuStatusHistory, Long> {

}
