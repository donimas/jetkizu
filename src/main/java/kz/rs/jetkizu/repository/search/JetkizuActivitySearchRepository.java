package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.JetkizuActivity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the JetkizuActivity entity.
 */
public interface JetkizuActivitySearchRepository extends ElasticsearchRepository<JetkizuActivity, Long> {
}
