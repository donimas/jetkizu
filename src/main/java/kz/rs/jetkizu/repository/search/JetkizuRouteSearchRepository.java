package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.JetkizuRoute;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the JetkizuRoute entity.
 */
public interface JetkizuRouteSearchRepository extends ElasticsearchRepository<JetkizuRoute, Long> {
}
