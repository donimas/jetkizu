package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.RouteOptimization;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RouteOptimization entity.
 */
public interface RouteOptimizationSearchRepository extends ElasticsearchRepository<RouteOptimization, Long> {
}
