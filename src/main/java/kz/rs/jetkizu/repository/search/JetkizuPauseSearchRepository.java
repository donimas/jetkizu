package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.JetkizuPause;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the JetkizuPause entity.
 */
public interface JetkizuPauseSearchRepository extends ElasticsearchRepository<JetkizuPause, Long> {
}
