package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.AttachmentFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AttachmentFile entity.
 */
public interface AttachmentFileSearchRepository extends ElasticsearchRepository<AttachmentFile, Long> {
}
