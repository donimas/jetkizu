package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.AreaNode;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AreaNode entity.
 */
public interface AreaNodeSearchRepository extends ElasticsearchRepository<AreaNode, Long> {
}
