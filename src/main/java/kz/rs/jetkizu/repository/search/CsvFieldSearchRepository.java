package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.CsvField;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CsvField entity.
 */
public interface CsvFieldSearchRepository extends ElasticsearchRepository<CsvField, Long> {
}
