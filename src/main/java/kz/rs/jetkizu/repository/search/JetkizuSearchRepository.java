package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.Jetkizu;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Jetkizu entity.
 */
public interface JetkizuSearchRepository extends ElasticsearchRepository<Jetkizu, Long> {
}
