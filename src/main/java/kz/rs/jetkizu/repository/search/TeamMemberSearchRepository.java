package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.TeamMember;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TeamMember entity.
 */
public interface TeamMemberSearchRepository extends ElasticsearchRepository<TeamMember, Long> {
}
