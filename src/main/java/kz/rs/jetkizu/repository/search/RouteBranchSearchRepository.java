package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.RouteBranch;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RouteBranch entity.
 */
public interface RouteBranchSearchRepository extends ElasticsearchRepository<RouteBranch, Long> {
}
