package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the JetkizuStatusHistory entity.
 */
public interface JetkizuStatusHistorySearchRepository extends ElasticsearchRepository<JetkizuStatusHistory, Long> {
}
