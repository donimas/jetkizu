package kz.rs.jetkizu.repository.search;

import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the RouteBranchStatusHistory entity.
 */
public interface RouteBranchStatusHistorySearchRepository extends ElasticsearchRepository<RouteBranchStatusHistory, Long> {
}
