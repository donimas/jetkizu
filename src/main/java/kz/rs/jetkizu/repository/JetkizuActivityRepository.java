package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.JetkizuActivity;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JetkizuActivity entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JetkizuActivityRepository extends JpaRepository<JetkizuActivity, Long> {

}
