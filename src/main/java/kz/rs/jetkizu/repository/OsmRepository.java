package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface OsmRepository extends JpaRepository<Location, Long> {

    @Query(value = "SELECT seq, node, cost, agg_cost FROM pgr_eucledianTSP(" +
        "'SELECT osm_id as id, st_x(the_geom) as x, st_y(the_geom) as y " +
        "FROM osm_nodes where osm_id in (' || ?1 || ')', ?2) order by seq;", nativeQuery = true)
    List<Object[]> findOptimalRouteRoundTrip(String nodeIds, Long source);

    @Query(value = "SELECT seq, node, cost, agg_cost FROM pgr_eucledianTSP(" +
        "'SELECT osm_id as id, st_x(the_geom) as x, st_y(the_geom) as y " +
        "FROM osm_nodes where osm_id in (' || ?1 || ')', ?2, ?3) order by seq;", nativeQuery = true)
    List<Object[]> findOptimalRoute(String nodeIds, Long source, Long target);

    @Query(value = "select svals(hstore(each(members))) from osm_ways where osm_id = ?1 limit 1;", nativeQuery = true)
    Object[] findOsmNodeIdByWay(Long osm_id);

}
