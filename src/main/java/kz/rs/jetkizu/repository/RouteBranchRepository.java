package kz.rs.jetkizu.repository;

import com.vividsolutions.jts.geom.Geometry;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the RouteBranch entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RouteBranchRepository extends JpaRepository<RouteBranch, Long> {

    public Page<RouteBranch> findAllByJetkizuRouteIdAndFlagDeletedFalse(Long id, Pageable pageable);
    public Page<RouteBranch> findAllByFlagDeletedFalseAndRouteBranchStatusHistory_Status(RouteBranchStatus status, Pageable pageable);

    @Query("select r from RouteBranch r where r.routeBranchStatusHistory.status = 'BACKLOG' " +
        "and within(r.endTo.whereIs, ?1) = true")
    public List<RouteBranch> findAllWithinPolygon(Geometry polygon);

    @Query(value = "select * from route_branch r " +
        "join route_branch_status_history s on s.id = r.route_branch_status_history_id " +
        "join location l on l.id = r.end_to_id " +
        "where s.status = 'BACKLOG' " +
        "and st_within(l.where_is, (select ST_Union(jet_polygon) from area_node where id in ?1)) = false", nativeQuery = true)
    public List<RouteBranch> findAllWithoutPolygons(List<Long> areaNodeIds);

    Long countByJetkizuRouteId(Long id);

}
