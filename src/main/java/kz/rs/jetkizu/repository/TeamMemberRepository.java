package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.TeamMember;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.util.List;


/**
 * Spring Data JPA repository for the TeamMember entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TeamMemberRepository extends JpaRepository<TeamMember, Long> {

    List<TeamMember> findAllByTeamIdAndFlagDeletedFalse(Long id);

}
