package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RouteBranchStatusHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RouteBranchStatusHistoryRepository extends JpaRepository<RouteBranchStatusHistory, Long> {

}
