package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.JetkizuRoute;
import kz.rs.jetkizu.domain.enumeration.JetkizuRouteStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the JetkizuRoute entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JetkizuRouteRepository extends JpaRepository<JetkizuRoute, Long> {

    Page<JetkizuRoute> findAllByFlagDeletedFalse(Pageable pageable);
    Page<JetkizuRoute> findAllByFlagStoppedFalseAndFlagDeletedFalse(Pageable pageable);
    Page<JetkizuRoute> findAllByStatus(JetkizuRouteStatus status, Pageable pageable);

    @Query("select jetkizu_route from JetkizuRoute jetkizu_route where jetkizu_route.startedBy.login = ?#{principal.username}")
    List<JetkizuRoute> findByStartedByIsCurrentUser();

    @Query("select jetkizu_route from JetkizuRoute jetkizu_route where jetkizu_route.stoppedBy.login = ?#{principal.username}")
    List<JetkizuRoute> findByStoppedByIsCurrentUser();

}
