package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Jetkizu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JetkizuRepository extends JpaRepository<Jetkizu, Long> {

    Page<Jetkizu> findAllByFlagDeletedFalseAndJetkizuStatusHistory_Status(JetkizuStatus status, Pageable pageable);

}
