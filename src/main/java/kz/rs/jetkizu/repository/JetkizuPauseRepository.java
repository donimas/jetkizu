package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.JetkizuPause;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the JetkizuPause entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JetkizuPauseRepository extends JpaRepository<JetkizuPause, Long> {

}
