package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.CsvField;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the CsvField entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CsvFieldRepository extends JpaRepository<CsvField, Long>, JpaSpecificationExecutor<CsvField> {

}
