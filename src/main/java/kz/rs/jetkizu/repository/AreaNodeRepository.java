package kz.rs.jetkizu.repository;

import kz.rs.jetkizu.domain.Area;
import kz.rs.jetkizu.domain.AreaNode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the AreaNode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AreaNodeRepository extends JpaRepository<AreaNode, Long> {

    public Page<AreaNode> findAllByAreaId(Long areaId, Pageable pageable);

}
