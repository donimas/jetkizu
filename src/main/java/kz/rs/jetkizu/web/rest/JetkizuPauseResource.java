package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.JetkizuPause;
import kz.rs.jetkizu.service.JetkizuPauseService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import kz.rs.jetkizu.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JetkizuPause.
 */
@RestController
@RequestMapping("/api")
public class JetkizuPauseResource {

    private final Logger log = LoggerFactory.getLogger(JetkizuPauseResource.class);

    private static final String ENTITY_NAME = "jetkizuPause";

    private final JetkizuPauseService jetkizuPauseService;

    public JetkizuPauseResource(JetkizuPauseService jetkizuPauseService) {
        this.jetkizuPauseService = jetkizuPauseService;
    }

    /**
     * POST  /jetkizu-pauses : Create a new jetkizuPause.
     *
     * @param jetkizuPause the jetkizuPause to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jetkizuPause, or with status 400 (Bad Request) if the jetkizuPause has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/jetkizu-pauses")
    @Timed
    public ResponseEntity<JetkizuPause> createJetkizuPause(@Valid @RequestBody JetkizuPause jetkizuPause) throws URISyntaxException {
        log.debug("REST request to save JetkizuPause : {}", jetkizuPause);
        if (jetkizuPause.getId() != null) {
            throw new BadRequestAlertException("A new jetkizuPause cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JetkizuPause result = jetkizuPauseService.save(jetkizuPause);
        return ResponseEntity.created(new URI("/api/jetkizu-pauses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jetkizu-pauses : Updates an existing jetkizuPause.
     *
     * @param jetkizuPause the jetkizuPause to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jetkizuPause,
     * or with status 400 (Bad Request) if the jetkizuPause is not valid,
     * or with status 500 (Internal Server Error) if the jetkizuPause couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/jetkizu-pauses")
    @Timed
    public ResponseEntity<JetkizuPause> updateJetkizuPause(@Valid @RequestBody JetkizuPause jetkizuPause) throws URISyntaxException {
        log.debug("REST request to update JetkizuPause : {}", jetkizuPause);
        if (jetkizuPause.getId() == null) {
            return createJetkizuPause(jetkizuPause);
        }
        JetkizuPause result = jetkizuPauseService.save(jetkizuPause);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jetkizuPause.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jetkizu-pauses : get all the jetkizuPauses.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jetkizuPauses in body
     */
    @GetMapping("/jetkizu-pauses")
    @Timed
    public ResponseEntity<List<JetkizuPause>> getAllJetkizuPauses(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of JetkizuPauses");
        Page<JetkizuPause> page = jetkizuPauseService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jetkizu-pauses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jetkizu-pauses/:id : get the "id" jetkizuPause.
     *
     * @param id the id of the jetkizuPause to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jetkizuPause, or with status 404 (Not Found)
     */
    @GetMapping("/jetkizu-pauses/{id}")
    @Timed
    public ResponseEntity<JetkizuPause> getJetkizuPause(@PathVariable Long id) {
        log.debug("REST request to get JetkizuPause : {}", id);
        JetkizuPause jetkizuPause = jetkizuPauseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jetkizuPause));
    }

    /**
     * DELETE  /jetkizu-pauses/:id : delete the "id" jetkizuPause.
     *
     * @param id the id of the jetkizuPause to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/jetkizu-pauses/{id}")
    @Timed
    public ResponseEntity<Void> deleteJetkizuPause(@PathVariable Long id) {
        log.debug("REST request to delete JetkizuPause : {}", id);
        jetkizuPauseService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/jetkizu-pauses?query=:query : search for the jetkizuPause corresponding
     * to the query.
     *
     * @param query the query of the jetkizuPause search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/jetkizu-pauses")
    @Timed
    public ResponseEntity<List<JetkizuPause>> searchJetkizuPauses(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of JetkizuPauses for query {}", query);
        Page<JetkizuPause> page = jetkizuPauseService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/jetkizu-pauses");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
