package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.JetkizuActivity;
import kz.rs.jetkizu.service.JetkizuActivityService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JetkizuActivity.
 */
@RestController
@RequestMapping("/api")
public class JetkizuActivityResource {

    private final Logger log = LoggerFactory.getLogger(JetkizuActivityResource.class);

    private static final String ENTITY_NAME = "jetkizuActivity";

    private final JetkizuActivityService jetkizuActivityService;

    public JetkizuActivityResource(JetkizuActivityService jetkizuActivityService) {
        this.jetkizuActivityService = jetkizuActivityService;
    }

    /**
     * POST  /jetkizu-activities : Create a new jetkizuActivity.
     *
     * @param jetkizuActivity the jetkizuActivity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jetkizuActivity, or with status 400 (Bad Request) if the jetkizuActivity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/jetkizu-activities")
    @Timed
    public ResponseEntity<JetkizuActivity> createJetkizuActivity(@Valid @RequestBody JetkizuActivity jetkizuActivity) throws URISyntaxException {
        log.debug("REST request to save JetkizuActivity : {}", jetkizuActivity);
        if (jetkizuActivity.getId() != null) {
            throw new BadRequestAlertException("A new jetkizuActivity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JetkizuActivity result = jetkizuActivityService.save(jetkizuActivity);
        return ResponseEntity.created(new URI("/api/jetkizu-activities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jetkizu-activities : Updates an existing jetkizuActivity.
     *
     * @param jetkizuActivity the jetkizuActivity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jetkizuActivity,
     * or with status 400 (Bad Request) if the jetkizuActivity is not valid,
     * or with status 500 (Internal Server Error) if the jetkizuActivity couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/jetkizu-activities")
    @Timed
    public ResponseEntity<JetkizuActivity> updateJetkizuActivity(@Valid @RequestBody JetkizuActivity jetkizuActivity) throws URISyntaxException {
        log.debug("REST request to update JetkizuActivity : {}", jetkizuActivity);
        if (jetkizuActivity.getId() == null) {
            return createJetkizuActivity(jetkizuActivity);
        }
        JetkizuActivity result = jetkizuActivityService.save(jetkizuActivity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jetkizuActivity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jetkizu-activities : get all the jetkizuActivities.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of jetkizuActivities in body
     */
    @GetMapping("/jetkizu-activities")
    @Timed
    public List<JetkizuActivity> getAllJetkizuActivities() {
        log.debug("REST request to get all JetkizuActivities");
        return jetkizuActivityService.findAll();
        }

    /**
     * GET  /jetkizu-activities/:id : get the "id" jetkizuActivity.
     *
     * @param id the id of the jetkizuActivity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jetkizuActivity, or with status 404 (Not Found)
     */
    @GetMapping("/jetkizu-activities/{id}")
    @Timed
    public ResponseEntity<JetkizuActivity> getJetkizuActivity(@PathVariable Long id) {
        log.debug("REST request to get JetkizuActivity : {}", id);
        JetkizuActivity jetkizuActivity = jetkizuActivityService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jetkizuActivity));
    }

    /**
     * DELETE  /jetkizu-activities/:id : delete the "id" jetkizuActivity.
     *
     * @param id the id of the jetkizuActivity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/jetkizu-activities/{id}")
    @Timed
    public ResponseEntity<Void> deleteJetkizuActivity(@PathVariable Long id) {
        log.debug("REST request to delete JetkizuActivity : {}", id);
        jetkizuActivityService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/jetkizu-activities?query=:query : search for the jetkizuActivity corresponding
     * to the query.
     *
     * @param query the query of the jetkizuActivity search
     * @return the result of the search
     */
    @GetMapping("/_search/jetkizu-activities")
    @Timed
    public List<JetkizuActivity> searchJetkizuActivities(@RequestParam String query) {
        log.debug("REST request to search JetkizuActivities for query {}", query);
        return jetkizuActivityService.search(query);
    }

}
