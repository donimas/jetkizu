package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.RouteOptimization;
import kz.rs.jetkizu.service.RouteOptimizationService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RouteOptimization.
 */
@RestController
@RequestMapping("/api")
public class RouteOptimizationResource {

    private final Logger log = LoggerFactory.getLogger(RouteOptimizationResource.class);

    private static final String ENTITY_NAME = "routeOptimization";

    private final RouteOptimizationService routeOptimizationService;

    public RouteOptimizationResource(RouteOptimizationService routeOptimizationService) {
        this.routeOptimizationService = routeOptimizationService;
    }

    /**
     * POST  /route-optimizations : Create a new routeOptimization.
     *
     * @param routeOptimization the routeOptimization to create
     * @return the ResponseEntity with status 201 (Created) and with body the new routeOptimization, or with status 400 (Bad Request) if the routeOptimization has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/route-optimizations")
    @Timed
    public ResponseEntity<RouteOptimization> createRouteOptimization(@RequestBody RouteOptimization routeOptimization) throws URISyntaxException {
        log.debug("REST request to save RouteOptimization : {}", routeOptimization);
        if (routeOptimization.getId() != null) {
            throw new BadRequestAlertException("A new routeOptimization cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RouteOptimization result = routeOptimizationService.save(routeOptimization);
        return ResponseEntity.created(new URI("/api/route-optimizations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /route-optimizations : Updates an existing routeOptimization.
     *
     * @param routeOptimization the routeOptimization to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated routeOptimization,
     * or with status 400 (Bad Request) if the routeOptimization is not valid,
     * or with status 500 (Internal Server Error) if the routeOptimization couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/route-optimizations")
    @Timed
    public ResponseEntity<RouteOptimization> updateRouteOptimization(@RequestBody RouteOptimization routeOptimization) throws URISyntaxException {
        log.debug("REST request to update RouteOptimization : {}", routeOptimization);
        if (routeOptimization.getId() == null) {
            return createRouteOptimization(routeOptimization);
        }
        RouteOptimization result = routeOptimizationService.save(routeOptimization);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, routeOptimization.getId().toString()))
            .body(result);
    }

    /**
     * GET  /route-optimizations : get all the routeOptimizations.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of routeOptimizations in body
     */
    @GetMapping("/route-optimizations")
    @Timed
    public List<RouteOptimization> getAllRouteOptimizations() {
        log.debug("REST request to get all RouteOptimizations");
        return routeOptimizationService.findAll();
        }

    /**
     * GET  /route-optimizations/:id : get the "id" routeOptimization.
     *
     * @param id the id of the routeOptimization to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the routeOptimization, or with status 404 (Not Found)
     */
    @GetMapping("/route-optimizations/{id}")
    @Timed
    public ResponseEntity<RouteOptimization> getRouteOptimization(@PathVariable Long id) {
        log.debug("REST request to get RouteOptimization : {}", id);
        RouteOptimization routeOptimization = routeOptimizationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(routeOptimization));
    }

    /**
     * DELETE  /route-optimizations/:id : delete the "id" routeOptimization.
     *
     * @param id the id of the routeOptimization to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/route-optimizations/{id}")
    @Timed
    public ResponseEntity<Void> deleteRouteOptimization(@PathVariable Long id) {
        log.debug("REST request to delete RouteOptimization : {}", id);
        routeOptimizationService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/route-optimizations?query=:query : search for the routeOptimization corresponding
     * to the query.
     *
     * @param query the query of the routeOptimization search
     * @return the result of the search
     */
    @GetMapping("/_search/route-optimizations")
    @Timed
    public List<RouteOptimization> searchRouteOptimizations(@RequestParam String query) {
        log.debug("REST request to search RouteOptimizations for query {}", query);
        return routeOptimizationService.search(query);
    }

}
