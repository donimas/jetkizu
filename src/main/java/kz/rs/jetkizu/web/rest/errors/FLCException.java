package kz.rs.jetkizu.web.rest.errors;

import kz.rs.jetkizu.web.rest.constants.Errors;

import java.util.Map;

/**
 * Created by naik on 14.05.17.
 */
public class FLCException extends RuntimeException{

    private Map<String,Object> params;

    public FLCException(String message){
        this(message, null);
    }

    public FLCException(String message, Map<String,Object> params){
        super(message);
        this.params = params;
    }

    public static FLCException getErrorOccuredInstance(){
        return new FLCException(Errors.OCCURED);
    }
    public static FLCException getErrorOccuredTryAgainInstance(){
        return new FLCException(Errors.OCCURED_TRY_AGAIN);
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }
}
