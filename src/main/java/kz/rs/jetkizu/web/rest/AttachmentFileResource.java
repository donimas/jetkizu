package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.AttachmentFile;
import kz.rs.jetkizu.service.AttachmentFileService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import kz.rs.jetkizu.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AttachmentFile.
 */
@RestController
@RequestMapping("/api")
public class AttachmentFileResource {

    private final Logger log = LoggerFactory.getLogger(AttachmentFileResource.class);

    private static final String ENTITY_NAME = "attachmentFile";

    private final AttachmentFileService attachmentFileService;

    public AttachmentFileResource(AttachmentFileService attachmentFileService) {
        this.attachmentFileService = attachmentFileService;
    }

    /**
     * POST  /attachment-files : Create a new attachmentFile.
     *
     * @param attachmentFile the attachmentFile to create
     * @return the ResponseEntity with status 201 (Created) and with body the new attachmentFile, or with status 400 (Bad Request) if the attachmentFile has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/attachment-files")
    @Timed
    public ResponseEntity<AttachmentFile> createAttachmentFile(@RequestBody AttachmentFile attachmentFile) throws URISyntaxException {
        log.debug("REST request to save AttachmentFile : {}", attachmentFile);
        if (attachmentFile.getId() != null) {
            throw new BadRequestAlertException("A new attachmentFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AttachmentFile result = attachmentFileService.save(attachmentFile);
        return ResponseEntity.created(new URI("/api/attachment-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /attachment-files : Updates an existing attachmentFile.
     *
     * @param attachmentFile the attachmentFile to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated attachmentFile,
     * or with status 400 (Bad Request) if the attachmentFile is not valid,
     * or with status 500 (Internal Server Error) if the attachmentFile couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/attachment-files")
    @Timed
    public ResponseEntity<AttachmentFile> updateAttachmentFile(@RequestBody AttachmentFile attachmentFile) throws URISyntaxException {
        log.debug("REST request to update AttachmentFile : {}", attachmentFile);
        if (attachmentFile.getId() == null) {
            return createAttachmentFile(attachmentFile);
        }
        AttachmentFile result = attachmentFileService.save(attachmentFile);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, attachmentFile.getId().toString()))
            .body(result);
    }

    /**
     * GET  /attachment-files : get all the attachmentFiles.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of attachmentFiles in body
     */
    @GetMapping("/attachment-files")
    @Timed
    public ResponseEntity<List<AttachmentFile>> getAllAttachmentFiles(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of AttachmentFiles");
        Page<AttachmentFile> page = attachmentFileService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/attachment-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /attachment-files/:id : get the "id" attachmentFile.
     *
     * @param id the id of the attachmentFile to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the attachmentFile, or with status 404 (Not Found)
     */
    @GetMapping("/attachment-files/{id}")
    @Timed
    public ResponseEntity<AttachmentFile> getAttachmentFile(@PathVariable Long id) {
        log.debug("REST request to get AttachmentFile : {}", id);
        AttachmentFile attachmentFile = attachmentFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(attachmentFile));
    }

    /**
     * DELETE  /attachment-files/:id : delete the "id" attachmentFile.
     *
     * @param id the id of the attachmentFile to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/attachment-files/{id}")
    @Timed
    public ResponseEntity<Void> deleteAttachmentFile(@PathVariable Long id) {
        log.debug("REST request to delete AttachmentFile : {}", id);
        attachmentFileService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/attachment-files?query=:query : search for the attachmentFile corresponding
     * to the query.
     *
     * @param query the query of the attachmentFile search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/attachment-files")
    @Timed
    public ResponseEntity<List<AttachmentFile>> searchAttachmentFiles(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of AttachmentFiles for query {}", query);
        Page<AttachmentFile> page = attachmentFileService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/attachment-files");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @PostMapping("/attachment-files/import-csv")
    @Timed
    public ResponseEntity<Void> importCsv(@RequestBody AttachmentFile attachmentFile) throws URISyntaxException {
        log.debug("REST request to import csv: {}", attachmentFile);
        AttachmentFile result = attachmentFileService.importCsv(attachmentFile);
        return ResponseEntity.ok().build();
    }

}
