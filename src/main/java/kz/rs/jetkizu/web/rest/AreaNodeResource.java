package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.AreaNode;
import kz.rs.jetkizu.service.AreaNodeService;
import kz.rs.jetkizu.service.dto.AreaNodeDto;
import kz.rs.jetkizu.service.util.AreaNodeConverter;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AreaNode.
 */
@RestController
@RequestMapping("/api")
public class AreaNodeResource {

    private final Logger log = LoggerFactory.getLogger(AreaNodeResource.class);

    private static final String ENTITY_NAME = "areaNode";

    private final AreaNodeService areaNodeService;

    public AreaNodeResource(AreaNodeService areaNodeService) {
        this.areaNodeService = areaNodeService;
    }

    /**
     * POST  /area-nodes : Create a new areaNode.
     *
     * @param areaNodeDto the areaNode to create
     * @return the ResponseEntity with status 201 (Created) and with body the new areaNode, or with status 400 (Bad Request) if the areaNode has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/area-nodes")
    @Timed
    public ResponseEntity<AreaNode> createAreaNode(@RequestBody AreaNodeDto areaNodeDto) throws URISyntaxException {
        log.debug("REST request to save AreaNode : {}", areaNodeDto);
        if (areaNodeDto.getId() != null) {
            throw new BadRequestAlertException("A new areaNode cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AreaNode areaNode = AreaNodeConverter.convert(areaNodeDto);
        AreaNode result = areaNodeService.save(areaNode);
        return ResponseEntity.created(new URI("/api/area-nodes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /area-nodes : Updates an existing areaNode.
     *
     * @param areaNodeDto the areaNode to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated areaNode,
     * or with status 400 (Bad Request) if the areaNode is not valid,
     * or with status 500 (Internal Server Error) if the areaNode couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/area-nodes")
    @Timed
    public ResponseEntity<AreaNode> updateAreaNode(@RequestBody AreaNodeDto areaNodeDto) throws URISyntaxException {
        log.debug("REST request to update AreaNode : {}", areaNodeDto);
        if (areaNodeDto.getId() == null) {
            return createAreaNode(areaNodeDto);
        }
        AreaNode areaNode = AreaNodeConverter.convert(areaNodeDto);
        AreaNode result = areaNodeService.save(areaNode);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, areaNode.getId().toString()))
            .body(result);
    }

    /**
     * GET  /area-nodes : get all the areaNodes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of areaNodes in body
     */
    @GetMapping("/area-nodes")
    @Timed
    public List<AreaNodeDto> getAllAreaNodes() {
        log.debug("REST request to get all AreaNodes");
        return areaNodeService.findAll();
    }

    /**
     * GET  /area-nodes/:id : get the "id" areaNode.
     *
     * @param id the id of the areaNode to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the areaNode, or with status 404 (Not Found)
     */
    @GetMapping("/area-nodes/{id}")
    @Timed
    public ResponseEntity<AreaNodeDto> getAreaNode(@PathVariable Long id) {
        log.debug("REST request to get AreaNode : {}", id);
        AreaNodeDto areaNodeDto = areaNodeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(areaNodeDto));
    }

    /**
     * DELETE  /area-nodes/:id : delete the "id" areaNode.
     *
     * @param id the id of the areaNode to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/area-nodes/{id}")
    @Timed
    public ResponseEntity<Void> deleteAreaNode(@PathVariable Long id) {
        log.debug("REST request to delete AreaNode : {}", id);
        areaNodeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/area-nodes?query=:query : search for the areaNode corresponding
     * to the query.
     *
     * @param query the query of the areaNode search
     * @return the result of the search
     */
    @GetMapping("/_search/area-nodes")
    @Timed
    public List<AreaNodeDto> searchAreaNodes(@RequestParam String query) {
        log.debug("REST request to search AreaNodes for query {}", query);
        return areaNodeService.search(query);
    }

    @GetMapping("/area-nodes/by-area/{areaId}")
    @Timed
    public List<AreaNodeDto> getAllAreaNodesByArea(@PathVariable Long areaId, Pageable pageable) {
        log.debug("REST request to get all AreaNodes of Area: {}", areaId);
        return areaNodeService.findAllByAreaId(areaId, pageable);
    }

}
