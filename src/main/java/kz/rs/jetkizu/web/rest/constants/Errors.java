package kz.rs.jetkizu.web.rest.constants;

@SuppressWarnings("unused")
public interface Errors {
    String OCCURED = "error.occured";
    String OCCURED_TRY_AGAIN = "error.occured-try-again";
    String ACTIVE_ROUTE_DELETE_ERROR = "error.activeRouteDelete";
    String ACTIVE_JETKIZU_DELETE_ERROR = "error.activeJetkizuDelete";
    String ACTIVE_BRANCH_DELETE_ERROR = "error.activeRouteBranchDelete";
    String JETKIZU_ROUTE_EMPTY = "error.jetkizuRouteEmpty";
    String JETKIZU_EMPTY = "error.jetkizuEmpty";
    String CUSTOMER_EMPTY = "error.customerEmpty";
    String LOCATION_EMPTY = "error.locationEmpty";
}
