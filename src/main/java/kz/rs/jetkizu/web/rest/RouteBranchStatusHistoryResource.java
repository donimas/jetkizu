package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import kz.rs.jetkizu.service.RouteBranchStatusHistoryService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RouteBranchStatusHistory.
 */
@RestController
@RequestMapping("/api")
public class RouteBranchStatusHistoryResource {

    private final Logger log = LoggerFactory.getLogger(RouteBranchStatusHistoryResource.class);

    private static final String ENTITY_NAME = "routeBranchStatusHistory";

    private final RouteBranchStatusHistoryService routeBranchStatusHistoryService;

    public RouteBranchStatusHistoryResource(RouteBranchStatusHistoryService routeBranchStatusHistoryService) {
        this.routeBranchStatusHistoryService = routeBranchStatusHistoryService;
    }

    /**
     * POST  /route-branch-status-histories : Create a new routeBranchStatusHistory.
     *
     * @param routeBranchStatusHistory the routeBranchStatusHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new routeBranchStatusHistory, or with status 400 (Bad Request) if the routeBranchStatusHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/route-branch-status-histories")
    @Timed
    public ResponseEntity<RouteBranchStatusHistory> createRouteBranchStatusHistory(@Valid @RequestBody RouteBranchStatusHistory routeBranchStatusHistory) throws URISyntaxException {
        log.debug("REST request to save RouteBranchStatusHistory : {}", routeBranchStatusHistory);
        if (routeBranchStatusHistory.getId() != null) {
            throw new BadRequestAlertException("A new routeBranchStatusHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RouteBranchStatusHistory result = routeBranchStatusHistoryService.save(routeBranchStatusHistory);
        return ResponseEntity.created(new URI("/api/route-branch-status-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /route-branch-status-histories : Updates an existing routeBranchStatusHistory.
     *
     * @param routeBranchStatusHistory the routeBranchStatusHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated routeBranchStatusHistory,
     * or with status 400 (Bad Request) if the routeBranchStatusHistory is not valid,
     * or with status 500 (Internal Server Error) if the routeBranchStatusHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/route-branch-status-histories")
    @Timed
    public ResponseEntity<RouteBranchStatusHistory> updateRouteBranchStatusHistory(@Valid @RequestBody RouteBranchStatusHistory routeBranchStatusHistory) throws URISyntaxException {
        log.debug("REST request to update RouteBranchStatusHistory : {}", routeBranchStatusHistory);
        if (routeBranchStatusHistory.getId() == null) {
            return createRouteBranchStatusHistory(routeBranchStatusHistory);
        }
        RouteBranchStatusHistory result = routeBranchStatusHistoryService.save(routeBranchStatusHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, routeBranchStatusHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /route-branch-status-histories : get all the routeBranchStatusHistories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of routeBranchStatusHistories in body
     */
    @GetMapping("/route-branch-status-histories")
    @Timed
    public List<RouteBranchStatusHistory> getAllRouteBranchStatusHistories() {
        log.debug("REST request to get all RouteBranchStatusHistories");
        return routeBranchStatusHistoryService.findAll();
        }

    /**
     * GET  /route-branch-status-histories/:id : get the "id" routeBranchStatusHistory.
     *
     * @param id the id of the routeBranchStatusHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the routeBranchStatusHistory, or with status 404 (Not Found)
     */
    @GetMapping("/route-branch-status-histories/{id}")
    @Timed
    public ResponseEntity<RouteBranchStatusHistory> getRouteBranchStatusHistory(@PathVariable Long id) {
        log.debug("REST request to get RouteBranchStatusHistory : {}", id);
        RouteBranchStatusHistory routeBranchStatusHistory = routeBranchStatusHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(routeBranchStatusHistory));
    }

    /**
     * DELETE  /route-branch-status-histories/:id : delete the "id" routeBranchStatusHistory.
     *
     * @param id the id of the routeBranchStatusHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/route-branch-status-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteRouteBranchStatusHistory(@PathVariable Long id) {
        log.debug("REST request to delete RouteBranchStatusHistory : {}", id);
        routeBranchStatusHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/route-branch-status-histories?query=:query : search for the routeBranchStatusHistory corresponding
     * to the query.
     *
     * @param query the query of the routeBranchStatusHistory search
     * @return the result of the search
     */
    @GetMapping("/_search/route-branch-status-histories")
    @Timed
    public List<RouteBranchStatusHistory> searchRouteBranchStatusHistories(@RequestParam String query) {
        log.debug("REST request to search RouteBranchStatusHistories for query {}", query);
        return routeBranchStatusHistoryService.search(query);
    }

}
