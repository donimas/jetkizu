package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.JetkizuRoute;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.service.JetkizuRouteService;
import kz.rs.jetkizu.service.dto.JetkizuRouteContainerDto;
import kz.rs.jetkizu.service.dto.JetkizuRouteDTO;
import kz.rs.jetkizu.service.util.JetkizuRouteConverter;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import kz.rs.jetkizu.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JetkizuRoute.
 */
@RestController
@RequestMapping("/api")
public class JetkizuRouteResource {

    private final Logger log = LoggerFactory.getLogger(JetkizuRouteResource.class);

    private static final String ENTITY_NAME = "jetkizuRoute";

    private final JetkizuRouteService jetkizuRouteService;

    public JetkizuRouteResource(JetkizuRouteService jetkizuRouteService) {
        this.jetkizuRouteService = jetkizuRouteService;
    }

    /**
     * POST  /jetkizu-routes : Create a new jetkizuRoute.
     *
     * @param jetkizuRoute the jetkizuRoute to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jetkizuRoute, or with status 400 (Bad Request) if the jetkizuRoute has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/jetkizu-routes")
    @Timed
    public ResponseEntity<JetkizuRoute> createJetkizuRoute(@RequestBody JetkizuRoute jetkizuRoute) throws URISyntaxException {
        log.debug("REST request to save JetkizuRoute : {}", jetkizuRoute);
        if (jetkizuRoute.getId() != null) {
            throw new BadRequestAlertException("A new jetkizuRoute cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JetkizuRoute result = jetkizuRouteService.create(jetkizuRoute);
        return ResponseEntity.created(new URI("/api/jetkizu-routes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jetkizu-routes : Updates an existing jetkizuRoute.
     *
     * @param jetkizuRoute the jetkizuRoute to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jetkizuRoute,
     * or with status 400 (Bad Request) if the jetkizuRoute is not valid,
     * or with status 500 (Internal Server Error) if the jetkizuRoute couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/jetkizu-routes")
    @Timed
    public ResponseEntity<JetkizuRoute> updateJetkizuRoute(@Valid @RequestBody JetkizuRoute jetkizuRoute) throws URISyntaxException {
        log.debug("REST request to update JetkizuRoute : {}", jetkizuRoute);
        if (jetkizuRoute.getId() == null) {
            return createJetkizuRoute(jetkizuRoute);
        }
        JetkizuRoute result = jetkizuRouteService.save(jetkizuRoute);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jetkizuRoute.getId().toString()))
            .body(result);
    }

    @PutMapping("/jetkizu-routes/with-new-elements")
    @Timed
    public ResponseEntity<JetkizuRoute> updateJetkizuRouteWithNewElements(@Valid @RequestBody JetkizuRouteDTO jetkizuRouteDTO) throws URISyntaxException {
        log.debug("REST request to update JetkizuRoute with new elements: {}", jetkizuRouteDTO);
        if (jetkizuRouteDTO.id == null) {
            return createJetkizuRoute(JetkizuRouteConverter.convert(jetkizuRouteDTO));
        }
        JetkizuRoute result = jetkizuRouteService.updateWithNewElements(jetkizuRouteDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jetkizuRouteDTO.id.toString()))
            .body(result);
    }

    /**
     * GET  /jetkizu-routes : get all the jetkizuRoutes.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of jetkizuRoutes in body
     */
    @GetMapping("/jetkizu-routes")
    @Timed
    public ResponseEntity<List<JetkizuRoute>> getAllJetkizuRoutes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of JetkizuRoutes");
        Page<JetkizuRoute> page = jetkizuRouteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jetkizu-routes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jetkizu-routes/:id : get the "id" jetkizuRoute.
     *
     * @param id the id of the jetkizuRoute to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jetkizuRoute, or with status 404 (Not Found)
     */
    @GetMapping("/jetkizu-routes/{id}")
    @Timed
    public ResponseEntity<JetkizuRoute> getJetkizuRoute(@PathVariable Long id) {
        log.debug("REST request to get JetkizuRoute : {}", id);
        JetkizuRoute jetkizuRoute = jetkizuRouteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jetkizuRoute));
    }

    /**
     * DELETE  /jetkizu-routes/:id : delete the "id" jetkizuRoute.
     *
     * @param id the id of the jetkizuRoute to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/jetkizu-routes/{id}")
    @Timed
    public ResponseEntity<Void> deleteJetkizuRoute(@PathVariable Long id) {
        log.debug("REST request to delete JetkizuRoute : {}", id);
        jetkizuRouteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/jetkizu-routes?query=:query : search for the jetkizuRoute corresponding
     * to the query.
     *
     * @param query the query of the jetkizuRoute search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/jetkizu-routes")
    @Timed
    public ResponseEntity<List<JetkizuRoute>> searchJetkizuRoutes(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of JetkizuRoutes for query {}", query);
        Page<JetkizuRoute> page = jetkizuRouteService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/jetkizu-routes");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/jetkizu-routes/not-stopped")
    @Timed
    public ResponseEntity<List<JetkizuRoute>> getAllNotStoppedJetkizuRoutes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of not stopped JetkizuRoutes");
        Page<JetkizuRoute> page = jetkizuRouteService.findByFlagStoppedFalse(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jetkizu-routes/not-stopped");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/jetkizu-routes/all-started")
    @Timed
    public ResponseEntity<List<JetkizuRouteDTO>> getAllStartedJetkizuRoutes(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of all started JetkizuRoutes");
        Page<JetkizuRoute> page = jetkizuRouteService.findStarted(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jetkizu-routes/all-started");
        return new ResponseEntity<>(page.getContent().stream().map(route -> {
            return new JetkizuRouteDTO(
                route.getId(), route.getJetkizuDate(), route.getStartedDateTime(),
                route.getName(), route.getStatus(), route.getTeam(), route.getFlagDeleted(),
                route.getFlagStopped(), route.getTotalPlanDistance(), route.getTotalPlanDuration(),
                route.getTotalFactDuration(), route.getTotalFactDistance(), this.jetkizuRouteService.getBranchesAmount(route.getId())
            );
        }).collect(Collectors.toList()), headers, HttpStatus.OK);
    }

    @PostMapping("/jetkizu-routes/start-optimized")
    @Timed
    public ResponseEntity<Set<Long>> startOptimizedRoute(@RequestBody JetkizuRouteContainerDto jetkizuRouteContainerDto) throws URISyntaxException {
        log.debug("REST request to start optimized JetkizuRouteContainer : {}", jetkizuRouteContainerDto);
        Set<Long> result = jetkizuRouteService.startOptimizedRoute(jetkizuRouteContainerDto);
        // TODO: change result alert to started status
        return ResponseEntity.created(new URI("/jetkizu-routes/start-optimized"))
            .body(result);
    }

    @PostMapping("/jetkizu-routes/addresses")
    @Timed
    public ResponseEntity<List<JetkizuRouteDTO>> getRouteAddresses(@RequestBody JetkizuRouteDTO jetkizuRoute) throws URISyntaxException {
        log.debug("REST request to get addresses of JetkizuRoute : {}", jetkizuRoute);
        List<JetkizuRouteDTO> result = jetkizuRouteService.getRouteAddresses(jetkizuRoute);
        return ResponseEntity.created(new URI("/api/jetkizu-routes/addresses/"))
            .body(result);
    }

    @PostMapping("/jetkizu-routes/optimized-routes")
    @Timed
    public ResponseEntity<JetkizuRouteContainerDto> getOptimizedRoutes(@RequestBody JetkizuRouteContainerDto jetkizuRouteContainerDto) throws URISyntaxException {
        log.debug("Rest request to optimize routes: {}", jetkizuRouteContainerDto);
        JetkizuRouteContainerDto result = jetkizuRouteService.optimizeRoutes(jetkizuRouteContainerDto);
        return ResponseEntity.created(new URI("/api/jetkizu-routes/optimized-routes"))
            .body(result);
    }

    @PutMapping("/jetkizu-routes/import-branches")
    @Timed
    public ResponseEntity<Void> importBranches(@RequestBody JetkizuRouteDTO jetkizuRouteDTO) throws URISyntaxException {
        log.debug("REST request to import JetkizuRoute : {}", jetkizuRouteDTO);
        Set<RouteBranch> result = jetkizuRouteService.importBranches(jetkizuRouteDTO);
        log.debug("Branches are imported: {}", result);
        return ResponseEntity.ok().headers(HeaderUtil.importedRouteBranches("routeBranch", result.size()+"")).build();
    }

}
