/**
 * View Models used by Spring MVC REST controllers.
 */
package kz.rs.jetkizu.web.rest.vm;
