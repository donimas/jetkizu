package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import kz.rs.jetkizu.service.JetkizuStatusHistoryService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing JetkizuStatusHistory.
 */
@RestController
@RequestMapping("/api")
public class JetkizuStatusHistoryResource {

    private final Logger log = LoggerFactory.getLogger(JetkizuStatusHistoryResource.class);

    private static final String ENTITY_NAME = "jetkizuStatusHistory";

    private final JetkizuStatusHistoryService jetkizuStatusHistoryService;

    public JetkizuStatusHistoryResource(JetkizuStatusHistoryService jetkizuStatusHistoryService) {
        this.jetkizuStatusHistoryService = jetkizuStatusHistoryService;
    }

    /**
     * POST  /jetkizu-status-histories : Create a new jetkizuStatusHistory.
     *
     * @param jetkizuStatusHistory the jetkizuStatusHistory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jetkizuStatusHistory, or with status 400 (Bad Request) if the jetkizuStatusHistory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/jetkizu-status-histories")
    @Timed
    public ResponseEntity<JetkizuStatusHistory> createJetkizuStatusHistory(@Valid @RequestBody JetkizuStatusHistory jetkizuStatusHistory) throws URISyntaxException {
        log.debug("REST request to save JetkizuStatusHistory : {}", jetkizuStatusHistory);
        if (jetkizuStatusHistory.getId() != null) {
            throw new BadRequestAlertException("A new jetkizuStatusHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JetkizuStatusHistory result = jetkizuStatusHistoryService.save(jetkizuStatusHistory);
        return ResponseEntity.created(new URI("/api/jetkizu-status-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jetkizu-status-histories : Updates an existing jetkizuStatusHistory.
     *
     * @param jetkizuStatusHistory the jetkizuStatusHistory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jetkizuStatusHistory,
     * or with status 400 (Bad Request) if the jetkizuStatusHistory is not valid,
     * or with status 500 (Internal Server Error) if the jetkizuStatusHistory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/jetkizu-status-histories")
    @Timed
    public ResponseEntity<JetkizuStatusHistory> updateJetkizuStatusHistory(@Valid @RequestBody JetkizuStatusHistory jetkizuStatusHistory) throws URISyntaxException {
        log.debug("REST request to update JetkizuStatusHistory : {}", jetkizuStatusHistory);
        if (jetkizuStatusHistory.getId() == null) {
            return createJetkizuStatusHistory(jetkizuStatusHistory);
        }
        JetkizuStatusHistory result = jetkizuStatusHistoryService.save(jetkizuStatusHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jetkizuStatusHistory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jetkizu-status-histories : get all the jetkizuStatusHistories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of jetkizuStatusHistories in body
     */
    @GetMapping("/jetkizu-status-histories")
    @Timed
    public List<JetkizuStatusHistory> getAllJetkizuStatusHistories() {
        log.debug("REST request to get all JetkizuStatusHistories");
        return jetkizuStatusHistoryService.findAll();
        }

    /**
     * GET  /jetkizu-status-histories/:id : get the "id" jetkizuStatusHistory.
     *
     * @param id the id of the jetkizuStatusHistory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jetkizuStatusHistory, or with status 404 (Not Found)
     */
    @GetMapping("/jetkizu-status-histories/{id}")
    @Timed
    public ResponseEntity<JetkizuStatusHistory> getJetkizuStatusHistory(@PathVariable Long id) {
        log.debug("REST request to get JetkizuStatusHistory : {}", id);
        JetkizuStatusHistory jetkizuStatusHistory = jetkizuStatusHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jetkizuStatusHistory));
    }

    /**
     * DELETE  /jetkizu-status-histories/:id : delete the "id" jetkizuStatusHistory.
     *
     * @param id the id of the jetkizuStatusHistory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/jetkizu-status-histories/{id}")
    @Timed
    public ResponseEntity<Void> deleteJetkizuStatusHistory(@PathVariable Long id) {
        log.debug("REST request to delete JetkizuStatusHistory : {}", id);
        jetkizuStatusHistoryService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/jetkizu-status-histories?query=:query : search for the jetkizuStatusHistory corresponding
     * to the query.
     *
     * @param query the query of the jetkizuStatusHistory search
     * @return the result of the search
     */
    @GetMapping("/_search/jetkizu-status-histories")
    @Timed
    public List<JetkizuStatusHistory> searchJetkizuStatusHistories(@RequestParam String query) {
        log.debug("REST request to search JetkizuStatusHistories for query {}", query);
        return jetkizuStatusHistoryService.search(query);
    }

}
