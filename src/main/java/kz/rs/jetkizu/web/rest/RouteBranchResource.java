package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.service.RouteBranchService;
import kz.rs.jetkizu.service.dto.RouteBranchDto;
import kz.rs.jetkizu.service.util.RouteBranchConverter;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import kz.rs.jetkizu.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing RouteBranch.
 */
@RestController
@RequestMapping("/api")
public class RouteBranchResource {

    private final Logger log = LoggerFactory.getLogger(RouteBranchResource.class);

    private static final String ENTITY_NAME = "routeBranch";

    private final RouteBranchService routeBranchService;

    public RouteBranchResource(RouteBranchService routeBranchService) {
        this.routeBranchService = routeBranchService;
    }

    /**
     * POST  /route-branches : Create a new routeBranch.
     *
     * @param routeBranchDto the routeBranch to create
     * @return the ResponseEntity with status 201 (Created) and with body the new routeBranch, or with status 400 (Bad Request) if the routeBranch has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/route-branches")
    @Timed
    public ResponseEntity<RouteBranch> createRouteBranch(@RequestBody RouteBranchDto routeBranchDto) throws URISyntaxException {
        log.debug("REST request to save RouteBranch : {}", routeBranchDto);
        if (routeBranchDto.getId() != null) {
            throw new BadRequestAlertException("A new routeBranch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RouteBranch routeBranch = RouteBranchConverter.convert(routeBranchDto);
        RouteBranch result = routeBranchService.create(routeBranch);
        return ResponseEntity.created(new URI("/api/route-branches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/route-branches/add-new-to-route")
    @Timed
    public ResponseEntity<RouteBranch> addNewToRoute(@RequestBody RouteBranch routeBranch) throws URISyntaxException {
        log.debug("REST request to add new to JetkizuRoute: {}", routeBranch);
        if (routeBranch.getId() != null) {
            throw new BadRequestAlertException("A new routeBranch cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RouteBranch result = routeBranchService.addNewToRoute(routeBranch);
        return ResponseEntity.created(new URI("/api/route-branches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/route-branches/add-to-route")
    @Timed
    public ResponseEntity<RouteBranch> addToRoute(@RequestBody RouteBranch routeBranch) throws URISyntaxException {
        log.debug("REST request to add to JetkizuRoute: {}", routeBranch);
        RouteBranch result = routeBranchService.addToRoute(routeBranch);
        return ResponseEntity.created(new URI("/api/route-branches/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /route-branches : Updates an existing routeBranch.
     *
     * @param routeBranchDto the routeBranch to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated routeBranch,
     * or with status 400 (Bad Request) if the routeBranch is not valid,
     * or with status 500 (Internal Server Error) if the routeBranch couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/route-branches")
    @Timed
    public ResponseEntity<RouteBranch> updateRouteBranch(@RequestBody RouteBranchDto routeBranchDto) throws URISyntaxException {
        log.debug("REST request to update RouteBranch : {}", routeBranchDto);
        if (routeBranchDto.getId() == null) {
            return createRouteBranch(routeBranchDto);
        }
        RouteBranch routeBranch = RouteBranchConverter.convert(routeBranchDto);
        RouteBranch result = routeBranchService.update(routeBranch);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, routeBranch.getId().toString()))
            .body(result);
    }

    /**
     * GET  /route-branches : get all the routeBranches.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of routeBranches in body
     */
    @GetMapping("/route-branches")
    @Timed
    public ResponseEntity<List<RouteBranch>> getAllRouteBranches(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RouteBranches");
        Page<RouteBranch> page = routeBranchService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/route-branches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /route-branches/:id : get the "id" routeBranch.
     *
     * @param id the id of the routeBranch to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the routeBranch, or with status 404 (Not Found)
     */
    @GetMapping("/route-branches/{id}")
    @Timed
    public ResponseEntity<RouteBranch> getRouteBranch(@PathVariable Long id) {
        log.debug("REST request to get RouteBranch : {}", id);
        RouteBranch routeBranch = routeBranchService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(routeBranch));
    }

    /**
     * DELETE  /route-branches/:id : delete the "id" routeBranch.
     *
     * @param id the id of the routeBranch to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/route-branches/{id}")
    @Timed
    public ResponseEntity<Void> deleteRouteBranch(@PathVariable Long id) {
        log.debug("REST request to delete RouteBranch : {}", id);
        routeBranchService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/route-branches?query=:query : search for the routeBranch corresponding
     * to the query.
     *
     * @param query the query of the routeBranch search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/route-branches")
    @Timed
    public ResponseEntity<List<RouteBranch>> searchRouteBranches(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of RouteBranches for query {}", query);
        Page<RouteBranch> page = routeBranchService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/route-branches");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/route-branches/route/{id}")
    @Timed
    public ResponseEntity<List<RouteBranch>> getAllRouteBranchesByJetkizuRoute(@PathVariable Long id, @ApiParam Pageable pageable) {
        log.debug("REST request to get a page of RouteBranches by Route: {}", id);
        Page<RouteBranch> page = routeBranchService.findAllByJetkizuRouteId(id, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/route-branches/route");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/route-branches/backlog")
    @Timed
    public ResponseEntity<List<RouteBranch>> getAllBacklogBranches(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of backlog RouteBranches by Route: {}");
        Page<RouteBranch> page = routeBranchService.findAllBacklogBranches(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/route-branches/backlog");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
