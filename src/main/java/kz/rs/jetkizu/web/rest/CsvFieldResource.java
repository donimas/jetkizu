package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.CsvField;
import kz.rs.jetkizu.service.CsvFieldService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import kz.rs.jetkizu.web.rest.util.PaginationUtil;
import kz.rs.jetkizu.service.dto.CsvFieldCriteria;
import kz.rs.jetkizu.service.CsvFieldQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CsvField.
 */
@RestController
@RequestMapping("/api")
public class CsvFieldResource {

    private final Logger log = LoggerFactory.getLogger(CsvFieldResource.class);

    private static final String ENTITY_NAME = "csvField";

    private final CsvFieldService csvFieldService;

    private final CsvFieldQueryService csvFieldQueryService;

    public CsvFieldResource(CsvFieldService csvFieldService, CsvFieldQueryService csvFieldQueryService) {
        this.csvFieldService = csvFieldService;
        this.csvFieldQueryService = csvFieldQueryService;
    }

    /**
     * POST  /csv-fields : Create a new csvField.
     *
     * @param csvField the csvField to create
     * @return the ResponseEntity with status 201 (Created) and with body the new csvField, or with status 400 (Bad Request) if the csvField has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/csv-fields")
    @Timed
    public ResponseEntity<CsvField> createCsvField(@RequestBody CsvField csvField) throws URISyntaxException {
        log.debug("REST request to save CsvField : {}", csvField);
        if (csvField.getId() != null) {
            throw new BadRequestAlertException("A new csvField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CsvField result = csvFieldService.save(csvField);
        return ResponseEntity.created(new URI("/api/csv-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /csv-fields : Updates an existing csvField.
     *
     * @param csvField the csvField to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated csvField,
     * or with status 400 (Bad Request) if the csvField is not valid,
     * or with status 500 (Internal Server Error) if the csvField couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/csv-fields")
    @Timed
    public ResponseEntity<CsvField> updateCsvField(@RequestBody CsvField csvField) throws URISyntaxException {
        log.debug("REST request to update CsvField : {}", csvField);
        if (csvField.getId() == null) {
            return createCsvField(csvField);
        }
        CsvField result = csvFieldService.save(csvField);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, csvField.getId().toString()))
            .body(result);
    }

    /**
     * GET  /csv-fields : get all the csvFields.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of csvFields in body
     */
    @GetMapping("/csv-fields")
    @Timed
    public ResponseEntity<List<CsvField>> getAllCsvFields(CsvFieldCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get CsvFields by criteria: {}", criteria);
        Page<CsvField> page = csvFieldQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/csv-fields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /csv-fields/:id : get the "id" csvField.
     *
     * @param id the id of the csvField to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the csvField, or with status 404 (Not Found)
     */
    @GetMapping("/csv-fields/{id}")
    @Timed
    public ResponseEntity<CsvField> getCsvField(@PathVariable Long id) {
        log.debug("REST request to get CsvField : {}", id);
        CsvField csvField = csvFieldService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(csvField));
    }

    /**
     * DELETE  /csv-fields/:id : delete the "id" csvField.
     *
     * @param id the id of the csvField to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/csv-fields/{id}")
    @Timed
    public ResponseEntity<Void> deleteCsvField(@PathVariable Long id) {
        log.debug("REST request to delete CsvField : {}", id);
        csvFieldService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/csv-fields?query=:query : search for the csvField corresponding
     * to the query.
     *
     * @param query the query of the csvField search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/csv-fields")
    @Timed
    public ResponseEntity<List<CsvField>> searchCsvFields(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of CsvFields for query {}", query);
        Page<CsvField> page = csvFieldService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/csv-fields");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
