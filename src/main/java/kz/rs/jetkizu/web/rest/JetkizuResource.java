package kz.rs.jetkizu.web.rest;

import com.codahale.metrics.annotation.Timed;
import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.service.JetkizuService;
import kz.rs.jetkizu.web.rest.errors.BadRequestAlertException;
import kz.rs.jetkizu.web.rest.util.HeaderUtil;
import kz.rs.jetkizu.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Jetkizu.
 */
@RestController
@RequestMapping("/api")
public class JetkizuResource {

    private final Logger log = LoggerFactory.getLogger(JetkizuResource.class);

    private static final String ENTITY_NAME = "jetkizu";

    private final JetkizuService jetkizuService;

    public JetkizuResource(JetkizuService jetkizuService) {
        this.jetkizuService = jetkizuService;
    }

    /**
     * POST  /jetkizus : Create a new jetkizu.
     *
     * @param jetkizu the jetkizu to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jetkizu, or with status 400 (Bad Request) if the jetkizu has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/jetkizus")
    @Timed
    public ResponseEntity<Jetkizu> createJetkizu(@RequestBody Jetkizu jetkizu) throws URISyntaxException {
        log.debug("REST request to save Jetkizu : {}", jetkizu);
        if (jetkizu.getId() != null) {
            throw new BadRequestAlertException("A new jetkizu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Jetkizu result = jetkizuService.create(jetkizu);
        return ResponseEntity.created(new URI("/api/jetkizus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /jetkizus : Updates an existing jetkizu.
     *
     * @param jetkizu the jetkizu to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jetkizu,
     * or with status 400 (Bad Request) if the jetkizu is not valid,
     * or with status 500 (Internal Server Error) if the jetkizu couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/jetkizus")
    @Timed
    public ResponseEntity<Jetkizu> updateJetkizu(@RequestBody Jetkizu jetkizu) throws URISyntaxException {
        log.debug("REST request to update Jetkizu : {}", jetkizu);
        if (jetkizu.getId() == null) {
            return createJetkizu(jetkizu);
        }
        Jetkizu result = jetkizuService.update(jetkizu);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jetkizu.getId().toString()))
            .body(result);
    }

    /**
     * GET  /jetkizus : get all the jetkizus.
     *
     * @param pageable the pagination information
     * @param filter the filter of the request
     * @return the ResponseEntity with status 200 (OK) and the list of jetkizus in body
     */
    @GetMapping("/jetkizus")
    @Timed
    public ResponseEntity<List<Jetkizu>> getAllJetkizus(@ApiParam Pageable pageable, @RequestParam(required = false) String filter) {
        if ("routebranch-is-null".equals(filter)) {
            log.debug("REST request to get all Jetkizus where routeBranch is null");
            return new ResponseEntity<>(jetkizuService.findAllWhereRouteBranchIsNull(),
                    HttpStatus.OK);
        }
        log.debug("REST request to get a page of Jetkizus");
        Page<Jetkizu> page = jetkizuService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jetkizus");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /jetkizus/:id : get the "id" jetkizu.
     *
     * @param id the id of the jetkizu to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jetkizu, or with status 404 (Not Found)
     */
    @GetMapping("/jetkizus/{id}")
    @Timed
    public ResponseEntity<Jetkizu> getJetkizu(@PathVariable Long id) {
        log.debug("REST request to get Jetkizu : {}", id);
        Jetkizu jetkizu = jetkizuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(jetkizu));
    }

    /**
     * DELETE  /jetkizus/:id : delete the "id" jetkizu.
     *
     * @param id the id of the jetkizu to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/jetkizus/{id}")
    @Timed
    public ResponseEntity<Void> deleteJetkizu(@PathVariable Long id) {
        log.debug("REST request to delete Jetkizu : {}", id);
        jetkizuService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/jetkizus?query=:query : search for the jetkizu corresponding
     * to the query.
     *
     * @param query the query of the jetkizu search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/jetkizus")
    @Timed
    public ResponseEntity<List<Jetkizu>> searchJetkizus(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Jetkizus for query {}", query);
        Page<Jetkizu> page = jetkizuService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/jetkizus");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    @GetMapping("/jetkizus/backlog")
    @Timed
    public ResponseEntity<List<Jetkizu>> getAllBacklogJetkizus(@ApiParam Pageable pageable, @RequestParam(required = false) String filter) {
        if ("routebranch-is-null".equals(filter)) {
            log.debug("REST request to get all backlog Jetkizus where routeBranch is null");
            return new ResponseEntity<>(jetkizuService.findAllWhereRouteBranchIsNull(),
                HttpStatus.OK);
        }
        log.debug("REST request to get a page of backlog Jetkizus");
        Page<Jetkizu> page = jetkizuService.findAllBacklog(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/jetkizus/backlog");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
