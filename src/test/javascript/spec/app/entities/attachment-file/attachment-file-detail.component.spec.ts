/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AttachmentFileDetailComponent } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file-detail.component';
import { AttachmentFileService } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.service';
import { AttachmentFile } from '../../../../../../main/webapp/app/entities/attachment-file/attachment-file.model';

describe('Component Tests', () => {

    describe('AttachmentFile Management Detail Component', () => {
        let comp: AttachmentFileDetailComponent;
        let fixture: ComponentFixture<AttachmentFileDetailComponent>;
        let service: AttachmentFileService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [AttachmentFileDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AttachmentFileService,
                    JhiEventManager
                ]
            }).overrideTemplate(AttachmentFileDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AttachmentFileDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AttachmentFileService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AttachmentFile(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.attachmentFile).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
