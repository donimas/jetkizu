/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JetkizuStatusHistoryDetailComponent } from '../../../../../../main/webapp/app/entities/jetkizu-status-history/jetkizu-status-history-detail.component';
import { JetkizuStatusHistoryService } from '../../../../../../main/webapp/app/entities/jetkizu-status-history/jetkizu-status-history.service';
import { JetkizuStatusHistory } from '../../../../../../main/webapp/app/entities/jetkizu-status-history/jetkizu-status-history.model';

describe('Component Tests', () => {

    describe('JetkizuStatusHistory Management Detail Component', () => {
        let comp: JetkizuStatusHistoryDetailComponent;
        let fixture: ComponentFixture<JetkizuStatusHistoryDetailComponent>;
        let service: JetkizuStatusHistoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [JetkizuStatusHistoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JetkizuStatusHistoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(JetkizuStatusHistoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JetkizuStatusHistoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JetkizuStatusHistoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new JetkizuStatusHistory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jetkizuStatusHistory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
