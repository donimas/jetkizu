/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JetkizuRouteDetailComponent } from '../../../../../../main/webapp/app/entities/jetkizu-route/jetkizu-route-detail.component';
import { JetkizuRouteService } from '../../../../../../main/webapp/app/entities/jetkizu-route/jetkizu-route.service';
import { JetkizuRoute } from '../../../../../../main/webapp/app/entities/jetkizu-route/jetkizu-route.model';

describe('Component Tests', () => {

    describe('JetkizuRoute Management Detail Component', () => {
        let comp: JetkizuRouteDetailComponent;
        let fixture: ComponentFixture<JetkizuRouteDetailComponent>;
        let service: JetkizuRouteService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [JetkizuRouteDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JetkizuRouteService,
                    JhiEventManager
                ]
            }).overrideTemplate(JetkizuRouteDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JetkizuRouteDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JetkizuRouteService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new JetkizuRoute(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jetkizuRoute).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
