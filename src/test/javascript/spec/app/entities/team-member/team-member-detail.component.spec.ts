/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { TeamMemberDetailComponent } from '../../../../../../main/webapp/app/entities/team-member/team-member-detail.component';
import { TeamMemberService } from '../../../../../../main/webapp/app/entities/team-member/team-member.service';
import { TeamMember } from '../../../../../../main/webapp/app/entities/team-member/team-member.model';

describe('Component Tests', () => {

    describe('TeamMember Management Detail Component', () => {
        let comp: TeamMemberDetailComponent;
        let fixture: ComponentFixture<TeamMemberDetailComponent>;
        let service: TeamMemberService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [TeamMemberDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    TeamMemberService,
                    JhiEventManager
                ]
            }).overrideTemplate(TeamMemberDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TeamMemberDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(TeamMemberService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new TeamMember(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.teamMember).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
