/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CsvFieldDetailComponent } from '../../../../../../main/webapp/app/entities/csv-field/csv-field-detail.component';
import { CsvFieldService } from '../../../../../../main/webapp/app/entities/csv-field/csv-field.service';
import { CsvField } from '../../../../../../main/webapp/app/entities/csv-field/csv-field.model';

describe('Component Tests', () => {

    describe('CsvField Management Detail Component', () => {
        let comp: CsvFieldDetailComponent;
        let fixture: ComponentFixture<CsvFieldDetailComponent>;
        let service: CsvFieldService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [CsvFieldDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    CsvFieldService,
                    JhiEventManager
                ]
            }).overrideTemplate(CsvFieldDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CsvFieldDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CsvFieldService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new CsvField(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.csvField).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
