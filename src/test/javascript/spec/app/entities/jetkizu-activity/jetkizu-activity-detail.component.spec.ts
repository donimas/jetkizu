/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JetkizuActivityDetailComponent } from '../../../../../../main/webapp/app/entities/jetkizu-activity/jetkizu-activity-detail.component';
import { JetkizuActivityService } from '../../../../../../main/webapp/app/entities/jetkizu-activity/jetkizu-activity.service';
import { JetkizuActivity } from '../../../../../../main/webapp/app/entities/jetkizu-activity/jetkizu-activity.model';

describe('Component Tests', () => {

    describe('JetkizuActivity Management Detail Component', () => {
        let comp: JetkizuActivityDetailComponent;
        let fixture: ComponentFixture<JetkizuActivityDetailComponent>;
        let service: JetkizuActivityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [JetkizuActivityDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JetkizuActivityService,
                    JhiEventManager
                ]
            }).overrideTemplate(JetkizuActivityDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JetkizuActivityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JetkizuActivityService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new JetkizuActivity(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jetkizuActivity).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
