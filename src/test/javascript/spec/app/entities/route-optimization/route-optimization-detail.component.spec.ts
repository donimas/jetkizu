/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RouteOptimizationDetailComponent } from '../../../../../../main/webapp/app/entities/route-optimization/route-optimization-detail.component';
import { RouteOptimizationService } from '../../../../../../main/webapp/app/entities/route-optimization/route-optimization.service';
import { RouteOptimization } from '../../../../../../main/webapp/app/entities/route-optimization/route-optimization.model';

describe('Component Tests', () => {

    describe('RouteOptimization Management Detail Component', () => {
        let comp: RouteOptimizationDetailComponent;
        let fixture: ComponentFixture<RouteOptimizationDetailComponent>;
        let service: RouteOptimizationService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [RouteOptimizationDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RouteOptimizationService,
                    JhiEventManager
                ]
            }).overrideTemplate(RouteOptimizationDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RouteOptimizationDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RouteOptimizationService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RouteOptimization(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.routeOptimization).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
