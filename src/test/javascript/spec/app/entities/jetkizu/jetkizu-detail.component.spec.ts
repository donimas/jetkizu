/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JetkizuDetailComponent } from '../../../../../../main/webapp/app/entities/jetkizu/jetkizu-detail.component';
import { JetkizuService } from '../../../../../../main/webapp/app/entities/jetkizu/jetkizu.service';
import { Jetkizu } from '../../../../../../main/webapp/app/entities/jetkizu/jetkizu.model';

describe('Component Tests', () => {

    describe('Jetkizu Management Detail Component', () => {
        let comp: JetkizuDetailComponent;
        let fixture: ComponentFixture<JetkizuDetailComponent>;
        let service: JetkizuService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [JetkizuDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JetkizuService,
                    JhiEventManager
                ]
            }).overrideTemplate(JetkizuDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JetkizuDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JetkizuService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Jetkizu(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jetkizu).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
