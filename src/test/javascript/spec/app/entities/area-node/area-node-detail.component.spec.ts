/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { AreaNodeDetailComponent } from '../../../../../../main/webapp/app/entities/area-node/area-node-detail.component';
import { AreaNodeService } from '../../../../../../main/webapp/app/entities/area-node/area-node.service';
import { AreaNode } from '../../../../../../main/webapp/app/entities/area-node/area-node.model';

describe('Component Tests', () => {

    describe('AreaNode Management Detail Component', () => {
        let comp: AreaNodeDetailComponent;
        let fixture: ComponentFixture<AreaNodeDetailComponent>;
        let service: AreaNodeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [AreaNodeDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    AreaNodeService,
                    JhiEventManager
                ]
            }).overrideTemplate(AreaNodeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AreaNodeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AreaNodeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new AreaNode(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.areaNode).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
