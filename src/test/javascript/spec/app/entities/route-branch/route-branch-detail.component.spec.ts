/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RouteBranchDetailComponent } from '../../../../../../main/webapp/app/entities/route-branch/route-branch-detail.component';
import { RouteBranchService } from '../../../../../../main/webapp/app/entities/route-branch/route-branch.service';
import { RouteBranch } from '../../../../../../main/webapp/app/entities/route-branch/route-branch.model';

describe('Component Tests', () => {

    describe('RouteBranch Management Detail Component', () => {
        let comp: RouteBranchDetailComponent;
        let fixture: ComponentFixture<RouteBranchDetailComponent>;
        let service: RouteBranchService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [RouteBranchDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RouteBranchService,
                    JhiEventManager
                ]
            }).overrideTemplate(RouteBranchDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RouteBranchDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RouteBranchService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RouteBranch(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.routeBranch).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
