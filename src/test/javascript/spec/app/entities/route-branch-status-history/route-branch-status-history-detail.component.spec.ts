/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { RouteBranchStatusHistoryDetailComponent } from '../../../../../../main/webapp/app/entities/route-branch-status-history/route-branch-status-history-detail.component';
import { RouteBranchStatusHistoryService } from '../../../../../../main/webapp/app/entities/route-branch-status-history/route-branch-status-history.service';
import { RouteBranchStatusHistory } from '../../../../../../main/webapp/app/entities/route-branch-status-history/route-branch-status-history.model';

describe('Component Tests', () => {

    describe('RouteBranchStatusHistory Management Detail Component', () => {
        let comp: RouteBranchStatusHistoryDetailComponent;
        let fixture: ComponentFixture<RouteBranchStatusHistoryDetailComponent>;
        let service: RouteBranchStatusHistoryService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [RouteBranchStatusHistoryDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    RouteBranchStatusHistoryService,
                    JhiEventManager
                ]
            }).overrideTemplate(RouteBranchStatusHistoryDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RouteBranchStatusHistoryDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RouteBranchStatusHistoryService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new RouteBranchStatusHistory(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.routeBranchStatusHistory).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
