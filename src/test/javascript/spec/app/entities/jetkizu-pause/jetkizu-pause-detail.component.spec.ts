/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { JetkizuTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { JetkizuPauseDetailComponent } from '../../../../../../main/webapp/app/entities/jetkizu-pause/jetkizu-pause-detail.component';
import { JetkizuPauseService } from '../../../../../../main/webapp/app/entities/jetkizu-pause/jetkizu-pause.service';
import { JetkizuPause } from '../../../../../../main/webapp/app/entities/jetkizu-pause/jetkizu-pause.model';

describe('Component Tests', () => {

    describe('JetkizuPause Management Detail Component', () => {
        let comp: JetkizuPauseDetailComponent;
        let fixture: ComponentFixture<JetkizuPauseDetailComponent>;
        let service: JetkizuPauseService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JetkizuTestModule],
                declarations: [JetkizuPauseDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    JetkizuPauseService,
                    JhiEventManager
                ]
            }).overrideTemplate(JetkizuPauseDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(JetkizuPauseDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(JetkizuPauseService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new JetkizuPause(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.jetkizuPause).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
