package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.JetkizuActivity;
import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.repository.JetkizuActivityRepository;
import kz.rs.jetkizu.service.JetkizuActivityService;
import kz.rs.jetkizu.repository.search.JetkizuActivitySearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JetkizuActivityResource REST controller.
 *
 * @see JetkizuActivityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class JetkizuActivityResourceIntTest {

    private static final BigDecimal DEFAULT_PLAN_KM = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLAN_KM = new BigDecimal(2);

    private static final BigDecimal DEFAULT_FACT_KM = new BigDecimal(1);
    private static final BigDecimal UPDATED_FACT_KM = new BigDecimal(2);

    private static final BigDecimal DEFAULT_PLAN_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_PLAN_DURATION = new BigDecimal(2);

    private static final BigDecimal DEFAULT_FACT_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_FACT_DURATION = new BigDecimal(2);

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_FROM_LAT = "AAAAAAAAAA";
    private static final String UPDATED_FROM_LAT = "BBBBBBBBBB";

    private static final String DEFAULT_FROM_LON = "AAAAAAAAAA";
    private static final String UPDATED_FROM_LON = "BBBBBBBBBB";

    @Autowired
    private JetkizuActivityRepository jetkizuActivityRepository;

    @Autowired
    private JetkizuActivityService jetkizuActivityService;

    @Autowired
    private JetkizuActivitySearchRepository jetkizuActivitySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJetkizuActivityMockMvc;

    private JetkizuActivity jetkizuActivity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JetkizuActivityResource jetkizuActivityResource = new JetkizuActivityResource(jetkizuActivityService);
        this.restJetkizuActivityMockMvc = MockMvcBuilders.standaloneSetup(jetkizuActivityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JetkizuActivity createEntity(EntityManager em) {
        JetkizuActivity jetkizuActivity = new JetkizuActivity()
            .planKm(DEFAULT_PLAN_KM)
            .factKm(DEFAULT_FACT_KM)
            .planDuration(DEFAULT_PLAN_DURATION)
            .factDuration(DEFAULT_FACT_DURATION)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .fromLat(DEFAULT_FROM_LAT)
            .fromLon(DEFAULT_FROM_LON);
        // Add required entity
        RouteBranch routeBranch = RouteBranchResourceIntTest.createEntity(em);
        em.persist(routeBranch);
        em.flush();
        jetkizuActivity.setRouteBranch(routeBranch);
        return jetkizuActivity;
    }

    @Before
    public void initTest() {
        jetkizuActivitySearchRepository.deleteAll();
        jetkizuActivity = createEntity(em);
    }

    @Test
    @Transactional
    public void createJetkizuActivity() throws Exception {
        int databaseSizeBeforeCreate = jetkizuActivityRepository.findAll().size();

        // Create the JetkizuActivity
        restJetkizuActivityMockMvc.perform(post("/api/jetkizu-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuActivity)))
            .andExpect(status().isCreated());

        // Validate the JetkizuActivity in the database
        List<JetkizuActivity> jetkizuActivityList = jetkizuActivityRepository.findAll();
        assertThat(jetkizuActivityList).hasSize(databaseSizeBeforeCreate + 1);
        JetkizuActivity testJetkizuActivity = jetkizuActivityList.get(jetkizuActivityList.size() - 1);
        assertThat(testJetkizuActivity.getPlanKm()).isEqualTo(DEFAULT_PLAN_KM);
        assertThat(testJetkizuActivity.getFactKm()).isEqualTo(DEFAULT_FACT_KM);
        assertThat(testJetkizuActivity.getPlanDuration()).isEqualTo(DEFAULT_PLAN_DURATION);
        assertThat(testJetkizuActivity.getFactDuration()).isEqualTo(DEFAULT_FACT_DURATION);
        assertThat(testJetkizuActivity.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testJetkizuActivity.getFromLat()).isEqualTo(DEFAULT_FROM_LAT);
        assertThat(testJetkizuActivity.getFromLon()).isEqualTo(DEFAULT_FROM_LON);

        // Validate the JetkizuActivity in Elasticsearch
        JetkizuActivity jetkizuActivityEs = jetkizuActivitySearchRepository.findOne(testJetkizuActivity.getId());
        assertThat(jetkizuActivityEs).isEqualToComparingFieldByField(testJetkizuActivity);
    }

    @Test
    @Transactional
    public void createJetkizuActivityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jetkizuActivityRepository.findAll().size();

        // Create the JetkizuActivity with an existing ID
        jetkizuActivity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJetkizuActivityMockMvc.perform(post("/api/jetkizu-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuActivity)))
            .andExpect(status().isBadRequest());

        // Validate the JetkizuActivity in the database
        List<JetkizuActivity> jetkizuActivityList = jetkizuActivityRepository.findAll();
        assertThat(jetkizuActivityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllJetkizuActivities() throws Exception {
        // Initialize the database
        jetkizuActivityRepository.saveAndFlush(jetkizuActivity);

        // Get all the jetkizuActivityList
        restJetkizuActivityMockMvc.perform(get("/api/jetkizu-activities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].planKm").value(hasItem(DEFAULT_PLAN_KM.intValue())))
            .andExpect(jsonPath("$.[*].factKm").value(hasItem(DEFAULT_FACT_KM.intValue())))
            .andExpect(jsonPath("$.[*].planDuration").value(hasItem(DEFAULT_PLAN_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].factDuration").value(hasItem(DEFAULT_FACT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].fromLat").value(hasItem(DEFAULT_FROM_LAT.toString())))
            .andExpect(jsonPath("$.[*].fromLon").value(hasItem(DEFAULT_FROM_LON.toString())));
    }

    @Test
    @Transactional
    public void getJetkizuActivity() throws Exception {
        // Initialize the database
        jetkizuActivityRepository.saveAndFlush(jetkizuActivity);

        // Get the jetkizuActivity
        restJetkizuActivityMockMvc.perform(get("/api/jetkizu-activities/{id}", jetkizuActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jetkizuActivity.getId().intValue()))
            .andExpect(jsonPath("$.planKm").value(DEFAULT_PLAN_KM.intValue()))
            .andExpect(jsonPath("$.factKm").value(DEFAULT_FACT_KM.intValue()))
            .andExpect(jsonPath("$.planDuration").value(DEFAULT_PLAN_DURATION.intValue()))
            .andExpect(jsonPath("$.factDuration").value(DEFAULT_FACT_DURATION.intValue()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.fromLat").value(DEFAULT_FROM_LAT.toString()))
            .andExpect(jsonPath("$.fromLon").value(DEFAULT_FROM_LON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJetkizuActivity() throws Exception {
        // Get the jetkizuActivity
        restJetkizuActivityMockMvc.perform(get("/api/jetkizu-activities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJetkizuActivity() throws Exception {
        // Initialize the database
        jetkizuActivityService.save(jetkizuActivity);

        int databaseSizeBeforeUpdate = jetkizuActivityRepository.findAll().size();

        // Update the jetkizuActivity
        JetkizuActivity updatedJetkizuActivity = jetkizuActivityRepository.findOne(jetkizuActivity.getId());
        updatedJetkizuActivity
            .planKm(UPDATED_PLAN_KM)
            .factKm(UPDATED_FACT_KM)
            .planDuration(UPDATED_PLAN_DURATION)
            .factDuration(UPDATED_FACT_DURATION)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .fromLat(UPDATED_FROM_LAT)
            .fromLon(UPDATED_FROM_LON);

        restJetkizuActivityMockMvc.perform(put("/api/jetkizu-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJetkizuActivity)))
            .andExpect(status().isOk());

        // Validate the JetkizuActivity in the database
        List<JetkizuActivity> jetkizuActivityList = jetkizuActivityRepository.findAll();
        assertThat(jetkizuActivityList).hasSize(databaseSizeBeforeUpdate);
        JetkizuActivity testJetkizuActivity = jetkizuActivityList.get(jetkizuActivityList.size() - 1);
        assertThat(testJetkizuActivity.getPlanKm()).isEqualTo(UPDATED_PLAN_KM);
        assertThat(testJetkizuActivity.getFactKm()).isEqualTo(UPDATED_FACT_KM);
        assertThat(testJetkizuActivity.getPlanDuration()).isEqualTo(UPDATED_PLAN_DURATION);
        assertThat(testJetkizuActivity.getFactDuration()).isEqualTo(UPDATED_FACT_DURATION);
        assertThat(testJetkizuActivity.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testJetkizuActivity.getFromLat()).isEqualTo(UPDATED_FROM_LAT);
        assertThat(testJetkizuActivity.getFromLon()).isEqualTo(UPDATED_FROM_LON);

        // Validate the JetkizuActivity in Elasticsearch
        JetkizuActivity jetkizuActivityEs = jetkizuActivitySearchRepository.findOne(testJetkizuActivity.getId());
        assertThat(jetkizuActivityEs).isEqualToComparingFieldByField(testJetkizuActivity);
    }

    @Test
    @Transactional
    public void updateNonExistingJetkizuActivity() throws Exception {
        int databaseSizeBeforeUpdate = jetkizuActivityRepository.findAll().size();

        // Create the JetkizuActivity

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJetkizuActivityMockMvc.perform(put("/api/jetkizu-activities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuActivity)))
            .andExpect(status().isCreated());

        // Validate the JetkizuActivity in the database
        List<JetkizuActivity> jetkizuActivityList = jetkizuActivityRepository.findAll();
        assertThat(jetkizuActivityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJetkizuActivity() throws Exception {
        // Initialize the database
        jetkizuActivityService.save(jetkizuActivity);

        int databaseSizeBeforeDelete = jetkizuActivityRepository.findAll().size();

        // Get the jetkizuActivity
        restJetkizuActivityMockMvc.perform(delete("/api/jetkizu-activities/{id}", jetkizuActivity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean jetkizuActivityExistsInEs = jetkizuActivitySearchRepository.exists(jetkizuActivity.getId());
        assertThat(jetkizuActivityExistsInEs).isFalse();

        // Validate the database is empty
        List<JetkizuActivity> jetkizuActivityList = jetkizuActivityRepository.findAll();
        assertThat(jetkizuActivityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJetkizuActivity() throws Exception {
        // Initialize the database
        jetkizuActivityService.save(jetkizuActivity);

        // Search the jetkizuActivity
        restJetkizuActivityMockMvc.perform(get("/api/_search/jetkizu-activities?query=id:" + jetkizuActivity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuActivity.getId().intValue())))
            .andExpect(jsonPath("$.[*].planKm").value(hasItem(DEFAULT_PLAN_KM.intValue())))
            .andExpect(jsonPath("$.[*].factKm").value(hasItem(DEFAULT_FACT_KM.intValue())))
            .andExpect(jsonPath("$.[*].planDuration").value(hasItem(DEFAULT_PLAN_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].factDuration").value(hasItem(DEFAULT_FACT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].fromLat").value(hasItem(DEFAULT_FROM_LAT.toString())))
            .andExpect(jsonPath("$.[*].fromLon").value(hasItem(DEFAULT_FROM_LON.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JetkizuActivity.class);
        JetkizuActivity jetkizuActivity1 = new JetkizuActivity();
        jetkizuActivity1.setId(1L);
        JetkizuActivity jetkizuActivity2 = new JetkizuActivity();
        jetkizuActivity2.setId(jetkizuActivity1.getId());
        assertThat(jetkizuActivity1).isEqualTo(jetkizuActivity2);
        jetkizuActivity2.setId(2L);
        assertThat(jetkizuActivity1).isNotEqualTo(jetkizuActivity2);
        jetkizuActivity1.setId(null);
        assertThat(jetkizuActivity1).isNotEqualTo(jetkizuActivity2);
    }
}
