package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.RouteBranch;
import kz.rs.jetkizu.repository.RouteBranchRepository;
import kz.rs.jetkizu.service.RouteBranchService;
import kz.rs.jetkizu.repository.search.RouteBranchSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RouteBranchResource REST controller.
 *
 * @see RouteBranchResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class RouteBranchResourceIntTest {

    private static final Float DEFAULT_ORDER = 1F;
    private static final Float UPDATED_ORDER = 2F;

    @Autowired
    private RouteBranchRepository routeBranchRepository;

    @Autowired
    private RouteBranchService routeBranchService;

    @Autowired
    private RouteBranchSearchRepository routeBranchSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRouteBranchMockMvc;

    private RouteBranch routeBranch;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RouteBranchResource routeBranchResource = new RouteBranchResource(routeBranchService);
        this.restRouteBranchMockMvc = MockMvcBuilders.standaloneSetup(routeBranchResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RouteBranch createEntity(EntityManager em) {
        RouteBranch routeBranch = new RouteBranch()
            .order(DEFAULT_ORDER);
        return routeBranch;
    }

    @Before
    public void initTest() {
        routeBranchSearchRepository.deleteAll();
        routeBranch = createEntity(em);
    }

    @Test
    @Transactional
    public void createRouteBranch() throws Exception {
        int databaseSizeBeforeCreate = routeBranchRepository.findAll().size();

        // Create the RouteBranch
        restRouteBranchMockMvc.perform(post("/api/route-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranch)))
            .andExpect(status().isCreated());

        // Validate the RouteBranch in the database
        List<RouteBranch> routeBranchList = routeBranchRepository.findAll();
        assertThat(routeBranchList).hasSize(databaseSizeBeforeCreate + 1);
        RouteBranch testRouteBranch = routeBranchList.get(routeBranchList.size() - 1);
        assertThat(testRouteBranch.getOrder()).isEqualTo(DEFAULT_ORDER);

        // Validate the RouteBranch in Elasticsearch
        RouteBranch routeBranchEs = routeBranchSearchRepository.findOne(testRouteBranch.getId());
        assertThat(routeBranchEs).isEqualToComparingFieldByField(testRouteBranch);
    }

    @Test
    @Transactional
    public void createRouteBranchWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = routeBranchRepository.findAll().size();

        // Create the RouteBranch with an existing ID
        routeBranch.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRouteBranchMockMvc.perform(post("/api/route-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranch)))
            .andExpect(status().isBadRequest());

        // Validate the RouteBranch in the database
        List<RouteBranch> routeBranchList = routeBranchRepository.findAll();
        assertThat(routeBranchList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRouteBranches() throws Exception {
        // Initialize the database
        routeBranchRepository.saveAndFlush(routeBranch);

        // Get all the routeBranchList
        restRouteBranchMockMvc.perform(get("/api/route-branches?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routeBranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.doubleValue())));
    }

    @Test
    @Transactional
    public void getRouteBranch() throws Exception {
        // Initialize the database
        routeBranchRepository.saveAndFlush(routeBranch);

        // Get the routeBranch
        restRouteBranchMockMvc.perform(get("/api/route-branches/{id}", routeBranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(routeBranch.getId().intValue()))
            .andExpect(jsonPath("$.order").value(DEFAULT_ORDER.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingRouteBranch() throws Exception {
        // Get the routeBranch
        restRouteBranchMockMvc.perform(get("/api/route-branches/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRouteBranch() throws Exception {
        // Initialize the database
        routeBranchService.save(routeBranch);

        int databaseSizeBeforeUpdate = routeBranchRepository.findAll().size();

        // Update the routeBranch
        RouteBranch updatedRouteBranch = routeBranchRepository.findOne(routeBranch.getId());
        updatedRouteBranch
            .order(UPDATED_ORDER);

        restRouteBranchMockMvc.perform(put("/api/route-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRouteBranch)))
            .andExpect(status().isOk());

        // Validate the RouteBranch in the database
        List<RouteBranch> routeBranchList = routeBranchRepository.findAll();
        assertThat(routeBranchList).hasSize(databaseSizeBeforeUpdate);
        RouteBranch testRouteBranch = routeBranchList.get(routeBranchList.size() - 1);
        assertThat(testRouteBranch.getOrder()).isEqualTo(UPDATED_ORDER);

        // Validate the RouteBranch in Elasticsearch
        RouteBranch routeBranchEs = routeBranchSearchRepository.findOne(testRouteBranch.getId());
        assertThat(routeBranchEs).isEqualToComparingFieldByField(testRouteBranch);
    }

    @Test
    @Transactional
    public void updateNonExistingRouteBranch() throws Exception {
        int databaseSizeBeforeUpdate = routeBranchRepository.findAll().size();

        // Create the RouteBranch

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRouteBranchMockMvc.perform(put("/api/route-branches")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranch)))
            .andExpect(status().isCreated());

        // Validate the RouteBranch in the database
        List<RouteBranch> routeBranchList = routeBranchRepository.findAll();
        assertThat(routeBranchList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRouteBranch() throws Exception {
        // Initialize the database
        routeBranchService.save(routeBranch);

        int databaseSizeBeforeDelete = routeBranchRepository.findAll().size();

        // Get the routeBranch
        restRouteBranchMockMvc.perform(delete("/api/route-branches/{id}", routeBranch.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean routeBranchExistsInEs = routeBranchSearchRepository.exists(routeBranch.getId());
        assertThat(routeBranchExistsInEs).isFalse();

        // Validate the database is empty
        List<RouteBranch> routeBranchList = routeBranchRepository.findAll();
        assertThat(routeBranchList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchRouteBranch() throws Exception {
        // Initialize the database
        routeBranchService.save(routeBranch);

        // Search the routeBranch
        restRouteBranchMockMvc.perform(get("/api/_search/route-branches?query=id:" + routeBranch.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routeBranch.getId().intValue())))
            .andExpect(jsonPath("$.[*].order").value(hasItem(DEFAULT_ORDER.doubleValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RouteBranch.class);
        RouteBranch routeBranch1 = new RouteBranch();
        routeBranch1.setId(1L);
        RouteBranch routeBranch2 = new RouteBranch();
        routeBranch2.setId(routeBranch1.getId());
        assertThat(routeBranch1).isEqualTo(routeBranch2);
        routeBranch2.setId(2L);
        assertThat(routeBranch1).isNotEqualTo(routeBranch2);
        routeBranch1.setId(null);
        assertThat(routeBranch1).isNotEqualTo(routeBranch2);
    }
}
