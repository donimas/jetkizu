package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.CsvField;
import kz.rs.jetkizu.repository.CsvFieldRepository;
import kz.rs.jetkizu.service.CsvFieldService;
import kz.rs.jetkizu.repository.search.CsvFieldSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;
import kz.rs.jetkizu.service.dto.CsvFieldCriteria;
import kz.rs.jetkizu.service.CsvFieldQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.rs.jetkizu.domain.enumeration.CsvFieldType;
/**
 * Test class for the CsvFieldResource REST controller.
 *
 * @see CsvFieldResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class CsvFieldResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final CsvFieldType DEFAULT_TYPE = CsvFieldType.ORDER;
    private static final CsvFieldType UPDATED_TYPE = CsvFieldType.ORDER;

    @Autowired
    private CsvFieldRepository csvFieldRepository;

    @Autowired
    private CsvFieldService csvFieldService;

    @Autowired
    private CsvFieldSearchRepository csvFieldSearchRepository;

    @Autowired
    private CsvFieldQueryService csvFieldQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCsvFieldMockMvc;

    private CsvField csvField;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CsvFieldResource csvFieldResource = new CsvFieldResource(csvFieldService, csvFieldQueryService);
        this.restCsvFieldMockMvc = MockMvcBuilders.standaloneSetup(csvFieldResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CsvField createEntity(EntityManager em) {
        CsvField csvField = new CsvField()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .type(DEFAULT_TYPE);
        return csvField;
    }

    @Before
    public void initTest() {
        csvFieldSearchRepository.deleteAll();
        csvField = createEntity(em);
    }

    @Test
    @Transactional
    public void createCsvField() throws Exception {
        int databaseSizeBeforeCreate = csvFieldRepository.findAll().size();

        // Create the CsvField
        restCsvFieldMockMvc.perform(post("/api/csv-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(csvField)))
            .andExpect(status().isCreated());

        // Validate the CsvField in the database
        List<CsvField> csvFieldList = csvFieldRepository.findAll();
        assertThat(csvFieldList).hasSize(databaseSizeBeforeCreate + 1);
        CsvField testCsvField = csvFieldList.get(csvFieldList.size() - 1);
        assertThat(testCsvField.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCsvField.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCsvField.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCsvField.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the CsvField in Elasticsearch
        CsvField csvFieldEs = csvFieldSearchRepository.findOne(testCsvField.getId());
        assertThat(csvFieldEs).isEqualToComparingFieldByField(testCsvField);
    }

    @Test
    @Transactional
    public void createCsvFieldWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = csvFieldRepository.findAll().size();

        // Create the CsvField with an existing ID
        csvField.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCsvFieldMockMvc.perform(post("/api/csv-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(csvField)))
            .andExpect(status().isBadRequest());

        // Validate the CsvField in the database
        List<CsvField> csvFieldList = csvFieldRepository.findAll();
        assertThat(csvFieldList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCsvFields() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList
        restCsvFieldMockMvc.perform(get("/api/csv-fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(csvField.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getCsvField() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get the csvField
        restCsvFieldMockMvc.perform(get("/api/csv-fields/{id}", csvField.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(csvField.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where name equals to DEFAULT_NAME
        defaultCsvFieldShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the csvFieldList where name equals to UPDATED_NAME
        defaultCsvFieldShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCsvFieldShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the csvFieldList where name equals to UPDATED_NAME
        defaultCsvFieldShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where name is not null
        defaultCsvFieldShouldBeFound("name.specified=true");

        // Get all the csvFieldList where name is null
        defaultCsvFieldShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where code equals to DEFAULT_CODE
        defaultCsvFieldShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the csvFieldList where code equals to UPDATED_CODE
        defaultCsvFieldShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where code in DEFAULT_CODE or UPDATED_CODE
        defaultCsvFieldShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the csvFieldList where code equals to UPDATED_CODE
        defaultCsvFieldShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where code is not null
        defaultCsvFieldShouldBeFound("code.specified=true");

        // Get all the csvFieldList where code is null
        defaultCsvFieldShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where description equals to DEFAULT_DESCRIPTION
        defaultCsvFieldShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the csvFieldList where description equals to UPDATED_DESCRIPTION
        defaultCsvFieldShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultCsvFieldShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the csvFieldList where description equals to UPDATED_DESCRIPTION
        defaultCsvFieldShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where description is not null
        defaultCsvFieldShouldBeFound("description.specified=true");

        // Get all the csvFieldList where description is null
        defaultCsvFieldShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where type equals to DEFAULT_TYPE
        defaultCsvFieldShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the csvFieldList where type equals to UPDATED_TYPE
        defaultCsvFieldShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultCsvFieldShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the csvFieldList where type equals to UPDATED_TYPE
        defaultCsvFieldShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllCsvFieldsByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        csvFieldRepository.saveAndFlush(csvField);

        // Get all the csvFieldList where type is not null
        defaultCsvFieldShouldBeFound("type.specified=true");

        // Get all the csvFieldList where type is null
        defaultCsvFieldShouldNotBeFound("type.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultCsvFieldShouldBeFound(String filter) throws Exception {
        restCsvFieldMockMvc.perform(get("/api/csv-fields?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(csvField.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultCsvFieldShouldNotBeFound(String filter) throws Exception {
        restCsvFieldMockMvc.perform(get("/api/csv-fields?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingCsvField() throws Exception {
        // Get the csvField
        restCsvFieldMockMvc.perform(get("/api/csv-fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCsvField() throws Exception {
        // Initialize the database
        csvFieldService.save(csvField);

        int databaseSizeBeforeUpdate = csvFieldRepository.findAll().size();

        // Update the csvField
        CsvField updatedCsvField = csvFieldRepository.findOne(csvField.getId());
        updatedCsvField
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .type(UPDATED_TYPE);

        restCsvFieldMockMvc.perform(put("/api/csv-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCsvField)))
            .andExpect(status().isOk());

        // Validate the CsvField in the database
        List<CsvField> csvFieldList = csvFieldRepository.findAll();
        assertThat(csvFieldList).hasSize(databaseSizeBeforeUpdate);
        CsvField testCsvField = csvFieldList.get(csvFieldList.size() - 1);
        assertThat(testCsvField.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCsvField.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCsvField.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCsvField.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the CsvField in Elasticsearch
        CsvField csvFieldEs = csvFieldSearchRepository.findOne(testCsvField.getId());
        assertThat(csvFieldEs).isEqualToComparingFieldByField(testCsvField);
    }

    @Test
    @Transactional
    public void updateNonExistingCsvField() throws Exception {
        int databaseSizeBeforeUpdate = csvFieldRepository.findAll().size();

        // Create the CsvField

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCsvFieldMockMvc.perform(put("/api/csv-fields")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(csvField)))
            .andExpect(status().isCreated());

        // Validate the CsvField in the database
        List<CsvField> csvFieldList = csvFieldRepository.findAll();
        assertThat(csvFieldList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCsvField() throws Exception {
        // Initialize the database
        csvFieldService.save(csvField);

        int databaseSizeBeforeDelete = csvFieldRepository.findAll().size();

        // Get the csvField
        restCsvFieldMockMvc.perform(delete("/api/csv-fields/{id}", csvField.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean csvFieldExistsInEs = csvFieldSearchRepository.exists(csvField.getId());
        assertThat(csvFieldExistsInEs).isFalse();

        // Validate the database is empty
        List<CsvField> csvFieldList = csvFieldRepository.findAll();
        assertThat(csvFieldList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchCsvField() throws Exception {
        // Initialize the database
        csvFieldService.save(csvField);

        // Search the csvField
        restCsvFieldMockMvc.perform(get("/api/_search/csv-fields?query=id:" + csvField.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(csvField.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CsvField.class);
        CsvField csvField1 = new CsvField();
        csvField1.setId(1L);
        CsvField csvField2 = new CsvField();
        csvField2.setId(csvField1.getId());
        assertThat(csvField1).isEqualTo(csvField2);
        csvField2.setId(2L);
        assertThat(csvField1).isNotEqualTo(csvField2);
        csvField1.setId(null);
        assertThat(csvField1).isNotEqualTo(csvField2);
    }
}
