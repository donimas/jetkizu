package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.Vehicle;
import kz.rs.jetkizu.repository.VehicleRepository;
import kz.rs.jetkizu.service.VehicleService;
import kz.rs.jetkizu.repository.search.VehicleSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;
import kz.rs.jetkizu.service.dto.VehicleCriteria;
import kz.rs.jetkizu.service.VehicleQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.rs.jetkizu.domain.enumeration.VehicleType;
/**
 * Test class for the VehicleResource REST controller.
 *
 * @see VehicleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class VehicleResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_GOV_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_GOV_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_TECH_PASSPORT_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_TECH_PASSPORT_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_MODEL = "AAAAAAAAAA";
    private static final String UPDATED_MODEL = "BBBBBBBBBB";

    private static final Integer DEFAULT_BUILD_YEAR = 1;
    private static final Integer UPDATED_BUILD_YEAR = 2;

    private static final VehicleType DEFAULT_TYPE = VehicleType.FURA;
    private static final VehicleType UPDATED_TYPE = VehicleType.GAZEL;

    private static final Float DEFAULT_HEIGHT = 1F;
    private static final Float UPDATED_HEIGHT = 2F;

    private static final Float DEFAULT_LENGTH = 1F;
    private static final Float UPDATED_LENGTH = 2F;

    private static final Float DEFAULT_WIDTH = 1F;
    private static final Float UPDATED_WIDTH = 2F;

    private static final Float DEFAULT_TOTAL_WEIGHT = 1F;
    private static final Float UPDATED_TOTAL_WEIGHT = 2F;

    private static final BigDecimal DEFAULT_DELIVERY_WEIGHT = new BigDecimal(1);
    private static final BigDecimal UPDATED_DELIVERY_WEIGHT = new BigDecimal(2);

    private static final String DEFAULT_NOTE = "AAAAAAAAAA";
    private static final String UPDATED_NOTE = "BBBBBBBBBB";

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private VehicleSearchRepository vehicleSearchRepository;

    @Autowired
    private VehicleQueryService vehicleQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVehicleMockMvc;

    private Vehicle vehicle;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VehicleResource vehicleResource = new VehicleResource(vehicleService, vehicleQueryService);
        this.restVehicleMockMvc = MockMvcBuilders.standaloneSetup(vehicleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Vehicle createEntity(EntityManager em) {
        Vehicle vehicle = new Vehicle()
            .name(DEFAULT_NAME)
            .govNumber(DEFAULT_GOV_NUMBER)
            .techPassportNumber(DEFAULT_TECH_PASSPORT_NUMBER)
            .model(DEFAULT_MODEL)
            .buildYear(DEFAULT_BUILD_YEAR)
            .type(DEFAULT_TYPE)
            .height(DEFAULT_HEIGHT)
            .length(DEFAULT_LENGTH)
            .width(DEFAULT_WIDTH)
            .totalWeight(DEFAULT_TOTAL_WEIGHT)
            .deliveryWeight(DEFAULT_DELIVERY_WEIGHT)
            .note(DEFAULT_NOTE);
        return vehicle;
    }

    @Before
    public void initTest() {
        vehicleSearchRepository.deleteAll();
        vehicle = createEntity(em);
    }

    @Test
    @Transactional
    public void createVehicle() throws Exception {
        int databaseSizeBeforeCreate = vehicleRepository.findAll().size();

        // Create the Vehicle
        restVehicleMockMvc.perform(post("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isCreated());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeCreate + 1);
        Vehicle testVehicle = vehicleList.get(vehicleList.size() - 1);
        assertThat(testVehicle.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVehicle.getGovNumber()).isEqualTo(DEFAULT_GOV_NUMBER);
        assertThat(testVehicle.getTechPassportNumber()).isEqualTo(DEFAULT_TECH_PASSPORT_NUMBER);
        assertThat(testVehicle.getModel()).isEqualTo(DEFAULT_MODEL);
        assertThat(testVehicle.getBuildYear()).isEqualTo(DEFAULT_BUILD_YEAR);
        assertThat(testVehicle.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testVehicle.getHeight()).isEqualTo(DEFAULT_HEIGHT);
        assertThat(testVehicle.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testVehicle.getWidth()).isEqualTo(DEFAULT_WIDTH);
        assertThat(testVehicle.getTotalWeight()).isEqualTo(DEFAULT_TOTAL_WEIGHT);
        assertThat(testVehicle.getDeliveryWeight()).isEqualTo(DEFAULT_DELIVERY_WEIGHT);
        assertThat(testVehicle.getNote()).isEqualTo(DEFAULT_NOTE);

        // Validate the Vehicle in Elasticsearch
        Vehicle vehicleEs = vehicleSearchRepository.findOne(testVehicle.getId());
        assertThat(vehicleEs).isEqualToComparingFieldByField(testVehicle);
    }

    @Test
    @Transactional
    public void createVehicleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vehicleRepository.findAll().size();

        // Create the Vehicle with an existing ID
        vehicle.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVehicleMockMvc.perform(post("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isBadRequest());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkGovNumberIsRequired() throws Exception {
        int databaseSizeBeforeTest = vehicleRepository.findAll().size();
        // set the field null
        vehicle.setGovNumber(null);

        // Create the Vehicle, which fails.

        restVehicleMockMvc.perform(post("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isBadRequest());

        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVehicles() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList
        restVehicleMockMvc.perform(get("/api/vehicles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].govNumber").value(hasItem(DEFAULT_GOV_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].techPassportNumber").value(hasItem(DEFAULT_TECH_PASSPORT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL.toString())))
            .andExpect(jsonPath("$.[*].buildYear").value(hasItem(DEFAULT_BUILD_YEAR)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].totalWeight").value(hasItem(DEFAULT_TOTAL_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryWeight").value(hasItem(DEFAULT_DELIVERY_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }

    @Test
    @Transactional
    public void getVehicle() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get the vehicle
        restVehicleMockMvc.perform(get("/api/vehicles/{id}", vehicle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vehicle.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.govNumber").value(DEFAULT_GOV_NUMBER.toString()))
            .andExpect(jsonPath("$.techPassportNumber").value(DEFAULT_TECH_PASSPORT_NUMBER.toString()))
            .andExpect(jsonPath("$.model").value(DEFAULT_MODEL.toString()))
            .andExpect(jsonPath("$.buildYear").value(DEFAULT_BUILD_YEAR))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.height").value(DEFAULT_HEIGHT.doubleValue()))
            .andExpect(jsonPath("$.length").value(DEFAULT_LENGTH.doubleValue()))
            .andExpect(jsonPath("$.width").value(DEFAULT_WIDTH.doubleValue()))
            .andExpect(jsonPath("$.totalWeight").value(DEFAULT_TOTAL_WEIGHT.doubleValue()))
            .andExpect(jsonPath("$.deliveryWeight").value(DEFAULT_DELIVERY_WEIGHT.intValue()))
            .andExpect(jsonPath("$.note").value(DEFAULT_NOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllVehiclesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where name equals to DEFAULT_NAME
        defaultVehicleShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the vehicleList where name equals to UPDATED_NAME
        defaultVehicleShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllVehiclesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where name in DEFAULT_NAME or UPDATED_NAME
        defaultVehicleShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the vehicleList where name equals to UPDATED_NAME
        defaultVehicleShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllVehiclesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where name is not null
        defaultVehicleShouldBeFound("name.specified=true");

        // Get all the vehicleList where name is null
        defaultVehicleShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByGovNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where govNumber equals to DEFAULT_GOV_NUMBER
        defaultVehicleShouldBeFound("govNumber.equals=" + DEFAULT_GOV_NUMBER);

        // Get all the vehicleList where govNumber equals to UPDATED_GOV_NUMBER
        defaultVehicleShouldNotBeFound("govNumber.equals=" + UPDATED_GOV_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVehiclesByGovNumberIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where govNumber in DEFAULT_GOV_NUMBER or UPDATED_GOV_NUMBER
        defaultVehicleShouldBeFound("govNumber.in=" + DEFAULT_GOV_NUMBER + "," + UPDATED_GOV_NUMBER);

        // Get all the vehicleList where govNumber equals to UPDATED_GOV_NUMBER
        defaultVehicleShouldNotBeFound("govNumber.in=" + UPDATED_GOV_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVehiclesByGovNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where govNumber is not null
        defaultVehicleShouldBeFound("govNumber.specified=true");

        // Get all the vehicleList where govNumber is null
        defaultVehicleShouldNotBeFound("govNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByTechPassportNumberIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where techPassportNumber equals to DEFAULT_TECH_PASSPORT_NUMBER
        defaultVehicleShouldBeFound("techPassportNumber.equals=" + DEFAULT_TECH_PASSPORT_NUMBER);

        // Get all the vehicleList where techPassportNumber equals to UPDATED_TECH_PASSPORT_NUMBER
        defaultVehicleShouldNotBeFound("techPassportNumber.equals=" + UPDATED_TECH_PASSPORT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVehiclesByTechPassportNumberIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where techPassportNumber in DEFAULT_TECH_PASSPORT_NUMBER or UPDATED_TECH_PASSPORT_NUMBER
        defaultVehicleShouldBeFound("techPassportNumber.in=" + DEFAULT_TECH_PASSPORT_NUMBER + "," + UPDATED_TECH_PASSPORT_NUMBER);

        // Get all the vehicleList where techPassportNumber equals to UPDATED_TECH_PASSPORT_NUMBER
        defaultVehicleShouldNotBeFound("techPassportNumber.in=" + UPDATED_TECH_PASSPORT_NUMBER);
    }

    @Test
    @Transactional
    public void getAllVehiclesByTechPassportNumberIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where techPassportNumber is not null
        defaultVehicleShouldBeFound("techPassportNumber.specified=true");

        // Get all the vehicleList where techPassportNumber is null
        defaultVehicleShouldNotBeFound("techPassportNumber.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByModelIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where model equals to DEFAULT_MODEL
        defaultVehicleShouldBeFound("model.equals=" + DEFAULT_MODEL);

        // Get all the vehicleList where model equals to UPDATED_MODEL
        defaultVehicleShouldNotBeFound("model.equals=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllVehiclesByModelIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where model in DEFAULT_MODEL or UPDATED_MODEL
        defaultVehicleShouldBeFound("model.in=" + DEFAULT_MODEL + "," + UPDATED_MODEL);

        // Get all the vehicleList where model equals to UPDATED_MODEL
        defaultVehicleShouldNotBeFound("model.in=" + UPDATED_MODEL);
    }

    @Test
    @Transactional
    public void getAllVehiclesByModelIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where model is not null
        defaultVehicleShouldBeFound("model.specified=true");

        // Get all the vehicleList where model is null
        defaultVehicleShouldNotBeFound("model.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByBuildYearIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where buildYear equals to DEFAULT_BUILD_YEAR
        defaultVehicleShouldBeFound("buildYear.equals=" + DEFAULT_BUILD_YEAR);

        // Get all the vehicleList where buildYear equals to UPDATED_BUILD_YEAR
        defaultVehicleShouldNotBeFound("buildYear.equals=" + UPDATED_BUILD_YEAR);
    }

    @Test
    @Transactional
    public void getAllVehiclesByBuildYearIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where buildYear in DEFAULT_BUILD_YEAR or UPDATED_BUILD_YEAR
        defaultVehicleShouldBeFound("buildYear.in=" + DEFAULT_BUILD_YEAR + "," + UPDATED_BUILD_YEAR);

        // Get all the vehicleList where buildYear equals to UPDATED_BUILD_YEAR
        defaultVehicleShouldNotBeFound("buildYear.in=" + UPDATED_BUILD_YEAR);
    }

    @Test
    @Transactional
    public void getAllVehiclesByBuildYearIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where buildYear is not null
        defaultVehicleShouldBeFound("buildYear.specified=true");

        // Get all the vehicleList where buildYear is null
        defaultVehicleShouldNotBeFound("buildYear.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByBuildYearIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where buildYear greater than or equals to DEFAULT_BUILD_YEAR
        defaultVehicleShouldBeFound("buildYear.greaterOrEqualThan=" + DEFAULT_BUILD_YEAR);

        // Get all the vehicleList where buildYear greater than or equals to UPDATED_BUILD_YEAR
        defaultVehicleShouldNotBeFound("buildYear.greaterOrEqualThan=" + UPDATED_BUILD_YEAR);
    }

    @Test
    @Transactional
    public void getAllVehiclesByBuildYearIsLessThanSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where buildYear less than or equals to DEFAULT_BUILD_YEAR
        defaultVehicleShouldNotBeFound("buildYear.lessThan=" + DEFAULT_BUILD_YEAR);

        // Get all the vehicleList where buildYear less than or equals to UPDATED_BUILD_YEAR
        defaultVehicleShouldBeFound("buildYear.lessThan=" + UPDATED_BUILD_YEAR);
    }


    @Test
    @Transactional
    public void getAllVehiclesByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where type equals to DEFAULT_TYPE
        defaultVehicleShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the vehicleList where type equals to UPDATED_TYPE
        defaultVehicleShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultVehicleShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the vehicleList where type equals to UPDATED_TYPE
        defaultVehicleShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where type is not null
        defaultVehicleShouldBeFound("type.specified=true");

        // Get all the vehicleList where type is null
        defaultVehicleShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByHeightIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where height equals to DEFAULT_HEIGHT
        defaultVehicleShouldBeFound("height.equals=" + DEFAULT_HEIGHT);

        // Get all the vehicleList where height equals to UPDATED_HEIGHT
        defaultVehicleShouldNotBeFound("height.equals=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByHeightIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where height in DEFAULT_HEIGHT or UPDATED_HEIGHT
        defaultVehicleShouldBeFound("height.in=" + DEFAULT_HEIGHT + "," + UPDATED_HEIGHT);

        // Get all the vehicleList where height equals to UPDATED_HEIGHT
        defaultVehicleShouldNotBeFound("height.in=" + UPDATED_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByHeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where height is not null
        defaultVehicleShouldBeFound("height.specified=true");

        // Get all the vehicleList where height is null
        defaultVehicleShouldNotBeFound("height.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByLengthIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where length equals to DEFAULT_LENGTH
        defaultVehicleShouldBeFound("length.equals=" + DEFAULT_LENGTH);

        // Get all the vehicleList where length equals to UPDATED_LENGTH
        defaultVehicleShouldNotBeFound("length.equals=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByLengthIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where length in DEFAULT_LENGTH or UPDATED_LENGTH
        defaultVehicleShouldBeFound("length.in=" + DEFAULT_LENGTH + "," + UPDATED_LENGTH);

        // Get all the vehicleList where length equals to UPDATED_LENGTH
        defaultVehicleShouldNotBeFound("length.in=" + UPDATED_LENGTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByLengthIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where length is not null
        defaultVehicleShouldBeFound("length.specified=true");

        // Get all the vehicleList where length is null
        defaultVehicleShouldNotBeFound("length.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByWidthIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where width equals to DEFAULT_WIDTH
        defaultVehicleShouldBeFound("width.equals=" + DEFAULT_WIDTH);

        // Get all the vehicleList where width equals to UPDATED_WIDTH
        defaultVehicleShouldNotBeFound("width.equals=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByWidthIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where width in DEFAULT_WIDTH or UPDATED_WIDTH
        defaultVehicleShouldBeFound("width.in=" + DEFAULT_WIDTH + "," + UPDATED_WIDTH);

        // Get all the vehicleList where width equals to UPDATED_WIDTH
        defaultVehicleShouldNotBeFound("width.in=" + UPDATED_WIDTH);
    }

    @Test
    @Transactional
    public void getAllVehiclesByWidthIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where width is not null
        defaultVehicleShouldBeFound("width.specified=true");

        // Get all the vehicleList where width is null
        defaultVehicleShouldNotBeFound("width.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByTotalWeightIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where totalWeight equals to DEFAULT_TOTAL_WEIGHT
        defaultVehicleShouldBeFound("totalWeight.equals=" + DEFAULT_TOTAL_WEIGHT);

        // Get all the vehicleList where totalWeight equals to UPDATED_TOTAL_WEIGHT
        defaultVehicleShouldNotBeFound("totalWeight.equals=" + UPDATED_TOTAL_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByTotalWeightIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where totalWeight in DEFAULT_TOTAL_WEIGHT or UPDATED_TOTAL_WEIGHT
        defaultVehicleShouldBeFound("totalWeight.in=" + DEFAULT_TOTAL_WEIGHT + "," + UPDATED_TOTAL_WEIGHT);

        // Get all the vehicleList where totalWeight equals to UPDATED_TOTAL_WEIGHT
        defaultVehicleShouldNotBeFound("totalWeight.in=" + UPDATED_TOTAL_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByTotalWeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where totalWeight is not null
        defaultVehicleShouldBeFound("totalWeight.specified=true");

        // Get all the vehicleList where totalWeight is null
        defaultVehicleShouldNotBeFound("totalWeight.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByDeliveryWeightIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where deliveryWeight equals to DEFAULT_DELIVERY_WEIGHT
        defaultVehicleShouldBeFound("deliveryWeight.equals=" + DEFAULT_DELIVERY_WEIGHT);

        // Get all the vehicleList where deliveryWeight equals to UPDATED_DELIVERY_WEIGHT
        defaultVehicleShouldNotBeFound("deliveryWeight.equals=" + UPDATED_DELIVERY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByDeliveryWeightIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where deliveryWeight in DEFAULT_DELIVERY_WEIGHT or UPDATED_DELIVERY_WEIGHT
        defaultVehicleShouldBeFound("deliveryWeight.in=" + DEFAULT_DELIVERY_WEIGHT + "," + UPDATED_DELIVERY_WEIGHT);

        // Get all the vehicleList where deliveryWeight equals to UPDATED_DELIVERY_WEIGHT
        defaultVehicleShouldNotBeFound("deliveryWeight.in=" + UPDATED_DELIVERY_WEIGHT);
    }

    @Test
    @Transactional
    public void getAllVehiclesByDeliveryWeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where deliveryWeight is not null
        defaultVehicleShouldBeFound("deliveryWeight.specified=true");

        // Get all the vehicleList where deliveryWeight is null
        defaultVehicleShouldNotBeFound("deliveryWeight.specified=false");
    }

    @Test
    @Transactional
    public void getAllVehiclesByNoteIsEqualToSomething() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where note equals to DEFAULT_NOTE
        defaultVehicleShouldBeFound("note.equals=" + DEFAULT_NOTE);

        // Get all the vehicleList where note equals to UPDATED_NOTE
        defaultVehicleShouldNotBeFound("note.equals=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByNoteIsInShouldWork() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where note in DEFAULT_NOTE or UPDATED_NOTE
        defaultVehicleShouldBeFound("note.in=" + DEFAULT_NOTE + "," + UPDATED_NOTE);

        // Get all the vehicleList where note equals to UPDATED_NOTE
        defaultVehicleShouldNotBeFound("note.in=" + UPDATED_NOTE);
    }

    @Test
    @Transactional
    public void getAllVehiclesByNoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        vehicleRepository.saveAndFlush(vehicle);

        // Get all the vehicleList where note is not null
        defaultVehicleShouldBeFound("note.specified=true");

        // Get all the vehicleList where note is null
        defaultVehicleShouldNotBeFound("note.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultVehicleShouldBeFound(String filter) throws Exception {
        restVehicleMockMvc.perform(get("/api/vehicles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].govNumber").value(hasItem(DEFAULT_GOV_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].techPassportNumber").value(hasItem(DEFAULT_TECH_PASSPORT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL.toString())))
            .andExpect(jsonPath("$.[*].buildYear").value(hasItem(DEFAULT_BUILD_YEAR)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].totalWeight").value(hasItem(DEFAULT_TOTAL_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryWeight").value(hasItem(DEFAULT_DELIVERY_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultVehicleShouldNotBeFound(String filter) throws Exception {
        restVehicleMockMvc.perform(get("/api/vehicles?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingVehicle() throws Exception {
        // Get the vehicle
        restVehicleMockMvc.perform(get("/api/vehicles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVehicle() throws Exception {
        // Initialize the database
        vehicleService.save(vehicle);

        int databaseSizeBeforeUpdate = vehicleRepository.findAll().size();

        // Update the vehicle
        Vehicle updatedVehicle = vehicleRepository.findOne(vehicle.getId());
        updatedVehicle
            .name(UPDATED_NAME)
            .govNumber(UPDATED_GOV_NUMBER)
            .techPassportNumber(UPDATED_TECH_PASSPORT_NUMBER)
            .model(UPDATED_MODEL)
            .buildYear(UPDATED_BUILD_YEAR)
            .type(UPDATED_TYPE)
            .height(UPDATED_HEIGHT)
            .length(UPDATED_LENGTH)
            .width(UPDATED_WIDTH)
            .totalWeight(UPDATED_TOTAL_WEIGHT)
            .deliveryWeight(UPDATED_DELIVERY_WEIGHT)
            .note(UPDATED_NOTE);

        restVehicleMockMvc.perform(put("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVehicle)))
            .andExpect(status().isOk());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeUpdate);
        Vehicle testVehicle = vehicleList.get(vehicleList.size() - 1);
        assertThat(testVehicle.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVehicle.getGovNumber()).isEqualTo(UPDATED_GOV_NUMBER);
        assertThat(testVehicle.getTechPassportNumber()).isEqualTo(UPDATED_TECH_PASSPORT_NUMBER);
        assertThat(testVehicle.getModel()).isEqualTo(UPDATED_MODEL);
        assertThat(testVehicle.getBuildYear()).isEqualTo(UPDATED_BUILD_YEAR);
        assertThat(testVehicle.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testVehicle.getHeight()).isEqualTo(UPDATED_HEIGHT);
        assertThat(testVehicle.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testVehicle.getWidth()).isEqualTo(UPDATED_WIDTH);
        assertThat(testVehicle.getTotalWeight()).isEqualTo(UPDATED_TOTAL_WEIGHT);
        assertThat(testVehicle.getDeliveryWeight()).isEqualTo(UPDATED_DELIVERY_WEIGHT);
        assertThat(testVehicle.getNote()).isEqualTo(UPDATED_NOTE);

        // Validate the Vehicle in Elasticsearch
        Vehicle vehicleEs = vehicleSearchRepository.findOne(testVehicle.getId());
        assertThat(vehicleEs).isEqualToComparingFieldByField(testVehicle);
    }

    @Test
    @Transactional
    public void updateNonExistingVehicle() throws Exception {
        int databaseSizeBeforeUpdate = vehicleRepository.findAll().size();

        // Create the Vehicle

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVehicleMockMvc.perform(put("/api/vehicles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vehicle)))
            .andExpect(status().isCreated());

        // Validate the Vehicle in the database
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVehicle() throws Exception {
        // Initialize the database
        vehicleService.save(vehicle);

        int databaseSizeBeforeDelete = vehicleRepository.findAll().size();

        // Get the vehicle
        restVehicleMockMvc.perform(delete("/api/vehicles/{id}", vehicle.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean vehicleExistsInEs = vehicleSearchRepository.exists(vehicle.getId());
        assertThat(vehicleExistsInEs).isFalse();

        // Validate the database is empty
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        assertThat(vehicleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchVehicle() throws Exception {
        // Initialize the database
        vehicleService.save(vehicle);

        // Search the vehicle
        restVehicleMockMvc.perform(get("/api/_search/vehicles?query=id:" + vehicle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vehicle.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].govNumber").value(hasItem(DEFAULT_GOV_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].techPassportNumber").value(hasItem(DEFAULT_TECH_PASSPORT_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].model").value(hasItem(DEFAULT_MODEL.toString())))
            .andExpect(jsonPath("$.[*].buildYear").value(hasItem(DEFAULT_BUILD_YEAR)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].height").value(hasItem(DEFAULT_HEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.doubleValue())))
            .andExpect(jsonPath("$.[*].width").value(hasItem(DEFAULT_WIDTH.doubleValue())))
            .andExpect(jsonPath("$.[*].totalWeight").value(hasItem(DEFAULT_TOTAL_WEIGHT.doubleValue())))
            .andExpect(jsonPath("$.[*].deliveryWeight").value(hasItem(DEFAULT_DELIVERY_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].note").value(hasItem(DEFAULT_NOTE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Vehicle.class);
        Vehicle vehicle1 = new Vehicle();
        vehicle1.setId(1L);
        Vehicle vehicle2 = new Vehicle();
        vehicle2.setId(vehicle1.getId());
        assertThat(vehicle1).isEqualTo(vehicle2);
        vehicle2.setId(2L);
        assertThat(vehicle1).isNotEqualTo(vehicle2);
        vehicle1.setId(null);
        assertThat(vehicle1).isNotEqualTo(vehicle2);
    }
}
