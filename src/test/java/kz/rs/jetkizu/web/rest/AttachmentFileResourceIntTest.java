package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.AttachmentFile;
import kz.rs.jetkizu.repository.AttachmentFileRepository;
import kz.rs.jetkizu.service.AttachmentFileService;
import kz.rs.jetkizu.repository.search.AttachmentFileSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AttachmentFileResource REST controller.
 *
 * @see AttachmentFileResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class AttachmentFileResourceIntTest {

    private static final String DEFAULT_UID = "AAAAAAAAAA";
    private static final String UPDATED_UID = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FILE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT_TYPE = "BBBBBBBBBB";

    private static final Long DEFAULT_FILE_SIZE = 1L;
    private static final Long UPDATED_FILE_SIZE = 2L;

    private static final String DEFAULT_FILE_DESC = "AAAAAAAAAA";
    private static final String UPDATED_FILE_DESC = "BBBBBBBBBB";

    private static final String DEFAULT_FILE_HASH = "AAAAAAAAAA";
    private static final String UPDATED_FILE_HASH = "BBBBBBBBBB";

    private static final String DEFAULT_CONTAINER_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_CONTAINER_CLASS = "BBBBBBBBBB";

    private static final String DEFAULT_CONTAINER_ID = "AAAAAAAAAA";
    private static final String UPDATED_CONTAINER_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DOCUMENT_CATEGORY = "AAAAAAAAAA";
    private static final String UPDATED_DOCUMENT_CATEGORY = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_UPLOAD_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPLOAD_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final byte[] DEFAULT_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_FILE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_FILE_CONTENT_TYPE = "image/png";

    private static final byte[] DEFAULT_IMAGE_FILE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE_FILE = TestUtil.createByteArray(2, "1");
    private static final String DEFAULT_IMAGE_FILE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_FILE_CONTENT_TYPE = "image/png";

    @Autowired
    private AttachmentFileRepository attachmentFileRepository;

    @Autowired
    private AttachmentFileService attachmentFileService;

    @Autowired
    private AttachmentFileSearchRepository attachmentFileSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAttachmentFileMockMvc;

    private AttachmentFile attachmentFile;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AttachmentFileResource attachmentFileResource = new AttachmentFileResource(attachmentFileService);
        this.restAttachmentFileMockMvc = MockMvcBuilders.standaloneSetup(attachmentFileResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AttachmentFile createEntity(EntityManager em) {
        AttachmentFile attachmentFile = new AttachmentFile()
            .uid(DEFAULT_UID)
            .fileName(DEFAULT_FILE_NAME)
            .contentType(DEFAULT_CONTENT_TYPE)
            .fileSize(DEFAULT_FILE_SIZE)
            .fileDesc(DEFAULT_FILE_DESC)
            .fileHash(DEFAULT_FILE_HASH)
            .containerClass(DEFAULT_CONTAINER_CLASS)
            .containerId(DEFAULT_CONTAINER_ID)
            .documentCategory(DEFAULT_DOCUMENT_CATEGORY)
            .uploadDateTime(DEFAULT_UPLOAD_DATE_TIME)
            .file(DEFAULT_FILE)
            .fileContentType(DEFAULT_FILE_CONTENT_TYPE)
            .imageFile(DEFAULT_IMAGE_FILE)
            .imageFileContentType(DEFAULT_IMAGE_FILE_CONTENT_TYPE);
        return attachmentFile;
    }

    @Before
    public void initTest() {
        attachmentFileSearchRepository.deleteAll();
        attachmentFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createAttachmentFile() throws Exception {
        int databaseSizeBeforeCreate = attachmentFileRepository.findAll().size();

        // Create the AttachmentFile
        restAttachmentFileMockMvc.perform(post("/api/attachment-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentFile)))
            .andExpect(status().isCreated());

        // Validate the AttachmentFile in the database
        List<AttachmentFile> attachmentFileList = attachmentFileRepository.findAll();
        assertThat(attachmentFileList).hasSize(databaseSizeBeforeCreate + 1);
        AttachmentFile testAttachmentFile = attachmentFileList.get(attachmentFileList.size() - 1);
        assertThat(testAttachmentFile.getUid()).isEqualTo(DEFAULT_UID);
        assertThat(testAttachmentFile.getFileName()).isEqualTo(DEFAULT_FILE_NAME);
        assertThat(testAttachmentFile.getContentType()).isEqualTo(DEFAULT_CONTENT_TYPE);
        assertThat(testAttachmentFile.getFileSize()).isEqualTo(DEFAULT_FILE_SIZE);
        assertThat(testAttachmentFile.getFileDesc()).isEqualTo(DEFAULT_FILE_DESC);
        assertThat(testAttachmentFile.getFileHash()).isEqualTo(DEFAULT_FILE_HASH);
        assertThat(testAttachmentFile.getContainerClass()).isEqualTo(DEFAULT_CONTAINER_CLASS);
        assertThat(testAttachmentFile.getContainerId()).isEqualTo(DEFAULT_CONTAINER_ID);
        assertThat(testAttachmentFile.getDocumentCategory()).isEqualTo(DEFAULT_DOCUMENT_CATEGORY);
        assertThat(testAttachmentFile.getUploadDateTime()).isEqualTo(DEFAULT_UPLOAD_DATE_TIME);
        assertThat(testAttachmentFile.getFile()).isEqualTo(DEFAULT_FILE);
        assertThat(testAttachmentFile.getFileContentType()).isEqualTo(DEFAULT_FILE_CONTENT_TYPE);
        assertThat(testAttachmentFile.getImageFile()).isEqualTo(DEFAULT_IMAGE_FILE);
        assertThat(testAttachmentFile.getImageFileContentType()).isEqualTo(DEFAULT_IMAGE_FILE_CONTENT_TYPE);

        // Validate the AttachmentFile in Elasticsearch
        AttachmentFile attachmentFileEs = attachmentFileSearchRepository.findOne(testAttachmentFile.getId());
        assertThat(attachmentFileEs).isEqualToComparingFieldByField(testAttachmentFile);
    }

    @Test
    @Transactional
    public void createAttachmentFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = attachmentFileRepository.findAll().size();

        // Create the AttachmentFile with an existing ID
        attachmentFile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAttachmentFileMockMvc.perform(post("/api/attachment-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentFile)))
            .andExpect(status().isBadRequest());

        // Validate the AttachmentFile in the database
        List<AttachmentFile> attachmentFileList = attachmentFileRepository.findAll();
        assertThat(attachmentFileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAttachmentFiles() throws Exception {
        // Initialize the database
        attachmentFileRepository.saveAndFlush(attachmentFile);

        // Get all the attachmentFileList
        restAttachmentFileMockMvc.perform(get("/api/attachment-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(attachmentFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].contentType").value(hasItem(DEFAULT_CONTENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].fileSize").value(hasItem(DEFAULT_FILE_SIZE.intValue())))
            .andExpect(jsonPath("$.[*].fileDesc").value(hasItem(DEFAULT_FILE_DESC.toString())))
            .andExpect(jsonPath("$.[*].fileHash").value(hasItem(DEFAULT_FILE_HASH.toString())))
            .andExpect(jsonPath("$.[*].containerClass").value(hasItem(DEFAULT_CONTAINER_CLASS.toString())))
            .andExpect(jsonPath("$.[*].containerId").value(hasItem(DEFAULT_CONTAINER_ID.toString())))
            .andExpect(jsonPath("$.[*].documentCategory").value(hasItem(DEFAULT_DOCUMENT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].uploadDateTime").value(hasItem(sameInstant(DEFAULT_UPLOAD_DATE_TIME))))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].imageFileContentType").value(hasItem(DEFAULT_IMAGE_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageFile").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_FILE))));
    }

    @Test
    @Transactional
    public void getAttachmentFile() throws Exception {
        // Initialize the database
        attachmentFileRepository.saveAndFlush(attachmentFile);

        // Get the attachmentFile
        restAttachmentFileMockMvc.perform(get("/api/attachment-files/{id}", attachmentFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(attachmentFile.getId().intValue()))
            .andExpect(jsonPath("$.uid").value(DEFAULT_UID.toString()))
            .andExpect(jsonPath("$.fileName").value(DEFAULT_FILE_NAME.toString()))
            .andExpect(jsonPath("$.contentType").value(DEFAULT_CONTENT_TYPE.toString()))
            .andExpect(jsonPath("$.fileSize").value(DEFAULT_FILE_SIZE.intValue()))
            .andExpect(jsonPath("$.fileDesc").value(DEFAULT_FILE_DESC.toString()))
            .andExpect(jsonPath("$.fileHash").value(DEFAULT_FILE_HASH.toString()))
            .andExpect(jsonPath("$.containerClass").value(DEFAULT_CONTAINER_CLASS.toString()))
            .andExpect(jsonPath("$.containerId").value(DEFAULT_CONTAINER_ID.toString()))
            .andExpect(jsonPath("$.documentCategory").value(DEFAULT_DOCUMENT_CATEGORY.toString()))
            .andExpect(jsonPath("$.uploadDateTime").value(sameInstant(DEFAULT_UPLOAD_DATE_TIME)))
            .andExpect(jsonPath("$.fileContentType").value(DEFAULT_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.file").value(Base64Utils.encodeToString(DEFAULT_FILE)))
            .andExpect(jsonPath("$.imageFileContentType").value(DEFAULT_IMAGE_FILE_CONTENT_TYPE))
            .andExpect(jsonPath("$.imageFile").value(Base64Utils.encodeToString(DEFAULT_IMAGE_FILE)));
    }

    @Test
    @Transactional
    public void getNonExistingAttachmentFile() throws Exception {
        // Get the attachmentFile
        restAttachmentFileMockMvc.perform(get("/api/attachment-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAttachmentFile() throws Exception {
        // Initialize the database
        attachmentFileService.save(attachmentFile);

        int databaseSizeBeforeUpdate = attachmentFileRepository.findAll().size();

        // Update the attachmentFile
        AttachmentFile updatedAttachmentFile = attachmentFileRepository.findOne(attachmentFile.getId());
        updatedAttachmentFile
            .uid(UPDATED_UID)
            .fileName(UPDATED_FILE_NAME)
            .contentType(UPDATED_CONTENT_TYPE)
            .fileSize(UPDATED_FILE_SIZE)
            .fileDesc(UPDATED_FILE_DESC)
            .fileHash(UPDATED_FILE_HASH)
            .containerClass(UPDATED_CONTAINER_CLASS)
            .containerId(UPDATED_CONTAINER_ID)
            .documentCategory(UPDATED_DOCUMENT_CATEGORY)
            .uploadDateTime(UPDATED_UPLOAD_DATE_TIME)
            .file(UPDATED_FILE)
            .fileContentType(UPDATED_FILE_CONTENT_TYPE)
            .imageFile(UPDATED_IMAGE_FILE)
            .imageFileContentType(UPDATED_IMAGE_FILE_CONTENT_TYPE);

        restAttachmentFileMockMvc.perform(put("/api/attachment-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAttachmentFile)))
            .andExpect(status().isOk());

        // Validate the AttachmentFile in the database
        List<AttachmentFile> attachmentFileList = attachmentFileRepository.findAll();
        assertThat(attachmentFileList).hasSize(databaseSizeBeforeUpdate);
        AttachmentFile testAttachmentFile = attachmentFileList.get(attachmentFileList.size() - 1);
        assertThat(testAttachmentFile.getUid()).isEqualTo(UPDATED_UID);
        assertThat(testAttachmentFile.getFileName()).isEqualTo(UPDATED_FILE_NAME);
        assertThat(testAttachmentFile.getContentType()).isEqualTo(UPDATED_CONTENT_TYPE);
        assertThat(testAttachmentFile.getFileSize()).isEqualTo(UPDATED_FILE_SIZE);
        assertThat(testAttachmentFile.getFileDesc()).isEqualTo(UPDATED_FILE_DESC);
        assertThat(testAttachmentFile.getFileHash()).isEqualTo(UPDATED_FILE_HASH);
        assertThat(testAttachmentFile.getContainerClass()).isEqualTo(UPDATED_CONTAINER_CLASS);
        assertThat(testAttachmentFile.getContainerId()).isEqualTo(UPDATED_CONTAINER_ID);
        assertThat(testAttachmentFile.getDocumentCategory()).isEqualTo(UPDATED_DOCUMENT_CATEGORY);
        assertThat(testAttachmentFile.getUploadDateTime()).isEqualTo(UPDATED_UPLOAD_DATE_TIME);
        assertThat(testAttachmentFile.getFile()).isEqualTo(UPDATED_FILE);
        assertThat(testAttachmentFile.getFileContentType()).isEqualTo(UPDATED_FILE_CONTENT_TYPE);
        assertThat(testAttachmentFile.getImageFile()).isEqualTo(UPDATED_IMAGE_FILE);
        assertThat(testAttachmentFile.getImageFileContentType()).isEqualTo(UPDATED_IMAGE_FILE_CONTENT_TYPE);

        // Validate the AttachmentFile in Elasticsearch
        AttachmentFile attachmentFileEs = attachmentFileSearchRepository.findOne(testAttachmentFile.getId());
        assertThat(attachmentFileEs).isEqualToComparingFieldByField(testAttachmentFile);
    }

    @Test
    @Transactional
    public void updateNonExistingAttachmentFile() throws Exception {
        int databaseSizeBeforeUpdate = attachmentFileRepository.findAll().size();

        // Create the AttachmentFile

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAttachmentFileMockMvc.perform(put("/api/attachment-files")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(attachmentFile)))
            .andExpect(status().isCreated());

        // Validate the AttachmentFile in the database
        List<AttachmentFile> attachmentFileList = attachmentFileRepository.findAll();
        assertThat(attachmentFileList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAttachmentFile() throws Exception {
        // Initialize the database
        attachmentFileService.save(attachmentFile);

        int databaseSizeBeforeDelete = attachmentFileRepository.findAll().size();

        // Get the attachmentFile
        restAttachmentFileMockMvc.perform(delete("/api/attachment-files/{id}", attachmentFile.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean attachmentFileExistsInEs = attachmentFileSearchRepository.exists(attachmentFile.getId());
        assertThat(attachmentFileExistsInEs).isFalse();

        // Validate the database is empty
        List<AttachmentFile> attachmentFileList = attachmentFileRepository.findAll();
        assertThat(attachmentFileList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAttachmentFile() throws Exception {
        // Initialize the database
        attachmentFileService.save(attachmentFile);

        // Search the attachmentFile
        restAttachmentFileMockMvc.perform(get("/api/_search/attachment-files?query=id:" + attachmentFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(attachmentFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].uid").value(hasItem(DEFAULT_UID.toString())))
            .andExpect(jsonPath("$.[*].fileName").value(hasItem(DEFAULT_FILE_NAME.toString())))
            .andExpect(jsonPath("$.[*].contentType").value(hasItem(DEFAULT_CONTENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].fileSize").value(hasItem(DEFAULT_FILE_SIZE.intValue())))
            .andExpect(jsonPath("$.[*].fileDesc").value(hasItem(DEFAULT_FILE_DESC.toString())))
            .andExpect(jsonPath("$.[*].fileHash").value(hasItem(DEFAULT_FILE_HASH.toString())))
            .andExpect(jsonPath("$.[*].containerClass").value(hasItem(DEFAULT_CONTAINER_CLASS.toString())))
            .andExpect(jsonPath("$.[*].containerId").value(hasItem(DEFAULT_CONTAINER_ID.toString())))
            .andExpect(jsonPath("$.[*].documentCategory").value(hasItem(DEFAULT_DOCUMENT_CATEGORY.toString())))
            .andExpect(jsonPath("$.[*].uploadDateTime").value(hasItem(sameInstant(DEFAULT_UPLOAD_DATE_TIME))))
            .andExpect(jsonPath("$.[*].fileContentType").value(hasItem(DEFAULT_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].file").value(hasItem(Base64Utils.encodeToString(DEFAULT_FILE))))
            .andExpect(jsonPath("$.[*].imageFileContentType").value(hasItem(DEFAULT_IMAGE_FILE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].imageFile").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE_FILE))));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AttachmentFile.class);
        AttachmentFile attachmentFile1 = new AttachmentFile();
        attachmentFile1.setId(1L);
        AttachmentFile attachmentFile2 = new AttachmentFile();
        attachmentFile2.setId(attachmentFile1.getId());
        assertThat(attachmentFile1).isEqualTo(attachmentFile2);
        attachmentFile2.setId(2L);
        assertThat(attachmentFile1).isNotEqualTo(attachmentFile2);
        attachmentFile1.setId(null);
        assertThat(attachmentFile1).isNotEqualTo(attachmentFile2);
    }
}
