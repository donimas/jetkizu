package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.Jetkizu;
import kz.rs.jetkizu.repository.JetkizuRepository;
import kz.rs.jetkizu.service.JetkizuService;
import kz.rs.jetkizu.repository.search.JetkizuSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JetkizuResource REST controller.
 *
 * @see JetkizuResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class JetkizuResourceIntTest {

    private static final BigDecimal DEFAULT_WEIGHT = new BigDecimal(1);
    private static final BigDecimal UPDATED_WEIGHT = new BigDecimal(2);

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private JetkizuRepository jetkizuRepository;

    @Autowired
    private JetkizuService jetkizuService;

    @Autowired
    private JetkizuSearchRepository jetkizuSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJetkizuMockMvc;

    private Jetkizu jetkizu;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JetkizuResource jetkizuResource = new JetkizuResource(jetkizuService);
        this.restJetkizuMockMvc = MockMvcBuilders.standaloneSetup(jetkizuResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Jetkizu createEntity(EntityManager em) {
        Jetkizu jetkizu = new Jetkizu()
            .weight(DEFAULT_WEIGHT)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .updateDateTime(DEFAULT_UPDATE_DATE_TIME)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return jetkizu;
    }

    @Before
    public void initTest() {
        jetkizuSearchRepository.deleteAll();
        jetkizu = createEntity(em);
    }

    @Test
    @Transactional
    public void createJetkizu() throws Exception {
        int databaseSizeBeforeCreate = jetkizuRepository.findAll().size();

        // Create the Jetkizu
        restJetkizuMockMvc.perform(post("/api/jetkizus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizu)))
            .andExpect(status().isCreated());

        // Validate the Jetkizu in the database
        List<Jetkizu> jetkizuList = jetkizuRepository.findAll();
        assertThat(jetkizuList).hasSize(databaseSizeBeforeCreate + 1);
        Jetkizu testJetkizu = jetkizuList.get(jetkizuList.size() - 1);
        assertThat(testJetkizu.getWeight()).isEqualTo(DEFAULT_WEIGHT);
        assertThat(testJetkizu.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testJetkizu.getUpdateDateTime()).isEqualTo(DEFAULT_UPDATE_DATE_TIME);
        assertThat(testJetkizu.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testJetkizu.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testJetkizu.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the Jetkizu in Elasticsearch
        Jetkizu jetkizuEs = jetkizuSearchRepository.findOne(testJetkizu.getId());
        assertThat(jetkizuEs).isEqualToComparingFieldByField(testJetkizu);
    }

    @Test
    @Transactional
    public void createJetkizuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jetkizuRepository.findAll().size();

        // Create the Jetkizu with an existing ID
        jetkizu.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJetkizuMockMvc.perform(post("/api/jetkizus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizu)))
            .andExpect(status().isBadRequest());

        // Validate the Jetkizu in the database
        List<Jetkizu> jetkizuList = jetkizuRepository.findAll();
        assertThat(jetkizuList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllJetkizus() throws Exception {
        // Initialize the database
        jetkizuRepository.saveAndFlush(jetkizu);

        // Get all the jetkizuList
        restJetkizuMockMvc.perform(get("/api/jetkizus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizu.getId().intValue())))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].updateDateTime").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getJetkizu() throws Exception {
        // Initialize the database
        jetkizuRepository.saveAndFlush(jetkizu);

        // Get the jetkizu
        restJetkizuMockMvc.perform(get("/api/jetkizus/{id}", jetkizu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jetkizu.getId().intValue()))
            .andExpect(jsonPath("$.weight").value(DEFAULT_WEIGHT.intValue()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.updateDateTime").value(sameInstant(DEFAULT_UPDATE_DATE_TIME)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJetkizu() throws Exception {
        // Get the jetkizu
        restJetkizuMockMvc.perform(get("/api/jetkizus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJetkizu() throws Exception {
        // Initialize the database
        jetkizuService.save(jetkizu);

        int databaseSizeBeforeUpdate = jetkizuRepository.findAll().size();

        // Update the jetkizu
        Jetkizu updatedJetkizu = jetkizuRepository.findOne(jetkizu.getId());
        updatedJetkizu
            .weight(UPDATED_WEIGHT)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .updateDateTime(UPDATED_UPDATE_DATE_TIME)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restJetkizuMockMvc.perform(put("/api/jetkizus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJetkizu)))
            .andExpect(status().isOk());

        // Validate the Jetkizu in the database
        List<Jetkizu> jetkizuList = jetkizuRepository.findAll();
        assertThat(jetkizuList).hasSize(databaseSizeBeforeUpdate);
        Jetkizu testJetkizu = jetkizuList.get(jetkizuList.size() - 1);
        assertThat(testJetkizu.getWeight()).isEqualTo(UPDATED_WEIGHT);
        assertThat(testJetkizu.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testJetkizu.getUpdateDateTime()).isEqualTo(UPDATED_UPDATE_DATE_TIME);
        assertThat(testJetkizu.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testJetkizu.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testJetkizu.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the Jetkizu in Elasticsearch
        Jetkizu jetkizuEs = jetkizuSearchRepository.findOne(testJetkizu.getId());
        assertThat(jetkizuEs).isEqualToComparingFieldByField(testJetkizu);
    }

    @Test
    @Transactional
    public void updateNonExistingJetkizu() throws Exception {
        int databaseSizeBeforeUpdate = jetkizuRepository.findAll().size();

        // Create the Jetkizu

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJetkizuMockMvc.perform(put("/api/jetkizus")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizu)))
            .andExpect(status().isCreated());

        // Validate the Jetkizu in the database
        List<Jetkizu> jetkizuList = jetkizuRepository.findAll();
        assertThat(jetkizuList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJetkizu() throws Exception {
        // Initialize the database
        jetkizuService.save(jetkizu);

        int databaseSizeBeforeDelete = jetkizuRepository.findAll().size();

        // Get the jetkizu
        restJetkizuMockMvc.perform(delete("/api/jetkizus/{id}", jetkizu.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean jetkizuExistsInEs = jetkizuSearchRepository.exists(jetkizu.getId());
        assertThat(jetkizuExistsInEs).isFalse();

        // Validate the database is empty
        List<Jetkizu> jetkizuList = jetkizuRepository.findAll();
        assertThat(jetkizuList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJetkizu() throws Exception {
        // Initialize the database
        jetkizuService.save(jetkizu);

        // Search the jetkizu
        restJetkizuMockMvc.perform(get("/api/_search/jetkizus?query=id:" + jetkizu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizu.getId().intValue())))
            .andExpect(jsonPath("$.[*].weight").value(hasItem(DEFAULT_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].updateDateTime").value(hasItem(sameInstant(DEFAULT_UPDATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Jetkizu.class);
        Jetkizu jetkizu1 = new Jetkizu();
        jetkizu1.setId(1L);
        Jetkizu jetkizu2 = new Jetkizu();
        jetkizu2.setId(jetkizu1.getId());
        assertThat(jetkizu1).isEqualTo(jetkizu2);
        jetkizu2.setId(2L);
        assertThat(jetkizu1).isNotEqualTo(jetkizu2);
        jetkizu1.setId(null);
        assertThat(jetkizu1).isNotEqualTo(jetkizu2);
    }
}
