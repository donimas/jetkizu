package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.RouteBranchStatusHistory;
import kz.rs.jetkizu.repository.RouteBranchStatusHistoryRepository;
import kz.rs.jetkizu.service.RouteBranchStatusHistoryService;
import kz.rs.jetkizu.repository.search.RouteBranchStatusHistorySearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.rs.jetkizu.domain.enumeration.RouteBranchStatus;
/**
 * Test class for the RouteBranchStatusHistoryResource REST controller.
 *
 * @see RouteBranchStatusHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class RouteBranchStatusHistoryResourceIntTest {

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final RouteBranchStatus DEFAULT_STATUS = RouteBranchStatus.DRAFT;
    private static final RouteBranchStatus UPDATED_STATUS = RouteBranchStatus.IN_PROCESS;

    @Autowired
    private RouteBranchStatusHistoryRepository routeBranchStatusHistoryRepository;

    @Autowired
    private RouteBranchStatusHistoryService routeBranchStatusHistoryService;

    @Autowired
    private RouteBranchStatusHistorySearchRepository routeBranchStatusHistorySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRouteBranchStatusHistoryMockMvc;

    private RouteBranchStatusHistory routeBranchStatusHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RouteBranchStatusHistoryResource routeBranchStatusHistoryResource = new RouteBranchStatusHistoryResource(routeBranchStatusHistoryService);
        this.restRouteBranchStatusHistoryMockMvc = MockMvcBuilders.standaloneSetup(routeBranchStatusHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RouteBranchStatusHistory createEntity(EntityManager em) {
        RouteBranchStatusHistory routeBranchStatusHistory = new RouteBranchStatusHistory()
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .status(DEFAULT_STATUS);
        return routeBranchStatusHistory;
    }

    @Before
    public void initTest() {
        routeBranchStatusHistorySearchRepository.deleteAll();
        routeBranchStatusHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createRouteBranchStatusHistory() throws Exception {
        int databaseSizeBeforeCreate = routeBranchStatusHistoryRepository.findAll().size();

        // Create the RouteBranchStatusHistory
        restRouteBranchStatusHistoryMockMvc.perform(post("/api/route-branch-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranchStatusHistory)))
            .andExpect(status().isCreated());

        // Validate the RouteBranchStatusHistory in the database
        List<RouteBranchStatusHistory> routeBranchStatusHistoryList = routeBranchStatusHistoryRepository.findAll();
        assertThat(routeBranchStatusHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        RouteBranchStatusHistory testRouteBranchStatusHistory = routeBranchStatusHistoryList.get(routeBranchStatusHistoryList.size() - 1);
        assertThat(testRouteBranchStatusHistory.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testRouteBranchStatusHistory.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testRouteBranchStatusHistory.getStatus()).isEqualTo(DEFAULT_STATUS);

        // Validate the RouteBranchStatusHistory in Elasticsearch
        RouteBranchStatusHistory routeBranchStatusHistoryEs = routeBranchStatusHistorySearchRepository.findOne(testRouteBranchStatusHistory.getId());
        assertThat(routeBranchStatusHistoryEs).isEqualToComparingFieldByField(testRouteBranchStatusHistory);
    }

    @Test
    @Transactional
    public void createRouteBranchStatusHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = routeBranchStatusHistoryRepository.findAll().size();

        // Create the RouteBranchStatusHistory with an existing ID
        routeBranchStatusHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRouteBranchStatusHistoryMockMvc.perform(post("/api/route-branch-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranchStatusHistory)))
            .andExpect(status().isBadRequest());

        // Validate the RouteBranchStatusHistory in the database
        List<RouteBranchStatusHistory> routeBranchStatusHistoryList = routeBranchStatusHistoryRepository.findAll();
        assertThat(routeBranchStatusHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = routeBranchStatusHistoryRepository.findAll().size();
        // set the field null
        routeBranchStatusHistory.setStatus(null);

        // Create the RouteBranchStatusHistory, which fails.

        restRouteBranchStatusHistoryMockMvc.perform(post("/api/route-branch-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranchStatusHistory)))
            .andExpect(status().isBadRequest());

        List<RouteBranchStatusHistory> routeBranchStatusHistoryList = routeBranchStatusHistoryRepository.findAll();
        assertThat(routeBranchStatusHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRouteBranchStatusHistories() throws Exception {
        // Initialize the database
        routeBranchStatusHistoryRepository.saveAndFlush(routeBranchStatusHistory);

        // Get all the routeBranchStatusHistoryList
        restRouteBranchStatusHistoryMockMvc.perform(get("/api/route-branch-status-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routeBranchStatusHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getRouteBranchStatusHistory() throws Exception {
        // Initialize the database
        routeBranchStatusHistoryRepository.saveAndFlush(routeBranchStatusHistory);

        // Get the routeBranchStatusHistory
        restRouteBranchStatusHistoryMockMvc.perform(get("/api/route-branch-status-histories/{id}", routeBranchStatusHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(routeBranchStatusHistory.getId().intValue()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRouteBranchStatusHistory() throws Exception {
        // Get the routeBranchStatusHistory
        restRouteBranchStatusHistoryMockMvc.perform(get("/api/route-branch-status-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRouteBranchStatusHistory() throws Exception {
        // Initialize the database
        routeBranchStatusHistoryService.save(routeBranchStatusHistory);

        int databaseSizeBeforeUpdate = routeBranchStatusHistoryRepository.findAll().size();

        // Update the routeBranchStatusHistory
        RouteBranchStatusHistory updatedRouteBranchStatusHistory = routeBranchStatusHistoryRepository.findOne(routeBranchStatusHistory.getId());
        updatedRouteBranchStatusHistory
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .status(UPDATED_STATUS);

        restRouteBranchStatusHistoryMockMvc.perform(put("/api/route-branch-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRouteBranchStatusHistory)))
            .andExpect(status().isOk());

        // Validate the RouteBranchStatusHistory in the database
        List<RouteBranchStatusHistory> routeBranchStatusHistoryList = routeBranchStatusHistoryRepository.findAll();
        assertThat(routeBranchStatusHistoryList).hasSize(databaseSizeBeforeUpdate);
        RouteBranchStatusHistory testRouteBranchStatusHistory = routeBranchStatusHistoryList.get(routeBranchStatusHistoryList.size() - 1);
        assertThat(testRouteBranchStatusHistory.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testRouteBranchStatusHistory.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testRouteBranchStatusHistory.getStatus()).isEqualTo(UPDATED_STATUS);

        // Validate the RouteBranchStatusHistory in Elasticsearch
        RouteBranchStatusHistory routeBranchStatusHistoryEs = routeBranchStatusHistorySearchRepository.findOne(testRouteBranchStatusHistory.getId());
        assertThat(routeBranchStatusHistoryEs).isEqualToComparingFieldByField(testRouteBranchStatusHistory);
    }

    @Test
    @Transactional
    public void updateNonExistingRouteBranchStatusHistory() throws Exception {
        int databaseSizeBeforeUpdate = routeBranchStatusHistoryRepository.findAll().size();

        // Create the RouteBranchStatusHistory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRouteBranchStatusHistoryMockMvc.perform(put("/api/route-branch-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeBranchStatusHistory)))
            .andExpect(status().isCreated());

        // Validate the RouteBranchStatusHistory in the database
        List<RouteBranchStatusHistory> routeBranchStatusHistoryList = routeBranchStatusHistoryRepository.findAll();
        assertThat(routeBranchStatusHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRouteBranchStatusHistory() throws Exception {
        // Initialize the database
        routeBranchStatusHistoryService.save(routeBranchStatusHistory);

        int databaseSizeBeforeDelete = routeBranchStatusHistoryRepository.findAll().size();

        // Get the routeBranchStatusHistory
        restRouteBranchStatusHistoryMockMvc.perform(delete("/api/route-branch-status-histories/{id}", routeBranchStatusHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean routeBranchStatusHistoryExistsInEs = routeBranchStatusHistorySearchRepository.exists(routeBranchStatusHistory.getId());
        assertThat(routeBranchStatusHistoryExistsInEs).isFalse();

        // Validate the database is empty
        List<RouteBranchStatusHistory> routeBranchStatusHistoryList = routeBranchStatusHistoryRepository.findAll();
        assertThat(routeBranchStatusHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchRouteBranchStatusHistory() throws Exception {
        // Initialize the database
        routeBranchStatusHistoryService.save(routeBranchStatusHistory);

        // Search the routeBranchStatusHistory
        restRouteBranchStatusHistoryMockMvc.perform(get("/api/_search/route-branch-status-histories?query=id:" + routeBranchStatusHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routeBranchStatusHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RouteBranchStatusHistory.class);
        RouteBranchStatusHistory routeBranchStatusHistory1 = new RouteBranchStatusHistory();
        routeBranchStatusHistory1.setId(1L);
        RouteBranchStatusHistory routeBranchStatusHistory2 = new RouteBranchStatusHistory();
        routeBranchStatusHistory2.setId(routeBranchStatusHistory1.getId());
        assertThat(routeBranchStatusHistory1).isEqualTo(routeBranchStatusHistory2);
        routeBranchStatusHistory2.setId(2L);
        assertThat(routeBranchStatusHistory1).isNotEqualTo(routeBranchStatusHistory2);
        routeBranchStatusHistory1.setId(null);
        assertThat(routeBranchStatusHistory1).isNotEqualTo(routeBranchStatusHistory2);
    }
}
