package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.AreaNode;
import kz.rs.jetkizu.repository.AreaNodeRepository;
import kz.rs.jetkizu.service.AreaNodeService;
import kz.rs.jetkizu.repository.search.AreaNodeSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the AreaNodeResource REST controller.
 *
 * @see AreaNodeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class AreaNodeResourceIntTest {

    private static final String DEFAULT_LAT = "AAAAAAAAAA";
    private static final String UPDATED_LAT = "BBBBBBBBBB";

    private static final String DEFAULT_LON = "AAAAAAAAAA";
    private static final String UPDATED_LON = "BBBBBBBBBB";

    @Autowired
    private AreaNodeRepository areaNodeRepository;

    @Autowired
    private AreaNodeService areaNodeService;

    @Autowired
    private AreaNodeSearchRepository areaNodeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restAreaNodeMockMvc;

    private AreaNode areaNode;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AreaNodeResource areaNodeResource = new AreaNodeResource(areaNodeService);
        this.restAreaNodeMockMvc = MockMvcBuilders.standaloneSetup(areaNodeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AreaNode createEntity(EntityManager em) {
        AreaNode areaNode = new AreaNode()
            .lat(DEFAULT_LAT)
            .lon(DEFAULT_LON);
        return areaNode;
    }

    @Before
    public void initTest() {
        areaNodeSearchRepository.deleteAll();
        areaNode = createEntity(em);
    }

    @Test
    @Transactional
    public void createAreaNode() throws Exception {
        int databaseSizeBeforeCreate = areaNodeRepository.findAll().size();

        // Create the AreaNode
        restAreaNodeMockMvc.perform(post("/api/area-nodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaNode)))
            .andExpect(status().isCreated());

        // Validate the AreaNode in the database
        List<AreaNode> areaNodeList = areaNodeRepository.findAll();
        assertThat(areaNodeList).hasSize(databaseSizeBeforeCreate + 1);
        AreaNode testAreaNode = areaNodeList.get(areaNodeList.size() - 1);
        assertThat(testAreaNode.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testAreaNode.getLon()).isEqualTo(DEFAULT_LON);

        // Validate the AreaNode in Elasticsearch
        AreaNode areaNodeEs = areaNodeSearchRepository.findOne(testAreaNode.getId());
        assertThat(areaNodeEs).isEqualToComparingFieldByField(testAreaNode);
    }

    @Test
    @Transactional
    public void createAreaNodeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = areaNodeRepository.findAll().size();

        // Create the AreaNode with an existing ID
        areaNode.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAreaNodeMockMvc.perform(post("/api/area-nodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaNode)))
            .andExpect(status().isBadRequest());

        // Validate the AreaNode in the database
        List<AreaNode> areaNodeList = areaNodeRepository.findAll();
        assertThat(areaNodeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllAreaNodes() throws Exception {
        // Initialize the database
        areaNodeRepository.saveAndFlush(areaNode);

        // Get all the areaNodeList
        restAreaNodeMockMvc.perform(get("/api/area-nodes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(areaNode.getId().intValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.toString())));
    }

    @Test
    @Transactional
    public void getAreaNode() throws Exception {
        // Initialize the database
        areaNodeRepository.saveAndFlush(areaNode);

        // Get the areaNode
        restAreaNodeMockMvc.perform(get("/api/area-nodes/{id}", areaNode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(areaNode.getId().intValue()))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.toString()))
            .andExpect(jsonPath("$.lon").value(DEFAULT_LON.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingAreaNode() throws Exception {
        // Get the areaNode
        restAreaNodeMockMvc.perform(get("/api/area-nodes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAreaNode() throws Exception {
        // Initialize the database
        areaNodeService.save(areaNode);

        int databaseSizeBeforeUpdate = areaNodeRepository.findAll().size();

        // Update the areaNode
        AreaNode updatedAreaNode = areaNodeRepository.findOne(areaNode.getId());
        updatedAreaNode
            .lat(UPDATED_LAT)
            .lon(UPDATED_LON);

        restAreaNodeMockMvc.perform(put("/api/area-nodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAreaNode)))
            .andExpect(status().isOk());

        // Validate the AreaNode in the database
        List<AreaNode> areaNodeList = areaNodeRepository.findAll();
        assertThat(areaNodeList).hasSize(databaseSizeBeforeUpdate);
        AreaNode testAreaNode = areaNodeList.get(areaNodeList.size() - 1);
        assertThat(testAreaNode.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testAreaNode.getLon()).isEqualTo(UPDATED_LON);

        // Validate the AreaNode in Elasticsearch
        AreaNode areaNodeEs = areaNodeSearchRepository.findOne(testAreaNode.getId());
        assertThat(areaNodeEs).isEqualToComparingFieldByField(testAreaNode);
    }

    @Test
    @Transactional
    public void updateNonExistingAreaNode() throws Exception {
        int databaseSizeBeforeUpdate = areaNodeRepository.findAll().size();

        // Create the AreaNode

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restAreaNodeMockMvc.perform(put("/api/area-nodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(areaNode)))
            .andExpect(status().isCreated());

        // Validate the AreaNode in the database
        List<AreaNode> areaNodeList = areaNodeRepository.findAll();
        assertThat(areaNodeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteAreaNode() throws Exception {
        // Initialize the database
        areaNodeService.save(areaNode);

        int databaseSizeBeforeDelete = areaNodeRepository.findAll().size();

        // Get the areaNode
        restAreaNodeMockMvc.perform(delete("/api/area-nodes/{id}", areaNode.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean areaNodeExistsInEs = areaNodeSearchRepository.exists(areaNode.getId());
        assertThat(areaNodeExistsInEs).isFalse();

        // Validate the database is empty
        List<AreaNode> areaNodeList = areaNodeRepository.findAll();
        assertThat(areaNodeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchAreaNode() throws Exception {
        // Initialize the database
        areaNodeService.save(areaNode);

        // Search the areaNode
        restAreaNodeMockMvc.perform(get("/api/_search/area-nodes?query=id:" + areaNode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(areaNode.getId().intValue())))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.toString())))
            .andExpect(jsonPath("$.[*].lon").value(hasItem(DEFAULT_LON.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AreaNode.class);
        AreaNode areaNode1 = new AreaNode();
        areaNode1.setId(1L);
        AreaNode areaNode2 = new AreaNode();
        areaNode2.setId(areaNode1.getId());
        assertThat(areaNode1).isEqualTo(areaNode2);
        areaNode2.setId(2L);
        assertThat(areaNode1).isNotEqualTo(areaNode2);
        areaNode1.setId(null);
        assertThat(areaNode1).isNotEqualTo(areaNode2);
    }
}
