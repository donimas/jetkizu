package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.RouteOptimization;
import kz.rs.jetkizu.repository.RouteOptimizationRepository;
import kz.rs.jetkizu.service.RouteOptimizationService;
import kz.rs.jetkizu.repository.search.RouteOptimizationSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RouteOptimizationResource REST controller.
 *
 * @see RouteOptimizationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class RouteOptimizationResourceIntTest {

    private static final BigDecimal DEFAULT_MAX_VEHICLE_DISTANCE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_VEHICLE_DISTANCE = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MAX_ROUTE_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_ROUTE_DURATION = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MAX_WEIGHT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_WEIGHT = new BigDecimal(2);

    private static final BigDecimal DEFAULT_MAX_ROUTE_REVENUE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_ROUTE_REVENUE = new BigDecimal(2);

    private static final Integer DEFAULT_MAX_ROUTE_STOPS = 1;
    private static final Integer UPDATED_MAX_ROUTE_STOPS = 2;

    @Autowired
    private RouteOptimizationRepository routeOptimizationRepository;

    @Autowired
    private RouteOptimizationService routeOptimizationService;

    @Autowired
    private RouteOptimizationSearchRepository routeOptimizationSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRouteOptimizationMockMvc;

    private RouteOptimization routeOptimization;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RouteOptimizationResource routeOptimizationResource = new RouteOptimizationResource(routeOptimizationService);
        this.restRouteOptimizationMockMvc = MockMvcBuilders.standaloneSetup(routeOptimizationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RouteOptimization createEntity(EntityManager em) {
        RouteOptimization routeOptimization = new RouteOptimization()
            .maxVehicleDistance(DEFAULT_MAX_VEHICLE_DISTANCE)
            .maxRouteDuration(DEFAULT_MAX_ROUTE_DURATION)
            .maxWeight(DEFAULT_MAX_WEIGHT)
            .maxRouteRevenue(DEFAULT_MAX_ROUTE_REVENUE)
            .maxRouteStops(DEFAULT_MAX_ROUTE_STOPS);
        return routeOptimization;
    }

    @Before
    public void initTest() {
        routeOptimizationSearchRepository.deleteAll();
        routeOptimization = createEntity(em);
    }

    @Test
    @Transactional
    public void createRouteOptimization() throws Exception {
        int databaseSizeBeforeCreate = routeOptimizationRepository.findAll().size();

        // Create the RouteOptimization
        restRouteOptimizationMockMvc.perform(post("/api/route-optimizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeOptimization)))
            .andExpect(status().isCreated());

        // Validate the RouteOptimization in the database
        List<RouteOptimization> routeOptimizationList = routeOptimizationRepository.findAll();
        assertThat(routeOptimizationList).hasSize(databaseSizeBeforeCreate + 1);
        RouteOptimization testRouteOptimization = routeOptimizationList.get(routeOptimizationList.size() - 1);
        assertThat(testRouteOptimization.getMaxVehicleDistance()).isEqualTo(DEFAULT_MAX_VEHICLE_DISTANCE);
        assertThat(testRouteOptimization.getMaxRouteDuration()).isEqualTo(DEFAULT_MAX_ROUTE_DURATION);
        assertThat(testRouteOptimization.getMaxWeight()).isEqualTo(DEFAULT_MAX_WEIGHT);
        assertThat(testRouteOptimization.getMaxRouteRevenue()).isEqualTo(DEFAULT_MAX_ROUTE_REVENUE);
        assertThat(testRouteOptimization.getMaxRouteStops()).isEqualTo(DEFAULT_MAX_ROUTE_STOPS);

        // Validate the RouteOptimization in Elasticsearch
        RouteOptimization routeOptimizationEs = routeOptimizationSearchRepository.findOne(testRouteOptimization.getId());
        assertThat(routeOptimizationEs).isEqualToComparingFieldByField(testRouteOptimization);
    }

    @Test
    @Transactional
    public void createRouteOptimizationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = routeOptimizationRepository.findAll().size();

        // Create the RouteOptimization with an existing ID
        routeOptimization.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRouteOptimizationMockMvc.perform(post("/api/route-optimizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeOptimization)))
            .andExpect(status().isBadRequest());

        // Validate the RouteOptimization in the database
        List<RouteOptimization> routeOptimizationList = routeOptimizationRepository.findAll();
        assertThat(routeOptimizationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllRouteOptimizations() throws Exception {
        // Initialize the database
        routeOptimizationRepository.saveAndFlush(routeOptimization);

        // Get all the routeOptimizationList
        restRouteOptimizationMockMvc.perform(get("/api/route-optimizations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routeOptimization.getId().intValue())))
            .andExpect(jsonPath("$.[*].maxVehicleDistance").value(hasItem(DEFAULT_MAX_VEHICLE_DISTANCE.intValue())))
            .andExpect(jsonPath("$.[*].maxRouteDuration").value(hasItem(DEFAULT_MAX_ROUTE_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].maxWeight").value(hasItem(DEFAULT_MAX_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].maxRouteRevenue").value(hasItem(DEFAULT_MAX_ROUTE_REVENUE.intValue())))
            .andExpect(jsonPath("$.[*].maxRouteStops").value(hasItem(DEFAULT_MAX_ROUTE_STOPS)));
    }

    @Test
    @Transactional
    public void getRouteOptimization() throws Exception {
        // Initialize the database
        routeOptimizationRepository.saveAndFlush(routeOptimization);

        // Get the routeOptimization
        restRouteOptimizationMockMvc.perform(get("/api/route-optimizations/{id}", routeOptimization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(routeOptimization.getId().intValue()))
            .andExpect(jsonPath("$.maxVehicleDistance").value(DEFAULT_MAX_VEHICLE_DISTANCE.intValue()))
            .andExpect(jsonPath("$.maxRouteDuration").value(DEFAULT_MAX_ROUTE_DURATION.intValue()))
            .andExpect(jsonPath("$.maxWeight").value(DEFAULT_MAX_WEIGHT.intValue()))
            .andExpect(jsonPath("$.maxRouteRevenue").value(DEFAULT_MAX_ROUTE_REVENUE.intValue()))
            .andExpect(jsonPath("$.maxRouteStops").value(DEFAULT_MAX_ROUTE_STOPS));
    }

    @Test
    @Transactional
    public void getNonExistingRouteOptimization() throws Exception {
        // Get the routeOptimization
        restRouteOptimizationMockMvc.perform(get("/api/route-optimizations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRouteOptimization() throws Exception {
        // Initialize the database
        routeOptimizationService.save(routeOptimization);

        int databaseSizeBeforeUpdate = routeOptimizationRepository.findAll().size();

        // Update the routeOptimization
        RouteOptimization updatedRouteOptimization = routeOptimizationRepository.findOne(routeOptimization.getId());
        updatedRouteOptimization
            .maxVehicleDistance(UPDATED_MAX_VEHICLE_DISTANCE)
            .maxRouteDuration(UPDATED_MAX_ROUTE_DURATION)
            .maxWeight(UPDATED_MAX_WEIGHT)
            .maxRouteRevenue(UPDATED_MAX_ROUTE_REVENUE)
            .maxRouteStops(UPDATED_MAX_ROUTE_STOPS);

        restRouteOptimizationMockMvc.perform(put("/api/route-optimizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRouteOptimization)))
            .andExpect(status().isOk());

        // Validate the RouteOptimization in the database
        List<RouteOptimization> routeOptimizationList = routeOptimizationRepository.findAll();
        assertThat(routeOptimizationList).hasSize(databaseSizeBeforeUpdate);
        RouteOptimization testRouteOptimization = routeOptimizationList.get(routeOptimizationList.size() - 1);
        assertThat(testRouteOptimization.getMaxVehicleDistance()).isEqualTo(UPDATED_MAX_VEHICLE_DISTANCE);
        assertThat(testRouteOptimization.getMaxRouteDuration()).isEqualTo(UPDATED_MAX_ROUTE_DURATION);
        assertThat(testRouteOptimization.getMaxWeight()).isEqualTo(UPDATED_MAX_WEIGHT);
        assertThat(testRouteOptimization.getMaxRouteRevenue()).isEqualTo(UPDATED_MAX_ROUTE_REVENUE);
        assertThat(testRouteOptimization.getMaxRouteStops()).isEqualTo(UPDATED_MAX_ROUTE_STOPS);

        // Validate the RouteOptimization in Elasticsearch
        RouteOptimization routeOptimizationEs = routeOptimizationSearchRepository.findOne(testRouteOptimization.getId());
        assertThat(routeOptimizationEs).isEqualToComparingFieldByField(testRouteOptimization);
    }

    @Test
    @Transactional
    public void updateNonExistingRouteOptimization() throws Exception {
        int databaseSizeBeforeUpdate = routeOptimizationRepository.findAll().size();

        // Create the RouteOptimization

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRouteOptimizationMockMvc.perform(put("/api/route-optimizations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(routeOptimization)))
            .andExpect(status().isCreated());

        // Validate the RouteOptimization in the database
        List<RouteOptimization> routeOptimizationList = routeOptimizationRepository.findAll();
        assertThat(routeOptimizationList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRouteOptimization() throws Exception {
        // Initialize the database
        routeOptimizationService.save(routeOptimization);

        int databaseSizeBeforeDelete = routeOptimizationRepository.findAll().size();

        // Get the routeOptimization
        restRouteOptimizationMockMvc.perform(delete("/api/route-optimizations/{id}", routeOptimization.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean routeOptimizationExistsInEs = routeOptimizationSearchRepository.exists(routeOptimization.getId());
        assertThat(routeOptimizationExistsInEs).isFalse();

        // Validate the database is empty
        List<RouteOptimization> routeOptimizationList = routeOptimizationRepository.findAll();
        assertThat(routeOptimizationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchRouteOptimization() throws Exception {
        // Initialize the database
        routeOptimizationService.save(routeOptimization);

        // Search the routeOptimization
        restRouteOptimizationMockMvc.perform(get("/api/_search/route-optimizations?query=id:" + routeOptimization.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(routeOptimization.getId().intValue())))
            .andExpect(jsonPath("$.[*].maxVehicleDistance").value(hasItem(DEFAULT_MAX_VEHICLE_DISTANCE.intValue())))
            .andExpect(jsonPath("$.[*].maxRouteDuration").value(hasItem(DEFAULT_MAX_ROUTE_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].maxWeight").value(hasItem(DEFAULT_MAX_WEIGHT.intValue())))
            .andExpect(jsonPath("$.[*].maxRouteRevenue").value(hasItem(DEFAULT_MAX_ROUTE_REVENUE.intValue())))
            .andExpect(jsonPath("$.[*].maxRouteStops").value(hasItem(DEFAULT_MAX_ROUTE_STOPS)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RouteOptimization.class);
        RouteOptimization routeOptimization1 = new RouteOptimization();
        routeOptimization1.setId(1L);
        RouteOptimization routeOptimization2 = new RouteOptimization();
        routeOptimization2.setId(routeOptimization1.getId());
        assertThat(routeOptimization1).isEqualTo(routeOptimization2);
        routeOptimization2.setId(2L);
        assertThat(routeOptimization1).isNotEqualTo(routeOptimization2);
        routeOptimization1.setId(null);
        assertThat(routeOptimization1).isNotEqualTo(routeOptimization2);
    }
}
