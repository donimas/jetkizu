package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.JetkizuStatusHistory;
import kz.rs.jetkizu.repository.JetkizuStatusHistoryRepository;
import kz.rs.jetkizu.service.JetkizuStatusHistoryService;
import kz.rs.jetkizu.repository.search.JetkizuStatusHistorySearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.rs.jetkizu.domain.enumeration.JetkizuStatus;
import kz.rs.jetkizu.domain.enumeration.FailType;
/**
 * Test class for the JetkizuStatusHistoryResource REST controller.
 *
 * @see JetkizuStatusHistoryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class JetkizuStatusHistoryResourceIntTest {

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    private static final JetkizuStatus DEFAULT_STATUS = JetkizuStatus.BACKLOG;
    private static final JetkizuStatus UPDATED_STATUS = JetkizuStatus.DRAFT;

    private static final FailType DEFAULT_FAIL_TYPE = FailType.ADDRESS_NOT_FOUND;
    private static final FailType UPDATED_FAIL_TYPE = FailType.PHONE_NOT_ALLOWED;

    @Autowired
    private JetkizuStatusHistoryRepository jetkizuStatusHistoryRepository;

    @Autowired
    private JetkizuStatusHistoryService jetkizuStatusHistoryService;

    @Autowired
    private JetkizuStatusHistorySearchRepository jetkizuStatusHistorySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJetkizuStatusHistoryMockMvc;

    private JetkizuStatusHistory jetkizuStatusHistory;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JetkizuStatusHistoryResource jetkizuStatusHistoryResource = new JetkizuStatusHistoryResource(jetkizuStatusHistoryService);
        this.restJetkizuStatusHistoryMockMvc = MockMvcBuilders.standaloneSetup(jetkizuStatusHistoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JetkizuStatusHistory createEntity(EntityManager em) {
        JetkizuStatusHistory jetkizuStatusHistory = new JetkizuStatusHistory()
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .flagDeleted(DEFAULT_FLAG_DELETED)
            .status(DEFAULT_STATUS)
            .failType(DEFAULT_FAIL_TYPE);
        return jetkizuStatusHistory;
    }

    @Before
    public void initTest() {
        jetkizuStatusHistorySearchRepository.deleteAll();
        jetkizuStatusHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createJetkizuStatusHistory() throws Exception {
        int databaseSizeBeforeCreate = jetkizuStatusHistoryRepository.findAll().size();

        // Create the JetkizuStatusHistory
        restJetkizuStatusHistoryMockMvc.perform(post("/api/jetkizu-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuStatusHistory)))
            .andExpect(status().isCreated());

        // Validate the JetkizuStatusHistory in the database
        List<JetkizuStatusHistory> jetkizuStatusHistoryList = jetkizuStatusHistoryRepository.findAll();
        assertThat(jetkizuStatusHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        JetkizuStatusHistory testJetkizuStatusHistory = jetkizuStatusHistoryList.get(jetkizuStatusHistoryList.size() - 1);
        assertThat(testJetkizuStatusHistory.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testJetkizuStatusHistory.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
        assertThat(testJetkizuStatusHistory.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testJetkizuStatusHistory.getFailType()).isEqualTo(DEFAULT_FAIL_TYPE);

        // Validate the JetkizuStatusHistory in Elasticsearch
        JetkizuStatusHistory jetkizuStatusHistoryEs = jetkizuStatusHistorySearchRepository.findOne(testJetkizuStatusHistory.getId());
        assertThat(jetkizuStatusHistoryEs).isEqualToComparingFieldByField(testJetkizuStatusHistory);
    }

    @Test
    @Transactional
    public void createJetkizuStatusHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jetkizuStatusHistoryRepository.findAll().size();

        // Create the JetkizuStatusHistory with an existing ID
        jetkizuStatusHistory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJetkizuStatusHistoryMockMvc.perform(post("/api/jetkizu-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuStatusHistory)))
            .andExpect(status().isBadRequest());

        // Validate the JetkizuStatusHistory in the database
        List<JetkizuStatusHistory> jetkizuStatusHistoryList = jetkizuStatusHistoryRepository.findAll();
        assertThat(jetkizuStatusHistoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = jetkizuStatusHistoryRepository.findAll().size();
        // set the field null
        jetkizuStatusHistory.setStatus(null);

        // Create the JetkizuStatusHistory, which fails.

        restJetkizuStatusHistoryMockMvc.perform(post("/api/jetkizu-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuStatusHistory)))
            .andExpect(status().isBadRequest());

        List<JetkizuStatusHistory> jetkizuStatusHistoryList = jetkizuStatusHistoryRepository.findAll();
        assertThat(jetkizuStatusHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJetkizuStatusHistories() throws Exception {
        // Initialize the database
        jetkizuStatusHistoryRepository.saveAndFlush(jetkizuStatusHistory);

        // Get all the jetkizuStatusHistoryList
        restJetkizuStatusHistoryMockMvc.perform(get("/api/jetkizu-status-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuStatusHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].failType").value(hasItem(DEFAULT_FAIL_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getJetkizuStatusHistory() throws Exception {
        // Initialize the database
        jetkizuStatusHistoryRepository.saveAndFlush(jetkizuStatusHistory);

        // Get the jetkizuStatusHistory
        restJetkizuStatusHistoryMockMvc.perform(get("/api/jetkizu-status-histories/{id}", jetkizuStatusHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jetkizuStatusHistory.getId().intValue()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.failType").value(DEFAULT_FAIL_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJetkizuStatusHistory() throws Exception {
        // Get the jetkizuStatusHistory
        restJetkizuStatusHistoryMockMvc.perform(get("/api/jetkizu-status-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJetkizuStatusHistory() throws Exception {
        // Initialize the database
        jetkizuStatusHistoryService.save(jetkizuStatusHistory);

        int databaseSizeBeforeUpdate = jetkizuStatusHistoryRepository.findAll().size();

        // Update the jetkizuStatusHistory
        JetkizuStatusHistory updatedJetkizuStatusHistory = jetkizuStatusHistoryRepository.findOne(jetkizuStatusHistory.getId());
        updatedJetkizuStatusHistory
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .flagDeleted(UPDATED_FLAG_DELETED)
            .status(UPDATED_STATUS)
            .failType(UPDATED_FAIL_TYPE);

        restJetkizuStatusHistoryMockMvc.perform(put("/api/jetkizu-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJetkizuStatusHistory)))
            .andExpect(status().isOk());

        // Validate the JetkizuStatusHistory in the database
        List<JetkizuStatusHistory> jetkizuStatusHistoryList = jetkizuStatusHistoryRepository.findAll();
        assertThat(jetkizuStatusHistoryList).hasSize(databaseSizeBeforeUpdate);
        JetkizuStatusHistory testJetkizuStatusHistory = jetkizuStatusHistoryList.get(jetkizuStatusHistoryList.size() - 1);
        assertThat(testJetkizuStatusHistory.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testJetkizuStatusHistory.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
        assertThat(testJetkizuStatusHistory.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testJetkizuStatusHistory.getFailType()).isEqualTo(UPDATED_FAIL_TYPE);

        // Validate the JetkizuStatusHistory in Elasticsearch
        JetkizuStatusHistory jetkizuStatusHistoryEs = jetkizuStatusHistorySearchRepository.findOne(testJetkizuStatusHistory.getId());
        assertThat(jetkizuStatusHistoryEs).isEqualToComparingFieldByField(testJetkizuStatusHistory);
    }

    @Test
    @Transactional
    public void updateNonExistingJetkizuStatusHistory() throws Exception {
        int databaseSizeBeforeUpdate = jetkizuStatusHistoryRepository.findAll().size();

        // Create the JetkizuStatusHistory

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJetkizuStatusHistoryMockMvc.perform(put("/api/jetkizu-status-histories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuStatusHistory)))
            .andExpect(status().isCreated());

        // Validate the JetkizuStatusHistory in the database
        List<JetkizuStatusHistory> jetkizuStatusHistoryList = jetkizuStatusHistoryRepository.findAll();
        assertThat(jetkizuStatusHistoryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJetkizuStatusHistory() throws Exception {
        // Initialize the database
        jetkizuStatusHistoryService.save(jetkizuStatusHistory);

        int databaseSizeBeforeDelete = jetkizuStatusHistoryRepository.findAll().size();

        // Get the jetkizuStatusHistory
        restJetkizuStatusHistoryMockMvc.perform(delete("/api/jetkizu-status-histories/{id}", jetkizuStatusHistory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean jetkizuStatusHistoryExistsInEs = jetkizuStatusHistorySearchRepository.exists(jetkizuStatusHistory.getId());
        assertThat(jetkizuStatusHistoryExistsInEs).isFalse();

        // Validate the database is empty
        List<JetkizuStatusHistory> jetkizuStatusHistoryList = jetkizuStatusHistoryRepository.findAll();
        assertThat(jetkizuStatusHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJetkizuStatusHistory() throws Exception {
        // Initialize the database
        jetkizuStatusHistoryService.save(jetkizuStatusHistory);

        // Search the jetkizuStatusHistory
        restJetkizuStatusHistoryMockMvc.perform(get("/api/_search/jetkizu-status-histories?query=id:" + jetkizuStatusHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuStatusHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].failType").value(hasItem(DEFAULT_FAIL_TYPE.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JetkizuStatusHistory.class);
        JetkizuStatusHistory jetkizuStatusHistory1 = new JetkizuStatusHistory();
        jetkizuStatusHistory1.setId(1L);
        JetkizuStatusHistory jetkizuStatusHistory2 = new JetkizuStatusHistory();
        jetkizuStatusHistory2.setId(jetkizuStatusHistory1.getId());
        assertThat(jetkizuStatusHistory1).isEqualTo(jetkizuStatusHistory2);
        jetkizuStatusHistory2.setId(2L);
        assertThat(jetkizuStatusHistory1).isNotEqualTo(jetkizuStatusHistory2);
        jetkizuStatusHistory1.setId(null);
        assertThat(jetkizuStatusHistory1).isNotEqualTo(jetkizuStatusHistory2);
    }
}
