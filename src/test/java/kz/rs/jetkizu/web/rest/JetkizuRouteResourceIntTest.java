package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.JetkizuRoute;
import kz.rs.jetkizu.repository.JetkizuRouteRepository;
import kz.rs.jetkizu.service.JetkizuRouteService;
import kz.rs.jetkizu.repository.search.JetkizuRouteSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.rs.jetkizu.domain.enumeration.JetkizuRouteStatus;
/**
 * Test class for the JetkizuRouteResource REST controller.
 *
 * @see JetkizuRouteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class JetkizuRouteResourceIntTest {

    private static final LocalDate DEFAULT_JETKIZU_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_JETKIZU_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final ZonedDateTime DEFAULT_STARTED_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STARTED_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_STOPPED_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_STOPPED_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final JetkizuRouteStatus DEFAULT_STATUS = JetkizuRouteStatus.DRAFT;
    private static final JetkizuRouteStatus UPDATED_STATUS = JetkizuRouteStatus.BACKLOG;

    @Autowired
    private JetkizuRouteRepository jetkizuRouteRepository;

    @Autowired
    private JetkizuRouteService jetkizuRouteService;

    @Autowired
    private JetkizuRouteSearchRepository jetkizuRouteSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJetkizuRouteMockMvc;

    private JetkizuRoute jetkizuRoute;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JetkizuRouteResource jetkizuRouteResource = new JetkizuRouteResource(jetkizuRouteService);
        this.restJetkizuRouteMockMvc = MockMvcBuilders.standaloneSetup(jetkizuRouteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JetkizuRoute createEntity(EntityManager em) {
        JetkizuRoute jetkizuRoute = new JetkizuRoute()
            .jetkizuDate(DEFAULT_JETKIZU_DATE)
            .startedDateTime(DEFAULT_STARTED_DATE_TIME)
            .stoppedDateTime(DEFAULT_STOPPED_DATE_TIME)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .status(DEFAULT_STATUS);
        return jetkizuRoute;
    }

    @Before
    public void initTest() {
        jetkizuRouteSearchRepository.deleteAll();
        jetkizuRoute = createEntity(em);
    }

    @Test
    @Transactional
    public void createJetkizuRoute() throws Exception {
        int databaseSizeBeforeCreate = jetkizuRouteRepository.findAll().size();

        // Create the JetkizuRoute
        restJetkizuRouteMockMvc.perform(post("/api/jetkizu-routes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuRoute)))
            .andExpect(status().isCreated());

        // Validate the JetkizuRoute in the database
        List<JetkizuRoute> jetkizuRouteList = jetkizuRouteRepository.findAll();
        assertThat(jetkizuRouteList).hasSize(databaseSizeBeforeCreate + 1);
        JetkizuRoute testJetkizuRoute = jetkizuRouteList.get(jetkizuRouteList.size() - 1);
        assertThat(testJetkizuRoute.getJetkizuDate()).isEqualTo(DEFAULT_JETKIZU_DATE);
        assertThat(testJetkizuRoute.getStartedDateTime()).isEqualTo(DEFAULT_STARTED_DATE_TIME);
        assertThat(testJetkizuRoute.getStoppedDateTime()).isEqualTo(DEFAULT_STOPPED_DATE_TIME);
        assertThat(testJetkizuRoute.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testJetkizuRoute.getStatus()).isEqualTo(DEFAULT_STATUS);

        // Validate the JetkizuRoute in Elasticsearch
        JetkizuRoute jetkizuRouteEs = jetkizuRouteSearchRepository.findOne(testJetkizuRoute.getId());
        assertThat(jetkizuRouteEs).isEqualToComparingFieldByField(testJetkizuRoute);
    }

    @Test
    @Transactional
    public void createJetkizuRouteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jetkizuRouteRepository.findAll().size();

        // Create the JetkizuRoute with an existing ID
        jetkizuRoute.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJetkizuRouteMockMvc.perform(post("/api/jetkizu-routes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuRoute)))
            .andExpect(status().isBadRequest());

        // Validate the JetkizuRoute in the database
        List<JetkizuRoute> jetkizuRouteList = jetkizuRouteRepository.findAll();
        assertThat(jetkizuRouteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = jetkizuRouteRepository.findAll().size();
        // set the field null
        jetkizuRoute.setStatus(null);

        // Create the JetkizuRoute, which fails.

        restJetkizuRouteMockMvc.perform(post("/api/jetkizu-routes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuRoute)))
            .andExpect(status().isBadRequest());

        List<JetkizuRoute> jetkizuRouteList = jetkizuRouteRepository.findAll();
        assertThat(jetkizuRouteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJetkizuRoutes() throws Exception {
        // Initialize the database
        jetkizuRouteRepository.saveAndFlush(jetkizuRoute);

        // Get all the jetkizuRouteList
        restJetkizuRouteMockMvc.perform(get("/api/jetkizu-routes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuRoute.getId().intValue())))
            .andExpect(jsonPath("$.[*].jetkizuDate").value(hasItem(DEFAULT_JETKIZU_DATE.toString())))
            .andExpect(jsonPath("$.[*].startedDateTime").value(hasItem(sameInstant(DEFAULT_STARTED_DATE_TIME))))
            .andExpect(jsonPath("$.[*].stoppedDateTime").value(hasItem(sameInstant(DEFAULT_STOPPED_DATE_TIME))))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getJetkizuRoute() throws Exception {
        // Initialize the database
        jetkizuRouteRepository.saveAndFlush(jetkizuRoute);

        // Get the jetkizuRoute
        restJetkizuRouteMockMvc.perform(get("/api/jetkizu-routes/{id}", jetkizuRoute.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jetkizuRoute.getId().intValue()))
            .andExpect(jsonPath("$.jetkizuDate").value(DEFAULT_JETKIZU_DATE.toString()))
            .andExpect(jsonPath("$.startedDateTime").value(sameInstant(DEFAULT_STARTED_DATE_TIME)))
            .andExpect(jsonPath("$.stoppedDateTime").value(sameInstant(DEFAULT_STOPPED_DATE_TIME)))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingJetkizuRoute() throws Exception {
        // Get the jetkizuRoute
        restJetkizuRouteMockMvc.perform(get("/api/jetkizu-routes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJetkizuRoute() throws Exception {
        // Initialize the database
        jetkizuRouteService.save(jetkizuRoute);

        int databaseSizeBeforeUpdate = jetkizuRouteRepository.findAll().size();

        // Update the jetkizuRoute
        JetkizuRoute updatedJetkizuRoute = jetkizuRouteRepository.findOne(jetkizuRoute.getId());
        updatedJetkizuRoute
            .jetkizuDate(UPDATED_JETKIZU_DATE)
            .startedDateTime(UPDATED_STARTED_DATE_TIME)
            .stoppedDateTime(UPDATED_STOPPED_DATE_TIME)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .status(UPDATED_STATUS);

        restJetkizuRouteMockMvc.perform(put("/api/jetkizu-routes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJetkizuRoute)))
            .andExpect(status().isOk());

        // Validate the JetkizuRoute in the database
        List<JetkizuRoute> jetkizuRouteList = jetkizuRouteRepository.findAll();
        assertThat(jetkizuRouteList).hasSize(databaseSizeBeforeUpdate);
        JetkizuRoute testJetkizuRoute = jetkizuRouteList.get(jetkizuRouteList.size() - 1);
        assertThat(testJetkizuRoute.getJetkizuDate()).isEqualTo(UPDATED_JETKIZU_DATE);
        assertThat(testJetkizuRoute.getStartedDateTime()).isEqualTo(UPDATED_STARTED_DATE_TIME);
        assertThat(testJetkizuRoute.getStoppedDateTime()).isEqualTo(UPDATED_STOPPED_DATE_TIME);
        assertThat(testJetkizuRoute.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testJetkizuRoute.getStatus()).isEqualTo(UPDATED_STATUS);

        // Validate the JetkizuRoute in Elasticsearch
        JetkizuRoute jetkizuRouteEs = jetkizuRouteSearchRepository.findOne(testJetkizuRoute.getId());
        assertThat(jetkizuRouteEs).isEqualToComparingFieldByField(testJetkizuRoute);
    }

    @Test
    @Transactional
    public void updateNonExistingJetkizuRoute() throws Exception {
        int databaseSizeBeforeUpdate = jetkizuRouteRepository.findAll().size();

        // Create the JetkizuRoute

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJetkizuRouteMockMvc.perform(put("/api/jetkizu-routes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuRoute)))
            .andExpect(status().isCreated());

        // Validate the JetkizuRoute in the database
        List<JetkizuRoute> jetkizuRouteList = jetkizuRouteRepository.findAll();
        assertThat(jetkizuRouteList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJetkizuRoute() throws Exception {
        // Initialize the database
        jetkizuRouteService.save(jetkizuRoute);

        int databaseSizeBeforeDelete = jetkizuRouteRepository.findAll().size();

        // Get the jetkizuRoute
        restJetkizuRouteMockMvc.perform(delete("/api/jetkizu-routes/{id}", jetkizuRoute.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean jetkizuRouteExistsInEs = jetkizuRouteSearchRepository.exists(jetkizuRoute.getId());
        assertThat(jetkizuRouteExistsInEs).isFalse();

        // Validate the database is empty
        List<JetkizuRoute> jetkizuRouteList = jetkizuRouteRepository.findAll();
        assertThat(jetkizuRouteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJetkizuRoute() throws Exception {
        // Initialize the database
        jetkizuRouteService.save(jetkizuRoute);

        // Search the jetkizuRoute
        restJetkizuRouteMockMvc.perform(get("/api/_search/jetkizu-routes?query=id:" + jetkizuRoute.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuRoute.getId().intValue())))
            .andExpect(jsonPath("$.[*].jetkizuDate").value(hasItem(DEFAULT_JETKIZU_DATE.toString())))
            .andExpect(jsonPath("$.[*].startedDateTime").value(hasItem(sameInstant(DEFAULT_STARTED_DATE_TIME))))
            .andExpect(jsonPath("$.[*].stoppedDateTime").value(hasItem(sameInstant(DEFAULT_STOPPED_DATE_TIME))))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JetkizuRoute.class);
        JetkizuRoute jetkizuRoute1 = new JetkizuRoute();
        jetkizuRoute1.setId(1L);
        JetkizuRoute jetkizuRoute2 = new JetkizuRoute();
        jetkizuRoute2.setId(jetkizuRoute1.getId());
        assertThat(jetkizuRoute1).isEqualTo(jetkizuRoute2);
        jetkizuRoute2.setId(2L);
        assertThat(jetkizuRoute1).isNotEqualTo(jetkizuRoute2);
        jetkizuRoute1.setId(null);
        assertThat(jetkizuRoute1).isNotEqualTo(jetkizuRoute2);
    }
}
