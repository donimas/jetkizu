package kz.rs.jetkizu.web.rest;

import kz.rs.jetkizu.JetkizuApp;

import kz.rs.jetkizu.domain.JetkizuPause;
import kz.rs.jetkizu.repository.JetkizuPauseRepository;
import kz.rs.jetkizu.service.JetkizuPauseService;
import kz.rs.jetkizu.repository.search.JetkizuPauseSearchRepository;
import kz.rs.jetkizu.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.rs.jetkizu.web.rest.TestUtil.sameInstant;
import static kz.rs.jetkizu.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.rs.jetkizu.domain.enumeration.JetkizuPauseType;
/**
 * Test class for the JetkizuPauseResource REST controller.
 *
 * @see JetkizuPauseResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JetkizuApp.class)
public class JetkizuPauseResourceIntTest {

    private static final BigDecimal DEFAULT_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_DURATION = new BigDecimal(2);

    private static final JetkizuPauseType DEFAULT_TYPE = JetkizuPauseType.DELIVER;
    private static final JetkizuPauseType UPDATED_TYPE = JetkizuPauseType.TECHNICAL_PROBLEM;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATE_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private JetkizuPauseRepository jetkizuPauseRepository;

    @Autowired
    private JetkizuPauseService jetkizuPauseService;

    @Autowired
    private JetkizuPauseSearchRepository jetkizuPauseSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restJetkizuPauseMockMvc;

    private JetkizuPause jetkizuPause;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JetkizuPauseResource jetkizuPauseResource = new JetkizuPauseResource(jetkizuPauseService);
        this.restJetkizuPauseMockMvc = MockMvcBuilders.standaloneSetup(jetkizuPauseResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JetkizuPause createEntity(EntityManager em) {
        JetkizuPause jetkizuPause = new JetkizuPause()
            .duration(DEFAULT_DURATION)
            .type(DEFAULT_TYPE)
            .description(DEFAULT_DESCRIPTION)
            .createDateTime(DEFAULT_CREATE_DATE_TIME)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return jetkizuPause;
    }

    @Before
    public void initTest() {
        jetkizuPauseSearchRepository.deleteAll();
        jetkizuPause = createEntity(em);
    }

    @Test
    @Transactional
    public void createJetkizuPause() throws Exception {
        int databaseSizeBeforeCreate = jetkizuPauseRepository.findAll().size();

        // Create the JetkizuPause
        restJetkizuPauseMockMvc.perform(post("/api/jetkizu-pauses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuPause)))
            .andExpect(status().isCreated());

        // Validate the JetkizuPause in the database
        List<JetkizuPause> jetkizuPauseList = jetkizuPauseRepository.findAll();
        assertThat(jetkizuPauseList).hasSize(databaseSizeBeforeCreate + 1);
        JetkizuPause testJetkizuPause = jetkizuPauseList.get(jetkizuPauseList.size() - 1);
        assertThat(testJetkizuPause.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testJetkizuPause.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testJetkizuPause.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testJetkizuPause.getCreateDateTime()).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testJetkizuPause.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);

        // Validate the JetkizuPause in Elasticsearch
        JetkizuPause jetkizuPauseEs = jetkizuPauseSearchRepository.findOne(testJetkizuPause.getId());
        assertThat(jetkizuPauseEs).isEqualToComparingFieldByField(testJetkizuPause);
    }

    @Test
    @Transactional
    public void createJetkizuPauseWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jetkizuPauseRepository.findAll().size();

        // Create the JetkizuPause with an existing ID
        jetkizuPause.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJetkizuPauseMockMvc.perform(post("/api/jetkizu-pauses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuPause)))
            .andExpect(status().isBadRequest());

        // Validate the JetkizuPause in the database
        List<JetkizuPause> jetkizuPauseList = jetkizuPauseRepository.findAll();
        assertThat(jetkizuPauseList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = jetkizuPauseRepository.findAll().size();
        // set the field null
        jetkizuPause.setType(null);

        // Create the JetkizuPause, which fails.

        restJetkizuPauseMockMvc.perform(post("/api/jetkizu-pauses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuPause)))
            .andExpect(status().isBadRequest());

        List<JetkizuPause> jetkizuPauseList = jetkizuPauseRepository.findAll();
        assertThat(jetkizuPauseList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllJetkizuPauses() throws Exception {
        // Initialize the database
        jetkizuPauseRepository.saveAndFlush(jetkizuPause);

        // Get all the jetkizuPauseList
        restJetkizuPauseMockMvc.perform(get("/api/jetkizu-pauses?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuPause.getId().intValue())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void getJetkizuPause() throws Exception {
        // Initialize the database
        jetkizuPauseRepository.saveAndFlush(jetkizuPause);

        // Get the jetkizuPause
        restJetkizuPauseMockMvc.perform(get("/api/jetkizu-pauses/{id}", jetkizuPause.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(jetkizuPause.getId().intValue()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.createDateTime").value(sameInstant(DEFAULT_CREATE_DATE_TIME)))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingJetkizuPause() throws Exception {
        // Get the jetkizuPause
        restJetkizuPauseMockMvc.perform(get("/api/jetkizu-pauses/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJetkizuPause() throws Exception {
        // Initialize the database
        jetkizuPauseService.save(jetkizuPause);

        int databaseSizeBeforeUpdate = jetkizuPauseRepository.findAll().size();

        // Update the jetkizuPause
        JetkizuPause updatedJetkizuPause = jetkizuPauseRepository.findOne(jetkizuPause.getId());
        updatedJetkizuPause
            .duration(UPDATED_DURATION)
            .type(UPDATED_TYPE)
            .description(UPDATED_DESCRIPTION)
            .createDateTime(UPDATED_CREATE_DATE_TIME)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restJetkizuPauseMockMvc.perform(put("/api/jetkizu-pauses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedJetkizuPause)))
            .andExpect(status().isOk());

        // Validate the JetkizuPause in the database
        List<JetkizuPause> jetkizuPauseList = jetkizuPauseRepository.findAll();
        assertThat(jetkizuPauseList).hasSize(databaseSizeBeforeUpdate);
        JetkizuPause testJetkizuPause = jetkizuPauseList.get(jetkizuPauseList.size() - 1);
        assertThat(testJetkizuPause.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testJetkizuPause.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testJetkizuPause.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testJetkizuPause.getCreateDateTime()).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testJetkizuPause.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);

        // Validate the JetkizuPause in Elasticsearch
        JetkizuPause jetkizuPauseEs = jetkizuPauseSearchRepository.findOne(testJetkizuPause.getId());
        assertThat(jetkizuPauseEs).isEqualToComparingFieldByField(testJetkizuPause);
    }

    @Test
    @Transactional
    public void updateNonExistingJetkizuPause() throws Exception {
        int databaseSizeBeforeUpdate = jetkizuPauseRepository.findAll().size();

        // Create the JetkizuPause

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJetkizuPauseMockMvc.perform(put("/api/jetkizu-pauses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jetkizuPause)))
            .andExpect(status().isCreated());

        // Validate the JetkizuPause in the database
        List<JetkizuPause> jetkizuPauseList = jetkizuPauseRepository.findAll();
        assertThat(jetkizuPauseList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteJetkizuPause() throws Exception {
        // Initialize the database
        jetkizuPauseService.save(jetkizuPause);

        int databaseSizeBeforeDelete = jetkizuPauseRepository.findAll().size();

        // Get the jetkizuPause
        restJetkizuPauseMockMvc.perform(delete("/api/jetkizu-pauses/{id}", jetkizuPause.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean jetkizuPauseExistsInEs = jetkizuPauseSearchRepository.exists(jetkizuPause.getId());
        assertThat(jetkizuPauseExistsInEs).isFalse();

        // Validate the database is empty
        List<JetkizuPause> jetkizuPauseList = jetkizuPauseRepository.findAll();
        assertThat(jetkizuPauseList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchJetkizuPause() throws Exception {
        // Initialize the database
        jetkizuPauseService.save(jetkizuPause);

        // Search the jetkizuPause
        restJetkizuPauseMockMvc.perform(get("/api/_search/jetkizu-pauses?query=id:" + jetkizuPause.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(jetkizuPause.getId().intValue())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(sameInstant(DEFAULT_CREATE_DATE_TIME))))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JetkizuPause.class);
        JetkizuPause jetkizuPause1 = new JetkizuPause();
        jetkizuPause1.setId(1L);
        JetkizuPause jetkizuPause2 = new JetkizuPause();
        jetkizuPause2.setId(jetkizuPause1.getId());
        assertThat(jetkizuPause1).isEqualTo(jetkizuPause2);
        jetkizuPause2.setId(2L);
        assertThat(jetkizuPause1).isNotEqualTo(jetkizuPause2);
        jetkizuPause1.setId(null);
        assertThat(jetkizuPause1).isNotEqualTo(jetkizuPause2);
    }
}
